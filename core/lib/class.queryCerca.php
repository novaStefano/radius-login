<?php
/*
$cerca = $web->cercaQuery($str);
$cerca->loadFromPost('valore');

$cerca->setCondizione('cercaAtleta', "and ( atleta.nome like '%~campo%' or atleta.cognome like '%~campo%'
or atleta.cellulare like '%~campo%' or  atleta.mail like '%~campo%' or atleta.indirizzo like '%~campo%'
or concat(atleta.nome,' ',atleta.cognome) like '%~campo%' or concat(atleta.cognome,' ',atleta.nome) like '%~campo%'  ) ");
$cerca->setCondizione('indirizzo', ' and indirizzo like \'%~campo%\'  ');
$cerca->setCondizione('telefono', ' and ( telefono like \'%~campo%\' or cellulare like \'%~campo%\'  ) ');
$cerca->setCondizione('stato', ' and ( atleta.stato=\'~campo\'  ) ');
$cerca->creaWhere();
$res = $cerca->execute();
*/

/**
* Classe per la creazione delle query di ricerca
*/

class QueryCerca extends WrapperUtility
{ //gestione select con where mysql
    use tRicerche;
    private $where = ''; //contenente where finale
    private $whereEle = []; //contenente variabile per filtri dinamici
    private $whereEleClausola = []; //contenente variabile per filtri dinamici
    private $query;
    private $debug = false;
    private $filtro = [];
    private $aliasMain="";
    private $lastQuery; //buffer con l'ultima query eseguita

    private $emptyNoFilter=false;
    private $nClasuola=0;

    private $creatoWhere=false; //flag di stato valorizzato a true una volta creato il where dinamico. Se non valorizzato abilita in automatico la creazione da execute per risparmiare una funzione
    private $tableMaster=null; //setto la tabella in caso voglio usare alias o
    private $activeSoftDelete=true; //attiva il controllo automatico del softdelete
    private $bindedFilter=[]; //array con i bind da aggiungere

    public function __construct(Utility $utility,string $query)
    {
        parent::__construct($utility);
        $this->query = $query;
        $this->where = " where 1=1 ";
        $this->loadFromPost();
    }

    /**
     * Disabilita il soft delete
     * @param boolean $soft [description]
     */
    public function disableSoftDelete(bool $soft=false):void{
      $this->activeSoftDelete=$soft;
    }

    /**
    * Imposta alias per il soft Delete
    * @param string $alias
    */
    public function setAliasMain(string $alias){
        $this->aliasMain=$alias;
    }

    public function getAliasMain():string{
        return $this->aliasMain;
    }

    public function emptyNoFilter()
    {
        $this->where=" where 1=2    ";
        $this->emptyNoFilter=true;
    }


    private function firstWhere()
    {
        if ($this->nClasuola==0) {
            return true;
        } else {
            return false;
        }
    }

    //aggiunge la clausola alla variabile di ricerca
    public function addWhere($add)
    {
        if ($this->firstWhere()) {
            if ($this->emptyNoFilter) {
                $this->where.=' or ( 1=1 ';
            }
        }
        $this->where.=$add;
        $this->nClasuola++;
    }

    //attiva una condizione di ricerca che poi lavoreraà con whereEle
    public function setCondizione(string $elemento, string $clausola,string $likeInizio="",string $likeFine="")
    {
        if(empty($this->whereEle[$elemento]['val'])){
            return 0;
        }
        $clausola=str_replace("?",":$elemento",$clausola);
        $this->whereEleClausola[$elemento] = $clausola;
        if( (!empty($likeInizio))||(!empty($likeFine)) )
        {
            if(isset($this->whereEle[$elemento])){
                $this->whereEle[$elemento]['val']=$likeInizio.$this->whereEle[$elemento]['val'].$likeFine;
            }
        }
    }

    //stampa il where creato, per debug può essere comodo
    public function printWhere()
    {
        echo $this->where;
    }


    //creo il where partend dal post semplice
    public function creaWhereSimple()
    {
        //nome post come chiave - valore lo uso nella sostituzione
        foreach ($_POST as $campo => $valore) {
            if (isset($this->whereEleClausola[$campo])) {
                $clausola = ($this->whereEleClausola[$campo]);
            } else {
                $clausola = "";
            }

            if ($clausola != "") {
                $this->addRicerca($valore, $clausola);
            }
        }
        //where creato, commuto la variabile di stato
        $this->creatoWhere=true;
    }





    /**
    * Creo il where partendo dai dati caricati in post
    * @return [type] [description]
    */
    public function creaWhere():void
    {
        if(!$this->whereEle){
            $this->creatoWhere=false;
            return;
        }

        //Metto in relazioni i dati presi dal post e quelli bindati sulla classe
        foreach ($this->whereEle as $elemento) {
            $campo = $elemento['id'];
            if (isset($this->whereEleClausola[$campo])) {
                $clausola = ($this->whereEleClausola[$campo]);
                $this->addWhere($clausola);
            } else {
                // $this->warning('Clausola Query cerca', "$clausola Non trovata in query Cerca");
                $clausola = "";
            }
        }
        //where creato, commuto la variabile di stato
        if (($this->emptyNoFilter)&&(!$this->firstWhere())) {
            $this->addWhere(' ) '); //per qualche motivo voglio il dataset vuoto
        }

        $this->creatoWhere=true;
        return;
    }


    //formatta il campo di ricerca a seconda del tipo cercato
    private function procVal($elemento)
    {
        switch ($elemento) {
            case 'data':
            case 'date':
            $date = $elemento['val'];
            if (($date == "") || (strlen($date) < 8)) {
                $val = '';
            } else {
                $d = DateTime::createFromFormat('d/m/Y', $date);
                $val = $d->format('Y-m-d');
            }
            break;

            default:
            $val=$elemento['val'];
            // $val =$this->web->estr($elemento['val']);
            break;
        }
        return $val;
    }


    //carica le varibile direttamente dal post - comodo per utilizzare la funzione con seleIns
    public function loadFromPost(string $post="filter")
    {
        if (isset($_POST[$post])) {
            $this->whereEle = $this->post($post);
        } else {
            return 0;
        }
    }

    public function addRicerca($campo, $clausola)
    { //aggiunge gli elementi nel where finale, è possibile usare sia la sostituzione con ~campo che l'accodamento
        if ($campo != "") {
            $str = str_replace('~campo', $campo, $clausola);
            $this->addWhere($str);
        } else {
            return 0;
        }
    }


    /**
    * Attiva la modalitàdi debug query
    * @return [type] [description]
    */
    public function debug()
    {
        echo ('Attenzione query Cerca in debug');
        $this->debug = true;
    }

    /**
    * Torna l'ultima query eseguita
    * @return string query eseguita
    */
    public function getLastQuery()
    {
        return $this->lastQuery;
    }

    private function bindPrepare():void
    {

        if(!$this->whereEle){
            return;
        }
        foreach ($this->whereEle as $elemento) {
            if(!empty($this->whereEleClausola[$elemento['id']] )){ //controllo se è un parametro da bindare sul serio

                $campo = $elemento['id'];

                $val = $this->procVal($elemento);
                $this->getUtility()->bindP($campo, $val);
            }
        }
        return;
    }

    /**
    * Controlla se il soft delete è valido per la sessione
    * @return string
    */
    private function checkSoftDelete():string{
        $main=$this->getAliasMain();
        if(!$this->getUtility()->isHardDelete() && $this->activeSoftDelete){
            return $this->getUtility()->getSoftDeleteWhere($main);
        }else{
            return "";
        }
    }


    public function setTableMaster(string $table):void{
      $this->tableMaster=$table;
    }



    private function getTableMaster():string{
      return $this->tableMaster;
    }

    public function bindFilter(string $idBind,string $idFilter){
      if(empty($this->whereEle[$idFilter]['val'])){
        $this->error("parametro su filtro non valido");
      }
      $bind["bind"]=$idBind;
      $bind['filter']=$idFilter;
      array_push($this->bindedFilter,$bind);
    }

    /**
     * Bind fuori dal where automatico
     * @return [type] [description]
     */
    private function customBind(){
      foreach($this->bindedFilter as $filt){
        $key=$filt['bind'];
        $filter=$filt['filter'];
        $elem=$this->post("filter")[$filter];

        $this->getUtility()->bindP($key,$elem['val'],$elem['type']);
      }
    }

    /**
    * Crea ed esegue la query
    * @return [type] [description]
    */
    public function execute()
    {
        if (!$this->creatoWhere) {//creo il where partendo dai post e dalle regole assegnate
            $this->creaWhere();
        }
        $query = $this->query;

        $adWhere=$this->checkSoftDelete();

        $query = str_replace('~where', $this->where.$adWhere, $query);
        if ($this->debug) {
            $this->getUtility()->warning("query di debug",$query);
            echo $query;
            die;
        }

        $this->lastQuery=$query;
        $this->getUtility()->startPrepare($query);
        $this->bindPrepare();
        $this->customBind();
        return $this->getUtility()->executePrepare($query);
    }
}
