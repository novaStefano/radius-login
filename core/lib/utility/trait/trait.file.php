<?php


/**
*Trait per le operazioni su file
*/
trait File
{
  /**
  * Torna e alloca la cartella dei file temporanei
  * @return string
  */
  public function getTmpDir():string{
    $path=$this->getPath("TMP");
    if(!file_exists($path)){
      mkdir($path,0655);
    }
    return $path;
  }

  /**
  * Torna la cartella temporanea univoca
  * @return string cartella generata
  */
  public function getRandomTmpDir():string{
    $path=$this->getTmpDir();
    $rand=$this->getRandomString(6);
    $path=$path.$rand.DIRECTORY_SEPARATOR;
    mkdir($path,0655);
    return $path;
  }

  /**
  * Ritorna il nome del file senza estensione
  * @param  string $path nome del file
  * @return string fileaName senza estensione
  */
  public function getFileNameNotExt(string $path):string{
    return pathinfo($path, PATHINFO_FILENAME);
  }


  /**
  * Torna il peso del file formattato
  * @param  string $path path assoluta del file
  * @return string       filesize calcolato in stringa
  */
  public function formatFileSize(string $path="0"):string
  {
    $peso=$this->getFileSize($path);
    $peso=$peso/1000;
    if ($peso<1000) {
      return $peso." kb";
    }

    $peso=$peso/1000;
    if ($peso<1000) {
      return $peso." mb";
    }

    $peso=$peso/1000;
    if ($peso<1000) {
      return $peso." gb";
    }

    $peso=$peso/1000;
    if ($peso<1000) {
      return $peso." tb";
    }
  }

  /**
  * Torna il peso del file richiesto
  * @param  string $path percorso del file
  * @return string       peso del file
  */
  public function getFileSize(string $path){
    if(is_dir($path)){ //caso directory
      $bytestotal = 0;
      $path = realpath($path);
      if($path!==false && $path!='' && file_exists($path)){
        foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object){
          $bytestotal += $object->getSize();
        }
      }
      return $bytestotal;
    }else{      //caso del file
      if($peso=filesize($path)){
        return $peso;
      }else{
        return 0;
      }
    }
  }


  /**
  * Scansione della cartella ad array
  * @param  string  $path     cartella
  * @param  boolean $absolute percorso totale?
  * @return array             risultato scansione
  */
  public function scandir(string $path,bool $absolute=false):array
  {
    $scan = scandir($path);
    $dirs=[];
    //ciclo ogni cartella /file per sistemare gli include
    foreach ($scan as $rec) {
      if (($rec!=".")&&($rec!="..")) { //salto i riferimenti per la navigazione
        if($absolute){
          $app=$path.$rec;
        }else{
          $app=$rec;
        }
        array_push($dirs,$app );
      }
    }//end foreach
    return $dirs;
  }

  /**
  * Rimuove le cartelle ricorsivamente
  * @param  strign $dir
  * @return bool
  */
  public function removeFile(string $dir):bool{
    if($dir=="/"){
      return false;
    }
    if($dir=="/home/"){
      return false;
    }

    if (!file_exists($dir)) {
      return true;
    }
    if (!is_dir($dir)) {
      return unlink($dir);
    }
    foreach (scandir($dir) as $item) {
      if ($item == '.' || $item == '..') {
        continue;
      }
      if (!$this->removeFile($dir . DIRECTORY_SEPARATOR . $item)) {
        return false;
      }
    }
    return rmdir($dir);
  }

  /**
  * Zippa il file richiesto
  * @param  string $file       file da zippare
  * @param  string $fileResult fileRisultato (vuoto prende un percorso temporaneo)
  * @return string fileName file generato
  */
  public function zipFile(string $file,string $fileResult=""):string{
    if(empty($fileResult)){ //nome di default in caso di non specificato file result
      $fileResult=$this->getRandomTmpDir().$this->getFileNameNotExt($fileResult).".zip";
    }
    $zip = new ZipArchive();
    if ($zip->open($fileResult) === TRUE) {
      $zip->addFile($file, basename($file));
      $zip->close();
      return $fileResult;
    } else {
      $this->warning("ERRORE ZIP FILE $file");
      return "ERROR";
    }
  }

  /**
  * Zippa una cartella
  * @param  string $dir        cartella scelta
  * @param  string $fileResult fileGenerato (vuoto prende un tmp di defautl)
  * @return string $file generato
  */
  public function zipFolder(string $dir,string $fileResult=""):string{
    // Initialize archive object
    if(empty($fileResult)){
      $fileResult=$this->getRandomTmpDir().$dir.".zip";
    }
    $zip = new ZipArchive();
    if(!$zip->open($fileResult, ZipArchive::CREATE | ZipArchive::OVERWRITE)){
      $this->warning("ERRORE ZIP DIRECTORY $dir");
      return "ERROR";
    }

    // Create recursive directory iterator
    /** @var SplFileInfo[] $files */
    $files = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator($rootPath),
      RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $name => $file)
    {
      // Skip directories (they would be added automatically)
      if (!$file->isDir())
      {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath) + 1);
        // Add current file to archive
        $zip->addFile($filePath, $relativePath);
      }
    }
    // Zip archive will be created only after closing object
    $zip->close();
    return $fileResult;
  }



  /**
  *Scarica un csv con l'array formattato
  */
  private function downloadCsv(string $fileName,array $data,array $option=[]){
    // tell the browser it's going to be a csv file
    header('Content-Type: application/csv');
    // tell the browser we want to save it instead of displaying it
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    // make php send the generated csv lines to the browser
    $f = fopen('php://output', 'w');
    $first=true;
    foreach($data as $row){
      if($first){
        foreach($row as $key ){
          fputcsv($f,$key);
        }
      }
      fputcsv($f,$data);
    }
  }

  public function deleteDir($path) {
    if(empty($path)){
      return;
    }
    if($path=="/"){
      return 0;
    }

    $class_func = array(__CLASS__, __FUNCTION__);
    return is_file($path) ?
    @unlink($path) :
    array_map($class_func, glob($path.'/*')) == @rmdir($path);
  }



  /**
  * Copia tutti i file e le sotttocartelle nella destinazione scelta
  * @param  string $source sorgente cartella
  * @param  string $dest   destinazione
  * @return bool
  */
  public function cpDirectory(string $source,string $target, array $option=[]  ):bool {

    if ( is_dir( $source ) ) {

      $excludeDir=[];
      if(!empty($option['excludeDir'])){
        $excludeDir=$option['excludeDir'];
      }

      @mkdir( $target );
      $d = dir( $source );
      while ( FALSE !== ( $entry = $d->read() ) ) {
        if ( $entry == '.' || $entry == '..' ) {
          continue;
        }

        $Entry = $source . '/' . $entry;
        if ( is_dir( $Entry ) ) {
          if(in_array($entry,$excludeDir)){
            continue; //salto per l'opzione
          }

          $this->cpDirectory( $Entry, $target . '/' . $entry,$option);
          continue;
        }
        copy( $Entry, $target . '/' . $entry );
      }

      $d->close();
    }else {
      copy( $source, $target );
    }

    return true;

  }

  /**
  * Ritorna il file tramite header
  * @return [type] [description]
  */
  public function getHeaderFile(string $file,string $fileUrl){
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.$file.'"');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($fileUrl)); //Absolute URL
    ob_clean();
    flush();
    readfile($fileUrl); //Absolute URL
    die;
  }

  /**
  * Ritorna il file tramite header
  * @return [type] [description]
  */
  public function getHeaderImg(string $file,string $fileUrl){
    $filename = basename($file);
    $file_extension = strtolower(substr(strrchr($filename,"."),1));
    switch( $file_extension ) {
      case "gif": $ctype="image/gif"; break;
      case "png": $ctype="image/png"; break;
      case "jpeg":
      case "jpg": $ctype="image/jpeg"; break;
      case "svg": $ctype="image/svg+xml"; break;
      default:
    }
    header('Content-type: ' . $ctype);
    ob_clean();
    flush();
    $file=file_get_contents($fileUrl);
    echo base64_encode($file);
    // readfile($fileUrl); //Absolute URL
    die;
  }


  /**
  * Crea una cartella con i permessi standard
  * @param  string $dir [description]
  * @return bool        [description]
  */
  public function makeDir(string $dir):bool{
    if(file_exists($dir)){
      return true;
    }
    if (!mkdir($dir, 0755, true)) {
      return false;
    }else{
      return true;
    }
  }

  public function maxCarDir(string $dir,int $car=50):string{
    if (strlen($dir)>$car) {
      return substr($dir, 0, $car)."..";
    } else {
      return $dir;
    }
  }


  public function getExtFile(string $path){
    $info=pathinfo($path);
    if(!isset($info['extension'])){
      return "";
    }
    return $info['extension'];
  }


  public function getCacheDir(){
    $cache=$this->getPath("cache");
    if(!file_exists($cache)){
      $this->makeDir($cache);
    }
    return $cache;
  }


}//end classe
