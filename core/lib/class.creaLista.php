<?php

/**
 * Classe per la creazione delle liste dinamiche selezionabili
 */

class CreaLista extends WrapperUtility
{
    private $html = ""; //variabile di buffer per html

    private function addHtml(string $html)
    {
        $this->html .= $html;
    }

    private function makeArray(&$row)
    {   
        if(!$row){
			return;
		}
        $i=0;
        foreach ($row as $key => $value) {
           $row[$i]=$value;
           if(strpos($key,"--label") ){
                $row['--label']=$value;
            }
            $i++;
        }
    }

    private function procRow($row, &$first)
    {       
        $this->makeArray($row);
        $id = $this->getUtility()->escapeHtml($row[0]);
        if (!isset($row[1])) { //solo valore senza id
            $row[1] = $row[0];
        }
        $valore = $this->getUtility()->escapeHtml($row[1]);

        // if (!empty($row[2])) {
        //     $terzo = $this->getUtility()->escapeHtml($row[2]);
        //     $terzo = " <b>$terzo</b>";
        // } else {
            // }
        $attr="";
        $terzo = "";
            
        $label = "";
        //comandi tipo creatab per l'abilitazione del baloon
        if (isset($row['--label'])) {
            try {
                $r = explode(".", $row['--label'], 2);
                if (count($r) < 2) {
                    return "";
                }
                $color = $r[0];
                $label = $r[1];
            } catch (Exception $err) {
                $this->error("ERRORE BALOON CREALISTA");
            }
            $label = "<span class='label label-$color' style='float:right;'>$label</span>";
            $attr=" label-class='inlineSearch-label-$color' ";
        }




        if ($first) {
            $class = " class='selectedInSearch' ";
            $first = false;
        } else {
            $class = "";
        }
        $this->addHtml("<li onclick='inSearch.selecLista(this)' $attr  $class source-id='$id' ><i class='fa fa-dot-circle'></i> <span>$valore$terzo</span> $label </li>");
    }

    public function execute(string $ds = "")
    {
        $first = true;
        $this->addHtml("<ul class='ulCercaLista'>");
        while ($row = $this->getUtility()->fetch($ds)) {
            $this->procRow($row, $first);
        }
        $this->addHtml("</ul>");
        echo $this->html;
    }
}//fine classe
