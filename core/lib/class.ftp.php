<?php


class Ftp extends WrapperUtility{

  private $logged=false;
  private $ftp=null;


  private function getFtp(){

    return $this->ftp;

  }

  public function isLogged(){
    return $this->logged;
  }

  public function login(string $host,string $username="",string $password="",array $option=[]){
    try{
      $ftpConn=ftp_connect($host);
    }catch(Exception $err){
      $this->warning("Errore connessione a host FTP: ".$err->getMessage());
      return false;
    }

    try{
      if(!ftp_login($ftpConn,$username,$password)){
        $this->ftp=null;
        $this->logged=false;
        return false;
      }else{
        // turn passive mode on
        if(!empty($option['passive'])){
          ftp_pasv($ftpConn, true);
        }

        if(!empty($option['active'])){
          ftp_pasv($ftpConn, false);


        }

        $this->ftp=$ftpConn;
        $this->logged=true;
        return true;
      }
    }catch(Exception $err){
      $this->warning("Errore auth a host FTP: ".$err->getMessage());
      return false;
    }
  }

  /**
   * Apre la directory richiesta
   * @param  string $dir [description]
   * @return bool        [description]
   */
  public function openDir(string $dir):bool{
    try{
      return @ftp_chdir($this->getFtp(),$dir);
    }catch(Exception $err){
      $this->warning("Errore open dir FTP: ".$err->getMessage());
      return false;
    }
  }

  public function turnToHome(){
    try{
      return @ftp_chdir($this->getFtp(),"~");
    }catch(Exception $err){
      $this->warning("Errore open dir HOME FTP: ".$err->getMessage());
      return false;
    }
  }


  public function getCurrentDir(){
    try{
      $cur=ftp_pwd($this->getFtp());
      return $cur;
    }catch(Exception $err){
      $this->warning("Errore getcurrent dir FTP: ".$err->getMessage());
      return false;
    }

  }

  /**
   * Crea una cartella o una lista di cartelle
   * @param  string $newDir [description]
   * @return [type]         [description]
   */
  public function makeDir(string $newDir,array $option=[]){
    try{
    $list=explode("/",$newDir);
    $getCur=$this->getCurrentDir();
    foreach($list as $l){
      if(empty($l)){
        continue;
      }
      if(!$this->openDir($l)){
        ftp_mkdir($this->getFtp(), $l);
        $this->openDir($l);
      }
    }

    if(!isset($option['openNew']) || $option['openNew']==false ){
      $this->openDir($getCur);
    }
    return true;

  }catch(Excption $err){
    $this->warning("Errore mak dir FTP: ".$err->getMessage());
    return false;
  }

  }


  /**
   * Crea la nuova dir e la tiene aperta
   * @param  [type] $newDir [description]
   * @return [type]         [description]
   */
  public function makeDirAndOpen($newDir){
    $this->makeDir($newDir,['openNew'=>true]);
  }

  /**
   * Caricamento di un file
   */
  public function upload(string $localFile,string $newFile=""){
    try{
      $newDir=false;
      if(empty($newFile)){
        $newFile=basename($localFile);
      }
      if(basename($newFile!=$newFile)){
        $cur=$this->getCurrentDir();
        $this->makeDirAndOpen(dirname($newFile));
        $newFile=basename($newFile);
        $newDir=true;
      }

      $res= ftp_put($this->getFtp(),$newFile,$localFile);
      if($newDir){
        $this->openDir($cur);
      }
      return $res;
    }catch(Exception $err){
        $this->warning("Errore upload FTP: ".$err->getMessage());
        return false;
    }
  }

  public function list(){
    $list= ftp_nlist($this->getFtp(),".");
    $i=0;
    foreach($list as &$l){
      if($l=='./.' || $l=='./..' ){
        unset($list[$i]);
      }else{
        $l=$this->getUtility()->strReplaceFirst("./","",$l);
      }
      $i++;
    }
    return $list;
  }



  /**
  * Copia tutti i file e le sotttocartelle nella destinazione scelta
  * @param  string $source sorgente cartella
  * @param  string $dest   destinazione
  * @return bool
  */
  public function uploadDir(string $source, array $option=[]  ):bool {

    if ( is_dir( $source ) ) {

      $excludeDir=[];
      if(!empty($option['excludeDir'])){
        $excludeDir=$option['excludeDir'];
      }

      $dir=basename($source);
      $cur=$this->getCurrentDir();
      $this->makeDirAndOpen($dir); //creo la cartella principale richiesta

      $d = dir( $source );
      while ( FALSE !== ( $entry = $d->read() ) ) {
        if ( $entry == '.' || $entry == '..' ) {
          continue;
        }

        $Entry = $source . '/' . $entry;
        if ( is_dir( $Entry ) ) {
          if(in_array($entry,$excludeDir)){
            continue; //salto per l'opzione exclude
          }
          $this->uploadDir( $Entry, $option); //ricorsione per la creazione della nuova cartella
          continue;
        }
        $this->upload($Entry);
      }
      $d->close();
      $this->openDir($cur); //torno alla root di prima
    }else {
      $this->upload( $source);
    }
    return true;
  }


} //end class
