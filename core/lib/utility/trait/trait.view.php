<?php
/**
* Trait per la gestione delle view
*/
trait utilityView
{
	private $startBufferOb=false; //controllo se la vista sta caricando un buffer attivo

	/**
	* Inizia a cattuare tutto l'html che esce nello STDOUT
	* @return void
	*/
	public function startBufferHtml():void
	{
		$this->startBufferOb=true;
		ob_start();
		return;
	}

	/**
	* Aggiunge il buffer alla variabile HTML
	* @return void
	*/
	public function getBufferHtml()
	{
		if (!$this->startBufferOb) {
			return;
		}
		$this->startBufferOb=false;
		$html= ob_get_contents();
		ob_end_clean();
		return $html;
	}


	public function loadCssG(string $css,array $options=[]){
		$options=array_merge($options,["path"=>$this->getPath("A_APP")."view/"] );
		return $this->loadCss($css,$options);

	}

	/**
	* Ritora il caricamento di css
	* @return string
	*/
	public function loadCss(string $css,array $options=[]){
		//se ho delle sotto cartelle
		$exp=explode("/",$css);
		$c=count($exp);
		if($c>1){
			$dir=$this->str_lreplace($exp[$c-1],"",$css);
			$css=$exp[$c-1]; //l'ultimo elemento è solo il nome
		}else{
			$dir="";
		}


		if(empty($options['path'])){
			$path="";
		}else{
			$path=$options['path'];
		}

		$data=[];
		$css=$path.$dir."css/$css.css";
		$add="<style>";
		$add.=$this->loadText($css,$data,$options);
		$add.="</style>";
		return $add;
	}


	public function loadText(string $fileText,array $data=[],array $options=[])
	{
		$this->startBufferHtml(); //start del buffer per la vista
		if (!file_exists($fileText)) { //validità di esistenza della vista
			$this->warning("ERRORE CARICAMENTO FILE $fileText");
			$this->halt();
			die;
			return false;
		}
		try{ //controllo se l'include va a buon fine
			if($data!=[]){ //estraggo i dati solo in caso ci siano
				$this->escapeArray($data); //escape di tutti gli elementi per evitare xss o altri injection
				extract($data); //estraggo i dati per farli caricari alla view
			}
			include $fileText; //caricamento della view
		} catch (exception $e) { //interrompo la request in caso di Catch della routine
			$html=$this->getBufferHtml();
			$this->warning($e->message);
			$this->error("Caricamento TEXT");
			die;
		}
		return $this->getBufferHtml(); //torno l'html caricato dalla view
	}


	/**
	 * Carica la view dal percorso globale
	 * @return
	 */
	public function loadViewG(string $nameView,array $data=[],array $options=[]){

		$options["path"]=$this->getPath("A_APP");
		return $this->loadView($nameView,$data,$options);
	}



	public function bladeView($nameView,$dir,$data):string{
		$blade=$this->getBlade($dir);
		return $blade->run($nameView,$data);
	}

	/**
	* Include una view esterna
	* @param  nome vista $nameView
	* @param  array $data parametri da passare alla view
	* @return void nessun ritorno
	*/
	public function loadView(string $nameView,array $data=[],array $options=[])
	{

		//se ho delle sotto cartelle
		$exp=explode("/",$nameView);

		$c=count($exp);
		if($c>1){
			$dir=$exp[0];
			$nameView=$exp[$c-1]; //l'ultimo elemento è solo il nome
		}else{
			$dir="";
		}

		if(!empty($dir)){
			$dir="/".$dir;
		}

		if(empty($options['path'])){
			$path=getcwd()."/";
		}else{
			$path=$options['path'];
		}
		$data['utility']=$this;
		if($this->getIni("viewEngine")=='blade'){
			return $this->bladeView($nameView,$path."view".$dir,$data) ;
		}

		$fileView="view/".$dir."view.".$nameView.".php"; //path di default relativa (si parte dal modulo che richiama la vista)


		// return $this->loadFile($fileView,$data,$options);
		if(!empty($options['path'])){
			$fileView=$options['path'].$fileView;
		}

			$this->startBufferHtml(); //start del buffer per la vista
			if (!file_exists($fileView)) { //validità di esistenza della vista
				$this->warning("ERRORE CARICAMENTO VIEW $nameView");
				$this->error("Caricamento VIEW $fileView");
				die;
			}
			try{ //controllo se l'include va a buon fine
				if($data!=[]){ //estraggo i dati solo in caso ci siano
					$this->escapeArray($data); //escape di tutti gli elementi per evitare xss o altri injection
					extract($data); //estraggo i dati per farli caricari alla view
				}
				include $fileView; //caricamento della view
			} catch (exception $e) { //interrompo la request in caso di Catch della routine
				$html=$this->getBufferHtml();
				$this->warning($e->message);
				$this->error("Caricamento VIEW");
				die;
			}
			return $this->getBufferHtml(); //torno l'html caricato dalla view
	}


	/**
	* stampa una view esterna
	* @param  nome vista $nameView
	* @param  array $data parametri da passare alla view
	* @return void nessun ritorno
	*/
	public function printView(string $nameView,array $data=[],array $options=[])
	{
		echo $this->loadView($nameView,$data,$options);
	}


} //end trait



?>
