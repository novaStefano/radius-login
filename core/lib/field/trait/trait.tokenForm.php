<?php

/*Metodi per la generazione del token
*/

trait tokenForm
{
  private $tokenSessionField=""; //token di registrazione della form.
  private $id="";//id autoincrement associato al record del token
  private $colInsert="";//variabile per gestire l'insert
  private $valInsert=""; //variabile per gestire i valori dell'insert
  private $startToken=false; // controllo se ho fatto partire la routine di registrazione del token

  //dati per la registrazione del token
  private $table=""; //nome della tabella per cui è stata registrata la sessione
  private $idTable="";//campo id della tabella
  private $idVal=""; //valore dell'id della tabella aperta
  private $customTokenId=""; //idToken custom per le sessioni con multi tabelle
  private $curToken=0; //nel caso mi servano token annidati per più tabelle
  private $useTokenForm=null;
  /**
  * validazione e log su db
  */
  private $registerField=[]; //array contente i file da registrare in db

  private $recordLocked=false; // controlla il record sia libero da eventuali lock

  /**
  * Torna il token in sessione per la form
  */
  public function getTokenField()
  {
    return $this->tokenSessionField;
  }

  /**
  * Torna l'indice attuale del token partendo da 0
  * @return [type] [description]
  */
  private function getCurTokenIndex()
  {
    return $this->curToken;
  }

  /**
  * Imposta il token custom
  * @param string $token token da usare nella sessione
  */
  private function setCustomTokenId(string $tokenId)
  {
    $this->customTokenId=$tokenId;
  }

  /**
  * Torna il token customizzato se c'è, atrlimenti prende l'index standard
  * @return string customToken
  */
  private function getCurToken()
  {
    if (empty($this->customTokenId)) {
      return $this->getCurTokenIndex();
    } else {
      return $this->customTokenId;
    }
  }

  /**
  * Genera il token univoco per l'uitilizzo del form
  * @return bool
  */
  private function generateToken()
  {
    return $this->getOtpToken();
  }

  private function registerToken(){
    $this->tokenSessionField=$this->generateToken();
  }

  /**
  * Registra il token per la sessione in corse
  */
  private function registerTokenForm(string $table, string $id, $val="")
  {
    if(!$this->isActiveTokenForm()){
      return 0;
    }
    $this->startToken=true;
    $this->table=$table;
    $this->idTable=$id;
    $this->idVal=$val;
    $this->registerToken();
    $this->setCustomTokenId($table);
    $this->curToken++;
  }


  /**
  * Controlla se la sessione deve permettere il lock del record, nel caso controllo il record lo sto modificando io
  */
  private function checkLockRecord():bool{

    if (!$this->isActiveTokenForm()) { //proseguo solo se è attiva la funzione
      return 0;
    }

    if(!empty($this->recordLocked)){
      return $this->recordLocked;
    }
    if(empty($this->idVal)){
      $this->recordLocked=false;
      return false; // evito il controllo su insert, altrimenti posso inserire solo un record alla volta
    }

    if(!$this->getUtility()->ifLockRecord()){
      $this->recordLocked=false;
      return false;
    }

    $timeout=$this->getUtility()->getTimeoutLock();
    $idUtente=$this->getUtility()->getIdUser();

    if(!$this->isOnInsert()){ //devo attivare il lock solo se sto modificando e solo se sono in update
      if(!$this->checkPermesso($this->table,"update")){ //non posso modificare, il lock non mi serve
        $this->recordLocked=false;
        return false;
      }
    }else{ //sono in insert non serve verificare il lock
      $this->recordLocked=false;
      return false; 
    }




    $this->startPrepare(" SELECT idVal,log.idUtente from utente_sessione_form  as form
      inner join utente_sessione as log on log.idSessione= form.tokenLogin and log.idUtente<>'$idUtente'
      where tableName=:table and idval=:id and date_add(dataSessione, interval $timeout SECOND)>now()   ");
      $this->bindP("table",$this->table);
      $this->bindP("id",$this->idVal);
      $this->executePrepare();

      if($this->isDatasetEmpty()){
        $this->recordLocked=false;
        return false;
      }else{

        $idUtente=$this->fetch()['idUtente'];
        $this->userLocked=$this->getModel("Utente")->where($idUtente)->first()['nome'];
        $this->recordLocked=true;
        return true;
      }

    }


    public function getLockRecord():bool{
      return $this->recordLocked;
    }

    private function printMessageLock(){
      if($this->checkLockRecord()){
        $this->gDiv();
        $this->title("Record in stato di <b>LOCK</b> la risorsa è attualmente aperta dall'utente $this->userLocked ");
        $this->chiudi_gDiv();
      }
    }


    /**
    * registra il token nel database per essere usato nella prossima sessione
    * @return [type] [description]
    */
    public function executeTokenForm()
    {

      if(!$this->isActiveTokenForm()){
        return 0;
      }


      if(!$this->startToken){
        return 0;
      }

      $this->printMessageLock();

      if (!$this->startToken) {
        return 0;
      }
      $tokenField=$this->getTokenField();
      if (!$this->isActiveTokenForm()) {
        $logDb='0';
      } else {
        $logDb='1';
      }
      $str="INSERT INTO utente_sessione_form (tableName,idCol,idVal,queryBuild,tokenForm,tokenLogin,logDB,dataSessione) values
      ('{$this->table}','$this->idTable','$this->idVal','$this->colInsert','$tokenField','".$this->getUtility()->getIdTokenLogin()."',$logDb,now()) ";
      $this->query($str);
      echo "<script>{$this->getObjJs()}.addTokenForm('$tokenField','{$this->getCurToken()}');</script> ";
      $this->registerTokenField();
      $this->clearAfterExecute();
    }


    public function disableTokenForm(){
      $this->useTokenForm=false;
    }


    /**
    * Controllo se è attivo la registrazione dei campi + validazione sul db
    * @return boolean [description]
    */
    private function isActiveTokenForm()
    {
      if($this->useTokenForm===null){
        $this->useTokenForm=$this->getUtility()->getIni("useTokenForm");
      }
      return $this->useTokenForm;
    }

    private function clearAfterExecute(){
      $this->idTable="";
      $this->idVal="";
      $this->table="";
      $this->colInsert="";
      $this->registerField=[];

    }

    /**
    * Registro i campi che servono per la validazione
    */
    private function registerTokenField()
    {
      if (!$this->isActiveTokenForm()) {
        return 0; //skippo se la funzione è disabilitata
      }
      $idTokenForm=$this->getUtility()->getLastInsertId();
      $str="INSERT INTO utente_sessione_form_campi (idUtenteForm,nameCol,valCol,
        required,validazione,type,useOninsert,useOnUpdate,forceValue)values     ";
        $first=true;
        foreach ($this->registerField as $field) {
          if (!$first) {
            $str.=",";
          } else {
            $first=false;
          }
          $str.=" ('$idTokenForm','{$field['col']}','{$field['valCol']}','{$field['required']}','','{$field['type']}',
          '{$field['insert']}','{$field['update']}','{$field['forceValue']}' ) ";
        }

        $this->query($str);
      }



      /**
      * Imposta l'id da usare per i sottosalvataggi
      * @param string $idName nome della colonna Master
      * @param string $idVal  valore dell'id
      */
      public function setIdMaster(string $idName, string $idVal)
      {
        $param['id']=$idName;
        $param['type']="idMaster";
        $param['insert']='1';
        $param['update']='0';
        $param['val']=$idVal;
        $param['forceValue']='1';
        $param['required']='1';
        $this->regFieldSession($param);
      }


      public function getDescribe(){
        while($data=$this->fetch()){

          if(isset($data['Field'])){
            return $data['Field'];
          }
        }
      }

      /**
      * Funzione per il delete solo per i form inLine
      * *@param sezione da eliminare
      *  @param colonne da aggiungere come chiave
      */
      public function registerDeleteInline(string $tipo,string $sezione,array $deleteKey=[])
      {
        if (!$this->isActiveTokenForm()) { //proseguo solo se è attiva la funzione
          return 0;
        }
        //eseguo le query in form per evitare di distruggermi altre sessioni nel caso ci siano

        $col=$this->getUtility()->getColumnStruct();
        $table=reset($col)['table'];
        $this->setWorkTableInline($table);
        if (!$this->checkPermesso($table, "elimina")) {
          if (!$this->getUtility()->ifPermessoNonTrovato($table)) { //se non ho il permesso già inserito posso ignorare il controllo - vedi casi subTable
            return false;
          }
        }
        $where=" ";
        $this->query("DESCRIBE $table  "); //prendo l'id autoinc dell'inlineForm e mi serve dopo nel caso devo eliminare
        $id=$this->getDescribe();


        $where.=" $id = :{$tipo}Id ";
        foreach ($deleteKey as $key => $val) {
          $where.=" AND $key = \"$val\" ";
        }
        $tokenField=$this->generateToken(); //genero il codice per l'occasione
        $str="INSERT INTO utente_sessione_form (tableName,queryBuild,tokenForm,tokenLogin,idCol) values
        ('{$table}',' WHERE {$where} ','{$tokenField}','{$this->getUtility()->getIdTokenLogin()}','$id') ";
        $this->query($str);
        echo "<script>$tipo.addTokenFormDelete('$tokenField','{$tipo}Delete-$sezione'); </script> ";
        return true;
      }


      /**
      * registra il singolo campo con le informazioni di validazione
      * @return [type] [description]
      */
      private function regFieldSession(array $param)
      {

        if (!$this->isActiveTokenForm()) { //proseguo solo se è attiva la funzione
          return 0;
        }

        //se è un id devo saltarlo
        if ($param['type']=='id') {
          return 0;
        }

        if( isset($param['disabled']) ){
          return 0;
        }

        $colName=$param['id'];
        $adQuery=true; //controllo se devo aggiungere il campo al modello query

        //sono in inserimento ma questo parametro non serve
        if (($param['insert']!="1")&&($this->isOnInsert())) {
          $adQuery=false;
        }
        //sono in update ma questo parametro non serve
        if (($param['update']!="1")&&($this->isOnUpdate())) {
          $adQuery=false;
        }

        if ($adQuery) {
          if (!empty($this->colInsert)) {
            $this->colInsert.=",";
          }
          //creo un array per salvare i dati successivi per poi registrarli nella tabella. Deve essere attivatata la funzione
          $this->colInsert.=$colName."= :$colName " ;
        }



        $add=[];
        if(isset($param['cType'])){
          $add['type']=$param['cType'];
        }else{
          $add['type']=$param['type'];
        }
        $add['col']=$colName;
        $add['required']=$param['required'];
        $add['insert']=$param['insert'];
        $add['update']=$param['update'];
        if (empty($param['forceValue'])) {
          $force="0";
          $add['valCol']="";
        } else {
          $force="1";
          $add['valCol']=$param['val'];
        }
        $add['forceValue']=$force;
        array_push($this->registerField, $add);
      }
    } //fine classe
