<?php

// include_once "../../iniz.php";
// $web->incBaseFunction();

class festivita extends classBase{
    private $dataInizio;
    private $intervallo=1;
    private $includiSabato=0;
    private $resAsData=true;
    //
    // public function __construct($web){
    //   $this->constructStd($web);
    //   $this->launcher();
    // }

    public function launcher($azione){
        switch ($azione) {
            case 'calcoloGiorniFestivi':
            $this->web->post('resAsData', '0', $resAsData);
            $res = $this->calcoloGiorniFestivi($resAsData);
            if( !empty($res) ){
                echo json_encode( array("ok" => true, "res" => $res) );
            } else { echo json_encode( array("ok" => false, "res" => $res) ); }
            break;

            default:
            header("X-Error: impossibile trovare l'azione corretta.", false, 400);
            break;
        }
    }


    public function loadParamFromPost(){
        $web=$this->getWeb();
        $web->post('dataInizio', '', $dataInizio);
        $web->post('intervallo', '1', $intervallo);
        $web->post('includiSabato', '0', $includiSabato);
    }

    public function setParam($dataInizio,$intervallo,$sabato){
        $this->dataInizio =$dataInizio;
        $this->intervallo = $intervallo;
        $this->includiSabato = $sabato;
    }

    public function resAsData($flag){
        $this->resAsData=$flag;
    }

    private function checkDbData($dataInEsame){
        /* Controllo se è stata segnata qualche festività a db per questo giorno. */
        $anno = date('Y', strtotime($dataInEsame));
        // echo $anno;
        $que = " SELECT * FROM festivita AS f
        WHERE '{$dataInEsame}' >= REPLACE(f.dataInizio, '0000', '{$anno}')
        AND '{$dataInEsame}' <= REPLACE(f.dataFine, '0000', '{$anno}') ";
        $res = $this->web->query($que);
        if($res->num_rows > 0){
            // echo 'È festivo';
            return true;
        }
        return false;
    }

    public function calcoloGiorniFestivi(){
        $web=$this->getWeb();


        /* Calcolo dei giorni festivi all'interno dell'intervallo. */
        if($this->includiSabato == '1'){ $maxDay = 5; } else { $maxDay = 6; }
        $dataInEsame = $this->dataInizio;
        $giorniFestivi = 0;
        $intervallo=$this->intervallo;
        for( $i=0; $i<= $intervallo; $i++ ){
            if ( date('N', strtotime($dataInEsame)) > $maxDay){
                /* Siamo di domenica (o nel caso siana abilitato il conteggio, di sabato. Nessun controllo db da fare ) */
                $giorniFestivi++;
                /* Diminuisco l'indice dei cicli in quanto questa data non è lavorabile */
                $i--;
            } else {
                if($this->checkDbData($dataInEsame)){
                    /* La data è stata trovata in una festività. aggiungiamo un giorno festivo all'intervallo. */
                    $giorniFestivi++;
                    /* Diminuisco l'indice dei cicli in quanto questa data non è lavorabile */
                    $i--;
                }
            }

            /* Scorro al prossimo giorno */
            $dataInEsame = date('Y-m-d', strtotime($dataInEsame." +1 day"));
        }

        /* Incremento l'intervallo del numero di giorni festivi trovati. */
        $newIntervallo = $intervallo + $giorniFestivi;

        /* Inserita la possibilita di scegliere se si vuole il risultato in giorni o in data specifica. */
        if( $this->resAsData){
            return date('Y-m-d', strtotime($this->dataInizio." +$newIntervallo day"));
        }else {
            return $newIntervallo;
        }
    }



}//fine class



?>
