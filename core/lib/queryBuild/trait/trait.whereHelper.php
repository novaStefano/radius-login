<?php

trait qBuildWhereHelper{

    /**
     * Auto form per lo stato
     */
    public function whereFormStato(){
        $idName=$this->getIdName();
        $statoName=$idName."_stato";
        return $this->whereFormA($statoName);
    }   

    public function whereFormA(string $idName,$condition="",$table=""){
        if(empty($table)){
            $table=$this->getTableName();
        }
        $idName=$table.".".$idName;
        if(empty($condition)){
            return $this->whereForm($idName);
        }else{
            return $this->whereForm($idName,$condition);
        }
    }



        public function whereFormFrom(string $idName,$table=""){
            $id="from".$idName;
            if(empty($table)){
                $table=$this->getTableName();
            }
            return $this->whereFormA($id," and $table"."."."$idName >= ? ",$table);
        }


        public function whereFormTo(string $idName,$table=""){
            $id="to".$idName;
            if(empty($table)){
                $table=$this->getTableName();
            }
            return $this->whereFormA($id," and $table"."."."$idName <= ? ",$table);
        }

 




}
