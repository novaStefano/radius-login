<?php

trait qBuildSelectHelper{


  /**
   * Formatta la datatime in EU
   * @param  string $col   [description]
   * @param  string $alias [description]
   * @return [type]        [description]
   */
  public function selDateTime(string $col, string $alias = null)
  {
    return $this->selectDateTime($col, $alias);
  }

  public function selectDateTime(string $col, string $alias = null)
  {
    $res = $this->getUtility()->datetimeMy($col, $alias);
    return $this->select($res);
  }

  /**
   * Crea un bottone in tabella 
   */
  public function selButtonEvent($event, $idCol, $label, $ico = "", $color = "")
  {
    if (!empty($ico)) {
      $ico = "/$ico/";
    }
    if (!empty($color)) {
      $color = "$color/";
    }
    return $this->select("concat('$event(',$idCol,')/$label$ico$color' ) as '--btn-event' ");
  }

  /**
   * Attiva il checkbox sulla tabella
   */
  public function selCheckboxEvent($event, $idCol = "", $valCol = "", $label = "")
  {
    return $this->select("concat('$event(this,\"',ifnull($idCol,''),'\")/',ifnull($valCol,'')) as '$label--check-event' ");
  }

  /**
   * Attiva il bottone di apri dettaglio 
   */
  public function selOpenDetail($modulo = "", $col = "", $label = "", $blank = true)
  {
    if (empty($modulo)) {
      $modulo = $this->getTableName();
    }
    if (empty($col)) {
      $col = $modulo.".".$this->getIdName();
    }
    $ad = "";
    if ($blank) {
      $ad = "--blank";
    }

    return $this->select("concat('$modulo.',$col ) as '$label--open-detail$ad' ");
  }

  /**
   * Stato automatico con tanto di join e alias. Funziona con il from principale
   */
  public function selStato($alias = "stato")
  {
    $this->autoStato();
    return $this->labelStato($alias);
  }

  /**
   * Join automatica per il comune
   */
  public function selComune($alias = "comune")
  {
    $this->leftJoin("comune", false)->using("idComune");
    return $this->select(" comune.comune as '$alias' ");
  }

  public function labelStato($alias = "stato")
  {
    return $this->selLabel("stato.colore", "stato.stato", $alias);
  }

  /**
   * Buble colorata con la label
   */
  public function selLabel($color, $label, $alias = null)
  {
    if ($alias == null) {
      $alias = $label;
    }
    return $this->select("concat($color,'.',$label) as '$alias--label' ");
  }

  /**
   * Attiva il bold in tabella per la colonna
   */
  public function selBold($col, $alias = null)
  {
    if ($alias == null) {
      $alias = $col;
    }
    return $this->select("concat('<b>',$col,'</b>') as '$alias--html' ");
  }


  /**
   * Formatta la data in EU
   * @param  string $col   [description]
   * @param  string $alias [description]
   * @return [type]        [description]
   */
  public function selDate(string $col, string $alias = null)
  {
    return $this->selectDate($col, $alias);
  }

  public function selectDate(string $col, string $alias = null)
  {
    if(empty($alias)){
      $alias=$col;
    }
    $res = $this->getUtility()->dateMy($col, $alias);
    return $this->select($res);
  }

  /**
   * Bottone per elimina
   */
  public function btnDelete(string $colId, string $tab)
  {
    return $this->select(" concat($colId,'.$tab') as '--btn-delete' ");
  }



}
