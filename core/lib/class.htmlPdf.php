<?php

/* Classe per creare report PDF partendo da HTML
*/
use Spipu\Html2Pdf\Html2Pdf;

class HtmlPdf extends wrapperUtility
{
  private $htmlPdf=null; //Oggetto Html2Pdf
  private $html=""; //buffer HTML
  private $startBufferOb=false;
  private $downOption="D";
  private $iPage=0;
  private $font=null;
  private $reportDebug=false;
  private $modelReport=null;
  private $page_config = array('backtop' => '8mm', 'backright' => '10mm', 'backbottom' => '8mm', 'backleft' => '10mm');
  private $pageHeader="";
  private $pageFooter="";
  /**
  * Alloca e torna la libreria per generare i pdf
  * @return Html2Pdf [description]
  */
  private function getHtmlPdf():Html2Pdf
  {
    if (empty($this->htmlPdf)) {
      $this->htmlPdf = new Html2Pdf('P', 'A4', 'en', true, 'UTF-8');
    }
    // if(empty($this->font)){
    //   $this->setFont($this->getIni("report_font"));
    // }
    return $this->htmlPdf;
  }


  public function getModelDefault(){
    if(empty($this->modelReport)){
      $this->modelReport=$this->getUtility()->getModel("ReportDefault");
    }
    return $this->modelReport;
  }

  /**
   * Carica i css comuni del progetto / in customFrameWork css
   * @return caricamento css
   */
  public function loadGeneralCss(string $name="report"){
    $this->addHtml("<style>");
    $this->addHtml( $this->getUtility()->loadText($this->getUtility()->getPath("A_APP")."/view/report/css/$name.css"));
    $this->addHtml("</style>");
    $this->addHtml("\n\n\n");
  }

  /**
   * Carica i css del modulo
   * @param  [type] $css   [description]
   * @param  array  $param [description]
   * @return [type]        [description]
   */
  public function loadReportCss($css,array $param=[]){
    $this->addHtml($this->getUtility()->loadCss($css,$param));
  }

  /**
  * Include una view esterna per il report
  * @param  nome vista $nameView
  * @param  array $data parametri da passare alla view
  * @return void nessun ritorno
  */
  public function loadView(string $nameView,array $data=[],array $options=[])
  {
    $this->addHtml($this->getUtility()->loadView($nameView,$data,$options));
  }

  /**
  * Carica l'html generato da field, serve per il dettaglio automatico delle form
  * @return void, caricamento del field
  */
  public function loadFromField(){
    $html=$this->getUtility()->getField()->getPrintHtml();
    $this->addHtml($html);
  }

  /**
   * Metodo di rendering veloce per la query
   * @param  string $dataSet [description]
   * @return [type]          [description]
   */
  public function renderQueryField(string $fileName="renderQuery.pdf",string $dataSet=""){
    $this->loadGeneralCss();
    $this->loadQueryField($dataSet);
    $this->execute($fileName);
  }

  /**
   * Stampo il report con il risultato della query come label value
   * @param  string $dataSet dataset per la vista
   * @return bool          esito operazione
   */
  public function loadQueryField(string $dataSet=""){
    $field=$this->getUtility()->getField();
    $param=[];
    if(!empty($dataSet)){
      $param['dataset']=$dataSet;
    }
    $field->creatElementFromRow($param); //init del modulo prendendo i dati dalla query eseguita prima
    $field->forceAllColLabelValue();
    $field->setStartToken(false);
    $this->loadFromField();
    return true;
  }

  /**
  * accende il buffer su tutto il progetto
  */
  public function startBuffer(){
    $this->startBufferOb=true;
    ob_start();
  }


  /**
  * Spegne e restituisce il buffer
  */
  public function getBufferHtml(){
    if($this->startBufferOb){
      $this->startBufferOb=false;
      $this->addHtml(ob_get_contents());
    }

  }

  /**
  * Torna l'html in memoria
  * @return string [description]
  */
  private function getHtml():string
  {
    $this->getBufferHtml(); //nel caso sia aperto la lettura Buffer controllo se effettivamente la routine sia chiusa
    return $this->html;
  }


  /**
  * Aggiunge l'html
  * @param string $html html da aggiugnere alla variabile
  */
  public function addHtml(string $html):void
  {
    $this->html.=$html;
    return;
  }


  public function closePage()
  {
    if($this->iPage>0){
      $this->iPage--;
      $this->addHtml("</page>");
    }
  }

  public function setFooterInfo(){
    $def=$this->getModelDefault();
    $az=$def->getDataAzienda();
    $rag=$az['ragioneSociale'];
    $deno=$az['denominazione'];
    $piva=$az['partitaIva'];
    $mail=$az['email'];
    $html="<div class='footer'><b>$rag $deno</b> p.iva $piva <span style='color:blue; text-decoration: underline;'>$mail</span></div>";
    $this->pageFooter=$html;
  }

  public function getFooter():string{
    return $this->pageFooter;
  }

  public function setHeader(string $header){
    $this->pageHeader=$header;
  }

  public function getHeader(){
    return $this->pageHeader;
  }

  public function newPage($new_page_conf=[]):void
  {
    if ($this->iPage>0) {
      $this->closePage();
    }

    //aggiunta dei parametri in classe
    $config = '';
    $page_conf = ( !empty($new_page_conf) ) ? $new_page_conf : $this->page_config;
    foreach ($page_conf as $key => $value) {
        $config .= ' '.$key.'="'.$value.'" ';
    }

    $this->addHtml("<page $config>");
    $header=$this->getHeader();
    if (!empty($header)) {
      $this->addHtml("<page_header>");
      $this->addHtml($header);
      $this->addHtml("</page_header>");
    }
    $footer=$this->getFooter();
    if (!empty($footer)) {
      $this->addHtml("<page_footer>");
      $this->addHtml($footer);
      $this->addHtml("</page_footer>");
    }
    $this->iPage++;

  }

  /**
  * Imposta il report in modalità DEBUG
  */
  public function setReportDebug()
  {
    $this->reportDebug=true;
  }


  public function autoHeader($idCliente){
    $this->printIntestazioneAzienda();
    $this->printIntestazioneCliente($idCliente);
    $this->printLogo();
  }

  public function printIntestazioneAzienda($data=""){
    $this->loadGeneralCss("report_fiscali");
    $this->newPage();
    if(empty($data)){
      $data=$this->getModelDefault()->getDataAzienda();
    }
    $this->addHtml($this->getUtility()->loadViewG("report/intestazioneAzienda",$data));
  }


  public function printIntestazioneCliente($data){
    if(!is_array($data)){
      $data=$this->getModelDefault()->getDataCliente($data);
    }
    $this->addHtml($this->getUtility()->loadViewG("report/intestazioneCliente",$data));
  }

  public function getDefaultLogo(){
    return $this->getModelDefault()->getImgLogo();
  }

  public function getDefaultImgIntestazione(){
    return $this->getModelDefault()->getImg();
  }

  public function printLogo($img=""){
    if(empty($data)){
      $data['img']=$this->getDefaultImgIntestazione();
    }
    $this->addHtml($this->getUtility()->loadViewG("report/intestazioneLogo",$data));
  }

  public function setFont(string $font){
    $this->font=$font;
  }

  /**
  * Stampa il report effettivamente - renderizza il pdf
  * @return
  */
  public function execute(string $fileName="report.pdf"):bool
  {

    $this->closePage();
    if ($this->reportDebug) { //debug stampa l'html in buffer direttamente
      echo $this->getHtml();
      die;
    }
    try {
      $htmlPdf=$this->getHtmlPdf();
      $htmlPdf->writeHTML($this->getHtml());
      //bonifica nome
      $fileName=str_replace(".","",$fileName).".pdf";

      error_reporting(E_ERROR | E_PARSE); //disable warning per html2pdf

      $htmlPdf->output($fileName, $this->downOption);

      return true;
    } catch (HTML2PDF_exception $e) {
      $this->error($e);
      return false;
    }
  }
} //fine classe
