<?php

use eftec\bladeone\BladeOne;

function getBladeOne($views,$cache,$debug){
  if($debug){
    $ad=BladeOne::MODE_DEBUG;
  }else{
    $ad="";
  }
  $blade = new BladeOne($views,$cache,$ad); // MODE_DEBUG allows to pinpoint troubles.
  return $blade;
}


?>
