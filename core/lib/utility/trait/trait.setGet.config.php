<?php

/** METODI DI RITORNO DEI PARAMETRI INI **/

trait iniSetGet
{


  /**
  * Torna la key standard del proetto per la
  */
  public function getSecretKey():string{
    return $this->secretKey;
  }

  /**
  * Torna le sezioni del progetto
  * @return array [description]
  */
  public function getProjectSections():array
  {
    return $this->projectSections;
  }


  /**
  * Controlla se un record aperto da due utenti contemporaneamente è modificabile
  */
  public function ifLockRecord(){
    return $this->lockrecord;
  }

  /**
  * Torna l'ini del timeout sul lock del record
  * @return
  */
  public function getTimeoutLock(){
    return $this->lockTimeout;
  }

  public function getDeleteMode():string{
    return $this->deleteMode;
  }

  /**
  * Controllo se il record è in hard mode o soft mode per il delete
  * @return bool [description]
  */
  public function isHardDelete():bool{
    if($this->deleteMode=="hard"){
      return true;
    }else{
      return false;
    }
  }


  /**
  * Controllo se il record è in hard mode o soft mode per il delete
  * @return bool [description]
  */
  public function isLogUserUpdated():bool{
    return $this->logUserUpdated;

  }




  /**
  * Imposto i css caricati da ini
  */
  public function rootCss()
  {
    $primaryColor=$this->primaryColor; ?>
    <style>
    :root{
      --top-background: <?= $primaryColor ?>;
      --button-background: <?= $primaryColor ?>;
      --input-group-addon-background: <?= $primaryColor ?>;
    }
    .loader{
      display:block;
    }
    </style>
    <?php
  }


  /**
  * Prende i parametri da ini
  * @param  [type] $param [description]
  * @return [type]        [description]
  */
  public function getIni($param){
    if(!isset($this->$param)){
      return false;
    }
    return $this->$param;
  }



  /**
  * js di inizializzaione
  * @return [type] [description]
  */
  public function loadInitScript(bool $login=true){
    $title=$this->getIni('titleProject');
    $description=$this->getIni('description');
    ?>
    <script>
    $('meta[name=description]').remove();
    $('head').append( '<meta name="<?=$description ?>" >' );
    <?php if($login){ ?>
      $('#userNameUi').html('<?= $this->getUsername(); ?>');
    <?php } ?>
    document.title='<?=$title?>';
    </script>

      <?php
    }




  }



  ?>
