<?php

/**
 * Gestione delle form attraverso le sessioni
 */

class SessionForm extends wrapperUtility
{
    private $curSection = ""; //sezione di interrogazione
    private $loadedSessionForm = null; //ho già caricato la sessionForm per questa istanza?
    private $baseIndex = "index_form_session"; //index di base che contiene tutti i dati dell'indice session
    /**
     * Imposta l'indece di sessione per la form, ogni apertura form ha il proprio indice dedicato
     */
    public function getIndexSessionForm()
    {

        if (isset($this->loadedSessionForm)) { //devo caricarlo solo con la prima tabella del caricamento form, le altre sono uguali
            return $this->loadedSessionForm;
        }


        if (!empty($this->post("indexSession"))) { //ho una chiamata con index marcato, carico quell'index per la sessione
            $this->loadedSessionForm = $this->post("indexSession");
            return $this->loadedSessionForm;
        }

        $index = $this->getSession("index_form_session"); //estraggo il nuvo numero
        if (empty($index)) {
            $index = 1;
        } else {
            $index++;
        }
        $this->setSession("index_form_session", $index); //agiorno la nuova index
        $this->loadedSessionForm = $index;

        $this->getUtility()->getField()->addScript("proto.activeModalSessionForm('$index');"); //registro la sessione nel frontend per richiamre le chiamate successive
        return $index;
    }

    /**
     * setter per la sezione, posso impostare modalForm come prefisso
     */
    public function setSection(string $section)
    {
        return "modalForm";
        $this->curSection = $section;
    }


    /**
     * Getter section / posso impostare modalForm ad esempio come prefisso
     */
    public function getSection()
    {
        return $this->curSection;
    }


    /**
     * Torna la radice indice da registrare che contine tutti i dati della form
     */
    public function getIndexName()
    {
        $index = $this->getIndexSessionForm();
        $name = $this->baseIndex . "_" . $index;
        return $name;
    }

    /**
     * Prende i dati dalla sessione in base alla chiave richiesta
     */
    public function getData($form = "", $key = "")
    {

        $name = $this->getIndexName();
        $sessionData = $this->getSession($name);

        if (empty($form)) {
            return $sessionData;
        }
        $form = $this->getSection() . $form;

        if (empty($key)) {
            if (empty($sessionData[$form])) {
                return false;
            }
            return $sessionData[$form];
        }
        if (empty($sessionData[$form][$key])) {
            return [];
        }
        return $sessionData[$form][$key];
    }

    /**
     * Imposta i dati dalla sessione in base alla chiave
     */
    public function setData($form, $key, $dataSet = null)
    {
        $section = $this->getSection();
        $form = $section . $form;

        $data = $this->getData(); //recupero sempre il totale dei dati
        if (!isset($dataSet)) { //chiave valore semplice
            $data[$form] = $key;
        } else {

            if (!isset($data[$form])) {
                $data[$form] = [];
            }
            if (!isset($data[$form][$key])) {
                $data[$form][$key] = [];
            }
            $data[$form][$key] = $dataSet;
        }
        $name = $this->getIndexName();
        $this->setSession($name, $data);
    }

    /**
     * Sovrascrive i dati da una form nel caso sia il primo caricamento
     */
    public function loadDataFromSession($id)
    {
        $data = $this->getData($this->post("sezione"), "form");
        if (!empty($data[$id])) {
            $field = $this->getUtility()->getField();
            foreach ($data[$id] as $key => $val) {
                if (isset($field->arParam[$key])) {
                    $field->setValore($key, $val['val']);
                }
            }
        }
    }


    /**
     * Torna il nuovo indice per l'insert
     */
    public function getNewIndexInsert()
    {
        $ind = $this->getData("indexInsert");
        if (empty($ind)) {
            $ind = 0;
        }
        $ind++;
        $this->setData("indexInsert", $ind);
        return "n" . $ind;
    }


    public function updateDataFromDataset($sezione, $session, $idModal = null)
    {
        foreach ($session as $id => &$sess) {
            if (!is_array($sess)) {
                $sess = array("val" => $sess, "type" => "string", "id" => $id);
            }
        }
        $this->updateDataOnSession($sezione, $session, $idModal);
    }

    public function updateDataOnSession($sezione, $session, $idModal = "")
    {

        $form = $this->getData($sezione, "form");

        if (empty($idModal)) {
            $idModal = $this->getNewIndexInsert();
        }

        $form[$idModal] = $session;
        $this->setData($sezione, "form", $form);
        $update = $this->getData($sezione, "update");
        if (!isset($update[$idModal])) { //segno i dati aggiornati
            $update[$idModal] = $idModal;
            $this->setData($sezione, "update", $update);
        }
        $this->refreshTableData($sezione, $session, $idModal);
    }

    /**
     * Aggiorna i dati presi dalla form / evento standard collegata alla form
     */
    public function updateSession($sezione, $idModal, $forms)
    {

        $modulo = $this->getUtility()->getCurModulo();
        $this->getUtility()->packSubData($addData, $modulo);

        if (empty($idModal)) {
            $idModal = $this->getNewIndexInsert();
        }

        foreach ($forms as $key => $form) {
            if (empty($form['data'])) {
                continue; //token di servizio tipo inline
            }
            $dataForm = $form['data']; //prendo solo i dati del form
            if (!empty($addData)) {
                $dataForm = array_merge($dataForm, $addData); //posso addizzionare dei dati nel caso è una sub Form o c'è qualche chiamata custom
            }

            // $session['section'] = $sezione;
            // $session['table'] = $key;
            // $session['data'] = $dataForm;
            // $form = $this->getData($sezione, "form");
            // $form[$idModal] = $session;
            $this->getUtility()->savingModule = $key;
            // $this->setData($sezione, "form", $form);
            $this->updateDataOnSession($sezione, $dataForm, $idModal);

            // $update = $this->getData($sezione, "update");
            // if (!isset($update[$idModal])) { //segno i dati aggiornati
            // $update[$idModal] = $idModal;
            // $this->setData($sezione, "update", $update);
            // }

            $this->json("newId", $idModal);
        }
        return true;
    }


    /**
     * Elimina un record dalla sessione
     */
    public function delete($sezione, $id)
    {
        $form = $this->getData($sezione, "form");
        unset($form[$id]);
        $this->setData($sezione, "form", $form);

        $delete = $this->getData($sezione, "delete");
        $delete[$id] = "1";
        $this->setData($sezione, "delete", $delete);

        $table = $this->getData($sezione, "table");
        unset($table[$id]);
        $this->setData($sezione, "table", $table);
        return true;
    }

    /**
     * Inizializza la sessione con i dati della tabella
     */
    public function initSessionTab($ds, $table, $col, $tableName)
    {

        $sort = [];

        foreach ($table as $tab) {
            $sort[$tab['id']] = $tab;
        }

        $this->setData($ds, "table", $sort);
        $this->setData($ds, "col", $col);
        $this->setData($ds, "section", $ds);
        $this->setData($ds, "tableName", $tableName);


        $dsactive = $ds;
        $res = $this->getData("activeForm");
        if (empty($res)) {
            $res = [];
        }
        array_push($res, $dsactive);
        $this->setData("activeForm", $res);
    }

    private function translateColTable($cols, &$session)
    {   

        foreach ($session as $col => $val) {
            if(in_array($col,$cols)){
                continue;
            }
            
            if($val['type']=='date'){
                $val['val']=$this->getUtility()->dateFormatEu($val['val']);
            }

            $isId=false;
            $colConv = $col;
            if (substr($col, 0, 2) == "id") {
                $colConv = substr($col, 2);
                $isId=true;
            }
            $se = strtolower($colConv);
            $seCol = strtolower($col);
            $find = false;
            foreach ($cols as $c) { //corrispondenza tra tabella e form? 
                $low = strtolower($c);
                $low=str_replace(" ","",$low);
                if ($low == $se || $seCol == $low) {
                    $find = true;
                    break;
                }
            }

            if ($find) { //dato presente in tabella, lo cerco
                $aliasVal=$val['val'];
                if($isId){
                $model = $this->getModel($colConv);
                $model->dsSearchGet($val['val']);
                $data = $this->getUtility()->fetchArray();
                $aliasVal=$data[1];
                }
                $session[$c] = array("val" => $aliasVal, "type" => "string", "id" => $c);
            }
        }
    }

    public function refreshTableData($sezione, $session, $id)
    {
        $table = $this->getData($sezione, "table");
        $cols = $this->getData($sezione, "col");
        $this->translateColTable($cols, $session);

        if (!isset($table[$id])) {
            $tab = [];
            //inserimento del record nella sessione tabella
            foreach ($cols as $col) {
                if ($col == "id") {
                    $tab['id'] = $id;
                    continue;
                }
                if (isset($session[$col]['val'])) {
                    $tab[$col] = $session[$col]['val'];
                } else {
                    $tab[$col] = "";
                }
            }
        } else {
            //update del record
            $tab = $table[$id];
            foreach ($session as $col => $val) {

                if (isset($tab[$col])) {
                    $tab[$col] = $val['val'];
                }
            }
        }
        $table[$id] = $tab;
        $this->setData($sezione, "table", $table);
    }


    public function getAllFormData()
    {
        $activeForm = $this->getData("activeForm");
        if (empty($activeForm)) {
            return false;
        }
        $res = [];
        foreach ($activeForm as $act) {
            array_push($res, $this->getData($act));
        }
        return $res;
    }


    public function updateDone($sezione, $id, $newId)
    {
        $update = $this->getData($sezione, "update");
        unset($update[$id]);
        $this->setData($sezione, "update", $update);

        if ($id != $newId) {
            $form = $this->getData($sezione, "form");
            $form[$newId] = $form[$id];
            unset($form[$id]);
            $form = $this->setData($sezione, "form", $form);
            $table = $this->getData($sezione, "table");
            $table[$newId] = $table[$id];
            $table[$newId]["id"] = $newId;
            unset($table[$id]);
            $this->setData($sezione, "table", $table);
        }
    }

    public function unsetDelete($sezione)
    {
        $this->setData($sezione, "delete", []);
    }
}
