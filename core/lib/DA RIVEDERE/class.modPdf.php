<?php

/**
 * Classe per la gestione di tutti i dati del form pdf
 */
class pdfField
{
    public $name="";
    public $value="";
    public $type="";

    public function __construct(string $name, $value, string $type="")
    {
        $this->name=$name;
        $this->value=$value;
        $this->type=$type; //se voglio validare anche il tipo potrebbe essere utile
    }
}


/**
* Classe per la manipolazione dei pdf
*/
class modPdf
{
    private $cmd="pdftk";
    private $openPdf="";
    private $tmpFdfd="";
    public $fieldRead=[]; // campi letti dal pdf
    private $loadDataForm=false;

    /**
    * Esegue la chiamata wrapper a pdftk con il pdf aperto
    * @param  string $cmd [description]
    * @return [type]      [description]
    */
    private function execCmd(string $cmd)
    {
        if ($this->ifOpenPdf()==false) {
            return false;
        }
        $exec=$this->cmd." ".$this->getPdf()." ".$cmd;


        exec($exec, $ret);
        return $ret;
    }


    /**
    * Controlla l'esistenza del file e lo tiene in memoria
    */
    public function openPdf(string $path):string
    {
        if (file_exists($path)) {
            $this->openPdf=$path;
            return true;
        } else {
            return false;
        }
    }

    /**
    * Torna il pdf aperto
    * @return string path del dpfs
    */
    public function getPdf():string
    {
        return $this->openPdf;
    }

    /**
    * Controlal se c'è un pdf aperto
    * @return bool
    */
    public function ifOpenPdf():bool
    {
        if (empty($this->getPdf())) {
            return false;
        } else {
            return true;
        }
    }


    /**
    * Ritorno della directory di temp per gli fdfd
    * @return string [description]
    */
    private function getTmpFdfd():string
    {
        if (empty($this->tmpFdfd)) {
            $this->tmpFdfd=date('ymd-Fdf')."/";
        }
        return $this->tmpFdfd;
    }

    /**
    * Crea la directory fdf per il form fillable
    */
    private function generateTmpFdf()
    {
        $tmp=$this->getTmpFdfd();
        if ((file_exists($tmp))&&(is_dir($tmp))) {
            return;
        } else {
            mkdir($tmp);
            return;
        }
    }

    /**
    * Salva il file fdf per i form
    * @param  string $content [description]
    * @return [type]          [description]
    */
    private function makeFdf(string $content, string $name)
    {
        $tmp=$this->getTmpFdfd();
        $this->generateTmpFdf();
        file_put_contents($tmp.$name, $content);
    }

    private function getDataProcRow(array &$res, string $row)
    {
        $cerca=["FieldType","FieldValue","FieldName"];
        foreach ($cerca as $find) {
            $pos=strpos($row, $find.":");
            if ($pos>-1) {
                // echo "aggiunto--->".$find." $pos - ".$row."\n\n\n";
                $row=str_replace($find.": ", "", $row);
                $res[$find]=$row;
                return;
            }
        }
    }


    private function getDataNewField(array $arr)
    {
        if (empty($arr['FieldValue'])) {
            $val="";
        } else {
            $val=$arr['FieldValue'];
        }
        $field=new pdfField($arr['FieldName'], $val, $arr['FieldType']);
        $this->fieldRead[$arr['FieldName']]= $field;
        $field=[];
    }

    /**
    * Legge i dati del form  e ritorna i dati
    * @return false / array
    */
    public function getDataPdf():bool
    {
        if (!$this->ifOpenPdf()) {
            $this->loadDataForm=false;
            return false; //nessun pdf
        }
        $ret=($this->execCmd("dump_data_fields"));
        $field=[];
        $first=true;
        foreach ($ret as $row) {
            if ($row=="---") { //carattere di separazione di pdftk
                if ($first==false) {
                    $this->getDataNewField($field);
                } else {
                    $first=false;
                }
            } else {
                $this->getDataProcRow($field, $row);
            }
        }

        $this->getDataNewField($field);
        $this->loadDataForm=true;
        // $this->dumpPdf();
        return true;
    }

    public function dumpPdf()
    {
        foreach ($this->fieldRead as $key => $value) {
            echo "***** $key*******\n";
            print_r($value);
            echo "\n\n\n";
        }
    }

    private function ifLoadDataForm():bool
    {
        return $this->loadDataForm;
    }

    public function getFieldValue(string $field)
    {
        if ($this->ifLoadDataForm()) {
            if (empty($this->getFieldRead()[$field])) {
                return false;
            }
            return $this->getFieldRead()[$field]->value;
        } else {
            return false;
        }
    }

    private function getFieldRead()
    {
        return $this->fieldRead;
    }


    public function getFieldAsHash():array
    {
        $ret=[];
        foreach ($this->getFieldRead() as $key => $value) {
            $ret[$key]=$value->value;
        }
        return $ret;
    }


    private function validateData(string $field, string $val):bool
    {
        $res=$this->getFieldValue($field);
        if (!isset($res)) {
            return false;
        } else {
            return true;
        }
    }


    /**
    * Valorizza il pdf caricato con i valori chiesti
    * @param  array $data array key value da utilizzare
    * @return bool        esito operazione
    */
    public function fillPdf(array $data):bool
    {
        if (!$this->ifOpenPdf()) {
            return false;
        }
        if (!$this->getDataPdf()) {
            return false;
        }
        ob_start();
        $this->headerFdf();
        $notFind=false;
        foreach ($data as $field => $val) {
            if (!$this->validateData($field, $val)) {
                $notFind=true;
                $addNot=$field;
            }
            $this->valueFdf($field, $val);
        }

        if ($notFind) {
            $bufout=ob_get_contents();
            ob_end_clean();
            echo "not find ---> ".$addNot;
            die;
        }
        $this->footerFdf();
        $bufout=ob_get_contents();
        ob_end_clean();
        $name=date("his");
        $this->makeFdf($bufout, $name);
        $path=$this->getTmpFdfd().$name;
        $fdf=$path."fdf";
        $out=$path.".pdf";
        $this->execCmd(" fill_form $fdf output $out ");
        return true;
    }



    private function valueFdf(string $field, string $val)
    {
        ?>
        << /V (<?= $val ?>)/T (<?=$field ?>)>><?php
    }

    private function headerFdf()
    {
        ?>
        %FDF-1.2
        %âãÏÓ
        1 0 obj
        <<
        /FDF << /Fields [<?php
    }

    private function footerFdf()
    {
        ?>
        ] /ID [ <>
        ] >>
        >>
        endobj
        trailer
        <<
        /Root 1 0 R

        >>
        %%EOF<?php
    }
} //fine classe

// $pdf=new modPdf();
// $pdf->openPdf("./bif.pdf");
// $arr['cognome']="hackerino";
// $arr['nome']="topolino con il nome lunghissimosismsdaosdmasdasdasdasdasdasdas! ' e simbolo di batman ~ '";
//
// $pdf->fillPdf($arr);
// print_r($pdf->getFieldAsHash());
