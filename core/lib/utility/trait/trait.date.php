<?php


trait DateUtil {


  public function getCurDate(){
    $now=date("Y-m-d H:i:s");
    return $now;
  }

  public function isDateMajorNow(string $date){
    $now=date("Y-m-d H:i:s");
    if($date>$now){
      return true;
    }else{
      return false;
    }
  }


  public function dateDiffNow($data,$format="s"){
    return $this->dateDiff($data,date("Y-m-d H:i:s"),$format);
  }

  public function dateDiff($dt_menor , $dt_maior , $str_interval = 's' )
{

    try{
    $relative=false;

     if( is_string( $dt_menor)) $dt_menor = date_create( $dt_menor);
     if( is_string( $dt_maior)) $dt_maior = date_create( $dt_maior);

     $diff = date_diff( $dt_menor, $dt_maior, ! $relative);

     switch( $str_interval){
         case "y":
             $total = $diff->y + $diff->m / 12 + $diff->d / 365.25;
             break;
         case "m":
             $total= $diff->y * 12 + $diff->m + $diff->d/30 + $diff->h / 24;
             break;
         case "d":
             $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24 + $diff->i / 60;
             break;
         case "h":
             $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + $diff->i/60;
             break;
         case "i":
             $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + $diff->s/60;
             break;
         case "s":
             $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i)*60 + $diff->s;
             break;
          default:
              $this->error("FORMATO NON VALIDO");
            break;
        }

     // if( $diff->invert)
     //         return -1 * $total;
     // else

       return $total;

  }catch(Exception $err){
    echo $err->getMessage();
    $this->warning("Errore durante un time differenze");
    return false;
  }
}


  /**
  * Controllo se la stringa è data in eu o US
  * @param  string $date data da controllare
  * @return bool         esito controllo
  */
  public function isDate(string $date=""):bool{
    if(strlen($date)<6){
      return false;
    }
    if(empty($date)){
      return false;
    }
    return (bool)strtotime($date);
  }


  /**
  * Controlla se la data è una datetime
  * @param  string $datetime [description]
  * @return bool             [description]
  */
  public function isDatetime(string $datetime):bool{
    if (DateTime::createFromFormat('Y-m-d H:i:s', $datetime) !== FALSE) {
      return true;
    }
    if (DateTime::createFromFormat('d/m/Y H:i:s', $datetime) !== FALSE) {
      return true;
    }
    return false;
  }



  /**
  * Controlla se la data è nel formato europeo
  * @param  string $date Data da controllare
  * @return bool
  */
  public function checkDateEu(string $date):bool
  {
    if (empty($date)) {
      return false;
    }
    if (strlen($date)<10) {
      return false;
    }

    $subDate=substr($date, 4);
    if (strpos($subDate, "/")) { //se nei primi 4 caratteri c'è una barra è sicuramente una data europea
      return true;
    }
    return false;
  }

  /**
  * Formatta la data nel formato europeo
  * @param  string data da formattare
  * @return string data formattata
  */
  public function dateFormatEu(string $date=""):string
  {
    if (empty($date)) {
      return "";
    }

    if (strlen($date)<10) {
      $this->warning("DATE format EU", "Attenzione data $date invalida");
      return "";
    }
    if ($this->checkDateEu($date)) {
      return $date;
    }
    $d = new DateTime($date, new DateTimeZone('Pacific/Nauru'));
    return $d->format('d/m/Y');
  }


  public function findFormatDate(string $date){
    if (preg_match("/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/", $date)){
      return "d/m/Y";
    }
    return false;

  }


  /**
  * Formatta da Eu a US
  * @param  string $date
  * @return string stringa formattata
  */
  public function dateFormatUs(string $date="",string $format=""):string
  {
    if (empty($date)) {
      return "";
    }

    if(empty($format)){
      $format=$this->findFormatDate($date);
    }


    try {
      if(empty($format)){
        $ndate = new DateTime($date);
      }else{
        $ndate = DateTime::createFromFormat($format,$date);

      }
    } catch (Exception $e) {
      $this->warning("Format bind date $date ",$e->getMessage());
      return false;
    }

    return $ndate->format('Y-m-d');
    //
    // if (!$this->checkDateEu($date)) {
    //   return $date;
    // }
    // $date=str_replace("/", "-", $date);
    // $d = new DateTime($date, new DateTimeZone('Europe/Rome'));
    // return $d->format('Y-m-d');
  }



  /**
  * Formatta da Eu a US
  * @param  string $date
  * @return string stringa formattata
  */
  public function datetimeFormatUs(string $date="",string $format=""):string
  {
    if (empty($date)) {
      return "";
    }

    if(empty($format)){
      $format=$this->findFormatDate($date);
    }


    try {
      if(empty($format)){
        $ndate = new DateTime($date);
      }else{
        $ndate = DateTime::createFromFormat($format,$date);

      }
    } catch (Exception $e) {
      $this->warning("Format bind date $date ",$e->getMessage());
      return false;
    }

    $date= $ndate->format('Y-m-d H:i').":000";
    return $date;
    //
    // if (!$this->checkDateEu($date)) {
    //   return $date;
    // }
    // $date=str_replace("/", "-", $date);
    // $d = new DateTime($date, new DateTimeZone('Europe/Rome'));
    // return $d->format('Y-m-d');
  }

  public function getMeseIta($mese):string{
    $this->startPrepare("SELECT mese from mese where idMese= :idMese ");
    $this->bindP("idMese",$mese);
    $this->executePrepare();
    $mese=$this->fetch()['mese'];
    return $mese;
  }


  public function addDayToNow(int $days=1){
    return Date('Y-m-d', strtotime('+'.$days.' days'));

  }

  public function addDate(string $startDate,int $add=1,$type){
    return Date('Y-m-d', strtotime($startDate." +".$add.' '.$type,));
  }


}

?>
