<?php

/**
* Trait per i modelli dei campi - i campi prendono di default le azioni impostate qui
*/
trait modelli
{

    /**
    * definisco la class di default per Tipologia a seconda del profilo di lavorazione
    * @param string $param [description]
    */
    public function BsetInputCss(&$param=[])
    {
        if (empty($param['id'])) {
            $id="";
        } else {
            $id=$param['id'];
        }

        if (!empty($this->customClass)) {
            $class=$this->customClass;
            $salta=true;
        } else {
            $salta=false;
        }
        if (!$salta) {
            $curPro=$this->getSezione();
            $class=" $curPro ";
            // switch ($curPro) {
            //     case 'cerca':
            //     $class=" cerca ";
            //     break;
            //
            //     case 'subForm':
            //     $class = ' subDettaglio ';
            //     break;
            //
            //     case 'insVel':
            //     $class = ' insVel  ';
            //     break;
            //
            //     default:
            //     $curPro="dettaglio";
            //     $class = " dettaglio ";
            //     break;
            // }
            $class.=" $id-$curPro ";
        }


        $class.=" form-control  ";
        if (!empty($param['table'])) {
            $table=$param['table'];
            $class.=$table." ".$id." ".$id."-".$table;
        }
        if ((!empty($param['required']))&&($param['required']=='1')) {
            $class.=" required ";
        }

        if(!empty($param['bindForm'])){
            $bindForm=$param['bindForm'];
            if(!is_array($param['bindForm'])){
                $bindForm=array($bindForm);
            }
            foreach($bindForm as $table){
                $class.=$table."  ".$id."-".$table;
            }
        }

        $param['class']=$class;
        return $class;
    }

    /**
    * Css standard dell'input
    */
    public function BinputModel(array $param=[])
    { //definisce i parametri di default delle classi
        $this->BsetInputCss($param);
        if (empty($param['autoDiv'])) {
            $param['autoDiv']=true;
        }

        if (empty($param['paramAutoDiv']['adClass'])) {
            $param['paramAutoDiv']['adClass']= "indDiv col-md-4 " ;
        }
        return $param;
    }


    public function BLabelValueModel()
    { //definisce i parametri di default delle classi
        $this->BsetInputCss($param);
        if (empty($param['autoDiv'])) {
            $param['autoDiv']=true;
        }

        if (empty($param['paramAutoDiv']['adClass'])) {
            $param['paramAutoDiv']['adClass']= "indDiv col-md-4 " ;
        }
        // $param['class']=" spanLabelValue ";
        // $param['noGroup']='1';
        // $param['autoDiv'] = '1'; //ogni elemeno ha il suo div di default
        return $param;
    }



    public function BbuttonModel()
    { //definisce i parametri di default delle classi
        $param['class'] = '  btn btn-lg btn-primary posBtn';
        return $param;
    }
    //
    // public function BselectModel()
    // { //definisce i parametri di default delle classi
    //     $curProfilo=$this->getProfile();
    //     switch ($curProfilo) {
    //         case 'subForm':
    //         $param['class'] = ' niceLook btn insSub modSub ';
    //         break;
    //         case 'insVel':
    //         $param['class']='niceLook btn insVel insSub ';
    //         break;
    //
    //         default:
    //         $param['class'] = '  niceLook ins mod btn ';
    //         break;
    //     }
    //     $param['autoDiv'] = '1'; //ogni elemeno ha il suo div di default
    //     return $param;
    // }

    public function BMultiModel()
    { //definisce i parametri di default dei campi dinamici
        $param['class'] = '   form-control insMulti  ';
        $param['autoDiv'] = '1'; //ogni elemeno ha il suo div di default

        return $param;
    }

    public function BtextModel(array $param=[])
    {
        $param['type']="text";
        if (empty($param['paramAutoDiv']['adClass'])) {
            $param['paramAutoDiv']['adClass']=" col-md-12 ";
        }
        $this->BsetInputCss($param);
        $param['autoDiv'] = true; //ogni elemeno ha il suo div di default
        return $param;
    }

	/**
	 * Crea l'hmlt standard del bottone con le fa awesome
	 * @param  string $valBtn Label del bottone
	 * @param  string $icon   classe dell'icona
	 * @return string         elemento generato
	 */
	public function getButtonHtml(string $valBtn,string $icon):string{
		return $this->getButtonIco("fa-".$icon)." ".$valBtn;
	}

	/**
	 * Icona standard del pulsante
	 * @param  string fa-icon da mettere nella i
	 * @return string i con le classi corrette
	 */
    public function getButtonIco(string $icona):string
    { //definisce l'icona del pulsante
        if(strpos($icona,"fa-")===false){
          $icona="fa-".$icona;
        }
        return " <i class=\"fa $icona\"></i> ";
    }

    public function getLabelClass()
    {
        return " class='control-label form-label' ";
    }



    private function inputGroup($ico, $event = "",array $options=[])
    { //definisce l'input group
        if ($this->getWork('noGroup') ) {
            return "";
        }

        $preHtml=$this->getWork('preHtml');
        $fineHtml=$this->getWork('fineHtml');

        if(isset($options['class'])){
            $adClass=$options['class'];
        }else{
            $adClass="";
        }


        if(empty($options['defaultClass'])){
          $class=" input-group-addon ";
        }else{
          $class=$options['defaultClass'];
        }

        if(empty($event)){
          $event=" onclick='groupFocus(this);'  ";
        }

        $this->paramWork['preHtml'] = $preHtml.'<div class=" input-group  '.$adClass.' ">
        <div class=" '.$class.' " ' . $event . '  >
        <i class="fa ' . $ico . '"></i>
        </div>';
        $this->paramWork['fineHtml'] = "</div>".$fineHtml;

        $this->paramWork['noGroup']=1;
    }



    private function autoRegex($regex){

      if(!empty($this->getWork("regex"))){
        return 0;
      }

      $this->setWork("regex",$regex);
    }

    //valorizza in automatico il raggruppamento in base al nome
    private function groupByName()
    {
        if ($this->getWork('noGroup') == '1') {
            return "";
        }



        $exclude=['hidden','id','checkbox'];

        if (in_array($this->getWork('type'),$exclude )) {
            return "";
        }
        $nome = $this->getWork('id');
        $type=strtolower($this->getWork('type'));
        $nome=strtolower($nome);

        $icon=$this->getWork("fa-icon");
        if($icon){
          $this->inputGroup(' fa-'.$icon);
          return 0;
        }

        switch ($nome) {

            case 'phone':
            case 'cellulare':
            case 'telefono':
            $this->eleTelefono();
            $this->autoRegex("phone");
            return 0;

            break;
            
            case 'ragionesociale':
                $this->inputGroup("fa-file-signature");

                break;


            case 'codice':
            case 'sigla':
            case 'code':
                $this->inputGroup("fa-code");
                break;



            case 'e_mail':
            case 'email';
            case 'mail':
            $this->inputGroup(' fa-envelope', 'onclick=groupMail(this);');
            $this->autoRegex("mail");
            return 0;

            break;


            case 'prezzo':
            case 'costo':
            case 'spese':
            case 'importo':
            case 'tariffa':
                $this->inputGroup(' fa-euro-sign');
                return 0;

                break;

                case 'nome':
                case 'firstname':
                case 'lastname':
                case 'cognome':
                case 'user':
                $this->inputGroup(' fa-user');
                return 0;
                break;

                case 'data':
                case 'mese':
                case 'anno':
                $this->inputGroup(' fa-calendar');
                return 0;

                break;

                case 'datanascita':
                $this->inputGroup(' fa-calendar-day');
                return 0;

                break;


                case 'stato':
                $this->inputGroup(' fa-toggle-on');
                return 0;

                break;

                case 'qt':
                case 'number':
                case 'numero':
                case 'quantita':
                case 'quantità':
                $this->inputGroup(' fa-sort-numeric-up ');
                return 0;
                break;

                case 'codicefiscale':
                case 'codice_fiscale':
                case 'cod_fis':
                case 'codfis':
                case 'id-card':
                $this->inputGroup(' fa-id-card ');
                $this->autoRegex("codiceFiscale");

                return 0;
                break;


                case 'piva':
                case 'partitaiva':
                $this->inputGroup(" fa-passport");
                $this->autoRegex("partitaIva");
                return 0;

                break;

                case 'idComune':
                    $this->inputGroup(' fa-city', 'onclick=startSearch(this);');
                    break;

                case 'civico':
                case 'frazione':
                case 'regione':
                case 'provincia':
                case 'idprovincia':
                case 'cap':
                case 'comune':
                case 'indirizzo':
                $this->inputGroup(' fa-map-marked');
                return 0;
                break;

               
            }//fine case





        switch ($type){

            case 'link':
            $this->inputGroup(' fa-external-link-alt');
            break;
  
  
            case 'select':
              $this->inputGroup(" fa-database ");
              break;
                

         
  
              case 'search':
              $this->inputGroup(' fa-external-link-square-alt', 'onclick=startSearch(this);');
              break;
            

              case 'mail':
                $this->inputGroup(' fa-envelope', 'onclick=groupMail(this);');
                $this->autoRegex("mail");   
                break;

  
            case 'color':
            $this->inputGroup(' fa-brush');
            break;
  
  
            case 'date';
            $this->inputGroup(' fa-calendar');
            break;
  
  
            case 'password':
            $this->inputGroup(' fa-lock');
            return 0;
            break;
  
            case 'file':
            $this->inputGroup(' fa-file');
            return 0;
            break;
            case 'phone':
            case 'telephone':
            $this->eleTelefono();
            return 0;
            break;
  
            default:
            $this->groupByNameCustom($nome,$type);
            break;
            
          }



    }
}

    //fine treats
