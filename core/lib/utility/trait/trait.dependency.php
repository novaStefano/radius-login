<?php


/**
 * Trait con la lista di dipendeze di moduli già pronti
 */
trait Dependency
{
  private $dependency = []; //lista di dipendenze già caricate in array
  private $vendorAutoLoad = false; //flag per verifiare se ho già eseguito un autoload Composer su Vendor
  public $debugModel = true;

  /**
   * Metodo per l'auto inclusione dei moduli durante una chiamata
   * @return [type] [description]
   */
  public function autoLoadInc()
  {
    if (!$this->stopAutoload) {
      $lib = $this->getPath('A_EXTENDS') . 'class.wrapperUtility.php';
      include_once $lib;
    }
  }


  public function checkPathModel()
  {
  }



  public function getPathIncludeModel($dir, $modulo, $name, &$modelFind, $option)
  {
    $modelFind = false;
    //primo passo cercare il model sulla path relativa come override
    if (!(isset($option['relativePath']) && $option['relativePath'] == false)) {
      $fileModel = "model/model.$name.php";
      if (file_exists($fileModel)) {
        $modelFind = true;
        return $fileModel;
      }
    }



    //terzo tentativo cerco nel modulo attivo in questo momento
    $p = $this->getPath("A_APP");
    if (empty($dir)) {
      $dir = $this->getCurWorkDir();
      if (empty($dir)) {
        $dir = "Moduli";
      }
      $dir .= "/";
    }
    $fileModel = $p . $dir . $this->getCurModulo() . "/model/model.$name.php";
    if (file_exists($fileModel)) {
      $modelFind = true;
      return $fileModel;
    }


    //secondo tentativo sulla modulo del nome model
    $p = $this->getPath("A_APP");
    if (empty($dir)) {
      $dir = "Moduli/";
    }
    $fileModel = $p . $dir . "$modulo/model/model.$name.php";
    if (file_exists($fileModel)) {
      $modelFind = true;
      return $fileModel;
    }



    $fileModel = $p . $dir . $name . "/model/model.$name.php";
    if (file_exists($fileModel)) {
      $modelFind = true;
      return $fileModel;
    }



    $fileModel = $p . "model/model.$name.php";
    if (file_exists($fileModel)) {
      $modelFind = true;
      return $fileModel;
    }


    return "";
  }

  private function checkClassModel(&$modelName)
  {
    if (!class_exists($modelName)) {
      $nName = $modelName;
      $nName = str_replace('Model', '', $nName);
      $nName .= "_Model";
      if (class_exists($nName)) {
        $modelName = $nName;
        return;
      }
    }
  }

  /**
   * Inclusione del model - classe fisica o astratta
   * @param  [type] $dir       directory
   * @param  [type] $modulo    modulo da dove prendere l'oggetto
   * @param  [type] $name      nome del model
   * @param  [type] $modelName [description]
   * @return [type]            [description]
   */
  private function includeModel($dir, $modulo, $name, $modelName, $option)
  {
    include_once $this->getLibaPath() . "/queryBuild/class.queryBuild.php";
    include_once $this->getPath("A_EXTENDS") . "class.model.php";
    $fileModel = $this->getPathIncludeModel($dir, $modulo, $name, $modelFind, $option);
    if ($modelFind) { //model fisico esiste quindi includo questo
      include_once $fileModel;
      $this->checkClassModel($modelName);
      $mod = new $modelName($this, $option);
    } else { //model astratto specializzato
      if (empty($option['table'])) {
        $option['table'] = lcfirst($name);
      }
      $this->warning("CARICAMENTO MODEL ASTRATTO $name - il file model.$name.php ancora non esiste");
      $mod = new Model($this, $option);
    }
    return $mod;
  }

  public function getModelG(string $name, array $option = [])
  {
    $name = str_replace("\\", "/", $name);
    $dir = "";
    $pos = strpos($name, "/");
    if ($pos > 0) {
      $dir = substr($name, 0, $pos) . "/";
      $name = substr($name, $pos + 1);
    }

    $p = $this->getPath("A_APP");
    $fileModel = $p . "model/" . $dir . "model.$name.php";
    if (file_exists($fileModel)) {
      include_once $fileModel;
      $mod = new $name($this, $option);
    } else {
      $this->error("Modello non trovato");
    }

    $modelName = $name . "Model";
    $depName = "model_" . $dir . $modelName;
    if (empty($this->dependency[$depName])) { //controllo se il modello è già esistente
      $this->dependency[$depName] = $mod;
    }
    return $this->dependency[$depName];
  }


  /**
   * Carica un model da un libreria
   * @param  string $model [description]
   * @param  string $name  [description]
   * @param  string $dir   [description]
   * @return [type]        [description]
   */
  public function getModel(string $modulo = "", string $name = "", array $option = [])
  {

    $modulo = str_replace("\\", "/", $modulo);
    $dir = "";
    $pos = strpos($modulo, "/"); // ho tutto sul nome es admin/user
    if ($pos > 0) {
      $dir = substr($modulo, 0, $pos);
      $modulo = substr($modulo, $pos + 1);
      $pos = strpos($modulo, "/");
      if ($pos > 0) { //model di un sotto modulo :D admin/service/serviceRun
        $name = substr($modulo, $pos + 1);
        $modulo = substr($modulo, 0, $pos);
      }
    }

    $modulo = ucwords($modulo);
    if (empty($modulo)) { //estrae il modulo di defualt per la risorsa
      $modulo = $this->getCurModulo();
      $dir = $this->getCurWorkDir();
    }
    if (empty($name)) {
      $name = $modulo;
    }
    $name = ucwords($name);
    if (!empty($dir)) {
      $dir .= "/";
    }
    $modelName = $name . "Model";
    $depName = "model_" . $dir . $modelName;
    if (empty($this->dependency[$depName])) { //controllo se il modello è già esistente
      $mod = $this->includeModel($dir, $modulo, $name, $modelName, $option); //include la classe per il modello
      $this->dependency[$depName] = $mod;
    }
    return $this->dependency[$depName];
  }

  /**
   * Torna l'oggetto Utility
   * @return Utility [description]
   */
  public function getUtility(): Utility
  {
    return $this;
  }

  /**
   * ritorna la path standard della cartella delle librerie
   * @return string path
   */
  private function getLibaPath(): string
  {
    return $this->getPath('A_LIB');
  }



  /**
   * Torna la libreria SessionForm standard
   * @return FieldToken
   */
  public function getSessionForm($section = ""):SessionForm
  {

    if (empty($this->dependency['sessionForm'])) {
      include_once $this->getLibaPath() . "class.sessionForm.php";
      $this->dependency['sessionForm'] = new SessionForm($this);
      if (!empty($section)) {
        $this->dependency['sessionForm']->setSection($section);
      }
    }
    return $this->dependency['sessionForm'];
  }

  /**
   * Torna la libreria FieldToken standard
   * @return FieldToken
   */
  public function getFieldToken()
  {
    include_once $this->getLibaPath() . "class.fieldToken.php";
    return new FieldToken($this);
  }

  /**
   * Alloca e ritorna il modulo corrente / scelto della classe notifica
   * @param  string $modulo da caricare
   * @return ModuloNotifica classe allocata contenente i metodi per il calcolo della notifica
   */
  public function getNotificaModulo(string $modulo = "", string $dir = "")
  {
    if (empty($modulo)) {
      $modulo = $this->getCurModulo();
    }
    if (empty($dir)) {
      $dir = $this->getCurWorkDir();
    }
    if (!empty($dir)) {
      $dir .= "/";
    }
    try {
      $lib = $this->getPath('A_EXTENDS') . 'class.notifica.php';
      include_once $lib;
      $not = $this->getPath('A_APP') . $dir . $modulo . "/class.$modulo.notifica.php";

      if (!file_exists($not)) {
        return false;
      }
      if (!include_once $not) {
        return false;
      }
    } catch (exception $e) {
      echo $e->message;
      die;
    }
    $classe = $modulo . "Notifica";
    return new $classe($this); //alloco e ritorno la classe
  }

  /**
   * oggetto per la ricerca dinamica dalle form di ricerca
   * @param  string $query Query da utilizzare
   * @return QueryCerca oggetto allocato
   */
  public function getQueryCerca(string $query)
  {
    include_once $this->getPath('A_CUSTOM_TRAIT') . 'trait.queryCerca.php';
    include_once $this->getLibaPath() . 'class.queryCerca.php';
    return new QueryCerca($this, $query);
  }

  /**
   * Tora la classe per mostrare i file in ajax
   * @return ScanFile
   */
  public function getScanFile(): ScanFile
  {
    if (empty($this->dependency['scanFile'])) {
      include_once $this->getLibaPath() . "/scanFile/class.scanFile.php";
      $this->dependency['scanFile'] = new scanFile($this);
    }
    return $this->dependency['scanFile'];
  }


  /**
   * libreria per la creazione degli elementi !da usare!
   * @return Field libreria che genra dom partendo da Dataset
   */
  public function getField()
  {
    if (empty($this->dependency['field'])) {
      include_once $this->getPath('A_CUSTOM_TRAIT') . 'trait.fieldCustom.php';
      include_once $this->getLibaPath() . "field/class.field.php";
      $this->dependency['field'] = new field($this);
    }
    return $this->dependency['field'];
  }

  /**
   * Istanzia l'oggetto per creare tabella da query
   * @return CreaTab object
   */
  public function getCreaTab()
  {
    include_once $this->getLibaPath() . 'class.creaTab.php';
    return new CreaTab($this);
  }

  /**
   * Torna l'oggetto per generare liste
   */
  public function getInputSearch(): creaLista
  {
    include_once $this->getLibaPath() . 'class.creaLista.php';
    return new creaLista($this);
  }

  /**
   * Gestione dei giorni feriali e non
   * @return festivita
   */
  public function getDataFeriali()
  {
    include_once $this->getLibaPath() . "classFeriali.php";
    $data = new festivita($this);
    return $data;
  }

  /**
   * Crea e ritona la classe di invio mail
   * @return SendMail
   */
  public function getSendMail()
  {
    $this->vendorAutoloader();
    // if (empty($this->dependency['sendMail'])) {
    include_once $this->getLibaPath() . "class.sendMail.php";
    return new SendMail($this);
    // }
    // return $this->dependency['sendMail'];
  }

  /**
   * Crea e ritona la classe per la gestione FTP
   * @return Ftp
   */
  public function getFtp(array $conn = [])
  {
    include_once $this->getLibaPath() . "class.ftp.php";
    $ftp = new Ftp($this);
    if (!empty($conn)) {
      $log = $ftp->login($conn['host'], $conn['username'], $conn['password'], $conn);
    }
    return $ftp;
  }



  public function getQueryBuild(): QueryBuild
  {
    include_once $this->getLibaPath() . "/queryBuild/class.queryBuild.php";
    return new QueryBuild($this);
  }


  public function queryBuild(): QueryBuild
  {
    return $this->getQueryBuild();
  }



  /**
   * Classe report Excel nativo
   * @return Excel
   */
  public function getExcel()
  {
    if (empty($this->dependency['excel'])) {
      $this->vendorAutoloader(); //devo prendere le dipendenze composer
      include_once $this->getLibaPath() . "class.excel.php";
      $this->dependency['excel'] = new ExcelPhp($this);
    }
    return $this->dependency['excel'];
  }

  /**
   * Classe report HTML excel
   * @return htmlExcel
   */
  public function getHtmlExcel()
  {
    if (empty($this->dependency['htmlExcel'])) {
      include_once $this->getLibaPath() . "class.htmlExcel.php";
      $this->dependency['htmlExcel'] = new HtmlExcel($this);
    }
    return $this->dependency['htmlExcel'];
  }



  /**
   * Classe report Excel nativo
   * @return HtmlPdf
   */
  public function getHtmlPdf()
  {
    if (empty($this->dependency['htmlPdf'])) {
      $this->vendorAutoloader(); //devo prendere le dipendenze composer
      include_once $this->getLibaPath() . "class.htmlPdf.php";
      $this->dependency['htmlPdf'] = new HtmlPdf($this);
    }
    return $this->dependency['htmlPdf'];
  }

  public function getFileForm()
  {
    if (empty($this->dependency['fileForm'])) {
      include_once $this->getLibaPath() . "class.fileForm.php";
      $this->dependency['fileForm'] = new FileForm($this);
    }
    return $this->dependency['fileForm'];
  }

  public function getBlade($views)
  {
    if (empty($this->dependency['bladeOne'])) {
      $this->vendorAutoloader(); //devo prendere le dipendenze composer
      include_once $this->getLibaPath() . "class.blade.php";
      $this->dependency['bladeOne'] = true;
    }
    return getBladeOne($views, $this->getCacheDir(), $this->ifDebug());
    // return $this->dependency['bladeOne'];
    //
  }




  /**
   * Attivo l'autoloader di composer
   */
  public function vendorAutoloader()
  {
    if (!$this->vendorAutoLoad) {
      require __DIR__ . '/../../../../vendor/autoload.php';
      $this->vendorAutoLoad = true;
    }
  }

  public function startTool(string $toolName, array $argv = [], $autostart = true)
  {
    // $httpRequest=1;
    $pathTool = $this->getPath("TOOL");
    include_once $pathTool . "tool.$toolName.php";

    if (empty($tool)) {
      echo "ERRORE";
      $this->error("TOOL NON TROVATO controlla variabili e nome file");
    } else {
      array_unshift($argv, "$toolName");
      $tool->setArgv($argv);
      if ($autostart) {
        echo "START LAUNCHER!\n";
        $tool->launcher();
      }
      return $tool;
    }
  }


  /**
   * Esegue gli script in tool
   * @return
   */
  public function startService($service = "", $param1 = "", $param2 = "", $param3 = "")
  {

    $adDir = "";
    //controllo se devo controllare sotto cartelle
    $xp = explode("/", $service);
    if (count($xp) > 1) {
      $adDir = $xp[0] . "/";
      $service = $xp[1];
    }
    $service = ucwords($service);
    $pathDir = $this->getPath("A_SERVICE");
    //classe del servizio
    $lib = $this->getPath('A_EXTENDS') . 'class.service.php';
    include_once $lib;
    $dir = $pathDir . "$adDir" . "serv." . $service . ".php";
    if (file_exists($dir)) {
      $this->log("Servizio trovato, procedo con l'esecuzione");
    } else {
      $this->error("SERVIZIO $adDir/$service NON TROVATO --> $dir");
    }
    include_once($dir);
    $name = ucwords($service);
    $class = $name . "Service";

    chdir($pathDir . $adDir);
    new $class();
  }

  public function getSchema()
  {
    if (empty($this->dependency['schema'])) {
      include_once $this->getPath("A_DB_SCHEMA") . "class.schema.php";
      $this->dependency['schema'] = new ModSchema($this->getUtility());
    }
    return $this->dependency['schema'];
  }
}
