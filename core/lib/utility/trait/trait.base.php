<?php

/**
* Trait con le funzioni di base della classe - criptazione e controllo stringhe
*/
trait Base
{
  private $arConf=[]; //array di configurazione con dati da db
  private $moduloConf=[];
  private $sessionStart=false; //controlla i valori in sessione
  //carica la configurazione locale da file
  private function initModuloConf(string $modulo="",array $option=[]){

    if(empty($modulo)){
      $modulo=$this->getCurModulo();
    }
    if(empty($this->moduloConf[$modulo])){
      $confFile="conf/conf.$modulo.php";
      if(file_exists($confFile)){
        include_once $confFile;
        $class=$modulo."Conf";
        $conFile=new $class();
      }else{
        $conFile=false;
        return $confFile;
      }
      $this->moduloConf[$modulo]=$conFile;
    }
    return $this->moduloConf[$modulo];
  }

  /**
  * Ritorno dell'id preso dal post e salvato in sessione
  * @return [type] [description]
  */
  public function getId()
  {
    if (empty($this->id)) {
      $this->id=$this->post('id');
    }
    return $this->id;
  }


  /**
  * Ritorno dell'id preso dal post e salvato in sessione
  * @return [type] [description]
  */
  public function getIdSub()
  {
    if (empty($this->idSub)) {
      $this->idSub=$this->post('idSub');
    }
    return $this->idSub;
  }

  public function getSubId(){
    return $this->getIdSub();
  }


  public function getIdInline(){
    if (empty($this->idInLine)) {
      $this->idInLine=$this->post('idInLine');
    }
    return $this->idInLine;
  }


    public function getIdModal(){
      if (empty($this->idModal)) {
        $this->idModal=$this->post('modalId');
      }
      return $this->idModal;
    }


  public function getConfModulo($key,string $modulo="",array $option=[]){
    $data=$this->initModuloConf($modulo,$option);
    if(!$data){
      return false;
    }
    if(isset($data->$key)){
      return $data->$key;
    }
  }



  public function checkHtmlContent(string $str):string{
    $str=str_replace("<script>","",$str);
    $str=str_replace("</script>","",$str);
    $str=str_replace("javascript:","",$str);
    return $str;
  }

  /**
  * Escape anti xss attacck
  * @param  string $string stringa da escapare
  * @return string testo messo in sicurezza
  */
  public function escapeHtml($string)
  {
    if (empty($string)) {
      return $string;
    }
    return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
  }

  /**
  * Escape di ogni elemento dell'array (anche per i sotto array)
  * @param  array  $arr array da processare
  */
  public function escapeArray(array &$arr){

    foreach ($arr as $key => $value) {
      if(is_array($value)){ //controllo se devo escapre o rilanciare la routine in caso di sottoarray
        $this->escapeArray($arr[$key]); //in caso di sotto array devo escapere anche quelli
      }else{
        $arr[$key]=$this->escapeHtml($value); //escape dell'output
      }
    }//end foreach
  }


  /**
  * Contorllo se il numero / stringa è un float
  * @param  [type] $f [description]
  * @return bool      [description]
  */
  public function isFloat($f):bool{
    return ($f == (string)(float)$f);
  }

  public function isInt($i):bool{
    if(!is_numeric($i)){
      return false;
    }
    if(strpos($i,",")){
      return false;
    }
    if(strpos($i,".")){
      return false;
    }
    return true;
  }

  /**
  * Camel case a Stringa per Umani
  * @param   string $str stringa da far diventare normale
  * @return string Stringa elaborata in normal
  */
  public function parseCamelCase(string $str):string
  {
    return preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]|[0-9]{1,}/', ' $0', $str);
  }

  /**
  * trasforma la stringa in snake case
  * @param  string $str [description]
  * @return string      [description]
  */
  public function getSnakeCase(string $str):string{
    $str=$this->humanCase($str);
    $str=str_replace(" ","_",$str);
    return strtolower($str);
  }

  /**
  * Trasforma la stringa in camelCase
  * @param  string $str stringa da far diventare camelCase
  * @return string      stringa elaborata
  */
  public function getCamelCase(string $str):string{
    $str=ucwords($str);
    $str=str_replace(" ","",$str);
    return lcfirst($str);
  }

  /**
  * Trasforma una tringa con _ e CamelCase in scrittura da umano
  * @param  string $str stringa daprocessare
  * @return string      stringa trasformata
  */
  public function humanCase(string $str):string
  {
    $str=str_replace("_", " ", $str);
    $str=$this->parseCamelCase($str);
    return $str;
  }




  /**
  * Wrapper del getPath presente in path, gestite il log della richiesta in caso di false
  * @param  string $pathName nome della path
  * @return mixed string path assoluta richiesta | FALSE path non trovata
  */
  public function getPath(string $pathName):string
  {
    if(!isset($this->path)){
      include_once __DIR__."/../../../../config/class.path.php"; //path di tutto il progetto per i percorsi assoluti
      $this->path=new Path();
    }


    $res=$this->path->getPath($pathName);
    if (!$res) {
      $this->warning("Path", "$pathName non trovata");
      return "";
    }
    return $res;
  }


  public function setPostFormVal(string $nomePost,string $valPost,string $group=""):void{
    if(empty($group)){
      $group=strtolower($this->getCurModulo());
    }
    $_POST['forms'][$group]['data'][$nomePost]['val']=$valPost;
  }

  /**
  * Ritorna il valore di un elemento caricato con selIns
  * @return string
  */
  public function getPostFormVal(string $nomePost, string $group="")
  {
    $post=$_POST['forms'];
    if(empty($group)){
      $group=strtolower($this->getCurModulo());
    }

    if (empty($post[$group])) {
      $this->warning("POST |$nomePost| - |$group| NON TROVATO in FormVal");
      $this->error("POST _GRUPPO $nomePost DATA_ $group NON TROVATO");
    }else{
      $post=$post[$group]['data'];
    }
    if (empty($post[$nomePost])) {
      $this->warning("post-data", "elemento $nomePost - $group non trovato in getPostFormVal");
      $this->error("elemento non trovato in getPostFormVal");
    }
    $post=$post[$nomePost];
    if (!isset($post['val'])) {
      $this->warning("VALORE $nomePost - $group POST DATA NON TROVATO");
      $this->error("VALORE  POST DATA NON TROVATO");
    }
    return $post['val'];
  }



  /**
  * Controllo se sono in modalità debug
  * @return bool
  */
  public function ifDebug():bool
  {
    return $this->debug;
  }

  /**
  * Start modalità debug, abilità warning esposti a video ed errori
  * @return void
  */
  public function debug():void
  {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $this->debug=true;
    return;
  }

  /**
  * Controllo se il post esiste
  * @param  string $post post richiesto
  * @return bool         esito verifica
  */
  public function checkPostExists(string $post):bool
  {
    if (isset($_POST[$post])) {
      return true;
    } else {
      return false;
    }
  }

  /**
  * Richiesta di un valore preso dal $_POST
  * @param  string nomePost da cui prendere il valore
  * @param  bool   Attivo il warning in caso di eccezzione
  * @return mixed FALSE ERRORE ACCESSO POST / Valore del POST
  */
  public function post(string $post, bool $warning=true)
  {
    if ($this->checkPostExists($post)) {
      return $_POST[$post];
    } else {
      if ($warning) {
        $this->warning('POST', 'POST '.$post.' non trovato');
      }
      return false;
    }
  }


  /**
  * Sostituisce la stringa solo al primo match
  * @param  string $from
  * @param  string $to      diversa
  * @param  string $subject stringa su cui cercare
  * @return string          stringa sostuita
  */
  public function strReplaceFirst( $from, $to,string $subject):string
  {
    $from = '/'.preg_quote($from, '/').'/';
    return preg_replace($from, $to, $subject, 1);
  }


  /**
  * Post con filter input automatico
  * @param  string $post POST richiesto
  * @return string
  */
  public function postStr(string $post):string
  {
    if ($this->checkPostExists($post)) {
      $str= filter_input(INPUT_POST, $post, FILTER_SANITIZE_STRING);
      if ($str) {
        return $str;
      } else {
        $this->error("Post $post non valido");
      }
    } else { //else checkpost
      $this->error("Post string $post non trovato");
    }
  }

  /**
  * Imposta un post in modifica
  * @param string $post chiave post
  * @param midex $key  chiave (se val vuoto diventa il valore)
  * @param mixed $val  valore (opzionale)
  */
  public function setPost(string $post, $key="", $val):void
  {
    if (isset($_POST[$post])) {
      $this->warning('SETPOST', 'POST '.$post.' non trovato nella funzione di setPOST');
    }
    if (empty($val)) { //controllo se è un array con chiave o è un valore diretto
      $_POST[$post]=$key;
    } else {
      $_POST[$post][$key]=$val;
    }
    return;
  }

  public function initSessionStart(){
    if($this->sessionStart){
      return;
    }
    session_start();
    $this->sessionStart=true;
  }


  public function getSession(string $key){
    if($this->skipSession){
      return false;
    }
    $this->initSessionStart();
    if(!isset($_SESSION[$key])){
      return false;
    }
    return $_SESSION[$key];
  }

  public function setSession(string $key,$val){
    $this->initSessionStart();


    $_SESSION[$key]=$val;
  }


  /**
  * @param  string  $colName nome del filtro da ritornare
  * @param  boolean $defualt valore di defualt da avere come ritorno. Di base è false
  * @return mixed   valore del ritorno
  */
  public function getFilt(string $colName, $default=false)
  {
    if (empty($this->filt)) {
      $this->filt=$this->post('filter');
    }
    $filt=$this->filt;
    if (isset($filt[$colName])) {
      return $filt[$colName];
    } else {
      return $default;
    }
  }

  /**
  * Metodo di base per la criptazione della stringa - wrap nel caso devo cambiare metodo nel futuro
  * @return hashstring stringa hashata
  *
  */
  public function hash(string $str):string
  {
    return password_hash($str, PASSWORD_BCRYPT);
  }




  /**
  * Controllo se l'hash corrisponde alla stringa - wrap nel caso devo cambiare metodo nel futuro
  * @param  string $str  [description]
  * @param  string $hash [description]
  * @return bool         Esito operazione
  */
  public function checkHash(string $str, string $hash):bool
  {
    return password_verify($str, $hash);
  }

  public function isHashed(string $str):bool{
    if(!strlen($str)==60){
      return false;
    }
    if(substr($str,0,4)!="$2y$" ){
      return false;
    }
    return true;
  }


  /**
  * Ritorna il valore della configurazione in db
  * @param  string configurazione su db
  * @return string valore della configurazione / bool false in caso di valore non esistente
  */
  public function getConf(string $conf):string
  { //controlle le configurazioni e restituisco il valore

    if (empty($this->arConf[$conf])) {
      $this->startPrepare('SELECT valore from configurazione where conf=:conf ');
      $this->bindP('conf', $conf);
      $this->executePrepare();
      if (!$this->isDatasetEmpty()) {
        $ret= $this->fetch()['valore'];
      } else {
        $this->warning("getConf", "Configurazione $conf non trovata");
        return "";
      }
      $this->arConf[$conf]=$ret;
    }
    return $this->arConf[$conf];
  }


  /**
  * COntrollo la configurazione se è attiva
  * @param  string $param parametro da confrontare
  * @param  [type] $app   valore
  * @return bool risultato controllo
  */
  public function ifConf(string $conf, string  $valore):bool
  { //controllo valore corrisponde alla configurazione attiva
    $val=$this->getConf($conf);
    if ($val == $valore) {
      return true;
    } else {
      return false;
    }
  }

  /**
  * Crea un token otp per la sessione, usato in diverse parti del sistema
  * @return string string otp password link
  */
  public function getOtpToken():string{
    $tok = md5(substr(md5(microtime()), rand(0, 26), 15).$this->getIdUser()) ;
    return $tok;
  }


  /**
  * Criptazione in blowfish
  * @param  string $pure_string
  * @return string stringa criptata
  */
  public function crypt(string $plaintext, string $key=""):string
  {
    if (empty($key)) {
      $key=$this->getSecretKey();
    }

    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    $ciphertext = base64_encode($iv.$hmac.$ciphertext_raw);
    return $ciphertext;
  }

  /**
  * Decrypta la stringa con scelta
  * @param  string $encryptedString Stringa da cryptare
  * @param  string $encryptedString chiave per il salt (vuoto ha un default)
  * @return string                  stringa decryptata
  */
  public function decrypt(string $ciphertext='', string $key=""):string
  {
    if (empty($ciphertext)) {
      return "";
    }
    if (empty($key)) {
      $key=$this->getSecretKey();
    }
    $c = base64_decode($ciphertext);
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len=32);
    $ciphertext_raw = substr($c, $ivlen+$sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    if (hash_equals($hmac, $calcmac)) {//PHP 5.6+ timing attack safe comparison
      return $original_plaintext;
    }
    return "";

  }


  /**
  * Torna una stringa casuale
  * @param  integer $length lunghezza stringa
  * @return string striga generata
  */
  public function getRandomString(int $length = 10):string {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }


  /**
  * Torna l'ip di chi si sta collegando al portale
  */
  public function getIpRequest()
  {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
  }

  /**
  * Torna la prima riga dell'array con prima riga di testata (vedi csv / excel)
  */
  public function getArrayHeaderFirstRow(array $array):array{
    $first=true;
    $newArray=[];
    foreach ($array as $row) {
      if($first){
        $header=$row; //inizializzo la testata
        $first=false;
        continue;
      }
      $i=0;
      $new=[];
      foreach($row as $col){ //
        $new[$header[$i]]=$col;
        $i++;
      }//end foreach col
      array_push($newArray,$new);
    }//end foreach array principale
    return $newArray;
  }

  /**
  * Crea una riga a databae per funzione di log
  * @param  string $mess commento del log
  * @param  string $sez  sezione dove viene richiamata , opzionale
  * @param  string $id   riferimetno, opzionale
  * @return
  */
  public function logDb(string $mess,string $sez="",string $id=""){
    // echo $mess;
    return;
    $modulo=$this->getCurModulo();
    $str="INSERT INTO log(commento,sezione,riferimento,data,modulo)values(:commento,:sez,:riferimento,now(),'$modulo') ";
    $this->startPrepare($str);
    $this->bindP("commento",$mess);
    $this->bindP("sez",$sez);
    $this->bindP("rif",$id);
    if(!$this->executePrepare()){
      $this->warning("Impossibile utilizzare i log di sistema a db");
    }else{
      return true;
    }
  }


  public function getLogUserUpdate():string{
    if($this->isLogUserUpdated()){
      $idUser=$this->getIdUser();
      return " , lastUpdatedUser = '$idUser' ";
    }
    return "";
  }


  public function getLogUserInsert():string{
    if($this->isLogUserUpdated()){
      $idUser=$this->getIdUser();
      return " , createdUser = '$idUser' ";
    }
    return "";
  }




  /**
  * Condizione di default per il softDelete
  * @return string
  */
  public function getSoftDeleteWhere(string $curTab=""):string{
    if($this->isHardDelete()){
      return "";
    }
    if(empty($curTab)){
      $curTab=$this->getCurModulo();
      $curTab=strtolower($curTab);
    }
    return " and ( $curTab.deletedRecord<>1 or $curTab.deletedRecord is null )
      and $curTab.deletedDate is null ";
  }


  public function softDelWhere(string $curTab=""):string{
    return $this->getSoftDeleteWhere($curTab);
  }



  /**
  * Esegue gli script in tool
  * @return
  */
  public function executeTool(string $tool="",string $param1="",string $param2="",string $param3=""){
    $dir=$this->getPath("TOOL");
    include_once($dir."/class/class.tool.php");
    $dir=$dir."tool.".$tool.".php";
    if(file_exists($dir)){
      $this->log("tool trovato, procedo con l'esecuzione");
    }else{
      $this->error("TOOL $tool NON TROVATO");
    }
    $cmd=$dir." ".$param1." ".$param2." ".$param3;
    $this->log("Esecuzione tool-----> $cmd");
    include_once $dir;    
    $class=$tool."Tool";
    $tool=new $class(array(0=>$param1,1=>$param1,2=>$param2,3=>$param3));
 }


  public function isArrayAssociative(array $var){
    if(array_keys($var) !== range(0, count($var) - 1)){
      return true;
    }else{
      return false;
    }
  }

  public function formatDecimal($float,$prec="2"){
    return number_format($float,$prec);
  }


  public function formatMoney($float,$prec="2"){
    return number_format($float,$prec)." €";
  }

/**
 * replace ultima trovata
 * @param  [type] $search  cerca
 * @param  [type] $replace sost.
 * @param  [type] $subject stringa
 * @return [type]          [description]
 */
public function str_lreplace($search, $replace, $subject)
{
    $pos = strrpos($subject, $search);

    if($pos !== false)
    {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }

    return $subject;
}


public function httpPost(string $url,array $data=[],array $opt){
  $opts = array(
      'http'=>array(
          'method'=>"POST",
          'content'=>http_build_query($data),
          'header' => "Content-type: application/x-www-form-urlencoded\r\n"
      )
  );
  $context = stream_context_create($opts);
  // Open the file using the HTTP headers set above
  if(str_pos("http")===false){
    $url="http://".$url;
  }
  $result = file_get_contents($url, false, $context);
  if ($encode) {
      $result=json_decode($result, true);
  }
  return $result;
}


public function http(string $url,array $data,array $option){
  $opts = array(
      'http'=>array(
          'method'=>"POST",
          'content'=>http_build_query($data),
          'header' => "Content-type: application/x-www-form-urlencoded\r\n"
      )
  );
  $context = stream_context_create($opts);
  // Open the file using the HTTP headers set above
  $url="http://localhost:8000/".$url;
  $result = file_get_contents($url, false, $context);
  if ($encode) {
      $result=json_decode($result, true);
  }
  return $result;
}

public function start404(){
    ob_clean();
    include_once __DIR__."/../../../router/404.php";
    die;
}


}//fine trait
