<?php
// Modulo per il caricamento, salvataggio e estrazione file


include_once __DIR__."/../modello/baseFunction.php";
class fileStore extends classBase{

  private $debug=false; //abilita il debug se true
  private $idGruppoFile='';// che idGruppo è aperto in questo momento?
  private $buffPost=''; //c'è una variabile post in lavorazione?
  private $iPostFile=0; //conta il numero di file nel post

  public function __construct($web){
    $this->setWeb($web);

    $azione=$this->getAzione();

    switch ($azione) {
      case 'upload':
      $this->startFromPost();
      break;
      case 'getFile':
      $this->retrieveFile($this->getPost('idFile'));
      break;

      case 'deleteFile':
      $this->deleteFile($this->getPost('idFile'));
      break;

      case 'newInputGroup':
      $this->addNewInputGroup();
      break;
      default:
      # code...
      break;
    }


  }

  //crea un nuovo elemento per uploader il file
  private function addNewInputGroup(){
    $idGruppo=$this->getPost('idGruppo');
    $i=$this->getPost('nGroup');
    $nomeGruppo=$this->getPost('nomeGruppo');
    $field=$this->getField();

    $field->addNewFileGroup($idGruppo,$nomeGruppo,$i);
    echo  $field->printHtml();
  }


  public function setDebug(){
    $this->debug=true;
  }

  private function isDebug(){
    return $this->debug;
  }


  //setta il post in lavorazione
  private function postWork($post){
    $this->buffPost=$post;
  }

  private function getPostWork(){
    return $this->buffPost;
  }

  private function incFilePost(){
    $this->iPostFile++;
  }

  private function clearFilePost(){
    $this->iPostFile=0;
  }

  private function getFilePost(){
    return $this->iPostFile;
  }

  public function log($str){
    if($this->isDebug()){
      $this->setRes('log',$str);
    }
  }

  public function setIdFileGroup($id){
    $this->idGruppoFile=$id;
  }

  private function getIdFileGroup(){
    if(empty($this->idGruppoFile)){
      return false;
    }else{
      return $this->idGruppoFile;
    }
  }

  //Restituisce la directory per salvare il file
  private function getDirUpload(){
    $app= $this->getPath('A_UPLOAD');
    $group=$this->getIdFileGroup();
    if(!$group){
      $app.="file/";
    }else{
      if(!file_exists($app."gruppoFile/")){
        mkdir($app."gruppoFile/");
      }
      $app.="gruppoFile/$group/";
    }
    if(!file_exists($app)){
      mkdir($app); //creo la cartella che serve

    }
    return $app;

  }

  //crea la nuova area di archivio per il gruppo dei file
  // Se devo archiviare tanti file per un'entità è la scelta giusta! Altrimenti no
  public function newStoreGroup($descrizione=''){
    $descrizione=$this->estr($descrizione);
    $str=" insert into file_gruppo (descrizione,dataInsert)values('$descrizione',now()) ";
    $this->query($str);
    $id= $this->getMyLastIns();
    $this->setIdFileGroup($id);
    $this->log('Nuovo gruppo file inserito - ID:'.$id);
    $this->setRes('idFileGruppo',$id);
    $this->setRes('fileGruppo',$descrizione);

    return $id;
  }

  //ottengo il percorso univoco per il file
  private function getPercFile($nomeFile,$i=""){
    if($i!=""){
      $pre=$i."-";
    }else{
      $pre="";
    }

    $newFile =$this->getDirUpload().$pre.basename($nomeFile);

    if(file_exists($newFile)){
      $nomeFile=$nomeFile; //aggiungo un contatore per avere il file univoco in cartella e in db
      $newFile= $this->getPercFile($nomeFile,$i+1);
    }
    return $newFile;
  }

  //setta l'array di ritorno con gli id inseriti
  private function setIdRes($nome,$id){
    $post=$this->getPostWork();
    // $this->web->resJson['idFile'][$post][$this->getPostWork()]['nome']=$nome;
    // $this->web->resJson['idFile'][$post][$this->getPostWork()]['id']=$id;

    $this->web->resJson['idFile']['nome']=$post;
    $this->web->resJson['idFile']['id']=$id;
    $this->web->resJson['idFile']['nomeFile']=$nome;
    $gr=$this->getIdFileGroup();

    // $this->web->resJson['idFile'][$post][$this->getPostWork()]['fileGroup']=$gr;
    $this->web->resJson['idFile']['fileGroup']=$gr;

  }


  //carica il file nel database
  private function insDbFile($nome,$path,$size='null'){
    $nome=$this->estr($nome);
    $path=$this->estr($path);
    $size=$this->estr($size);
    $gruppo=$this->getIdFileGroup();
    if(!$gruppo){$gruppo='null';}
    $post=$this->getPostWork();
    if(empty($post)){$post='null';}else{$post="'".$this->estr($post)."'";}
    $str=" insert into file(nome,size,path,idGruppo,id,dataInsert)values('$nome',0,'$path',$gruppo,$post,now()) ";
    $this->query($str);
    $idFile=$this->getMyLastIns();
    $this->log("File $nome POST $post inserito con id: $idFile");
    $this->setIdRes($nome,$idFile);
  }

  //carica il file nella cartella corretta e salva l'id - sempre dall'array $_FILES
  private function doStoreFilePost($file){
    $nomeFile=$file['name'];
    $path=$file['tmp_name'];
    $size=$file['size'];
    $newFile=$this->getPercFile($nomeFile);

    //spostamento del file nella directory giusta
    if (move_uploaded_file($path, $newFile)) {
      $this->log("File $newFile spostato nella cartella corretta ");
      $this->insDbFile($nomeFile,$newFile,$size);
    }else{
      $this->log(" Errore spostamento file $newFile ");
      $this->setRes('errore','Errore nel caricamento dei file -->'.$newFile.' non caricato');
    }
  }


  //metodo per il caricamento del file tramite post
  public function storeFileFromPost(){
    //controllo l'esistenza di più file esistenti
    $this->clearFilePost();

    foreach ($_FILES as $file) {

      $nome=$file['name'];
      $post=$this->getPost('file');
      $this->postWork($post);
      $this->doStoreFilePost($file);
      $this->incFilePost();
    }
  }




  //metodo di bootstrap per caricare i file da post come da chiamata generica AJAX fissata nel js fileStore.js
  public function startFromPost(){
    $descGruppo=$this->getPost('gruppo');
    $idGruppo=$this->getPost('idGruppo');

    if(empty($idGruppo)){ //se non esiste il gruppo forse devo crearlo
      if(!empty($descGruppo)){ //il caricamento opera in un gruppo?
        $this->newStoreGroup($descGruppo); //sì, allora devo crearlo
      }
    }else{ //il gruppo esiste, inizializzo quello
      $this->setIdFileGroup($idGruppo);
    }
    $this->deleteOldFile();
    $this->storeFileFromPost();
    $this->printResJ();
  }


  //devo eliminare il file vecchio? Lo faccio!
  private function deleteOldFile(){
    $old=$this->getPost('oldFile');
    if(!empty($old)){
      $this->deleteFile($old,false);
    }
  }

  //restituisce il dataset con il file cercato
  private function getRowFile($idFile){
    $row=$this->web->getRowMy('select nome,path,idFile from file where idFile='.$idFile.' ');
    return $row;
  }

  private function getPathTmpFile($file){
    $tmp=$this->getPath('A_TMP_UPLOAD');
    if(empty($tmp)){ //cartella upload disabilitata, procedo con il file diretto
      return false;
    }
    $rand=$this->web->codiceRandom(6);
    $tmp=$tmp.$rand."/";
    mkdir($tmp);
    return $tmp;
  }

  //mette il file in una zona sicura e restituice il posto per caricarlo
  public function retrieveFile($idFile){
    $row=$this->getRowFile($idFile);
    $newFile=$this->getPathTmpFile();
    $file=$row['path'];
    if($newFile){
      $newFile.=$row['nome'];
      if (!copy($file, $newFile)) {
        $this->log('File copiato '.$file);
      }else{
        $this->log('File non copiato!');
        $this->setRes('errore',"file non copiato");
      }
    }else{
      $newFile=$row['path']; //tolgo i dati del server e converto in web
    }

    if(file_exists($newFile)){
      $newFile=str_replace('/var/www/','http://',$newFile); //tolgo i dati del server e converto in web

      $this->setRes('urlFile',$newFile);
      $this->setRes('mess','file disponibile');
    }else{
      $this->setRes('errore','file non trovato '.$newFile);
    }
    $this->printResJ();
  }

  //elimina il file e elimina il record
  public function deleteFile($idFile,$printInfo=true){
    $row=$this->getRowFile($idFile);
    if(unlink($row['path'])){
      $this->log('File Eliminato '.$idFile);
      $this->setRes('del','file eliminato');
    }else{
      $this->log('Impossibile eliminare il file');
      $this->setRes('del','impossibile eliminare il file');
    }


    $str="delete from file where idFile=$idFile";
    $this->query($str);

    $this->setRes('delDb','File eliminato dal db '.$idFile);
    $this->setRes('mess','File eliminato con successo');
    if($printInfo){
      $this->printResJ();
    }


  }
}

include_once '../../iniz.php';
$cl=new fileStore($web);

?>
