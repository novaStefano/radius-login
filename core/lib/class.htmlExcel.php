<?php



class HtmlExcel extends WrapperUtility
{

    private $html;
    private function header()
    {
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=temp.xls");  //File name extension was wrong
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
    }

    public function __construct(Utility $ut)
    {
        parent::__construct($ut);
        $this->header();
    }

    public function title(string $title)
    {
        echo "<h1>$title</h1>";
    }


    private function printHeader(string $res=""){
        echo '<table class="table">';
        echo '<thead class="header"><tr>';
        $col=$this->getUtility()->getColumnName($res);
        foreach ($col as $property) {
            echo "<td><b>$property</b</td>";
        }
        echo '</tr></thead>';
    }

    /**
    * Stampa di un dataset su html
    * @param  string $res nome del dataset (opazionale)
    * @return
    */
    public function printTab(string $res="")
    {
        $this->printHeader($res);
        while($row=$this->fetch($res)){
            echo "<tr>";
            foreach ($row as $col) {
                echo "<td>$col</td>";
            }
            echo "</tr>";
        }//end while
    }


    public function execute()
    {
        die();
    }
}
