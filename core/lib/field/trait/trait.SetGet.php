<?php
/**
* Trait con tutti i metodi di manipolazione degli array dell'oggetto
*/

trait SetGet
{
	private $sezione="dettaglio"; //Indica cosa sta facendo field: se subDettaglio / insVeloce o qualcosa di standard
	private $formAzione = "insert"; //definisce se la form creata è per una modifica o per l'inserimento, di default è inserimento
	private $autoSubLabel=false;
	private $customSaveEvent=null;
	private $workId=null;
	private $lastTableSession=''; //ultima tabella attiva per la creazione dei form
	private $workTableInline=''; //tabella in lavorazione inline - serve per i succevisi test di permessi
	public $isOnInsert=null;
	private $bckParam=null; //buffer per creare un backup di paramwork / utile per le closure

	public function setAutoSubLabel(){
		$this->autoSubLabel=true;
	}



	public function bckParamWork(){
		$this->bckParam=$this->paramWork;
	}

	public function restoreParamWork(){
		if($this->bckParam==null){
			$this->error("ERRORE RIPRISTINO PARAMWORK");
		}
		$this->paramWork=$this->bckParam;
		$this->bckParam=null;
	}

	private function setWorkTableInline($tab):string{
			return $this->workTableInline=$tab;
		}

	public function getWorkTableInline():string{
		return $this->workTableInline;
	}

	private function setLastTableSession(string $table){
		$this->lastTableSession=$table;
	}

	public function getLastTableSession(){
		return $this->lastTableSession;
	}

	public function setSaveEvent(string $event){
		$this->customSaveEvent=$event;
	}


	public function setSezione(string $sezione)
	{
		$this->sezione=$sezione;
	}

	public function getSezione()
	{
		return $this->sezione;
	}




	/**
	* Ritorno di paramwork - array con i parametri dell'elemento in lavorazione
	* @return array paramWork globale
	*/
	public function getParamWork()
	{
		if (isset($this->paramWork)) {
			return $this->paramWork;
		} else {
			return false;
		}
	}

	/**
	* Imposta il paramwork in lavorazione
	* @param array $param paramWork in lavorazione
	*/
	public function setParamWork(array $param=[])
	{
		$this->paramWork=$param;
	}

	/**
	* Ritorna la variabile del paramwork attivo
	* @param  string $param  parametro richiesto
	* @return mixed          ritorno del valore
	*/
	private function getWork(string $param)
	{
		if (!isset($this->paramWork[$param])) {
			return false;
		} else {
			return $this->paramWork[$param];
		}
	}

	/**
	* Imposta il valore sul parametro del paramWork
	* @param string $param  chiave
	* @param string $val   valore
	*/
	public function setWork(string $param, string $val,$add=false)
	{
		if($add){
			if(!empty($this->paramWork[$param])){
				$val=$this->paramWork[$param]."  ".$val;
			}
		}
		$this->paramWork[$param]=$val;
	}

	/**
	 * Ritorna il parametro dell'auto div richiesto
	 * @param  string $param parametro richiesto
	 * @return ritorno mixed
	 */
	private function getWorkDiv(string $param){
		$div=$this->getWork("paramAutoDiv");
		if (!isset($div[$param]) || $div[$param]==false  ) {
			return false;
		} else {
			return $div[$param];
		}
	}

	private function setWorkDiv(string $param,string $val){
		$this->paramWork["paramAutoDiv"][$param]=$val;
	}

	/**
	* Tipologia di azione che sta eseguendo la richiesta
	* @return string tipoAzione
	*/
	public function getFormAction()
	{ //ins sono in inserimento, mod in modifica (Variabile definita in procVal()
		return $this->formAzione;
	}



	public function setOnInsert(bool $insert=true){
		$this->isOnInsert=$insert;
	}


	/**
	* Controlla se la form è in inserimento
	* @return boolean esito del controllo
	*/
	public function isOnInsert():bool
	{
		if($this->isOnInsert===null){
			$app=$this->getUtility()->post("onInsert");
			if($app=='false'){
				$app=false;
			}else{
				$app=true;
			}
			$this->isOnInsert=$app;
		}
		return  $this->isOnInsert;
	}


	/**
	* Controlla se la form è in modifica
	* @return boolean esito del controllo
	*/
	public function isOnUpdate():bool
	{
		if ($this->getFormAction()=="update") {
			return true;
		} else {
			return false;
		}
	}


	/**
	* Imposta l'azione della richiesta
	* @param string $modo richiesta eseguita
	*/
	public function setFormAction(string $modo)
	{
		$this->formAzione=$modo;
	}

	public function getArVar($elemento, $attr)
	{
		if (isset($this->arParam[$elemento][$attr])) { //se non è valorizzato non vado con l'eccezzione
			return $this->arParam[$elemento][$attr];
		} else {
			$this->warning("Get ar var", "ERRORE parametro $attr di $elemento non esiste");
			return false;
		}
	}

	/**
	* Torna l'oggetto js che opera nel contesto.
	* @return string nome della classe JS
	*/
	public function getObjJs()
	{
		switch ($this->getSezione()) {
			case 'subDettaglio':
			return "subProto";
			break;
			case 'inLine':
			return "inLine";
			break;
			case 'modalForm':
			return "modalForm";
			break;

			default:
			return "proto";
			break;
		}
	}




	/**
	* Torna l'html che ho in memoria
	*/
	public function getHtml()
	{
		return $this->html;
	}


	/**
	* Metodo principale per l'aggiunta di html
	* @param [type] $html [description]
	*/
	public function addScript($html)
	{ //aggiunge l'html alla variabile principale
		$this->addHtml("<script>$html</script>");
	}


	/**
	* Metodo principale per l'aggiunta di html
	* @param [type] $html [description]
	*/
	public function addHtml($html)
	{ //aggiunge l'html alla variabile principale
		if ($this->ifBuffer()) {
			$this->bufferHtml.=$html."\n";
		} else {
			$this->html.=$html."\n";
		}
	}

	/**
	 * Torna l'html elaborato dalla classe come stringa e non come stdout
	 * @param  bool   $chiudi chiudi i div generati
	 * @return string $stdour a string
	 */
	public function getPrintHtml($chiudi=true):string{
		ob_start();
		$this->printHtml($chiudi);
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}


	/**
	* Stampa l'html sullo stdou
	*/
	public function printHtml($chiudi=true)
	{
		if (!empty($this->arParam)) { //se ho un form ancora da stampare lo stampo ora prima del print
			$this->executeArField($chiudi); //generazione e stampa dei campi per
		}else{

			if($chiudi){ //chiudo gli ultimi elementi in automatico
				$this->chiudi_gDiv();
				$this->chiudi_schedaContainer();
			}

		}

		if (empty($this->html)) {
			$this->warning("printHtml", "HTML var vuota");
			$this->halt();
		}
		echo $this->html;
		$this->html="";
	}



	/**
	* Imposta il profilo su che livello deve lavorare
	* @param string $profilo su cui sto lavorando
	*/
	public function setProfile(string $profilo)
	{ //setta il profilo di lavoro
		$this->curProfile=$profilo;
	}

	/**
	* Ritorno il profilo attivo o verifica se la stringa passata è la vera
	* @param  string $profilo [description]
	* @return [type]          [description]
	*/
	public function getProfile(string $profilo="")
	{ //controllo il profilo attivo retunr true/false. Se la variabile è vuota restituisce il profilo attivo
		if ((!isset($profilo))||($profilo=="")) {
			return $this->curProfile;
		}

		if ($profilo==$this->curProfile) {
			return true;
		} else {
			return false;
		}
	}

	/**
	* Funzione per settare una variabile dell'array degli elementi in lavorazione
	* @param string $ele   elemento da variare
	* @param string  $attr attributo del parametro
	* @param mixed $var    valore del parametro
	*/
	public function setArParam(string $ele, string $attr, $var)
	{ //funzione per settare un parametro dell'array Globale
		if (empty($this->arParam[$ele])) {
			$this->warning('SET AR VAR', "indice non valido $ele ATTRIBUTO $attr");
			$this->halt();
		}
		$this->arParam[$ele][$attr] = $var;
	}

	/**
	**clase di base messa del buffer. Gli elementi se impostati prendono la classe in automcatico.
	*/
	public function setCustomClass($classe)
	{
		$this->customClass=$classe;
	}
	/**
	* Pulisce la variabile customclass
	* @return [type] [description]
	*/
	public function clearCustomClass()
	{
		unset($this->customClass);
	}



	/**
	* Valorizzazione della variabile Buffer
	*/
	public function setBuffer()
	{
		$this->bufferHtml='';
		$this->buffer='1';
	}

	/**
	* Ritorna il buffer valorizzato attivo
	* @return string ritorno del buffer
	*/
	public function getBuffer()
	{
		if ($this->ifBuffer()) {
			$this->buffer='';
			return $this->bufferHtml;
		} else {
			$this->warning("GetBuffer", "nessun buffer attivo");
		}
	}

	/**
	* Controllo se il buffer è attivo o no
	* @return Bool esito buffer attivo
	*/
	private function ifBuffer()
	{
		$buf=$this->buffer;
		if (empty($buf)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	* Controllo se l'id è attivo
	* @return boolean [description]
	*/
	public function isIdSet()
	{
		return $this->showElimina;
	}


	/**
	* Torna l'array con i parametri in lavorazione
	* @return [type] [description]
	*/
	public function getArParam()
	{
		return $this->arParam;
	}


	/**
	* Impostazione di una variabile sull'array globale
	* @param string $elemento elemento da variare
	* @param string $colonna  [description]
	* @param mixed$valore   [description]
	*/
	public function setArVar(string $elemento, string $colonna, $valore)
	{ //setta i parametri di un elemento di arParam
		if (empty($this->arParam[$elemento])) {
			$this->warning("setArVar", "ELEMENTO NON TROVATO ".$elemento." colonna - $colonna ");
			$this->halt();
		}
		$this->arParam[$elemento][$colonna] = $valore;
	}


	/**
	* Crea il js per modificare il titolo
	* @param [type] $html [description]
	*/
	public function setTitle($html)
	{
		$this->showTitolo($html);
		// $ht = "<script>$('#titlePage').html('$html');</script>";
		// $this->addHtml($ht);
	}


	public function getWorkingMultiScheda(){
		return $this->workingMultiScheda;
	}


	/**
	* Pulisce i parametri dell'oggetto per una nuova operazione
	*/
	public function pulisci()
	{ // è il metodo che pulisce e riporta come nuvo l'oggetto field
		$this->html = "";
		$this->paramWork = "";
		$this->arParam = [];
		$this->valori = [];
	}

	/**
	 * Registra l'id su cui la form sta operando
	 * @param string $id [description]
	 */
	public function setWorkId(string $id){
		$this->workId=$id;
	}


	public function getWorkId():string{
		return $this->workId;
	}



	/**
	* Controllo se la variabile è blowfish
	* @param  string $tipo da verificare
	* @return bool   esito controllo
	*/
	private function ifBlow(string &$tipo="")
	{
		if (strpos($tipo, '-blow')) {
			$tipo=str_replace('-blow', '', $tipo);
			return true;
		} else {
			return false;
		}
	}
}//end trait
