<?php

/**
*Trait con i metodi di gestione dei file
*/
trait fieldFile
{


	private function getExtFaIcon(string &$ext=""){

		switch($ext){
			case 'pdf':
			$ext='file-pdf';
			break;
			case 'xls':
			case 'xlsx':
			case 'xml';
			$ext="file-excel";
			break;

			case 'odt':
			case 'doc':
			case 'docx':
			$ext='file-word';
			break;
			default:
			$ext='file';
			break;
		}

	}



	public function getSpanOpenFile($file,$type,$id,$public){
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$this->getExtFaIcon($ext);
		$table = $this->getWork("table");
		if(empty($table)){
			$table=$this->post("source");
		}
		$this->setWork("style","display:none;",true);
		$this->inputGroup("none","",['defaultClass'=>" groupFormFile "]);
		$nameFile=$this->getUtility()->maxCarDir($file,23);
		$spanDoc='';

		if($public){
			$ev="openPublicFoto";
		}else{
			$ev="downloadMe";
		}

		$btn="";
		if(!$this->getWork("disabled")){
			$btn="	<button onclick='fileForm.cancelFile(event,this)' class='btn btn-primary btn-rounded btn-del-file'><i class='fa fa-times'></i></button>";
		}

		$spanDoc.="<span class='spanFormFile' title='$file' onclick='fileForm.$ev(this)'>
		$btn <i class='fa fa-$ext groupFormFile'></i> $nameFile";

		$pImg=$this->getPath("A_IMMAGINI");
    $noImg=$pImg."/noimg.png";

		if($type=='img'){
				if(!$public){
					$spanDoc.="<img id='img-file-form-$id-$table' onError=\"this.onerror=null;this.src='$noImg'\" class='col-12 img-file-form' src=''  > ";
				}else{
					$table=$this->getWork("table");
					$idForm=$this->getWorkId();
					$filePath=$this->getUtility()->getFileForm()->retrieveFilePublic($table,$id,$idForm,$file);
					$spanDoc.="<img id='img-file-form-$id' onError=\"this.onerror=null;this.src='$noImg'\" class='col-12 img-file-form' src='$filePath'  > ";
				}
		}


		$spanDoc.="</span> ";
		if($type=='img' && !$public ){
			$spanDoc.="<script>fileForm.initImgByClass('$id','$table');</script>  ";
		}

		return $spanDoc;
	}



	public function fileFormGetAcceptFile($acceptFile){
		switch($acceptFile){
			case 'img':
				return ' accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|image/*" ';
			break;

			case 'xlsx':
				return ' accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ';
			break;
		}
	}


	/**
	 * Crea un elemento di tipo file
	 * @param  array  $param [description]
	 * @return [type]        [description]
	 */
	public function newFile(array $param)
	{
		// id - type - valore(da arrya valore della class o come elemento nell'arrya) class style
		//SELEZIONE DATI
		if (empty($param['noDefault'])) {
			$param = $param + $this->BinputModel($param); //aggiungo i valor di default ai parametri
		}
		$this->setParamWork($param); //aggiungo l'array alla variabile di lavoro per la generazione dell'elemento
		$id = $this->getparamId();
		$valore = $this->procVal($id, $type);
		$file=$this->getWork("file");
		$public=$this->getWork("public");
		$accept=$this->getWork("accept");
		if(!empty($valore)){
			$spanDoc=$this->getSpanOpenFile($file,$type,$id,$public);
			$dis="disabled";
		}else{
			$dis="";
			$spanDoc="";
		}
		$table=$this->getWork("table");
		$adTipo = $this->getParamHtml('adTipo', 'type');
		$event = $this->getWork('event');
		$maxLen=$this->getWork('maxLenght');
		if ($maxLen) {
			$maxLen=' maxlength="'.$maxLen.'" ';
		}
		$this->setWork("adClass","fileForm",true);
		$fileType=$type;
		$type="file";
		$this->groupByName();
		$this->mergeClasse();
		$attr=$this->getAttr();
		$class = $this->getParamHtml("class", "class");
		// $preHtml = $this->getWork('preHtml');
		$fineHtml = $this->getWork('fineHtml');
		$style = $this->getParamHtml("style", "style");
		$sezione=$this->getSezione();

		$accept=$this->fileFormGetAcceptFile($accept);



		$input = ($this->getPreInput(). " <input $dis $accept public='$public' sezione='$sezione'
		$attr type='$type'
		 file-type='$fileType'
		 source='$table' $event $class
		$adTipo $maxLen $style id='$id' $valore >" .$spanDoc. $fineHtml);
		$this->printElement($input);
	}



	/**
	* Sezione per attivare l'area scanfile
	* @param  string $class nome della classe che prende il riferimento
	*/
	public function scanFile(string $class="",array $options=[])
	{
		if(empty($class)){
			$class=$this->getUtility()->getCurModulo();//uso il nome dell'area in caso di classe vuota
		}
        $className=$class."ScanFile";

		if ($this->isOnInsert()) {
			$style = 'style="display: none;"';
			$adClass = 'delBtn';
		}else{
			$adClass="";
			$style="";
		}

		$this->addHtml("<div class=' $class $adClass masterFileHandler' $style ondrop='return $className.onDragContainerDrop(event);' ondragend='$className.onDragHtmlEnd(event);' ondragover='".$className.".onDragOverContainerFile(event);'  >"); //div di container
		$this->gDiv(); //breadcrump / e barra upload

		// $this->inDiv(['adClass'=>'row']);


		$this->inDiv(['class'=>""]);
		$this->addHtml("<button title='Torna alla cartella precedente'
		class='btn btn-lg btn-primary btn-nav-upload' onclick='$className.backDir();'><i class='fa fa-reply'></i></button>"); //bottone per tornare sopra
		

		if($this->checkPermesso("inserisci")){ //se non posso inserire non posso neanche caricare file 
		$this->addHtml("<button title='Crea una nuova cartella'
		class='btn btn-lg btn-primary btn-nav-upload'  onclick='$className.newDir();'><i class='fa fa-folder-plus '></i></button>"); //refresh della cartella
		
		$this->addHtml("<span title='Carica un file nell\'area\' class='btn btn-lg btn-primary fileinput-button btn-nav-upload fa fa-file-upload'  for='fileupload'>
		<input id='fileupload-$class'  type='file' name='files[]' multiple>
		</span>"); //riferimetno per l'upload
		}
		
		$this->addHtml("<button title='Ricarica la cartella corrente' class='btn btn-lg btn-primary btn-nav-upload btn-refresh-scanFile'  onclick='$className.apriDir();'><i class='fa fa-redo-alt '></i></button>"); //refresh della cartella
		$this->chiudi_inDiv();
		//barra upload
		$this->inDiv(['adClass'=>'col-md-8 col-lg-8 col-5 ']);
		$this->addHtml("<div id='progressBarUpload-$class' class='progress' style='display: none;'>
		<div class='progress-bar progress-bar-success'></div>
		</div>");
		$this->chiudi_inDiv(); //fine barra


		$this->inDiv(['adClass'=>'row']); //breadcrump di navigazione
		$this->addHtml("<div id='fileBread-$class' class='fileBread' ></div>");
		$this->chiudi_inDiv(); //fine bread

		$this->chiudi_gDiv();//fine bread / barra

		$eventDrop=" onDrop='$className.dropFileHtml();'";

		$this->gDiv(['adClass'=>'fileDragAnimation-'.$class,"event"=>$eventDrop]); //drawer dei file / cartelle
		$this->addHtml("<div class=' scanFile-file-container $class-file-container fileHandler '>"); //file handler
		$this->addHtml("</div> "); //chiusura fileHandler
		$this->chiudi_gDiv(); //chiusura drawner
		$this->addHtml("</div> "); //chiusura containre

		$this->addHtml("<script>  var $className=new ScanFile('$class'); </script>"); //dichiarazione dell'oggetto
		$this->scanFileContextMenu($className,$options);
	}

	/**
	* Crea il menu con il tasto destro sui singoli file / div
	* @param  string $class   [description]
	* @param  array  $options [description]
	* @return [type]          [description]
	*/
	public function scanFileContextMenu(string $class,array $options){
		$add="";


		if($this->checkPermesso("insert")){ //se non posso inserire non posso neanche caricare file 
		 $add.='<li onclick="'.$class.'.contextUpload()"><i class="fa fa-upload" ></i> Upload</li>';
		}

		if($this->checkPermesso("update")){ //se non posso inserire non posso neanche caricare file 
			$add.='			<li onclick="'.$class.'.contextMoveHome()"><i class="fa fa-home" ></i> Sposta in home</li>';
			$add.='      	<li onclick="'.$class.'.contextRename()" ><i class="fa fa-pen-alt"></i> Rinomina</li>';
		}
		
		if($this->checkPermesso("delete")){ //se non posso inserire non posso neanche caricare file 
			$add.='			<li onclick="'.$class.'.contextDelete()"><i class="fa fa-trash-alt" ></i> Elimina</li>';
		}

		$this->addHtml('
		<div class="contextMenu contextMenuFile">
		<ul>
			<li onclick="'.$class.'.contextApri()" ><i class="fa fa-external-link-alt"></i> Apri</li>'.$add.'
			<li onclick="'.$class.'.contextBackDir()"><i class="fa fa-arrow-left" ></i> Indietro</li>
		</ul>
		</div>
		');
	}
}
