<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class SendMail extends WrapperUtility
{
    public $msg; //messaggio in lavorazione
    private $confName="";//configurazione da caricare. Vuoto prende il default
    private $forceConf=false; //forza a prendere la vera connessione anche in debug
    //flag - variabili di servizio
    private $nMail = 0;
    private $ultimoErrore;
    private $autoHeader=null;
    private $autoFooter=null;

    public function __construct(Utility $ut){
      parent::__construct($ut);

      $this->autoFooter=$this->getUtility()->getIni("mailAutoFooter");
      $this->autoHeader=$this->getUtility()->getIni("mailAutoHeader");

    }


    /**
    * Alloca e torna il messaggio creato
    * @return PHPMailer oggetto per l'invio mail
    */
    public function getMsg():PHPMailer
    {
        if (empty($this->msg)) {
            // $dir=$this->getUtility()->getPath('A_VENDOR');
            // require $dir.'PHPMailer/src/Exception.php';
            // require $dir.'PHPMailer/src/PHPMailer.php';
            // require $dir.'PHPMailer/src/SMTP.php';
            $this->msg = new PHPMailer();
        }
        return $this->msg;
    }

    /**
    * Controlla se la mail inserita è valida
    * @param  string $mail indirizzo mail
    * @return bool  esito controllo
    */
    private function checkValidMail(string $mail):bool
    {
        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            $this->addError("Errore aggiunta mail BCC " . $mail);
            return false;
        } else {
            return true;
        }
    }

    public function addCC(string $mail):bool
    {
        if ($this->checkValidMail($mail)) {
            $this->getMsg()->addCC($mail);
            $this->nMail++;
            return true;
        } else {
            return false;
        }
    }


    /**
    * Aggiunta indirzzo per l'invio in bcc
    * @param string $mail mail da aggiungere
    */
    public function addBcc(string $mail):bool
    {
        if ($this->checkValidMail($mail)) {
            $this->getMsg()->addBcc($mail);
            $this->nMail++;
            return true;
        } else {
            return false;
        }
    }

    public function setSubject(string $oggetto){
        $this->Subject=$oggetto;
        $this->getMsg()->Subject=$oggetto;
    }

    /**
    * Aggiunge un indirzzo alla classe
    * @param string $mail indirizzo mail
    */
    public function addAddress(string $mail):bool
    {
        if ($this->checkValidMail($mail)) {
            $this->getMsg()->AddAddress($mail);
            $this->nMail++;
            return true;
        } else {
            return false;
        }
    }

    /**
    * Inizializza una nuova mail
    * @param  string $oggetto   oggetto della mail
    * @param  string $corpo     corpo
    * @param  string $indirizzo Indirizzo, opzionale
    * @return void
    */
    public function initMail(string $oggetto, string $corpo, string $indirizzo=""):void
    {
        $mail=$this->getMsg();
        $mail->Subject = $oggetto;
        $mail->Body = $corpo;
        if (!empty($indirizzo)) {
            $this->addAddress($indirizzo);
        }
    }

    /**
    * Log di errore
    * @param string $errore Errore ineserito
    */
    private function addError(string $errore):void
    {
        $this->ultimoErrore = $errore;
        $this->getUtility()->warning("Invio mail", $errore);
    }

    /**
    * Ritorna la lista di errori
    * @return string [description]
    */
    public function getError():string
    {
        return $this->ultimoErrore;
    }

    /**
    * Imposta la configurazione per la connessione
    * @param string $confName nome configurazione
    */
    public function setConfName(string $confName,bool $force=false):void
    {
        $this->confName=$confName;
        $this->forceConf=$force;
    }

    /**
    * torna il nome della configurazine attuale
    * @return string torna la configurazione name
    */
    private function getConfName():string
    {

        if($this->forceConf==false){
            if ($this->getUtility()->ifDebug()) {
                $this->warning("configurazione smtp","Sono in debug, carico di default la dev conf");
                return "dev";
            }
        }
        return $this->confName;
    }

    /**
    * Carica la configurazione per l'invio smtp
    * @return bool esito configurazione
    */
    private function loadConf():bool
    {
        $msg=$this->getMsg();
        $conf=$this->getConfName();


        $this->query(" SELECT nomeMittente,email,username,password,ServerSMTP,porta,authType from configurazione_smtp
            WHERE descrizione='$conf' limit 1 ");
            if ($this->getUtility()->isDatasetEmpty()) {
                $this->addError("configurazione $conf non trovata ");
                return false;
            }
            $row = $this->fetch(); //load dei dati da db
            //impostazioni standard
            $msg->IsSMTP(); // Utilizzo della classe SMTP al posto del comando php mail()
            $msg->SMTPAuth = true; // Autenticazione SMTP
            $msg->SMTPKeepAlive = "true";
            $msg->CharSet = 'UTF-8';
            $msg->SMTPDebug = 0;
            $msg->IsHTML(true);

            //bind dei dati da db
            $msg->Username=$row['username'];
            $msg->Password=$this->getUtility()->decrypt($row['password']);
            $msg->Host = $row['ServerSMTP'];
            $msg->SMTPSecure =$row['authType'];
            $msg->Port = $row['porta'];
            $msg->From = $row['email'];
            $msg->FromName = $row['nomeMittente'];
            return true;
        }

        /**
         * Aggiunge un'immagine per usarla nell'html
         * @param string $img
         * @param string $name
         */
        public function addImage(string $img,string $name="img",string $imgName=""){
          if(!file_exists($img)){
            $this->warning("ATTENZIONE $img non trovata");
          }
          $this->getMsg()->AddEmbeddedImage($img, $name,$imgName);
        }

        /**
         * Aggiunge un allegato all'invio mail
         * @param string $file     [description]
         * @param string $fileName [description]
         */
        public function addAttachment(string $file,string $fileName){
          $this->getMsg()->AddAttachment($file,$fileName);
        }

        /**
         * Imposta il body della mail con la stringa
         * @param string $body [description]
         */
        public function setBody(string $body){
            $mail=$this->getMsg();
            $mail->Body=$body;
        }


        public function getBody():string{
          return  $this->getMsg()->Body;
        }

        /**
         * Aggiunge html al corpo della mail
         * @param string $html [description]
         */
        public function addBody(string $html){
          $mail=$this->getMsg();
          $mail->Body.=$html;
        }


        public function isAutoHeader(){
          return $this->autoHeader;
        }

        public function isAutoFooter(){
          return $this->autoFooter;
        }


        public function loadView(string $viewName,array $param=[],array $option=[] ){
          $html=parent::loadView($viewName,$param,$option);
          $this->addBody($html);
          $this->getUtility()->info("view $viewName caricata per la mail");
        }

        public function autoHtml():void{

          $body=$this->getBody();
          $css=$this->getUtility()->loadCssG("mail/mail");
          if($this->isAutoHeader()){
            $this->addImage($this->getPath("I_img")."logo1.png","logo","logo.png");
            $body=$this->getUtility()->loadViewG("mail/mail-header").$body;
          }

          if($this->isAutoFooter()){
            $body.=$this->getUtility()->loadViewG("mail/mail-footer");
          }

          $this->setBody($css.$body);
        }


        /**
        * Inivio della mail
        * @return [type] [description]
        */
        public function send():bool
        {

          $this->autoHtml();

            if ($this->nMail == 0) { //contorllo se qualche indirizzo c'è
                $this->addError("nessun indirizzo valido");
                return false;
            }
            if (!$this->loadConf()) { //init della configurazione mail
                return false;
            }
            $msg=$this->getMsg();
            try {
                if (!$msg->Send()) { //invio effettivo della mail
                    $this->addError("errore nella spedizione: " . $this->msg->ErrorInfo);
                    return false;
                } else {
                    $this->getUtility()->info("Mail inviata con successo")  ;
                    return true;
                }
            } catch (Exception $e) {
                $this->addError('Message could not be sent. Mailer Error: ', $mail->ErrorInfo);
                return false;
            }
        }//end sendMail
    } //fine classe
