<?php

//libreria per il caricamento / scaricamento dei file diretti da form

class FileForm extends wrapperUtility{


  public function getUploadDataPath():string{
    return $this->getUtility()->getPath("A_UPLOAD_FORM_DATA");
  }

  public function getUploadPublicDataPath():string{
    return $this->getUtility()->getPath("A_UPLOAD_PUBLIC_FORM_DATA");
  }



  public function getPublicPathForTab($tab,$col){
    return $this->getWebIdPath($tab,$col,"%sost-id");

  }

  private function buildPath(string $tab,string $col,string $id):string{
      return $tab."/".$id."/".$col."/";
  }

  public function getUploadIdPath(string $tab,string $col,string $id,$public=false):string{
    if(!$public){
      $base=$this->getUploadDataPath();
    }else{
      $base=$this->getUploadPublicDataPath();
    }
    return $base.$this->buildPath($tab,$col,$id);
  }

  public function getWebIdPath(string $tab,string $col,string $id){
    $base=$this->getUtility()->getPath("W_UPLOAD_PUBLIC_FORM_DATA");
    return $base.$this->buildPath($tab,$col,$id);
  }

  public function checkFileFromPath(string $path):string{

  }

  public function checkFileExist(string $tab,string $col,string $id):string{

  }

  private function checkFileName(string $file){
    if(strpos($file,"../")!==false){
      $this->error("FILE INVALIDO");
    }

    if(strpos($file,"/..")!==false){
      $this->error("FILE INVALIDO");
    }

  }



    public function retrieveFilePublic(string $source,string $col,string $id,string $file){
      $this->checkFileName($file);
      $storeFile=$this->getUploadIdPath($source,$col,$id,true).$file;
      if(!file_exists($storeFile)){
        if($this->getUtility()->ifDebug()){
          $file=$storeFile;
        }
        $this->json("retrieveFile","File $file non trovato");
        // $this->halt();
        // return false;
      }


      return $this->getWebIdPath($source,$col,$id).$file;
    }




  public function retrieveFile(string $source,string $col,string $id,string $file,$public=false){
    $this->checkFileName($file);
    $this->checkPermesso($source,"visualizza");

    $storeFile=$this->getUploadIdPath($source,$col,$id,$public).$file;
    if(!file_exists($storeFile)){
      $this->json("retrieveFile","File $file non trovato");
      return false;
    }
    return $storeFile;
  }

  public function deleteOldFile($source,$col,$id,$file,$public=false){
    $storeFile=$this->retrieveFile($source,$col,$id,$file,$public);
    if(!$storeFile){
      $this->warning("errore caricamento file");
      return false;
    }
    return unlink($storeFile);
  }


  public function retrieveHeaderImg(string $source,string $col,string $id,string $file,$public=false){
    $storeFile=$this->retrieveFile($source,$col,$id,$file,$public);
    if(!$storeFile){
      $this->warning("errore caricamento img");
      return false;
    }
    $this->getUtility()->getHeaderImg($file,$storeFile);
    return true;
  }


  public function retrieveHeaderFile(string $source,string $col,string $id,string $file,$public=false){
    $storeFile=$this->retrieveFile($source,$col,$id,$file,$public);
    if(!$storeFile){
      $this->warning("errore caricamento file");
      return false;
    }
    $this->getUtility()->getHeaderFile($file,$storeFile);
    return true;
  }

  public function uploadDocManual(string $colonna,string $section,string $id,bool $public){
    $files=$_FILES;
    $success=true;
    $section=strtolower($section);
    foreach($files as $file){
      foreach($file['tmp_name'] as $i=>$tmp ){
        $name=$file['name'][$i];
        $dirFormData=$this->getUploadIdPath($section,$colonna,$id,$public);
        $this->getUtility()->makeDir($dirFormData);
        $this->checkFileName($name);
        if(!move_uploaded_file($tmp,$dirFormData.$name)){
          $success=false;
        }else{
          $success=$name;
        }
      }
    }//end foreach
    return $success;
  }

  public function uploadDocForm(){
    $files=$_FILES;
    $colonna=$this->post("input");
    $section=$this->post("section");
    $public=$this->post("public");
    $id=$this->post("id");
    //$this->getUtility()->getIdTokenField(); AL MOMENTO NON è persistente
    $success=true;
    // $section=strtolower($section);

    foreach($files as $file){
      foreach($file['tmp_name'] as $i=>$tmp ){
        $name=$file['name'][$i];
        $dirFormData=$this->getUploadIdPath($section,$colonna,$id,$public);
        $this->getUtility()->makeDir($dirFormData);
        $this->checkFileName($name);
        if(!move_uploaded_file($tmp,$dirFormData.$name)){
          $success=false;
        }
      }
    }//end foreach
    if($success){
      $this->success("File caricati con successi");
    }else{
      $this->error("IMPOSSIBILE CARICARE I FILE - $name");
    }
    $this->halt();
  }


}
