<?php
/*TRAIT contenente tutti i metodi di accesso rapido all'array in costruzione
Tutti i valori sono accessibili tramite i metodi principali $field->setArVar, queste funzioni fanno esattamente la stessa cosa ma con più ordine
e in modo più elegante.
*/

trait multiFieldSet
{

  private $autoCloseSchedaElements = true;
  private $setUpComandiScheda = []; //definisce l'array del printcomandi alla fine di una scheda da form

  //comandi fighi


  /**
   * Setup dei comandi per printComandi automatico
   * @param array $arr [description]
   */
  public function setUpComandi(array $arr)
  {
    $this->setUpComandiScheda = $arr;
  }


  /**
   * Impossta il valore required nel
   * @param  [type] $nome parametro arParam
   */
  public function arRequired(string $nome)
  {
    $this->arAdClass($nome, 'required');
    $this->setArVar($nome, "required", true);
  }


  /**
   * Estende il campo su un altro form in pagina (Voglio salvare lo stesso dato in due tabelle diverse)
   */
  public function arBindToForm(string $nome,$bind){
    $this->setArVar($nome,"bindForm",$bind);
  }


  public function arOpenModule(string $id, string $module = "",$span="")
  {
    $this->arType($id, "openModule");
    if (empty($module)) {
      $module = str_replace("id", "", $id);
    }

    $module=ucfirst($module);

    $this->setArVar($id, "modulo", $module);
    if(empty($span)){
      $span=$module;
    }
    $this->setArVar($id, "span", $span);
    
  }


  public function arRegex(string $ele, $regex, $opt = [])
  {
    $this->setArVar($ele, "regex", $regex);
    if (!empty($opt['no-space'])) {
      $this->setArVar($ele, "regex-no-space", true);
    }
  }



  public function arBeforePrintCallback(string $ele, $function)
  {
    $this->setArVar($ele, 'afterPrintCallback', $function);
  }

  public function  arAfterPrintCallback(string $ele, $function)
  {
    $this->setArVar($ele, 'afterPrintCallback', $function);
  }

  public function  arAfterPrintContainerCallback(string $ele, $function)
  {
    $this->setArVar($ele, 'afterPrintContainerCallback', $function);
  }


  public function setVarAllElements($method, $data = null)
  {
    foreach ($this->arParam as $key => $val) {
      $this->$method($key, $data);
    }
  }


  public function arAdminElement($elem){
    if(is_array($elem)){
      foreach($elem as $e){
        $this->arAdmin($e);
      }
    }

    $this->setArVar($elem,"destroyIfNotAdmin",true);

  }


  public function arReadOnly(string $elem)
  {
    $this->setArVar($elem, "readonly", true);
    $attr = $this->getArVar($elem, "attr");
    $this->setArVar($elem, "attr", $attr . " readonly='true' enabled='false' disabled ");
  }



  public function arDisabled($elem)
  {
    $attr = $this->getArVar($elem, "attr");
    $this->setArVar($elem, "attr", $attr . " readonly='true' enabled='false' disabled ");
    $this->setArVar($elem, "disabled", true);
  }


  public function arHtml(string $param, $html)
  {
  }

  /**
   * Aggiunge una classe dove serve
   * @param  string $nome   elemento che deve ricevere la classe
   * @param  string $classe classe/i da aggiungere
   */
  public function arAdClass(string $nome, string $classe)
  {
    $app = $this->getArVar($nome, 'adClass');
    $app .= " $classe";
    $this->setArVar($nome, 'adClass', $app);
  }


  /**
   * Imposta il campo per l'inserimento
   */
  public function arInsert(string $col)
  {
    $this->setArVar($col, 'insert', '1');
  }

  /**
   * Imposta il campo per disabilitare il regex / automatico e non
   * @return [type] [description]
   */
  public function arNoRegex(string $col): void
  {
    $this->setArVar($col, "noRegex", "1");
  }

  /**
   * Imposta il campo per fare l'inserimento
   */
  public function arNotInsert(string $col)
  {
    $this->setArVar($col, 'insert', '0');
  }


  /**
   * Imposta il campo per l'update
   */
  public function arUpdate(string $col)
  {
    $this->setArVar($col, 'update', '1');
  }


  /**
   * Imposta il campo per non fare l'update
   */
  public function arNotUpdate(string $col)
  {
    $this->setArVar($col, 'update', '0');
  }

  /**
   * Apre il div automatico per ogni elemento nell'array
   * @param  array  $fields [description]
   * @return [type]         [description]
   */
  public function arApriDivArray(array $fields)
  {
    foreach ($fields as $f) {
      $this->arApriDiv($f);
    }
  }

  /**
   * Apre il paragrafo per il div selezionato
   * @param  string $nome   Nome del parametro
   * @param  string $titolo titolo "opzionale"
   * @param  string $param  parametro "opzionale"
   */
  public function arApriDiv($nome, string $titolo = "", $param = [])
  {
    if (is_array($nome)) {
      foreach ($nome as $n) {
        $this->arApriDiv($n, $titolo, $param);
      }
      return;
    }

    $this->setArVar($nome, 'apriDiv', '1'); //apre un gDiv in automatico
    if ($titolo != "") { //se settato apre anche il titolo
      $this->arApriDivTitle($nome, $titolo);
    }
    if (!empty($param)) { //imposta i parametri del gdiv
      $this->arApriDivParam($nome, $param);
    }
  }

  /**
   * Speculare di arApriDiv. Impedisce di aprire un gDiv Automatico
   * @param  string $nome [description]
   * @return [type]       [description]
   */
  public function arNotApriDiv(string $nome)
  {
    $this->setArVar($nome, 'apriDiv', '0'); //NON apre un gDiv in automatico
  }

  /**
   * Alias per apriDiv con il titolo in automatico
   * @param  [type] $nome  parametro da aprire
   * @param  [type] $title titolo da mostrare
   */
  public function arGDivTitle(string $nome, string $title)
  {
    $this->arApriDivTitle($nome, $title);
  }


  /**
   * modifica l'attr dell'elemento
   */
  public function arAttr(string $col, string $attr)
  {
    $this->setArVar($col, "attr", $attr);
  }
  /**
   * Titolo del gDiv automatico
   * @param  [type] $nome  [description]
   * @param  [type] $title [description]
   * @return [type]        [description]
   */
  public function arApriDivTitle(string $nome, string $title)
  {
    $app = $this->getGdivParam($nome);
    $app['title'] = $title;
    $this->setArVar($nome, 'apriDivParam', $app);
  }

  /**
   * Torna il parametro del gDiv dell'elemento scleto
   * @param  string $nome parametro richiesto
   * @return array
   */
  private function getGdivParam(string $nome): array
  {
    $app = $this->getArVar($nome, 'apriDivParam');
    if (empty($app)) {
      $app = [];
    }
    return $app;
  }

  /**
   * Imposta il parametro del gdiv
   * @param  string $nome  elemento richiesto
   * @param  array $param parametro da aggiungere
   */
  public function arApriDivParam(string $nome, array $param)
  {
    $app = $this->getGdivParam($nome);
    $app = array_merge($app, $param);
    $this->setArVar($nome, 'apriDivParam', $app);
  }

  /**
   * Imposta la classe del gDiv
   * @param  [type] $nome   [description]
   * @param  [type] $classe [description]
   * @return [type]         [description]
   */
  public function arApriDivClass(string $nome, string $classe)
  {
    $app = $this->getGdivParam($nome);
    $app['adClass'] = $classe;
    $this->setArVar($nome, 'apriDivParam', $app);
  }

  /**
   * Carica le classe di grid Bootstrap sull'elemento
   * @param $col colonna su cui aggiungere la grid
   */
  public function arGrid($col)
  {
    $grids = func_get_args();
    if (is_array($col)) {
      array_shift($grids);
      foreach ($col as $c) { //do la stessa griglia a più colonne - sono pigro e lo ricopio, dovrei usare il func_get_args ma non è andato al primo colpo. arrrgggh 
        $class = $this->getArDiv($c, "grid");
        if (empty($class)) {
          $class = "";
        }
        foreach ($grids as $grid) {
          $class .= " col-$grid ";
        }
        $this->setArDiv($c, "grid", $class);
      }
      return;
    }

    $class = $this->getArDiv($col, "grid");
    if (empty($class)) {
      $class = "";
    }
    foreach ($grids as $grid) {
      $class .= " col-$grid ";
    }

    $this->setArDiv($col, "grid", $class);
  }


  /**
   * Imposta la label per l'elemento
   * @param  string $nome  parametro da modifare la label
   * @param  string $label valore della label
   */
  public function arLabel(string $nome, string $label, string $subLabel = "")
  {
    $this->setArVar($nome, 'label', $label);
    if (!empty($subLabel)) {
      $this->setArVar($nome, "subLabel", $subLabel);
    }
  }

  public function arSuggest(string $nome, string $dataSource = "")
  {
    $this->arType($nome, "search");
    if (!empty($dataSource)) {
      $this->setArVar($nome, "dataSource", $dataSource);
    }
    $this->setArVar($nome, "suggest", true);
  }



  public function arPassword(string $nome)
  {
    $this->arType($nome, "password");
  }

  public function arSearch(string $nome, string $dataSource = "")
  {
    $this->arType($nome, "search");
    if (!empty($dataSource)) {
      $this->setArVar($nome, "dataSource", $dataSource);
    }
  }

  /**
   * Imposto il tipo dell'emento nell'array Param - alias di arType
   * @param  string $nome nome specificato
   * @param  string $tipo tipo specificato
   */
  public function arTipo($nome, string $tipo)
  {
    $this->arType($nome, $tipo);
  }
  /**
   * Imposta il tipo per l'elemento
   * @param  string $nome parametro scelto
   * @param  string $tipo tipo da impostare
   */
  public function arType($nome, string $tipo)
  {

    if (is_array($nome)) {
      foreach ($nome as $n) {
        $this->arType($n, $tipo);
      }
      return;
    }

    if ($tipo == 'search') {
      $this->arGrid($nome, "6", "md-4");
    }
    $this->setArVar($nome, 'type', $tipo);
  }


  /**
   * Imposta il parametro public per l'elemento
   * @param  string $nome parametro scelto
   * @param  string $tipo tipo da impostare
   */
  public function arPublic(string $nome, bool $pub = true)
  {
    $this->setArVar($nome, 'public', $pub);
  }

  /**
   * Imposta l'elemento come editor wysiwyg
   */
  public function arWysiwyg(string $nome): void
  {
    $this->arType($nome, "wysiwyg");
  }


  /**
   * imposta la classe dell'input div automatico generato dall'elemento - funziona solo se attivo au
   * @param  string $nome   [description]
   * @param  string $classe [description]
   */

  public function arDivAdClass(string $nome, string $classe)
  {
    $app = $this->getArDiv($nome, 'adClass');
    $app .= " $classe";
    $this->setArDiv($nome, 'adClass', $app);
  }


  public function getArDiv($ele, $attr)
  { //funzione per ottnere il div generato automaticamente di un elemento dell'array globale
    if (isset($this->arParam[$ele]['paramAutoDiv'][$attr])) {
      return $this->arParam[$ele]['paramAutoDiv'][$attr];
    } else {
      return "";
    }
  }

  public function setArDiv($ele, $attr, $var)
  { //funzione per modficare il div generato automaticamente di un elemento dell'array globale
    $param[$attr] = $var;
    $this->arParam[$ele]['paramAutoDiv'] = $param;
  }



  /**
   * Imposto l'evento da bindare al parametro
   * @param  string $nome  nome parametro
   * @param  string $event evento da bindare
   */
  public function arEvent(string $nome, string $event)
  {
    $this->setArVar($nome, 'event', $event);
  }

  /**
   * Imposta il dataset per i campi select
   * @param  [type] $nome [description]
   * @param  [type] $res  [description]
   * @return [type]       [description]
   */
  public function arRes(string $nome, $res)
  {
    if (is_array($res)) {
      $this->arResArray($nome, $res);
      return;
    }
    $this->setArVar($nome, 'res', $res);
  }

  public function arResArray(string $nome, array $ar)
  {
    $this->setArVar($nome, 'resArray', $ar);
  }




  public function setRadioSi(string $nome)
  {
    $res['0'] = ['id' => '0', 'val' => 'No'];
    $res['1'] = ['id' => '1', 'val' => 'Sì'];
    $this->setRadio($nome, $res);
  }


  /**
   * Impostazione del select da una query
   * @param [type] $nome  [description]
   * @param [type] $query [description]
   */
  public function setRadio(string $nome, $res)
  {
    $this->arType($nome, 'radio');
    if (is_array($res)) {
      $this->arResArray($nome, $res);
    } else {
      $this->getUtility()->query($res);
      $data = $this->getUtility()->fetchAll();
      $this->arResArray($nome, $data);
    }
  }

  public function arCheckbox($id)
  {
    $this->arType($id, "checkbox");
  }

  public function setMultiCheckbox(string $nome, $res)
  {
    $this->arType($nome, 'multiCheckbox');
    if (is_array($res)) {
      $this->arResArray($nome, $res);
    } else {
      $this->getUtility()->query($res);
      $data = $this->getUtility()->fetchAll();
      $this->arResArray($nome, $data);
    }
  }


  /**
   * Torna il valore del parametro in arParam
   * @param  [type] $col [description]
   * @return [type]      [description]
   */
  public function getValore(string $col)
  {
    $res = $this->getArVar($col, 'val');
    if (empty($res)) {
      return "";
    } else {
      return $res;
    }
  }

  /**
   * Imposta il valore per il campo specificato
   * @param string $col   campo scelto
   * @param string $param valore da assegnare visto poi nell'input
   */
  public function setValore(string $col, string $param)
  {
    $this->setArVar($col, 'val', $param);
  } 


  public function arSelect(string $nome){
    $this->setSelect($nome);
  }


  public function arSelectModel(string $nome,$model){
    $this->setSelect($nome,null,["model"=>$model]);
  }



  public function arLabelValue(string $nome,string $val=""){
    $this->arType($nome,"labelValue");
    if(!empty($val)){
      $this->setValore($nome,$val);
    }
  }

  public function arLabelValueDate(string $nome,string $val=""){
    if(empty($val)){
      $val=$this->getValore($nome);
    }
    $date=$this->getUtility()->dateFormatEu($val);
    $this->arLabelValue($nome,$date);
  }

  


  /**
   * Impostazione del select da una query
   * @param [type] $nome  [description]
   * @param [type] $query [description]
   */
  public function setSelect(string $nome,  $res = "",array $options=[])
  {
    $this->arType($nome, 'select');
    if (empty($res) || !empty($options['model']) ) { //nessuna query definita, estraggo il modello ed esegue la query standard
      if(!empty($options['model'])){
        $model=$options['model'];
      }else{
        $model=$nome;
      }
      $model=str_replace("id", "", str_replace("id_", "", $model));

      $model=$this->getModel($model);      
      $model->dsSelect();
      $ds = $this->getUtility()->getDataset();
      $this->arRes($nome, $ds);
      return;
    }

    if (is_array($res)) {
      $this->arResArray($nome, $res);
    } else {
      if (strpos(strtolower($res), "select") === false) {
        $res = "SELECT * FROM $res  ";
      }
      $this->arRes($nome, $this->getUtility()->query($res));
    }
  }

  public function arSelectType(string $nome, string $type = "")
  {
    $this->setArVar($nome, "selectType", $type);
  }

  /**
   * Imposta un valore di default sulla select
   * @param  string $nome  campo da utilizzare
   * @param  string $param valore del defualt
   */
  public function arDefSelect(string $nome, string $param = "--"): void
  {
    if (empty($param)) {
      $param = "--";
    }
    $this->setArVar($nome, "defNull", $param);
  }

  /**
   * Imposta il campo per avere l'evento dataJ
   */
  public function arDataJ(string $id, string $dataSource = "1")
  {
    $this->setArVar($id, "eventDataJ", true);
    $this->setArVar($id, "dataSourceCompile", $dataSource);
    $this->arBindDataJ($id);
  }


  public function arBindDataJ($id)
  {

    // $type = $this->getArVar($id, "type");
    // if ($type == "search") {
    //   $event = "blur";
    // } else {
      $event = "change";
    // }

    $sez = $this->getSezione();

    $timer = $this->getTimerAfter($id);

    $this->arAftetHtml($id, "<script>
    $('.$id-$sez').$event( function(){
    var elem=this;
    setTimeout(function(){
    getDataJ.changeInput(elem);
    },$timer);
  }  );  </script> ");
  }



  /**
   * Imposta una subSelect (select dipendete da un altor )
   * @param [type] $nome   [description]
   * @param [type] $parent [description]
   * @param [type] $query  [description]
   */
  public function setSubSelect(string $nome, string  $parent, string $query = ""): void
  {
    $this->arTipo($nome, 'select');
    if (empty($query)) {

      $val = $this->getValore($parent);
      $res = $this->getUtility()->getSubSelectRes($nome,$parent,$val);

      $this->arRes($nome, $res);

    } else {
      $this->arRes($nome, $this->getUtility()->query($query));
    }
    $sez = $this->getSezione();
    // $this->arEvent($parent, 'onchange="getProto().reloadSubSelect(this,\'' . $nome . '\',\'' . $sez . '\');" ');
    // $this->arEvent($parent, 'onchange="getProto().reloadSubSelect(this,\'' . $nome . '\',\'' . $sez . '\');" ');
    $this->arBindReloadEvent($parent, $nome);
  }

  
  /**
   * In caso di molte chiamate ritardo un pochino per rendere fluida l'interfaccia
   * 
   * $id dell'elemento che richiede un event in afterHtml
   */
  public function getTimerAfter($id)
  {
    $count = $this->getArVar($id, "countSubSelect");
    if (empty($count)) {
      $count = 1;
    }
    $timer = $count * 100;
    $count++;
    $this->setArVar($id, "countSubSelect", $count);
    return $timer;
  }

  public function arBindChange(string $id,string $event){
    $sez = $this->getSezione();

    $this->arAftetHtml($id, "<script>
    console.log('start bind');
    $('.$id-$sez').change(function(){
      $event
    });    
    </script>");
    
  
    // $('.$parent-$sez').$event( function(){
    //   var elem=this;
    //   setTimeout(function(){
    //   reloadSub.reloadSubSelect(elem,'$sub','$sez');
    //   },$timer);
    // }  );  </script> ");

  }

  public function arBindReloadEvent($parent, $sub)
  {
    $sez = $this->getSezione();

    $event="   
    console.log('change');
     var elem=this;
     setTimeout(function(){
     reloadSub.reloadSubSelect(elem,'$sub','$sez');
     },30);
    ";

    $this->arBindChange($parent,$event);




    // $type = $this->getArVar($parent, "type");
    // if ($type == "search") {
    //   $event = "blur";
    // } else {
      // $event = "change";
    // }

  //   $timer = $this->getTimerAfter($parent);
  //   $this->arAftetHtml($parent, "<script>  $('.$parent-$sez').change( function(){
  //   var elem=this;
  //   setTimeout(function(){
  //   reloadSub.reloadSubSelect(elem,'$sub','$sez');
  //   },$timer);
  // }  );  </script> ");
  }


  public function arAftetHtml($id, $html)
  {
    $old = $this->getArVar($id, "fineHtml");
    if (!empty($old)) {
      $html = $old . "\n" . $html;
    }
    $this->setArVar($id, "fineHtml", $html);
  }

  /**
   *apre una scheda - attenzione deve essere impostata prima! altrimenti brutte cose succedono!
   * @param  string $nome   nome parametro che deve aprire la scheda
   * @param  string $scheda nomeScheda fssata prima
   */
  public function arApriScheda(string $nome, string $scheda = "", $autoCloseSchedaElements = true): void
  {
    if (empty($scheda)) {
      $scheda = $nome;
    }
    $this->setArVar($nome, 'apriScheda', $scheda);
    $this->arApriDiv($nome);

    $this->autoCloseSchedaElements = $autoCloseSchedaElements;
  }

  /**
   * Setta il valore da modificare per il parametro
   * @param  string $nome parametro da modificare
   * @param  string $value valore da modificare
   */
  public function arValue(string $nome, $value)
  {
    $this->setArVar($nome, 'val', $value);
  }



  /**
   * Torna il valore per il parametro
   * @param  string $nome parametro da modificare
   * @param  string $value valore da modificare
   */
  public function getArValue(string $nome)
  {
    return $this->arParam[$nome]['val'];
  }


  /**
   * Imposta l'elemento per i comandi rapidi
   * @param  string $ele [description]
   */
  public function apriEle(string $ele)
  { //routine per l'elaborazione in cascata dei valori (apri elemento set css event ecc chiudi)
    $this->elOpen = $ele;
  }

  /**
   * Chiudi l'elemento aperto
   */
  public function chiudiEle()
  { //routine per l'elaborazione in cascata dei valori (Chiude l'elemento aperto)
    $this->elOpen = null;
  }

  /**
   * Imposta il parametro per l'elemento aperto
   * @param [type] $col [description]
   * @param [type] $var [description]
   * @param string $ele [description]
   */
  public function setEle(string $col, string $var, string $ele = '')
  { // funzione per settare l'elemento in modifica -- settato da apriEle, se il terzo parametro è impostato viene impostato automaticamente
    if ($ele != "") {
      $this->apriEle($ele);
    }
    if ($this->elOpen == null) {
      $this->warning("eleOpen- elementi", "nessun elemento aperto");
      $this->halt();
    }
    $this->setArVar($this->elOpen, $col, $var);
  }

  public function setEleDiv($col, $var, $ele = '')
  {
    if ($ele != "") {
      $this->apriEle($ele);
    }
    if ($this->elOpen == null) {
      echo "nessun elemento aperto";
      return 0;
    }
    $this->setArDiv($this->elOpen, $col, $var);
  }

  public function arDivClass($col, $css)
  {
    $this->setArDiv($col, "adClass", $css);
  }

  /**        IMPOSTAZIONI DI DEFAULT ****/


  public function arDefault($col, $defVal)
  {
    $val = $this->getValore($col);
    if ($val == '' || $val == null) {
      $this->setValore($col, $defVal);
    }
  }


  /**
   * Imposta l'evento di default sull'array arParam
   * @param [type] $event [description]
   */
  public function setDefEventAr($event)
  {
    $this->setDefArField(['event' => $event]);
  }

  //imposta i valori di default per i campi aggiunti manualmente
  public function setDefArField($arr)
  {
    $this->defArField = array_merge($this->defArField, $arr);
  }



  public function arReverseRadio(string $element, string $pre = "", string $after = "")
  {
    $this->setArParam($element, "style", "float:none");
    $this->setArParam($element, "reverse", true);
    $this->arApriDivClass($element, "reverseRadio");
    $this->setArParam(
      $element,
      "preHtml",
      "<span style='float:left;margin-top: 30px;margin-right: 20px;'>$pre</span>"
    );
    $this->setArParam(
      $element,
      "fineHtml",
      "<span style='float:left;margin-top: 30px;margin-right: 20px;'>$after</span>"
    );
  }


  public function arCustomInput(string $element)
  {
    $this->setArParam($element, "customInput", true);
  }

  public function arAllElement(string $event, $param)
  {
    foreach ($this->arParam as $ele => $key) {
      $this->$event($ele, $param);
    }
  }

  public function arBtn($id,$html="",$click="",$ico=""){
    $this->arType($id,"btn");
    $this->setArVar($id,"adClass","centerFormBtn");
    $this->setArVar($id,"html",$html);
    $this->setArVar($id,"click",$click);
    $this->setArVar($id,"ico",$ico);
  }


  public function arIco(string $element, string $icon)
  {
    $this->arIcon($element, $icon);
  }

  public function arIcon(string $element, string $icon)
  {
    $icon = str_replace("fa-", "", $icon);
    $this->setArVar($element, "fa-icon", $icon);
  }
} //fine trait
