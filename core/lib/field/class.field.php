<?php



include_once __DIR__ . '/trait/trait.SetGet.php'; //metodi di set / get degli attributi dell'oggetto
include_once __DIR__ . '/trait/trait.modelli.php'; //funzioni con modelli
include_once __DIR__ . '/trait/trait.field.php'; //generazione effettiva degli input HTML5 (textArea / combobox ecc)
include_once __DIR__ . '/trait/trait.div.php'; //generazione dei div standard
include_once __DIR__ . '/trait/trait.multiField.php'; //metodi per il caricamento della vista tramite array / dataset
include_once __DIR__ . '/trait/trait.multiFieldSet.php'; //metodi set / get attributi del multifield
include_once __DIR__ . '/trait/trait.tokenForm.php'; //registrazione del token-form  in sessione
include_once __DIR__ . '/trait/trait.comandi.php'; //bottoni standard e ragruppati
include_once __DIR__ . '/trait/trait.file.php'; //sezioni per il controllo file come scanFile


/**
* Classe per la creazione di html in base a dei parametri partendo da parametri di markup o dataset
*/
class Field extends WrapperUtility
{
    use newField,modelli,
    multiField,SetGet,multiFieldSet,div,tokenForm,
    customElement,comandi,fieldFile; //treat con i metodi di base personalizzabili

    private $html = ""; //variabile con l'html composto dai vari metodi
    public $paramWork=[]; //parametri dell'elemento in lavorazione
    private $elOpen = null; //elemento aperto
    private $showElimina = false; //caricato durante il printElemntsFromArray

    private $newElementObject; //variabile per la creazione dell'elemento statico a oggetti

    private $buffer; //gestiste il buffer su addHtml, fino a quando non è vuoto prende tutto dentro qua
    private $bufferHtml;
    //gestione profili - posso cambiare il comportamento di routine o variabili utilizzando il metodo setProfile($profilo);
    //comodo per definire i css / variabili nelle routine di subForm es.
    private $curProfile="default"; //profilo aperto al momento

    private $scheda=0; // contatore di schede aperte
    private $ngDiv=0; //conta i livelli dei gdiv per chiuderli al momento giusto
    private $customClass=""; //buffer per classe personalizzate nel dettaglio


}//fine classe
