<?php

/**
* Trait di base per la gestione dei permessi. I permessi sono salvati nel database, vengono caricati man mano che serve
* @var array contiene i permessi richiesta nell'attuale sessione
*/
trait Permessi
{
	private $sessionPermessi; //array con i permessi della richiesta attuale
	private $idUser; //idUtente che ha eseguito il login
	public $currentModule=""; //modulo caricato in sessione
	private $currentWorkDir=""; //cartella di lavoro dove sto eseguendo la richiesta
	private $idTokenLogin=""; //idToken caricato dalla funzione di login
	private $ifAdmin=null; //cache per la verifica se l'utente corrente è admin o meno
	private $ifMidAdmin=null;//cache per la verifica se l'utente corrente è midle - admin o meno
	private $tipoUtenteLog=null; //cache per il tipo utente

	public function getTipoUtente():string{
		if(empty($this->tipoUtenteLog)){
			$idUser=$this->getIdUser();
			$this->startPrepare("SELECT tipo from utente where idUtente=:id ");
			$this->bindP("id",$idUser);
			$this->executePrepare();
			if($this->isDatasetEmpty()){
				return false;
			}
			$f=$this->fetch();
			$this->tipoUtenteLog=$f['tipo'];
		}
		return $this->tipoUtenteLog;
	}


	public function setForceIdUser(string $idUser){
		$this->idUser=$idUser;
	}


	public function isMiddleAdmin():bool{
		if(!isset($this->ifMidAdmin)){
			$tipo=$this->getTipoUtente();
			$iniAdmin=$this->getIni("middleAdminUserType");
			if( ($iniAdmin!=false) && (in_array($tipo,$iniAdmin) )){ //dato all'interno dell'ini
				$this->ifMidAdmin=true;
			}else{
				$this->ifMidAdmin=false;
			}
		}
		return $this->ifMidAdmin;
	}


	public function isAdmin():bool{
		if(!isset($this->ifAdmin)){
			$tipo=$this->getTipoUtente();
			$iniAdmin=$this->getIni("adminUserType");

			if( ($iniAdmin!=false) && (in_array($tipo,$iniAdmin) )){ //dato all'interno dell'ini
				$this->ifAdmin=true;
			}else{
				$this->ifAdmin=false;
			}
		}
		return $this->ifAdmin;
	}

	/**
	* Torna l'utente che ha eseguito l'accesso
	* @return string idUtente
	*/
	public function getIdUser()
	{
		if(empty($this->idUser)){
			return $this->getSession("idUser");
		}
		return $this->idUser;
	}

	/**
	* Torna l'username dell'utente loggato
	* @return string [description]
	*/
	public function getUsername():string{
		$que = "SELECT  nome,user,mail from utente where idUtente = '{$this->getIdUser()}' ";
		$user=$this->query($que)->fetch();
		if(!empty($user['nome'])){
			return $user['nome'];
		}

		if(!empty($user['user'])){
			return $user['user'];
		}
		
		if(!empty($user['mail'])){
			return $user['mail'];
		}
		
	}


	/**
	* Torna l'idToken in sessione
	* @return string
	*/
	public function getIdTokenLogin()
	{
		return $this->idTokenLogin;
	}

	public function setCustomPermissionAction(string $modulo,string $action,$perm=null){
		if($perm==null ){
			$perm=$action;
			$action=$modulo;
			$modulo=strtolower($this->getCurModulo());
		}

		$this->ifPermessoNonTrovato($modulo);

		$this->aliasPermession($action);

		$this->sessionPermessi[$modulo][$action] = $perm;
	}


	public function setupCustomPermission(string $modulo,$visualizza,$inserisci,$modifica,$elimina){
		$permesso['permesso']=$modulo;
		$permesso['visualizza']=$visualizza;
		$permesso['inserisci']=$inserisci;
		$permesso['modifica']=$modifica;
		$permesso['elimina']=$elimina;
		$this->setPermesso($permesso);
	}

	/**
	* Registra il permesso e lo salva per la sessione nel caso serva riusarlo
	* @param datasetMysql $row permesso caricato
	*/
	public function setPermesso(array $row, $permessoNonTrovato=false)
	{ //setto il permesso
		$permesso=$row['permesso'];
		$permesso=strtolower($permesso);
		if (!empty($row["permesso"])) {

			if(!isset($this->sessionPermessi[$permesso]['visualizza']))
				$this->sessionPermessi[$permesso]['visualizza'] = $row['visualizza'];

		if(!isset($this->sessionPermessi[$permesso]['inserisci']))
				$this->sessionPermessi[$permesso]['inserisci'] = $row['inserisci'];

		if(!isset($this->sessionPermessi[$permesso]['modifica']))
				$this->sessionPermessi[$permesso]['modifica'] = $row['modifica'];

		if(!isset($this->sessionPermessi[$permesso]['elimina']))
			$this->sessionPermessi[$permesso]['elimina'] = $row['elimina'];

		}

		if ($permessoNonTrovato) {
			$this->sessionPermessi[$permesso]['nonTrovato'] = "1";
		}
	}



	/**
	* Controllo con il permesso non troato
	* @param  string $permesso [description]
	* @return [type]           [description]
	*/
	public function ifPermessoNonTrovato(string $permesso)
	{

		if (!$this->ifExistsPermesso($permesso)) {
			$this->setUpPermesso($permesso);
		}
		return $this->checkPermesso($permesso, 'nonTrovato');
	}

	/**
	* Controllo se il permesso esiste
	* @param  string $permesso permesso attivo
	* @return bool             esito controllo
	*/
	public function ifExistsPermesso(string $permesso):bool
	{
		$permesso=strtolower($permesso);
		if (isset($this->sessionPermessi[$permesso])) {
			return true;
		} else {
			return false;
		}
	}


	/**
	* Nel caso uso diversi tipi di permessi
	* @param  [type] $permission [description]
	* @return [type]             [description]
	*/
	public function aliasPermession(&$permission){
		switch($permission){
			case 'delete':
			$permission="elimina";
			break;

			case 'insert':
			$permission="inserisci";
			break;

			case 'update':
			$permission="modifica";
			break;
		}
	}


	public function checkPermessoOrUndefined(string $permName,string $permesso=""){
		if (empty($permesso)) {
			$permesso=$permName;
			$permName=$this->getCurModulo();
		}
		if (!$this->checkPermesso($permName, $permesso)) {
			if (!$this->getUtility()->ifPermessoNonTrovato($permName)) { //se non ho il permesso già inserito posso ignorare il controllo - vedi casi subTable
				return false;
			}
		}
		return true;
	}



	/**
	* Controllo l'azione per quel permesso
	* @param  int $scheda idPermesso richiesto
	* @param  string $azione tipo di permesso (inserisci, modifica, elimina....)
	* @return bool risposta sul permesso
	*/
	public function checkPermesso(string $permesso, string $azione="")
	{
		if (empty($azione)) {
			$azione=$permesso;
			$permesso=$this->getCurModulo();
		}
		$this->aliasPermession($azione);
		$permesso=strtolower($permesso);
		//posso eseguire questa azione per questa scheda?
		if (!$this->ifExistsPermesso($permesso)) {
			$this->setUpPermesso($permesso);
		}
		if(isset($this->sessionPermessi[$permesso][$azione])){
			$gPerm=$this->sessionPermessi[$permesso][$azione];
		}else{
			$gPerm='0';
		}
		//controllo il mio permesso in che stato è
		if (($gPerm == '1')||($gPerm=="n") || ($gPerm==true)) {
			return true;
		} else {
			return false;
		}
	}


	/**
	* Controllo se il permesso è valido per l'utente
	* @param  string $permesso [description]
	* @return [type]           [description]
	*/
	public function checkPermissionUser(string $permesso){
		$que = " SELECT p.nome as permesso, up.visualizza, up.inserisci, up.modifica, up.elimina
		from utente_permesso as up
		inner join permesso as p on up.idPermesso = p.idPermesso  and p.nome = :permesso
		where up.idUtente = :idUser  ";
		$this->startPrepare($que);
		$this->bindP('permesso', $permesso);
		$this->bindP('idUser',$this->getIdUser());
		$this->executePrepare();
		if($this->isDatasetEmpty()){
			return false;
		}else{
			return $this->fetch();
		}
	}




	/**
	* Controllo se il permesso è valido per il gruppo utente
	* @param  string $permesso [description]
	* @return [type]           [description]
	*/

	public function checkPermissionGroup(string $permesso){
		$que = " SELECT p.nome as permesso,
		if(up.visualizza is null or up.visualizza=0,upt.visualizza,up.visualizza) as visualizza,
		if(up.inserisci is null or up.inserisci=0,upt.inserisci,up.inserisci) as inserisci,
		if(up.modifica is null or up.modifica=0,upt.modifica,up.modifica) as modifica,
		if(up.elimina is null or up.elimina=0,upt.elimina,up.elimina) as elimina
		from utente as ut
		inner join permesso as p on p.nome = :permesso
		inner join utente_tipo_permesso as upt on ut.tipo=upt.idTipoUtente and ut.idUtente=:idUser and upt.idPermesso = p.idPermesso
		left join utente_permesso as up on up.idPermesso = p.idPermesso and up.idUtente=ut.idUtente ";

		$this->startPrepare($que);
		$this->bindP('permesso', $permesso);
		$this->bindP('idUser',$this->getIdUser());
		$this->executePrepare();
		if($this->isDatasetEmpty()){
			return false;
		}else{
			return $this->fetch();
		}
	}


	/**
	* Carica i permessi per un modulo / tabella
	* @param int permesso idPermesso richiesto
	*/
	public function setUpPermesso(string $permesso)
	{
		$perm=$this->checkPermissionGroup($permesso);
		if ($perm) {
			$this->setPermesso($perm);
		} else {
			$this->warning("permesso", "PERMESSO NON TROVATO $permesso. ");
			$this->setPermesso(["permesso" => $permesso, "visualizza" => 'n', "inserisci" => 'n', "modifica" => 'n', "elimina" => 'n','nonTrovato'=>'1'], true);
		}
	}

	/**
	* Auto controllo dei permessi sul modulo
	* @return throw ferma tutto in caso di errore caricamento permessi
	*/
	private function loadPermissionModule()
	{
		$root=$this->getPath('A_ROOT');
		$uri=$_SERVER['REQUEST_URI'];
		$uri=explode("/",$uri);
		$module=ucwords($uri[count($uri)-2]);
		if ($module==$root) { //area franca di launcher applicazioni.
			return true;
		}
		if(empty($this->currentModule)){
			$this->currentModule=$module;
		}
		$this->currentWorkDir=ucwords($uri[count($uri)-3]);
		if ($module=="Login") { //area franca per il template
			return true;
		}
		if (!$this->checkPermesso($this->currentModule, 'visualizza')) { //controllo se ho almeno i permessi di visualizza altrimenti l'utente sta feacendo il furbo
			$this->error('Errore privilegi');
		}
	}

	public function getCurWorkDir(){
		return $this->currentWorkDir;
	}

	/**
	* Ritorna il modulo su cui sto operando
	*/
	public function getCurModulo()
	{
		return $this->currentModule;
	}

	/**
	* Controlla se la chiamata ha un token valido in sessione
	* @return bool esito della chiamata
	*/
	public function checkToken()
	{
		$token=$this->postStr('token');
		if (empty($token)) {
			header('Location: index.php?invalido');
			die;
		}
		$que=" SELECT idUtente,idSessione from utente_sessione where codice=:token
		and date_add(refresh,INTERVAL 60 MINUTE )>=now() order by idSessione desc limit 1 ";

		$this->startPrepare($que);
		$this->bindP("token", $token);
		$this->executePrepare();
		if ($this->numRows()==0) {
			echo "<script>utility.logout();</script>";
			die;
		}
		$row=$this->fetch();
		$this->idUser=$row['idUtente'];
		$this->setSession("idUser",$this->idUser);
		$idToken=$row['idSessione'];
		$this->query("UPDATE utente_sessione set refresh=now() where idSessione='$idToken' ");
		$this->idTokenLogin=$idToken;
	}
}//end trait
