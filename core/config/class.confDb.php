<?php

/**
 * Configurazione delle varie connessione al database.
 * Viene gestita anche la modalità debug / area di test con connessioni su database di test.
 * I metodi sono richiamati dinamicamente con il nome istanza
*/

class ConfigDb
{
    /**
     * Connessione ai database di produzione
     * @param  string $connName nome del database
     * @return mixed Bool False in caso di connessione non trovata | array con dati della connessione richiesta
     */
    public function getConnection(string $connName="")
    {
        //tolgo la parte di errore dovuta al case sensitive
        $connName=strtolower($connName);
        $conn=[];

        switch ($connName) {
            case '':
            case 'default':
                $conn=[
                    "name"=>"default",
                    "host"=>"192.168.1.114",
                    "user"=>"test",
                    "password"=>"test",
                    "dbName"=>"radius",
                    "port"=>3306,
                    "driver"=>"mysql"
                ];
            break;
        } //end switch

        return $conn;
    }
} //fine classe
