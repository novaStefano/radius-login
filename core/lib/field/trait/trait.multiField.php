<?php

/**
 * Trait per la creazione massiva di input - partendo da Array o da una query crea
 * una form seguendo il proprio markup anche in maniera dinamica
 */

trait multiField
{
  private $defArField = [];
  public $arParam = []; //array con param per la creazione della vista in modalità automatica
  private $divAperto = false; //boolean per la gestione dei gDiv in coda
  private $arParamWork = []; //parametro in lavorazione
  private $startToken = false; //token avviato?
  private $useTokenForm = null; //devo usare il tokenform?
  private $workingMultiScheda = null; //che schedaMulti sto usando?
  private $isFirstAutoDiv = true; //è il primo div aperto? Serve per controllare se deve stampare il backbutton
  private $openContainerDiv = 0; //contatore di div aperti
  private $schedaGenerateFromForm = false; //verifica se la scheda in chiusura è generato da una form
  private $activeSchedaForm = false; //serve per l'auto print elementi
  private $initFirstForm=false; //sto processando la prima form?
  private $startSessionModal=[]; // devo stampare tabelle dopo la form? uso questa variabile
  private $htmlBeforeClose=""; // html prima di chiudere la form? la stampo qui
  private function mergeArParam(string $campo, string $param)
  { //crea un merge dei due array
    $this->arParam[$campo] = array_merge($this->arParam[$campo], $param);
  }


  public function addHtmlBeforeClose(string $html){
    $this->htmlBeforeClose=$html;
  }

  /**
   * Processo i dati per creare il form per singolo campo
   * @param  string $key    nome del campo
   * @param  array $val     dati strutturali del campo
   * @param  string $valore valore del campo
   * @return array          Array con i dati della colonna
   */
  private function procElementRow(string $key, array $val, $valore = "")
  {
    if ($val['primary'] == '1') { //Colonna auto inc. la tratto come campo chiave nella sessione token
      //devo registrare il token per la tabella sul database
      if (!$this->checkPermesso($val['table'], "visualizza")) {
        if (!$this->getUtility()->ifPermessoNonTrovato($val['table'])) { //se non ho il permesso già inserito posso ignorare il controllo - vedi casi subTable
          $this->error("Access denied - permessi invalidi");
        }
      }

      if(empty($valore) && $this->initFirstForm ){ //sono in inser
        $this->isOnInsert=true;
        $this->initFirstForm=false;
      }

      $this->setWorkId($valore);
      $this->registerTokenForm($val['table'], $key, $valore); //registro il token con l'id - nome tabella - e il valore
      if (!empty($valore)) { //sono in modifica
        $this->setFormAction('update');
      }
      $this->setLastTableSession($val['table']);
      return false;
    }

    //colonna non id, registro come fosse un campo normale
    $arTmp = [];
    $arTmp['id'] = $key;
    $arTmp['label'] = $key;
    $arTmp['type'] = $val['type'];
    $arTmp['table'] = $val['table'];
    $arTmp['required'] = $val['required'];
    $arTmp['unique'] = $val['unique'];
    $arTmp['insert'] = '1';
    $arTmp['update'] = '1';
    $arTmp['val'] = $valore;
    if ($val['type'] == "VAR_STRING") {
      $arTmp['type'] = "edit";
      $arTmp['maxLenght'] = $val['lenght'];
    }
    if (($val['type'] == "LONG") && ($val['lenght'] == 1)) {
      $arTmp['type'] = "checkbox";
    }

    if (($val['type'] == "LONG") && ($val['lenght'] > 1)) {
      $arTmp['type'] = "int";
    }

    if (($val['type'] == "NEWDECIMAL") && ($val['lenght'] > 1)) {
      $arTmp['type'] = "decimal";
    }

    if ($val['type'] == "BLOB") {
      $arTmp['type'] = "text";
    }

    if (!empty($default['default'])) {
      $arTmp = $arTmp + $default['default'];
    } //aggiungo i parametri comuni

    // $this->procAutoGrid($arTmp);

    return $arTmp;
  }


  /**
   * Creo i parametri partendo dall'array
   * @param  array   $default array con le impostazioni usabili nella funzione
   * @param  boolean $debug   attiva la modalità debug
   * @return void
   */
  public function creatElementFromRow(array $default = [], $debug = false)
  { //crea la definizione dell'array arParam partendo una riga MySql
    if (!empty($default['dataset'])) {
      $ds = $default['dataset'];
    } else {
      $ds = "";
    }
    if (empty($default['soloVista'])) {
      $row = $this->fetch($ds);
    } else {
      $row = [];
    }
    $first = true;
    $col = $this->getUtility()->getColumnStruct($ds);
    $arParam = [];
    $firstCol=true;
    foreach ($col as $key => $val) {
      if($firstCol){
        $firstCol=false;
        $firstElement=$val;
      }
      if (!$row) {
        $valore = "";
      } else {
        $valore = $row[$key];
      }
      $arTmp = $this->procElementRow($key, $val, $valore);
      if ($arTmp) {
        if ($first) {
          $arTmp['apriDiv'] = '1';
          $first = false;
        }
        $arParam[$key] = $arTmp;
      }
    } //end ciclo foreach
    $this->addArParam($arParam);

    //se non posso inserire / modificare metto tutto in disabled
    if($this->isOnInsert()){
      if(!$this->checkPermesso($firstElement['table'],'insert')){
        $this->setVarAllElements("arReadOnly");
      }
    }else{
      if(!$this->checkPermesso($firstElement['table'],'update')){
        $this->setVarAllElements("arReadOnly");
      }
    }


    $this->startToken = true; //marco la routine per passare sotto tokenForm
  }



  public function setStartToken($start = false)
  {
    $this->startToken = $start;
  }

  /**
   * Aggiunge un elemento all'array degli elementi dinamici
   * @param string $key chiave dell'elemento
   */
  public function addArField(string $key, array $default = [])
  {
    $arTmp = [];
    $arTmp['id'] = $key;
    $arTmp['label'] = $key;
    if (!empty($default['default'])) {
      $arTmp = $arTmp + $default['default'];
    } //aggiungo i parametri comuni
    if (!empty($this->defArField)) {
      $arTmp = array_merge($arTmp, $this->defArField);
    }
    $arParam[$key] = $arTmp;
    if (empty($this->arParam)) {
      $first = true;
    } else {
      $first = false;
    }
    $this->addArParam($arParam);
    if ($first) {
      $this->arApriDiv($key); //se sono al primo sicuramente mi serve un gDiv. Posso sempre disabilitarlo dopo a mano
    }
  }

  /**
   * Mergia i due parametri
   * @param array $arParam [description]
   */
  private function addArParam(array $arParam = [])
  {
    $this->arParam = array_merge($this->arParam, $arParam);
  }

  /**
   * Se sono in submodal metto un torna indietro rapido in alto
   * @return [type] [description]
   */
  public function checkAutoBackButton(array $option = [])
  {
    if (!$this->isFirstAutoDiv) {
      return "";
    }

    if (!$this->startToken) {
      return;
    }

    if ($this->sessionActiveSchede) {
      return;
    }

    $sezione = $this->getSezione();
    if ($sezione == 'inLine' || $sezione == 'modalForm') {
      return "";
    }

    $this->isFirstAutoDiv = false;
    $class = "";
    if (!isset($option['centerClass']) || $option['centerClass'] !== false) {
      $class = "centerFormBtn";
    }
    $this->backButton(["adClass" => " btn-rounded $class ", "html" => ""]);
  }


  /**
   * Controllo se devo aprire il div nell'elemento
   * @param  [type] $param     [description]
   * @param  [type] $divAperto [description]
   * @return [type]            [description]
   */
  private function checkApriDiv($param)
  {
    if (empty($param['apriDiv'])) {
      return 0;
    }
    if ($param['apriDiv'] == '1') {  //controllo apertura div
      if ($this->divAperto) {
        $this->chiudi_gDiv();
      }
      $divAperto = $this->divAperto = true;
      if (!empty($param['apriDivParam'])) {
        $div = $param['apriDivParam'];
      } else {
        $div = [];
      }

      if (isset($param['closeContainer'])) {
        $this->autoCloseContainer($param);
      }

      if (isset($param['container'])) {
        $this->autoCloseContainer($param);
        $this->addHtml("<div class='{$param['container']}'><!-- div container -->");
        $this->openContainerDiv++;
      }

      $this->gDiv($div);
      $this->checkAutoBackButton();
    }
  }


  public function autoCloseContainer($param){
    if($this->openContainerDiv>0 ){
      $this->addHtml("</div><!-- div container --> ");
      $this->openContainerDiv--;
    }

    if(isset($param['afterPrintContainerCallback'])){
      $param['afterPrintContainerCallback']();
    }
  }

  /**
   * Controllo cosa deve fare con la scheda
   * @param  [type] $param [description]
   * @return [type]        [description]
   */
  private function apriSchedaMulti(array $param)
  {
    if (empty($param['apriScheda'])) {
      return 0;
    }
    if ($this->scheda > 0) { //apro il container giusto
      $this->chiudi_gDiv();

      //sistema per il container / widget / multi col
      if( $this->openContainerDiv>0){
        $this->autoCloseContainer($param);
      }


      $this->chiudi_schedaDiv();
    }
    $this->apri_schedaDiv($param['apriScheda']);
    $this->schedaGenerateFromForm = true; //serve per il printComandi automatico
    $this->isFirstAutoDiv = true;
    $this->activeSchedaForm = true;
  }

  /**
   * Controllo se devo stampare la variabile html nell'elemento
   * @param  [type] $param [description]
   * @return [type]        [description]
   */
  private function aggiungiHtmlAr(array $param)
  {
    //aggiunta html nell'elemento
    if (isset($param['aggiungiHtmlAr'])) {
      $this->addHtml($param['aggiungiHtmlAr']);
    }
  }

  /**
   * Controllo se devo operare con il clearspace
   * @param  [type] $param [description]
   * @return [type]        [description]
   */
  public function clearAr(array $param)
  {
    //controllo se devo aggiungere un clearspace a fine elemento
    if (isset($param['clearAr'])) {
      $this->clearSpace();
    }
  }

  /**
   * attiva la sessione a fine print 
   */
  public function addSessionModalAfterForm($modal,$param=[]){
    $this->startSessionModal[$modal]=$param;
  }

  /**
   * Attiva le modal form in sesseione a fondo del form
   */
  private function startSessionModalAfterForm(){
    $modal=$this->startSessionModal;
    if(empty($modal)){
      return;
    }

    foreach($modal as $name=>$param){
      $this->getUtility()->getDivTabModalSession($name,$param); //sotto tabella per i contatti
    }


  }

  /**
   * Alias per printElementsFromArray
   * @param  boolean $chiudi Chiudi ultimo gDiv aperto
   * @return void
   */
  public function executeArField($chiudi = true)
  {
    $this->printElemntsFromArray($chiudi);
  }

  /**
   * Stampa i campi con i dati presi dall'array param dentro l'html
   * @param  boolean $chiudi chiudi l'ultimoGdiv - false se devo continuare a stampare sulla sezione
   * @return void
   */
  public function printElemntsFromArray($chiudi = true)
  { //stampa gli elementi dall'array arParam generato con creaElemetnFromROw
    //iniz variabili di ciclo
    $this->printMessageLock();
    $this->divAperto = false; //inizializzo la variabile per il ciclo
    foreach ($this->arParam as $param) { //ciclo gli elementi di dettaglio per stampare
      
      if(!empty($param['destroyIfNotAdmin']) && (!$this->getUtility()->isAdmin())){ //alcuni elmenti sono solo disponibili solo per l'utente di tipo admin, in quel caso salto il processo per non stamparlo
        continue; //salto il processo perchè non sono admin e questo elemento è visibile solo a quel tipo di utente
      }

      
      $this->apriSchedaMulti($param);
      $this->checkApriDiv($param);
      $this->creaMultiElementTipo($param);
      $this->aggiungiHtmlAr($param);
      $this->clearAr($param);
    } //chiusura foreach
    if ($chiudi) {
      if(!empty($this->htmlBeforeClose)){
        $this->addHtml($this->htmlBeforeClose);
        $this->htmlBeforeClose=""; //pulizia della variabile per il prossimo ciclo
      }
      $this->chiudi_gDiv();
      if ($this->openContainerDiv > 0) {
        $this->chiudi_div("Container auto");
      }
      if ($this->autoCloseSchedaElements) {
        $this->chiudi_schedaContainer();
      }
    }

    //  if($this->scheda>0){$this->chiudi_scheda();} //controllo se devo anche chiudere la scheda
    $this->clearArField();
    $this->bindLiveValidation();
    $this->executeTokenForm();

    $this->startSessionModalAfterForm();

    if (!$this->activeSchedaForm && $this->startToken) {
      $this->printComandi($this->setUpComandiScheda);
    }
    $this->startToken = false;
    $this->activeSchedaForm = false; //serve per il printComandi automatico

  }


  public function clearArField()
  {
    $this->workingMultiScheda = null;
    $this->arParam = [];
    $this->col = [];
  }

  private function bindLiveValidation()
  {
    $this->addHtml("<script>liveValidation.bindLiveValidation();</script>");
  }

  /**
   * Imposta tutte le colonne ad essere labelValue
   * @return [type] [description]
   */
  public function forceAllColLabelValue()
  {
    $arParam = $this->getArParam();
    foreach ($arParam as $key => $value) {
      $this->arType($key, "labelValue");
      $this->setArVar($key, "preHtml", "<br>");
      $this->setArVar($key, "fineHtml", "<br>");
    }
  }


  /**
   * Creo l'elemento in HTML partendo dal type effettivo
   * @param  [type] $type  [description]
   * @param  [type] $param [description]
   * @return [type]        [description]
   */
  private function creaMultiElementTipo(array $param)
  { //creo l'elemento a seconda del type trovato

    if (!empty($param['type'])) {
      $type = $param['type'];
    } else {
      $type = "";
    }


    $this->paramWork = [];

    $this->ifBlow($type);
    $this->setParamWork($param);
    switch ($type) {

      case 'multiCheckbox':
        $this->newMultiCheck($param);
        break;

      case 'radio':
        $this->newRadio($param);
        break;
      case 'select':
        $this->newSelect($param);
        break;
      case 'cercaCompila':
        $this->newElementCompila($param);
        break;

      case 'button':
      case 'btn':
        $this->newButton($param);
        return 0;
      break;
      case 'null':
      case 'none':
        return 0; //esco per evitare i prossimi controlli
        break;
      case 'search':
      case 'newInputSearch':
      case 'cerca':
        $this->newInputSearch($param);
        break;
      case 'wysiwyg':
        $param['wysiwyg'] = "1";
        $this->newText($param);
        break;
      case 'text':
        $this->newText($param);
        break;
      case 'html':
        $this->addHtml($param['html']);
        return 0; //esco per evitare i prossimi controlli
        break;
      case 'img':
      case 'file':
        $this->newFile($param);
        break;
      case 'labelvalue':
      case 'labelValue':
        $this->newLabelValue($param);
        return 0; //esco per evitare i prossimi controlli
        break;
      case 'groupFile':
      case 'gruppoFile':
        $this->newGroupFile($param);
        break;
      case 'id':
        //GESTITO DA TOKEN ORMAI - non dovrebbe più servire
        break;
        // case 'datetime':
      case 'datetime':
      case 'dateTime':
        $this->newDatetime($param);
        break;
      case 'hidden':
        $this->newHidden($param);
        break;
      case 'checkbox':
      case 'checkBox':
        $this->newCheckbox($param);
        break;
      case 'showPass':
        $this->newShowPass($param);
        break;

      case 'openModule':
        $this->newOpenModule($param);
        return 0; //esco per evitare i prossimi controlli
        break;
      
      case 'vimeo';
        $this->newVimeo($param);
        break;

      default:
        $this->newInput($param);
        break;
    } //end switch

    //vedo se devo  aggiungere l'elemento alla query - vedi trait TokenForm
    if (($this->startToken) && ($this->isActiveTokenForm())) {
      $this->regFieldSession($this->getParamWork());
    }
  }
}//fine classe
