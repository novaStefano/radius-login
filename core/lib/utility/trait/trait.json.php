<?php

/**
* Gestione dei messsaggi e dei log esposti in json
*/
trait Json
{
    public $resJson=[]; //array contentente il json della sessione



    /**
    *termina la procedura mostrando il log di quel che è successo presente nell'array json
    */
    public function halt()
    {
        $this->printJsonMess();
        die;
    }

    public function setTypeNotification(string $type=""){
      if(empty($type)){
        $type=$this->getIni("successTypeNotification");
      }
      $this->json("typeNotification", $type);
    }

    /**
    * Funzione standard per un evento andato a buon fine - imposta i json corretti e conclude la request
    * @param  string  $mess  messaggio
    * @param  boolean $halt  Devo fermare lo script?
    * @param  string  $title titolo - opzionale
    * @return void
    */
    public function success(string $mess, bool $halt=true, string $title=""):void
    {

        if(!isset($this->resJson['successTypeNotification'])){
          $this->setTypeNotification(); //imposta il tipo di messaggio al ritorno
        }
        $this->json("title", $title);
        $this->json("mess", $mess);
        $this->json("ok", true);
        if ($halt) {
            $this->halt();
        }
    }

    /**
    * Ferma la routine dando come ritorno l'ecezzione che ha fermato la routine
    * @param  string $error messaggio di errore
    * @return void  Ferma la routine
    */
    public function error(string $error):void
    {
        $this->json('error', $error);
        $this->json("err", "1");
        $this->halt();
    }

    /**
    * Metodo per le comunicazioni di info
    * Attivo solo se c'è debug=true
    * @param string $key chiave dell'info
    * @param string $mess messaggio da inserire nel json
    * @return [type] [description]
    */
    public function info(string $key, string $mess="")
    {
        if ($this->ifDebug()) {
            if (empty($mess)) {
                $mess=$key;
                $key="";
            } else {
                $key=" - ".$key;
            }
            $this->setJson("INFO".$key, $mess);
        }
    }

    /**
    * Funzione per la gestione dei log. Finiesce nel json di defautl
    * @param  string $log log da inserire json di defualt
    * @return void
    */
    public function log(string $log)
    {
        if ($this->ifDebug()) {
            $this->json("log", $log);
        }
    }

    /**
    * Restituisco in output il json in memoria e pulisco la variabile
    * @return void STDOUT del json
    */
    public function printJsonMess()
    {
        $j=$this->resJson;
        if (empty($j)) {
            return false;
        }
        $j=json_encode($j);
        echo $j;
        $this->resJson=[];
    }

    /**
    * Gestione dei warning, attivi solo se la richiesta è in modalità debug
    * @param  string $key     Chiave del warning
    * @param  string $warning Messaggio
    * @return void
    */
    public function warning(string $key, string $warning="")
    {
        if ($this->ifDebug()) {
            if (empty($warning)) {
                $warning=$key;
                $key="";
            } else {
                $key=" - ".$key;
            }
            $this->setJson("WARNING".$key, $warning);
        }
    }

    /**
    *     Gestisce un json direttamente da qui, in automatica salva informazioni di stato durante la chiamata.
    *     Nel caso di una chiave già usata cumula il testo in append
    * @param string  $key   Chiave del json
    * @param string  $val   valore del json
    * @param boolean $sovra sovrascrivo il valore all'interno?
    */
    public function setJson(string $key,  $val, bool $sovra=false)
    {

        try{
            if ((isset($this->resJson[$key]))&&($sovra==false)) { //ho già usato la chiave? cumulo i valori
                $val=$this->resJson[$key]." ".$val;
            }
            $this->resJson[$key]=$val;
        }catch(Exception $e){
            print_r($val);
            die;
        }
    }

    public function setJsonMess(string $key, $val, bool $sovra=false)
    {
        $this->setJson($key, $val, $sovra);
    }


    public function json(string $key,  $val, bool $sovra=false)
    {
        $this->setJson($key, $val, $sovra);
    }
}
