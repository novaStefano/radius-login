<?php


/**
* salvataggio della struttura in database  e log su database
*/
trait scanFileDb
{

  /**
  * Torna la cartella per il database, se devo formattare cose strane per la procedura di db lo faccio qui
  * @return string cartella su cui sto navigando
  */
  public function getDbDir():string{
    $dir=$this->adDir;
    if($dir==""){
      $dir="/";
    }
    return $dir;
  }


  /**
  * Log per verificare le modifiche eseguite sull'area scanFile
  * @param  string $operazione operazione che sto richiedendo
  * @param  string $file       file che sto manipolando
  */
  public function logFileDb(string $operazione,string $file,string $tipo){
    $idUtente=$this->getUtility()->getIdUser();
    $str=' INSERT INTO log_fileScan(operazione,file,tipo,cartella,idUtenteModifica,istanza,
      idRiferimento,dataModifica)
      values (:operazione,:file,:tipo,:cartella,:idUtente,:istanza,:idRiferimento,now()) ';
      $this->startPrepare($str);
      $this->bindP("operazione",$operazione);
      $this->bindP("file",$file);
      $this->bindP("tipo",$tipo);
      $this->bindP("cartella",$this->getDbDir());
      $this->bindP("idUtente",$idUtente);
      $this->bindP("operazione",$operazione);
      $this->bindP("istanza",$this->getIstance());
      $this->bindP("idRiferimento",$this->getIdRiferimento());
      $this->executePrepare();
    }


    /**
    * Torna l'id directory della sezione
    * @param  string $dir [description]
    */
    public function getDirId(string $dir){
      $this->startPrepare("SELECT idFile from file where istanza=:istanza and file=:dir and tipo=2 and idRiferimento=:idRif  " );
      $this->bindP("dir",$dir);
      $this->bindP("istanza",$this->getIstance());
      $this->bindP("idRif",$this->getIdRiferimento());
      $this->executePrepare();
      if($this->getUtility()->isDatasetEmpty()){
        $this->startPrepare("INSERT INTO file(file,cartella,istanza,idRiferimento,dataModifica,dataInserimento,tipo)values(:dir,:direct, :istanza,:idRif,now(),now(),'2' ) ");
        $this->bindP("dir",$dir);
        $this->bindP("direct",$dir);
        $this->bindP("istanza",$this->getIstance());
        $this->bindP("idRif",$this->getIdRiferimento());
        $this->executePrepare();
        return $this->getUtility()->getLastInsertId();
      }
      return $this->fetch()['idFile'];
    }


    /**
    * Registra il file / directory anche a database, utile per un po' eventuali interrogazioni esterne
    */
    public function dbFileInsert(string $file,string $tipo){
      if($tipo=="1"){ //calcolo il peso del file solo se non è una cartella
        $size=$this->getUtility()->getFileSize(($this->getDir().$file));
      }else{
        $size=null;
      }
      $dir=$this->getDbDir();
      $idDir=$this->getDirId($dir);
      $str=" INSERT into file(file,tipo,idCartella,cartella,istanza,idRiferimento,dataModifica,dataInserimento,size)
      values (:file,:tipo,:idDir,:dir,:istanza,:idRif,now(),now(),:size )  ";
      $this->startPrepare($str);
      $this->bindP("file",$file);
      $this->bindP("tipo",$tipo);
      $this->bindP("idDir",$idDir);
      $this->bindP("dir",$dir);
      $this->bindP("istanza",$this->getIstance());
      $this->bindP("idRif",$this->getIdRiferimento());
      $this->bindP("size",$size);
      $this->executePrepare();
    }

    /**
    * Rinomina il file nel database
    * @param  string $oldFile Vecchio file da cambiare
    * @param  string $newFile nuovo nome del file
    * @return
    */
    public function dbRenameFile(string $oldFile,string $newFile,string $tipo){
      $dir=$this->getDbDir();
      $this->startPrepare("UPDATE file set file=:file,dataModifica=now() where istanza=:istanza and file=:oldfile and idRiferimento=:idRif and cartella=:dir  " );
      $this->bindP("file",$newFile);
      $this->bindP("oldfile",$oldFile);
      $this->bindP("dir",$dir);
      $this->bindP("istanza",$this->getIstance());
      $this->bindP("idRif",$this->getIdRiferimento());
      $this->executePrepare();
    }

    /**
    * Elimina file anche a database
    * @param  string $file file
    */
    public function dbDeleteFile(string $file,string $tipo){
      $dir=$this->getDbDir();
      $str=" DELETE from file where file=:file and cartella=:dir and idRiferimento=:idRif and istanza=:istanza   ";
      $this->startPrepare($str);
      $this->bindP("file",$file);
      $this->bindP("dir",$dir);
      $this->bindP("istanza",$this->getIstance());
      $this->bindP("idRif",$this->getIdRiferimento());
      $this->executePrepare();
    }

  } //end trait db




  ?>
