<?php
/**
* Operazioni da eseguire con il field token registrato in field nella creazione
*/

class FieldToken extends wrapperUtility
{
  private $isNewRecord=false; //controllo se è un record già salvato o al primo insert
  private $dataSessione=[]; //dati caricati dal dataSessionForm
  private $tokenWork=""; //token di cui sto eseguendo la richiesta in questo momento
  private $idUtenteForm=""; //colonna idUtenteForm della colonna utente_sessione_form - caricato quando richiedo i dati del tokenForm
  private $tokenLoaded=false; //controllo se ho già caricato un nuovo token

  private $idSessionToken=[]; //variabile di appoggio per i salvataggi con sotto tab slave

  private $reloadQueryBuild=""; //usata solo in insert - restta il modello di query. In update potrebbero esserci campi diversi da modificare
  private $idInsert=""; //id inserito nell'update
  private $debug=false;


  private $useTokenForm=null; //attiva l'utilizzo del token log - settare solo se bisogna dare sovrascrivere l'ini globale su trai ini
  private $ifRegisterReturnJson=false; //controlla se ho già registrato il ritorno per la prima form

  private $AddColumnInsert="";
  private $firstSaveId=null; //primo id di salvataggio
  private $fetchUpdate=[]; //dati da usare nel bind della form

  /**
  * Torna lo stato del record (se sono in inserimento o in modifica)
  * @return bool
  */
  private function ifNewRecord():bool
  {
    return $this->isNewRecord;
  }

  /**
  * Controllo se la classe è arrivata al caricamento del token interrogando il flag apposito
  * @return bool token loaded
  */
  private function ifTokenLoaded()
  {
    return $this->tokenLoaded;
  }

  /**
  * Controllo se sono in debug - interroga la variabile boolean debug
  * @return bool controllo debug
  */
  private function ifDebug()
  {
    return $this->debug;
  }

  /**
  * Torna il post dal post forms
  * @param  string $post modulo
  * @return [type]       [description]
  */
  private function getTokenPostForm(string $post=""):string{
    if(!empty($_POST[$post])){
      return $this->postStr($post);
    }

    if((empty($post))&&(!empty($_POST['tokenForm']))){
      return $this->postStr('tokenForm');
    }

    if(empty($post)){
      $post=strtolower($this->getUtility()->getCurModulo());
    }
    $p=$_POST['forms'][$post]['tokenForm'];
    $this->getUtility()->json("check form token",$post." cerco --> ".$p);
    return $p;
  }

  /**
  * Inizializzo il token
  * @param  string $token da dove partire, se vuoto cerco nel post
  * @return
  */
  private function initToken(string $token=""):bool
  {
    //Per qualche motivo potrei selezionare il token a mano
    if (empty($token)) {
      $token=$this->getTokenPostForm();
    }
    $this->tokenWork=$token;
    $dataSession=$this->getDataSessioneForm(); //prendo i dati dalla chiamata standard di ricerca tokenField
    if (!$dataSession) {
      $this->warning("INIT token", " $token dati non presi, interrompo la procedura");
      $this->halt();
      return false;
    } else {
      $this->tokenLoaded=true;
      return true;
    }
  }

  /**
  * Torna l'id caricato dal tokenForm
  * @param  string $token [description]
  * @return mixed
  */
  public function getIdFromTokenField(string $token="")
  {
    if (!$this->ifTokenLoaded()) {
      if (!$this->initToken($token)) {
        return false;
      }
    }
    return $this->getIdVal();
  }


  /**
  * ritorno del where standard
  * @param  string $token [description]
  * @param  string $alias [description]
  * @return [type]        [description]
  */
  public function getWhere(string $token="", string $alias="")
  {
    if (!$this->initToken($token)) {
      return false;
    }

    if (empty($alias)) {
      $alias=$this->getTableName();
    }
    return "  $alias.{$this->getColName()} =:{$this->getColName()} ";
  }


  /**
  * Toglie il token dalla sessione
  * @param  string $token token da eliminare
  */
  private function deleteToken(string $token="")
  {
    $idLogin=$this->getUtility()->getIdTokenLogin();
    $this->startPrepare("DELETE from utente_sessione_form where tokenForm=:token and tokenLogin=:idLogin ");
    $this->bindP("token", $token);
    $this->bindP("idLogin", $idLogin);
    $this->executePrepare();
  }

  /**
  * Pulisce la lista di token presenti in sessione
  * @param  string $namePost post da dove recuperare i token
  */
  public function clearTokenFromPost(string $namePost="tokenForm")
  {
    $tokens=$this->post($namePost);
    if (empty($tokens)) {
      $this->getUtility()->info("clear token", "NESSUN TOKEN");
      return 0;
    }
    foreach ($tokens as $token) {//ciclo ogni token e lo elimino
      $this->deleteToken($token);
    }
    $this->getUtility()->info("Token Form", "pulizia token eseguita");
  }


  /**
  * Torna il token che è in lavorazione
  * @return string token in lavorazione
  */
  private function getTokenWork()
  {
    return $this->tokenWork;
  }

  /**
  * Ritorno dell'idUtenteForm in lavorazione
  * @return string
  */
  private function getIdUtenteForm()
  {
    return $this->idUtenteForm;
  }


  public function addFetchUpdate(string $col=""){
    $data['nameCol']=$col;
    $data['useOninsert']='1';
    $data['useOnUpdate']='1';
    $data['required']='1';
    $data['type']="text";
    $data["forceValue"]='0';
    array_push($this->fetchUpdate,$data);
  }

  /**
  * Salva le informazioni degli id in sessione - server per i token slave del master
  * @param array $row
  */
  private function setIdDataSessionFormMaster(array $row){
    $this->idSessionToken['idVal']=$row['idVal'];
    $this->idSessionToken['idCol']=$row['idCol'];
    $this->idSessionToken['active']=false;
  }

  private function activateIdSessionMaster(){
    $this->idSessionToken['active']=true;
  }

  private function ifActivateIdMaster(){
    $activate=$this->idSessionToken['active'];

    if(empty($activate)){
      return false;
    }else{
      return $activate;
    }

  }

  /**
  * Carica i dati della sessione partendo del token, controlla anche se il token è nella mia sessione
  * @param  string $token token del form
  * @return mixed  risultato della richiesta dei dati
  */
  private function getDataSessioneForm()
  {
    $this->startPrepare("SELECT idUtenteForm,tableName,idCol,idVal,queryBuild from utente_sessione_form
      where tokenLogin=:tokLogin and tokenForm=:tokenForm  ");
      $this->bindP("tokLogin", $this->getUtility()->getIdTokenLogin());
      $this->bindP("tokenForm", $this->getTokenWork());
      if(!$this->executePrepare()){
        $this->getUtility()->error("errore tokForm","ERRORE GETDATASESSION");
      }
      if ($this->getUtility()->numRows()!=1) {
        $this->warning("getDataSession form", "Token della sessione non trovato");
        return false;
      } else {
        $row= $this->fetch();
        $this->idUtenteForm= $row['idUtenteForm'];
        $this->setDataSession($row);
        if (empty($row['idVal'])) { //controllo se era un nuovo record o già salvato
          $this->isNewRecord=true;
        }
        return $row;
      }
    }
    /**
    * Imposta i dati di sessione per l'esecuzione delle azioni
    * @param array $row
    */
    private function setDataSession(array $row){

      //controllo se è il primo token da valorizzare, nel caso di default è master
      if(empty($this->idSessionToken)){
        $this->setIdDataSessionFormMaster($row);
      }else{
        // //tengo questi dati presi dall'id master
        // $row['idVal']=$row['idVal'];
        // $row['idCol']=$row['idCol'];
        $this->activateIdSessionMaster();
      }
      $this->dataSessione=$row;

    }

    public function getColName()
    {
      return $this->dataSessione['idCol'];
    }

    public function getTableName()
    {
      return $this->dataSessione['tableName'];
    }

    public function getIdVal()
    {
      return $this->dataSessione['idVal'];
    }

    public function getQueryBuild()
    {
      if($this->ifActivateIdMaster()){
        $idCol=$this->idSessionToken['idCol'];
        $add=", $idCol = :$idCol ";
      }else{
        $add="";
      }

      if($this->getModel($this->getTableName())->isLogUserUpdated()){
        $add.=$this->getUtility()->getLogUserUpdate();
      } 
      return $this->dataSessione['queryBuild'].$add;
    }


    public function setAddColumnInsert(string $add){
      $this->AddColumnInsert=$add;
    }

    public function getAddColumnInsert(){
      return $this->AddColumnInsert;
    }

    private function buildQueryInsert():string{
      $this->getUtility()->onInsert=true;
      $str="INSERT INTO {$this->getTableName()} set {$this->getQueryBuild()} ".$this->getAddColumnInsert()." ".$this->getUtility()->getLogUserInsert();
    if ($this->ifDebug()) {
        echo $str;
      }
      return $str;
    }


    /**
    * Costruisce la query poer il update
    * @return string query costruita
    */
    private function buildQueryUpdate():string{
      $str=" UPDATE {$this->getTableName()} set {$this->getQueryBuild()} WHERE {$this->getColName()} =:{$this->getColName()} ";

      if ($this->ifDebug()) {
        echo $str;
      }
      return $str;
    }


    /**
    * COstruisce la query con i dati precaricati
    * @return [type] [description]
    */
    private function startQuery()
    {
      if ($this->ifNewRecord()) { //sono in insert perchè non c'è l'id valorizzato
        $str=$this->buildQueryInsert();
      } else {
        $str=$this->buildQueryUpdate();
      }

      $this->startPrepare($str);
      if($this->ifActivateIdMaster()){ //bind se è un'operazione master slave
        $idVal=$this->idSessionToken['idVal'];
        $idCol=$this->idSessionToken['idCol'];
        $this->bindP($idCol, $idVal);
      }

      if (!$this->ifNewRecord()) { //se sono in update aggiuno l'id where
        $this->bindP($this->getColName(), $this->getIdVal());
      }
    }


    /**
    * Torna l'ultimo id Inserito
    * @return lastinsert id
    */
    public function getidInsert()
    {
      if (!empty($this->idInsert)) {
        return $this->idInsert;
      } else {
        return false;
      }
    }


    public function getFirstSaveId(){
      return $this->firstSaveId;
    }


    private function registerReturnJson(string $id,string $type){
      if($this->ifRegisterReturnJson){
        return 0;
      }
      $this->json("newId", $id);
      $this->json("actionUpdate",$type);
      $this->ifRegisterReturnJson=true;
      $this->firstSaveId=$id;

    }


    /**
    * Aggiorna il token - data e id
    */
    private function updateTokenForm()
    {
      if ($this->ifNewRecord()) {
        $id=$this->getUtility()->getLastInsertId();
        $this->idInsert=$id;
        $add=" idVal='$id', ";
        $add.=" queryBuild=:build, ";
        $this->registerReturnJson($id,'insert');
      } else {
        $this->registerReturnJson($this->getIdVal(),'update');
        $add="";
      }

      $str="UPDATE utente_sessione_form set $add ultimaDataEsecuzione=now() where tokenForm=:idToken  ";
      $this->startPrepare($str);
      $this->bindP("idToken", $this->getTokenWork());
      if ($this->ifNewRecord()) {
        $this->json("reload",$this->reloadQueryBuild);
        $this->bindP("build", $this->reloadQueryBuild); //ricarico la stringa modello per le query di update
      }
      $res=$this->executePrepare();
      if (!$res) {
        $this->warning("UPDATE TOKEN", "Errore update token");
        return false;
      } else {
        return true;
      }
    }


    private function initDataSessionFormField()
    {
      $id=$this->getIdUtenteForm(); //id form token registrata a db
      $this->query("SELECT nameCol,valCol,required,validazione,type,useOninsert,useOnUpdate,forceValue from utente_sessione_form_campi where idUtenteForm='$id' ");
    }

    /**
    * Controlla la validazione del form con i dati salvati
    * @param array $data Campo preso da ARRAY / POST da verificare se è valio
    * @param array $field Validazione presa dal database per il campo scelto
    * @return bool esito della validazione. In caso di errore la routine viene terminata. FALSE salta bind del ciclo principale, TRUE esegue il bind
    */
    private function validationBindForm(array &$data, array &$field)
    {
      //controllo se il campo è da usare in insert
      if (($this->ifNewRecord())&&($field['useOninsert']!='1')) {
        return false;
      }

      //controllo se il campo è da usare in update
      if ((!$this->ifNewRecord())&&($field['useOnUpdate']!='1')) {
        return false;
      }

      //Controllo se c'è un valore prefisato, ignoro qualsiasi cosa ci sia scritto nel form e uso quello. Comodo per i sottoId o altre cose simili
      if ($field['forceValue']=='1') {
        $this->bindP($field['nameCol'], $field['valCol']); //bindo qui il valore e faccio saltare il cilo standard da POST / ARRAY
        return false;
      }

      //controllo se è un campo required
      if (($field['required']=='1')&& ((!isset($data['val']))||($data['val']==""))) {
        $this->error("Il campo ".$field['nameCol']." è obbligatorio e deve essere compilato");
        return false;
      }

      //controllo se devo resettare la queryBuild della tabella. In caso di primo insert potrei dover inserire dei campi e in modifica altri
      if ($this->ifNewRecord()) {
        if ($field['useOnUpdate']) { //se il campo deve lavorae in update lo aggiungo al modello di query altrimenti lo salto
          if (!empty($this->reloadQueryBuild)) {
            $this->reloadQueryBuild.=",";
          }
          $this->reloadQueryBuild.=$field['nameCol']."=:".$field['nameCol'] ;
        }
      }

      //tipi particolari riconducibili a INT
      $type=$field['type'];
      if (strpos($type, "-blow")) {
        $type=str_replace("-blow", "", $type);
        $data['val']=$this->getUtility()->crypt($data["val"]);
      }
      if ($type=="cerca") {
        $field['type']="int";
      }

      //se arrivo alla fine posso proseguire
      return true;
    }


    /**
    * Controllo i permessi per il salvataggio (sia insert che update)
    * @return void in caso di errore interrompe la request
    */
    private function checkPermessiUpdate()
    {
      if ($this->ifNewRecord()) { //Controllo se ho i permessi in regola per accedere alla query
        $permesso="inserisci";
      } else {
        $permesso="modifica";
      }
      $permName=ucwords($this->getTableName()); //il permesso deve essere in uppercase
      if (!$this->checkPermesso($permName, $permesso)) {
        if (!$this->getUtility()->ifPermessoNonTrovato($permName)) { //se non ho il permesso già inserito posso ignorare il controllo - vedi casi subTable
          $this->error("permesso salvataggio", "Errore privilegi non validi"); //chiudo la chiamata in caso di permesso invalido
        }
      }
    }



    private function bindFetchUpdate($fetch,$data){
      $nomeCol=$fetch['nameCol']; //prendo il nome della colonna sulla tabella del database
      if (!empty($data[$nomeCol])) {
        $entry=$data[$nomeCol];
      } else {
        $entry=[];
      }
      if ($this->validationBindForm($entry, $fetch)) { //non sempre deve eseguire il prepare, dipende dalla validazione
        $this->bindP($nomeCol, $entry['val'], $fetch['type']);
        if ($this->ifDebug()) {
          echo $nomeCol." ---> ".$entry['val']."\n";
        }
      }
    }


    /**
    * Aggiorna il record corrispondente del token
    * @param  string $postName
    * @param  string $token
    */
    public function saveFromToken($data="forms", string $token=""):bool
    {
      if (!$this->initToken($token)) { //se il token non riesce ad essere inizializzato devo fermare la routine
        $this->log("start","Errore initToken");
        return false;
      }
      //per qualche motivo potrei passare un array già pronto. Altrimenti prendo il post
      if (!is_array($data)) {
        $data=$this->post($data);
      }

      $this->checkPermessiUpdate(); //controllo se posso eseguire l'update con i miei attuali permessi
      $this->startQuery(); //imposta la query prendendo dalla form registrata ed esegue il prepare
      $this->initDataSessionFormField();
      while ($fetch=$this->fetch()) { //bind dei parametri presi dal post
        $this->bindFetchUpdate($fetch,$data);
      } //end while bind parametri e post
      foreach($this->fetchUpdate as $fetch){
        $this->bindFetchUpdate($fetch,$data);
      }




      if ($this->getUtility()->executePrepare()) {
        $this->getUtility()->log("Query salvataggio eseguita con successo");
        $this->setJsonMess("salvataggio token", "Form aggiornata con successo");

        if ($this->updateTokenForm()) { //token aggiornato con successo
          return true;
        }
        $this->getUtility()->json("log","errore update tokenForm");
      }else{
        $this->log("log","errore SALVATAGGIO PREPARE");
      }
      $this->halt();

      return false;
    }

    /**
    * Delete il record da token
    * @param string $token token da eliminare, di base si carica la stringa da POST
    * @return bool
    */
    public function deleteFromToken(string $token=""):bool
    {
      $this->log("Start delete from token ".$token);
      if (!$this->initToken($token)) {
        $this->log("Token non avviato");
        return false;
      }
      $permesso="elimina";
      $permName=ucwords($this->getTableName()); //il permesso deve essere in uppercase
      if (!$this->checkPermesso($permName, $permesso)) {
        if (!$this->getUtility()->ifPermessoNonTrovato($permName)) { //se non ho il permesso già inserito posso ignorare il controllo - vedi casi subTable
          $this->error("permesso delete", "Errore privilegi non validi"); //chiudo la chiamata in caso di permesso invalido
        }
      }

      // Controllo l'ini come devo procedere con il delete
      if($this->getModel($permName)->isHardDelete()){
        $res=$this->doHardDelete();
      }else{
        $this->json("softDelete",true);
        $res=$this->doSoftDelete();
      }

      if ($res) {
        $this->setJsonMess("Elimina token", "Record eliminato con successo");
        $this->deleteToken($this->getTokenWork());
        return true;
      }
      return false;
    }

    /**
    * Eliminazione logica del record
    * @return ritorno esito query
    */
    private function doSoftDelete(){
      $add="";
      $table=$this->getTableName();

      if($this->getModel($table)->isLogUserUpdated()){
        $add=" , lastUpdatedUser = '".$this->getUtility()->getIdUser()."'  ";
      }

      $que=" UPDATE $table set deletedRecord=1,deletedDate=now() $add
       WHERE {$this->getColName()} =:{$this->getColName()} ";

      $this->startPrepare($que);
      $this->bindP($this->getColName(), $this->getIdVal());
      return $this->executePrepare();
    }


    /**
    * Hard delete del record
    * @return $res eisto della query
    */
    private function doHardDelete(){
      $que=" DELETE FROM {$this->getTableName()}  WHERE {$this->getColName()} =:{$this->getColName()} ";
      $this->startPrepare($que);
      $this->bindP($this->getColName(), $this->getIdVal());
      return $this->executePrepare();
    }


    /**
    * Delete il record da tokenInLine
    * @param string $token token da eliminare, di base si carica la stringa da POST
    * @return bool
    */
    public function deleteFromTokenInLine(string $idPost,string $token=""):bool
    {
      if (!$this->initToken($token)) {
        return false;
      }
      $permesso="elimina";
      $permName=ucwords($this->getTableName()); //il permesso deve essere in uppercase
      if (!$this->checkPermesso($permName, $permesso)) {
        if (!$this->getUtility()->ifPermessoNonTrovato($permName)) { //se non ho il permesso già inserito posso ignorare il controllo - vedi casi subTable
          $this->error("permesso delete", "Errore privilegi non validi"); //chiudo la chiamata in caso di permesso invalido
        }
      }

      if($this->getUtility()->isHardDelete()){
        $que=" DELETE FROM {$this->getTableName()}  ".$this->dataSessione['queryBuild'];
        $this->getUtility()->startPrepare($que);
      }else{
        if($this->getModel($this->getTableName())->isLogUserUpdated()){
          $add=" , lastUpdatedUser = '".$this->getUtility()->getIdUser()."'  ";
        }
        $que=" UPDATE {$this->getTableName()} set deletedRecord=1,deletedDate=now() $add
        ".$this->dataSessione['queryBuild'];

        $this->getUtility()->startPrepare($que);

      }
      $this->getUtility()->bindPost($idPost);

      // $this->buildWhereInline();
      if ($this->executePrepare()) {
        $this->json("Elimina token", "Record eliminato con successo");
        return true;
      }
      return false;
    }


  }//end trait
