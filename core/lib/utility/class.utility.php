<?php

include_once __DIR__."/trait/trait.db.php"; //metodi per accesso al database tramite PDO
include_once __DIR__."/trait/trait.dependency.php"; //allocazione e inclusione dipendenze esterne ( es mail / excel / field / ecc)
include_once __DIR__."/trait/trait.json.php"; //creazione standard dei ritorni ( success /error / info / warning / json($key,$val) )
include_once __DIR__."/trait/trait.base.php"; //metodi base come post / otpPassword / crypt /checkHash
include_once __DIR__."/trait/trait.permessi.php"; //Controllo e verifica permessi
include_once __DIR__."/trait/trait.file.php"; //metodi per manipolazione / check dei file
include_once __DIR__."/trait/trait.view.php"; //caricamento di file vista della sezione
include_once __DIR__."/trait/trait.setGet.config.php"; //setget delle configurazioni
include_once __DIR__."/trait/trait.date.php"; //date

/**
* Classe principale del progetto,
* contiene la base delle funzioni estendibili in ogni modulo
* I metodi per l'accesso alle dipendeze
* Connessione ai vari database - metodo di iterazione ai dati
* Controllo login / permessi
*/

class Utility
{
  use Db,Dependency,Ini,Json,Base,Permessi,
  File,utilityView,iniSetGet,DateUtil;

  private $db; //Connessione al database tramite PDO
  private $token; //Libreria per la gestione login e permessi
  private $debug=false;//Flag per la gestione del debug
  public $path; //Oggetto con la lista delle path che servono al progetto
  private $filt=""; //buffer accesso valori in ricerca dal post filter
  public $stopAutoload=false; //variabile estendibile per stoppare l'autoload
  public $skipLogin=false; //salta l'autoinit del login
  public $skipSession=false; //disabilita la sessione - utile per tools / service / rest
  public $onlyAdmin=false; //accesso solo per utente admin all'inizio della request

  /**
  * Costruttore per istanziare l'init della richiesta
  * @param boolean $noToken !!!! di default sempre false! Da tenere a true solo ed esclusivamente per le richieste non autenticati quali login e recupero password. E basta
  */
  public function __construct($noToken=false)
  {
    $this->autoStart(); //metodo presente nel trait di configurazione trait.ini.php nella cartella di configurazione - utile per il boot di certe impostazioni
    if ((!$noToken)&&(!$this->skipLogin)) {
      $this->checkToken(); //controllo se il token di autenticazione nella chiamata è valido e attivo
      $this->loadPermissionModule(); //controllo se ho accesso al singolo modulo - traitPermessi
    }
    date_default_timezone_set('Europe/Rome');
    $this->autoLoadInc();//inclusione delle librerie standard (al momento c'è solo il caricamento di base di wrapperUtility)

    if($this->onlyAdmin){
      if(!$this->isAdmin()){
        $this->error("Permission denied");
      }
    }

    //launcher di default
    $azione=$this->post("azione", "");
    $this->launcher($azione); //se non è dichiarato usa quello standard
    global $utility;
    $utility=$this;
  }





  /**
  * Metodo finto per evitare errori dove non ci sia il vero metodo launcher
  * Ogni classe se deve avere chiamate dall'esterno dovrebbe implementare la definizione di questo launcher
  *  con le route necessarie alla sezione
  * @return esecuzione launcher
  */
  public function launcher(string $azione="")
  {
    //METODO FINTO! è un po' una truffa
    $this->warning("launcher", "Vero launcher non trovato");
  }
} //fine classe



//helper!!!!!! da spostare in file esterno


function getModel(string $model){
  global $utility;
  return $utility->getModel($model);
}
