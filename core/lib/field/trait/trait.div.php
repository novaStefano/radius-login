<?php

/** Contenitore dei metodi per la creazione dei div
*/
trait div
{
  private $schedaOpen=0; //contatore di schede aperte
  private $activeSchede=false; //verifico se ci sono schede attive
  private $currentScheda=0;
  private $countSchede=0;
  private $activeSchedaWizard=false;
  private $forcePrintComandi=[]; //controlla quando deve far stampare i comandi di salva anche senza aver finito la form durante il cambio scheda
  private $forcePrintComandiOptions=[]; //opzioni per il cambio scheda su print comandi
  private $sessionActiveSchede=false; //verifica se ho avviato schede
  public $backSchedaBtn=true;
  public function activeSchedaWizard(bool $open=true){
    $this->activeSchedaWizard=$open;
  }

  private function isactiveSchedaWizard(){
    if( $this->activeSchedaWizard){
      return true;
    }else{
      return false;
    }
  }

  private function isSchedeOpen(){
    if($this->schedaOpen>0){
      return true;
    }else{
      return false;
    }
  }


  private function setterParam($param,$key){
    if (!empty($param[$key])) {
      $par = $param[$key];
    } else {
      $par="";
    }
    return $par;
  }


  /**
  * Apri il div gDiv standard
  * @param  array  $param parametri del
  */
  public function gDiv(array $param=[])
  {

    $nome=$this->setterParam($param,"id");
    $event=$this->setterParam($param,"event");
    $classe=$this->setterParam($param,"class");
    if (!empty($param['adClass'])) {
      $classe.=$param['adClass'];
    }

    if ($nome != "") {
      $nome = " id = $nome ";
    }
 

 

    if(!empty($param['hideOnInsert'])){
      if($this->isOnInsert()){
        $classe.=" masterSubTab ";
        if(empty($param['style'])){
          $param['style']="";
        }
        $param['style'].=" display:none; ";
      }
    }

    $style=$this->setterParam($param,"style");

    if ($style != "") {
      $style = " style='$style' ";
    }

    $this->ngDiv++; //controllo i livelli innestati di gdiv
    $html= "<div $nome class=' gDiv panel row panel-default $classe ' $event  $style   > <!-- Apertura Gdiv --> ";
    $this->addHtml($html);

    if ((isset($param['titolo']))||(isset($param['title']))) {
      if (empty($param['title'])) {
        $param['title']=$param['titolo'];
      }
      $this->titolo($param['title']);
    }
  }



  /**
  * Apri il div gDiv standard
  * @param  array  $param parametri del
  */
  public function div(string $class="",$id="",array $param=[])
  {

    $nome=$id;
    $event=$this->setterParam($param,"event");
    $classe=$class;
    if (!empty($param['adClass'])) {
      $classe.=$param['adClass'];
    }
    $style=$this->setterParam($param,"style");

    if ($nome != "") {
      $nome = " id = $nome ";
    }
    if ($style != "") {
      $style = " style='$style' ";
    }

    $this->ngDiv++; //controllo i livelli innestati di gdiv
    $html= "<div $nome class='$classe' $event  $style > <!--APRI DIV --> ";
    $this->addHtml($html);
    if ((isset($param['titolo']))||(isset($param['title']))) {
      if (empty($param['title'])) {
        $param['title']=$param['titolo'];
      }
      $this->titolo($param['title']);
    }
  }

  public function chiudi_div(string $comment="chiuso DIV"){
    $html= "</div> <!-- $comment --> ";
    $this->addHtml($html);
  }


  /**
  * Aggiunta di un clearspace nell'html
  * @return [type] [description]
  */
  public function clearSpace()
  {
    $this->addHtml("<div class='clearspace'> </div> ");
  }

  /**
  * Chiusi il gdiv con l'evento standard
  * @param  string $param [description]
  * @return [type]        [description]
  */
  public function chiudi_gDiv(string $param = 'Chiusura GDIV')
  {//chiusura div di ragruppo
    if ($this->ngDiv==0) {
      $this->warning("chiudi GDIV", "ATTENZIONE, gdiv chiuso sbagliato");
      return ""; //controllo se effettivamente devo chiudere il gDiv
    }
    $this->ngDiv--;
    $this->clearspace();
    $this->addHtml("</div> <!-- $param -->");
  }


  /**
  * Stampa elemento inDiv - ragruppa gli elementi per il form
  * @param  array parametri da impostare
  */
  public function inDiv($param=[])
  {
    $classe="";
    if (empty($param['id'])) {
      $nome="";
    } else {
      $nome ="id ='{$param['id']}'";
    }

    if (!empty($param['class'])) {
      $classe=$param['class'];
    }

    if(!empty($param['adClass'])){
      $classe.=$param['adClass'];
    }

    if(!empty($param['grid'])){
      $classe.=$param['grid'];
    }

    if (empty($param['style'])) {
      $style="";
    } else {
      $style = " style='{$param['style']}' ";
    }

    if(empty($classe))
    {
      $classe=" col-md-4 ";
    }
    $this->addHtml("<div $nome class=' inDiv $classe '  $style   ><!-- inDiv apri --> ");
  }

  /**
  * chiusura standard indDiv
  * @param  string $commento [description]
  * @return [type]           [description]
  */
  public function chiudi_InDiv($commento = "Chiusura inDiv")
  {
    $commento='<!-- '.$commento." --> ";
    $this->addHtml("</div> ".$commento);
  }


  /**
  * Stampa del titolo
  * @param  string $html HTML DEL TITOLO
  * @return [type]       [description]
  */
  public function titolo($html = '')
  {
    // $this->inDiv(['classe'=>'col-lg-12']);
    $this->addHtml("<h2 class='col-12' >$html</h2>");
    // $this->chiudi_inDiv();
  }

  /**
  * Stampa del titolo
  * @param  string $html HTML DEL TITOLO
  * @return [type]       [description]
  */
  public function centerTitle($html = '')
  {
    $this->addHtml("<center class='col-12'><h4 >$html</h4></center>");
  }




  /**
  * Stampa la linguetta della sezione
  * @param  string  $sezione nome sezione
  * @param  string  $html    Html mostrato sulla li
  * @param  string  $evento  evento da eseguire al click
  * @param  boolean $hidden  linguetta chiusa?
  */
  public function scheda_sezione(string $sezione, string $html="",array $option=[])
  {

    $adClass="";

    if(!empty($option['adClass'])){
      $adClass.=" ".$option['adClass'];
    }

    $notActive=false;
    if(!empty($option['hideOnInsert'])){
      if($this->isOnInsert()){
        $adClass.=" masterSubTab ";
        $option['hidden']=true;
        $notActive=true;
      }
    }

    $evento="";
    if(!empty($option['evento'])){
      $evento=$option['evento'];
    }
    if(!empty($option['event'])){
      $evento=$option['event'];
    }

    if(!empty($option['hidden'])){
      $hidden=$option['hidden'];
    }else{
      $hidden=false;
    }


    if ($this->scheda==0) { //dichiarazione del container
      $this->addHtml('<div role="l" class="active"><!-- container scheda -->');
      $this->addHtml('<ul class="row nav nav-tabs tabcolor5-bg" role="tablist">');
      if($this->backSchedaBtn){
        $this->addHtml('<li role="presentation" onclick="'.$this->getBackEvent().'" class="scheda-voce-field active">');
        $this->addHtml('<a  aria-controls="scheda-dett-lav" role="tab" data-toggle="tab" class="a-voce-field">');
        $this->addHtml('<i class="fa fa-arrow-left">');
        $this->addHtml('</i></a></li>');
      }
    }

    if($this->activeSchede==false && $notActive==false ){ //controllo che sia la prima scheda visibile
      $this->activeSchede=true;
      $adClass.=" active ";
      $this->activeSchede=$sezione;
    }


    if ($hidden) {
      $style="style=\"display: none;\"";
    } else {
      $style="";
    }
    //Contatore della scheda
    $this->scheda++;
    if (empty($html)) {
      $html=ucwords($sezione);
    }



    if($this->isactiveSchedaWizard()){
      $adClass.=" validate-on-change ";
    }

    if(!empty($option['ico'])){
      $ico=$this->getButtonIco($option['ico'])." ";
    }else{
      $ico="";
    }

    $ele='  <li id="linguetta-'.$sezione.'" sezione="'.$this->getSezione().'"
    scheda="'.$sezione.'"
    role="presentation"  '.$evento.' '.$style.'
    class="scheda-voce-field bind '.$this->getSezione().'  scheda-'.$sezione.'  '.$adClass.'    " >
    <a  class="a-voce-field '.$adClass.' '.$this->getSezione().'"
    aria-controls="scheda-dett-'.$sezione.'"
    role="tab" data-toggle="tab"  >'.$ico.$html.'</a></li> ';
    $this->addHtml($ele);
    $this->countSchede++;
    $this->sessionActiveSchede=true;
  }

  /**
  * Chiudi il container delle schede
  * @return [type] [description]
  */
  public function chiudi_scheda()
  {
    $this->scheda=0;
    $this->addHtml("</ul><!-- chiusura NAV --> ");
    $this->addHtml("<div class=\"tab-content\"> <!-- Div container chiudi_scheda --> ");
    $this->addHtml("<script>scheda.bindNewScheda();</script>");

  }

  /**
  * Chiudi la tab contente l'html della scheda
  * @param  string $scheda [description]
  * @return [type]         [description]
  */
  public function apri_schedaDiv(string $scheda,bool $autoClose=true)
  {
    if($autoClose){
      $this->chiudi_schedaDiv();
    }
    $this->currentScheda++;
    $this->schedaOpen++;
    $this->scheda++;
    if ($this->activeSchede == $scheda) {
      $ad="active";
    } else {
      $ad="";
    }
    $this->addHtml('<div role="tabpanel" class="row tab-pane scheda-dett '.$this->getSezione().' '.$ad.' " id="scheda-dett-'.$scheda.'">');
    $this->workingMultiScheda=$scheda;

    // $this->gDiv();
  }

  /**
  * Forza i tasti salva in ogni scheda
  * @param [type] $force [description]
  */
  public function setForcePrintComandi($force,$options=[]){
    $this->forcePrintComandiOptions=$options;
    if(is_array($force)){
      $this->forcePrintComandi=$force;
    }else{
      $this->forcePrintComandi=array($force);
    }
  }


  /**
  * Chiusura del div della scheda
  * @return [type] [description]
  */
  public function chiudi_schedaDiv()
  {
    if (!$this->schedaOpen>0){
      return 0;
    }

    if(($this->currentScheda != $this->countSchede)&&($this->isactiveSchedaWizard())) {
      $this->gDiv(["adClass"=>"chiudi_schedaDiv_btn_container"]);

      if(($this->currentScheda>1)){
        $this->backPageSchedaButton();
      }else{
        $this->backButton();
      }
    }

    if($this->isactiveSchedaWizard()){
      if($this->currentScheda != $this->countSchede){ //scheda intermedia, aggiungo l'avanti / indietro
        $this->nextPageSchedaButton();
        $this->chiudi_gDiv();
      }else{
        $this->printComandi($this->setUpComandiScheda); //scehda finale, stampo i comandi
      }
    }else{
      if($this->schedaGenerateFromForm){
        $this->printComandi($this->setUpComandiScheda); //attiva il setupUp per
      }
    }

    if(in_array($this->workingMultiScheda,$this->forcePrintComandi)){
      $this->printComandi($this->forcePrintComandiOptions);
    }


    $this->chiudi_gDiv("gDiv chiusura scheda");

    $this->addHtml("</div><!-- chiusura div scheda -->");
    $this->schedaOpen--;
    $this->schedaGenerateFromForm=false;
  }


  /**
  * Chiusura del container delle schede
  */
  public function chiudi_schedaContainer()
  {
    if(!$this->activeSchede){
      return 0;
    }
    $this->chiudi_schedaDiv();
    $this->addHtml("</div> <!-- div container scheda Container-->");
    $this->activeSchede=false;
  }




  /**
  * Stampa la tabella dentro i div giusti
  * @param  string $html  html della tabella
  * @param  string $param [description]
  * @return [type]        [description]
  */
  public function getTable(string $html="", $param="")
  {
    if (isset($param['id'])) {
      $id = " id='form-tab-" . $param['id'] . "'";
    } else {
      $id = "";
    }

    $attr="";
    if(isset($param['attr'])){
      $attr=$param['attr'];
    }

    $this->addHtml('<div '. $id . ' class="col-12" '.$attr.'>');
    $this->addHtml($html);
    $this->addHtml('</div><!--div tabella '.$id.'-->');
  }




  /**
  * Genera la tabella in base alla query e ai parametri
  * @param  [type] $query   [description]
  * @param  [type] $id      [description]
  * @param  array  $adParam [description]
  * @return [type]          [description]
  */
  public function divTab(string $id, array $adParam=[])
  {
    $ar=[];
    if (!empty($adParam['title'])) {
      $ar['title']=$adParam['title'];
    } else {
      $ar['title']=$id;
    }


    $ar['adClass']=' tab divTab masterSubTab sezione-'.$this->getSezione();
    if($this->isOnInsert()){
      $ar['adClass'].=" subTable ";
    }
    $ar['id']=$id;
    if (!empty($adParam['return'])) {
      $this->setBuffer();
    }
    if (empty($adParam['NogDiv'])) {
      $this->gDiv($ar);
    }

    if(isset($adParam['callback'])){
      $adParam['callback']();
    }

    if (isset($adParam['event'])) {
      $evento=$adParam['event'];
    } else {
      $evento="";
    }
    // $this->clearSpace();

    if (!empty($adParam['dataset'])) {
      $ds=$adParam['dataset'];
    } else {
      $ds="";
    }

    //devo mostrare la tabella anche in inserimento?
    // if((!$this->isOnInsert() )||(!empty($adParam['showOnInsert']))){
    $this->getTable($this->creaTabEle($id."-table", $adParam,$ds, $evento ),$ar);
    // }
    if (empty($adParam['NogDiv'])) {
      $this->chiudi_gDiv();
    }
    if (!empty($adParam['return'])) {
      return $this->getBuffer();
    } else {
      return true;
    }
  }

  /**
  * crea la tabella standar con i parametri preimpostati
  * @param  string $id    id della tabella
  * @param  string $que   query da eseguire
  * @param  string $event evento custom
  * @return string        html generato con la tabella
  */
  public function creaTabEle(string $id="", array $adParam=[] , string $dataset="", string $event="")
  {

    $tab = $this->getUtility()->getCreaTab();
    $tab->disablePaging();
    if(isset($adParam['activeEscape'])){
      $tab->setActiveEscape($adParam['activeEscape']);
    }
    if (!empty($event)) {
      $tab->setEventRow($event, ['id']);
    }

    if(!empty($adParam['colHtml'])){
      $tab->colHtml($adParam['colHtml']);
    }

    $tab->setReturnHtml();
    $html = $tab->crea($dataset);

    return $html;
  }


  public function openModal($id,array $option=[]){
    if(!empty($option['adClass'])){
      $class=$option['adClass'];
    }else{
      $class="";
    }
    $this->addHtml("<script>cleanModal('$id'); </script>");
    $this->addHtml("<div id='modal-$id' class='modal $class modal-$id '>");
    
  }

  public function chiudi_modal(){
    $this->closeModal();
  }

  public function closeModal(){
    $this->addHtml("</div> <!-- CLOSE MODAL -->");
  }


  public function modalGallery($id="nanoGallery"){
    $this->openModal($id,["adClass"=>"modal-gallery"]);
    $this->addHtml("Caricamento...");
    $this->closeModal();
  }


}//end trait
