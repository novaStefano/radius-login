<!DOCTYPE html>
<html lang="en" ng-controller="MainController as mainCtrl">

<head>
    <meta charset="utf-8">
    <title unifi-portal-custom-title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon-precomposed" href="images/favicons/favicon-152.png">
    <meta name="msapplication-TileColor" content="#0193d7">
    <meta name="msapplication-TileImage" content="images/favicons/favicon-144.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="images/favicons/favicon-152.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/favicons/favicon-144.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="images/favicons/favicon-120.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/favicons/favicon-72.png">
    <link rel="apple-touch-icon-precomposed" href="images/favicons/favicon-57.png">
    <link rel="icon" href="images/favicons/favicon-32.png" sizes="32x32">
    <link href="fonts/1.6.1/lato/style.css" rel="stylesheet">
    <link href="fonts/1.6.1/ubnt-icon/style.css" rel="stylesheet">
    <link href="css/app.css?v=1.6.1" rel="stylesheet">
    <script src="config/config.js?v=1.6.1"></script>
    <script src="js/vendor.js?v=1.6.1"></script>
    <script src="js/components.js?v=1.6.1"></script>
    <script src="js/main.js?v=1.6.1"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="css/index.css">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body ng-cloak unifi-portal-custom-body unifi-portal-custom-body-background-color="mainCtrl.getHotspotConfigModel().bg_color" unifi-portal-custom-body-background-image="mainCtrl.getHotspotConfigModel().getBgImageUrl()" unifi-portal-custom-body-background-image-tile="mainCtrl.getHotspotConfigModel().bg_image_tile"
    unifi-portal-rtl-support>
    
    <style>

        .ng-scope{
            display: none;
        }

    </style>

    
    <!--[if lte IE 9]><div class="appOutdatedBrowserWarning is-warning-visible unifiPortalToastContainer"><![endif]-->
    <!--[if gt IE 9]><!-->
    <div class="unifiPortalOutdatedBrowserWarning unifiPortalToastContainer ng-scope">
        <!--<![endif]-->
        <div class="unifiPortalToast unifiPortalToast--comfy unifiPortalToast--warning">
            <div class="unifiPortalToast__icon icon ubnt-icon--alert"></div>
            <div class="unifiPortalToast__content">
                <div class="unifiPortalToast__header">Outdated Browser</div>
                <div class="unifiPortalToast__body">
                    <p class="noMarginBottom">
                        You are using an <strong>outdated</strong> browser. Please
                        <a href='http://browsehappy.com/'>upgrade your browser</a> to improve your experience.

                    </p>
                </div>
            </div>
        </div>
    </div>
    <div hey-toaster class="unifiPortalToastContainer ng-scope">
        <div class="unifiPortalToast unifiPortalToast--{{toast.level}}">
            <div ng-class="{'ubnt-icon--info': toast.level == 'default', 'ubnt-icon--check-circle': toast.level == 'success', 'ubnt-icon--eye2': toast.level == 'warning', 'ubnt-icon--alert': toast.level == 'danger'}" class="unifiPortalToast__icon icon"></div>
            <div class="unifiPortalToast__content">
                <div class="unifiPortalToastBody">
                    <p class="noMarginBottom">{{toast.message}}</p>
                </div>
            </div>
            <div class="unifiPortalToast__close">
                <a ng-click="toast.close()" class="icon ubnt-icon--x"></a>
            </div>
        </div>
    </div>
    <div ui-view class="ng-scope"></div>
    <script src="js/bootstrap.js?v=1.6.1"></script>

    

    <div class="container">
	<div class="d-flex justify-content-center h-100">
		
		<div id="login" class="card">
			<div class="card-header">
				<h3>Login</h3>
				<!-- <div class="d-flex justify-content-end social_icon">
					<span><i class="fab fa-facebook-square"></i></span>
					<span><i class="fab fa-google-plus-square"></i></span>
					<span><i class="fab fa-twitter-square"></i></span>
				</div> -->
			</div>
			<div class="card-body">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" class="form-control" placeholder="username" id="log_username">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" id="log_password" placeholder="password">
					</div>
					<!-- <div class="row align-items-center remember">
						<input type="checkbox">Remember Me
					</div> -->
					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					Non sei registrato?<a href="#" onclick="activeRegistrati()">Registrati</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="#">Hai perso la password?</a>
				</div>
			</div>
		</div>



		<div id="subscription" class="card" style="display:none">
			<div class="card-header">
				<h3>Registrati</h3>

			</div>
			<div class="card-body">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" id="reg_name" class="form-control" placeholder="Nome">
					</div>


					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" class="form-control" id="reg_mail" placeholder="e-mail">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" id="reg_password" placeholder="password">
					</div>
					<!-- <div class="row align-items-center remember">
						<input type="checkbox">Remember Me
					</div> -->
					<div class="form-group">
						<input type="submit" value="Registrati" onclick="subscribe()" class="btn float-right login_btn">
					</div>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					Non sei registrato?<a href="#" onclick="activeRegistrati()">Registrati</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="#">Hai perso la password?</a>
				</div>
			</div>
		</div>

	</div>
</div>

<script>

function activeRegistrati(){
	$('#login').hide();
	$('#subscription').show();
}


function subscribe(){
	var data={};
	data.azione="registrati";
	data.email=$('#reg_mail').val();
	data.name=$('#reg_name').val();
	data.password=$("#reg_password").val();

	console.log(data);

	var res = $.ajax({
		method:"POST",
		url:"/core/reg.php",
		data:data
	});

	res.done(function(r){
		console.log(r);
	});
}


function login(){
    $('input[name="username"]').val($('log_username').val());
    $('input[name="password"]').val($('log_password').val());
    $('button.unifiPortalMainButton.unifiPortalCustomButton').click();
}

</script>










</body>

</html>