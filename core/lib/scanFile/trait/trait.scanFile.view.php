<?php

/**
* Trait con i metodi di manipolazione delle cartelle / file
*/
trait scanFileView
{

  /**
  * stampa ad output l'immagine della directory
  */
  private function getImgDir():void
  {
    echo $this->getUtility()->getPath('A_IMMAGINI')."/scanFile/dir.png";
  }


  private function getCustomExt($ext){

    switch($ext){
      case 'xls':
      $img="xlsx";
      break;

      case 'doc':
      $img='docx';
      break;
      default:
      $img="file";
      break;
    }
    return $img;
  }


  /**
  * stampa ad output l'immagine del file
  */
  private function getImgFile(string $file):void
  {

    $ext=$this->getUtility()->getExtFile($file);
    $ext=strtolower($ext);
    if($ext=='jpeg'){
      $ext='jpg';
    }
    $mapped=["pdf","docx","gif","html","jpg","xlsx","odt","ods","zip"];
    if(in_array($ext,$mapped)){
      $img=$ext;
    }else{
      $img=$this->getCustomExt($ext);
    }
    echo $this->getUtility()->getPath('A_IMMAGINI')."/scanFile/$img.png";
  }

  /**
  * taglia la cartella in caso sia troppo lungo
  * @param  string $dir [description]
  * @return [type]      [description]
  */
  private function maxCarDir(string $dir,$car):string
  {
    return $this->getUtility()->maxCarDir($dir,$car);
  }


  /**
  * Torna la classe per il js
  * @return string classe js
  */
  private function getClassJs()
  {
    return $this->classJs;
  }



  private function noFileFound(){
    echo "<h1><b>Nessun File trovato nell'area</b></h1>";
    die;
  }

  /**
  * Stampa i file i html
  * @return [type] [description]
  */
  public function printFile()
  {
    $allFile= $this->getMapFile();
    $find=false;
    sort($allFile);
    foreach ($allFile as $record) {
      $find=true;
      if ($record['dir']=='1') {
        $this->htmlDir($record);
      } else {
        continue;
      }
    }


    foreach ($allFile as $record) {
      $find=true;
      if ($record['dir']=='1') {
        continue;
      } else {
        $this->htmlFile($record);
      }
    }

    if(!$find){
      $this->noFileFound();
    }
  }


  /**
  * stampa l'html per la carella
  * @param  string $map map della directory
  * @return stodut      html dir
  */
  private function htmlDir(array $map):void
  {
    $dir=$map['name'];
    $data=$map['dataModified'];
    $classJs=$this->getClassJs();
    $title="Cartella $dir \nUltima Modifica: $data "; ?>
    <div title="<?= $title ?>" ondragover="<?= $this->getClassJs() ?>.dragOverDir('<?= $dir; ?>');"
      draggable="true"  ondragstart="<?= $classJs ?>.startDragFileHtml('<?= $dir; ?>');"
      ondrop="<?= $this->getClassJs() ?>.dropFileHtmlOverDir(event,'<?= $dir; ?>');"
      onclick="<?= $this->getClassJs() ?>.apriDir('<?= $dir; ?>')" class="fileContDiv fileStartContext" file="<?= $dir; ?>" type="dir" >
      <img class="imgScanFile" src="<?php $this->getImgDir(); ?>"><br>
      <div class="title"><?= $this->maxCarDir($dir,30); ?> </div>
    </div>
    <?php
  }


  /**
  * Generazione della vista per file
  * @param  [type] $map [description]
  * @return [type]      [description]
  */
  private function htmlFile(array $map):void
  {
    $file=$map['name'];
    $peso=$map['size'];
    $data=$map['dataModified'];
    $classJs=$this->getClassJs();
    $title="File $file \nDimesioni: $peso \nUltima Modifica: $data "; ?>
    <div title="<?= $title; ?>"
      draggable="true"  ondragstart="<?= $classJs ?>.startDragFileHtml('<?= $file; ?>');"
      onclick="<?= $classJs ?>.apriFile('<?= $file; ?>')"
      class="fileContDiv fileStartContext" file="<?= $file; ?>" type="file">
      <img class="imgScanFile" src="<?php $this->getImgFile($file); ?>"><br>
      <div class="title"><?= $this->maxCarDir($map['name'],30) ?> </div>
    </div>
    <?php
  }

}//end trait



?>
