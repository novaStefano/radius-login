<?php

/**
 * Modulo per il caricamento, salvataggio e estrazione file integrato con field
 * Da testare
 */

class FileStore extends wrapperUtility{
  private $idFile=""; //idD del file su cui sto lavorando

  /**
  * Torna l'id deltoken in sessione
  */
  private function loadIdFromToken(){
    $token=$this->getUtility()->getFieldToken();
    $idRecord= $token->getIdFromTokenField();
    $table=$token->getTableName();
    $col=$token->getColName();
    $this->startPrepare(" SELECT $col from $table where $col = :id  ");
    $this->bindP("id",$idRecord);
    $this->executePrepare();
    if($this->isDatasetEmpty()){
      $this->error("Record non trovato");
    }
    $this->setIdFile($this->fetch()[$col]);
  }


  /**
  * Setter dell'id file se voglio gestirlo senza tokenForm
  * @param string $idFile [description]
  */
  public function setIdFile(string $idFile){
    $this->idFile=$idFile;
  }

  /**
  * Torna l'id del file in lavorazione
  * @return string idFile
  */
  private function getIdFile():string{
    if(empty($this->idFile)){
      $this->loadIdFromToken();
    }
    return $this->idFile;
  }

  public function start(string $azione){

    switch ($azione) {
      case 'upload':
      $this->upload();
      break;
      case 'getFile':
      $this->retrieveFile();
      break;

      case 'deleteFile':
      $this->deleteFile($this->getPost('idFile'));
      break;

      default:
      break;
    }
  }//end post launcher




  private function incFilePost(){
    $this->iPostFile++;
  }

  private function clearFilePost(){
    $this->iPostFile=0;
  }

  private function getFilePost(){
    return $this->iPostFile;
  }

  private function log($str){
    if($this->isDebug()){
      $this->setRes('log',$str);
    }
  }

  public function setIdFileGroup($id){
    $this->idGruppoFile=$id;
  }

  private function getIdFileGroup(){
    if(empty($this->idGruppoFile)){
      return false;
    }else{
      return $this->idGruppoFile;
    }
  }

  //Restituisce la directory per salvare il file
  private function getDirUpload(){
    $app= $this->getPath('A_UPLOAD');
    $group=$this->getIdFileGroup();
    if(!$group){
      $app.="file/";
    }else{
      if(!file_exists($app."gruppoFile/")){
        mkdir($app."gruppoFile/");
      }
      $app.="gruppoFile/$group/";
    }
    if(!file_exists($app)){
      mkdir($app); //creo la cartella che serve

    }
    return $app;

  }

  //crea la nuova area di archivio per il gruppo dei file
  // Se devo archiviare tanti file per un'entità è la scelta giusta! Altrimenti no
  public function newStoreGroup(string $descrizione=''){

    $str=" insert into file_gruppo (descrizione,dataInsert)values(:desc,now()) ";
    $this->startPrepare($str);
    $this->bindP('desc',$descrizione);
    $this->executePrepare();
    $id= $this->getMyLastIns();
    $this->setIdFileGroup($id);
    $this->log('Nuovo gruppo file inserito - ID:'.$id);
    $this->logDb('Nuovo gruppo file inserito - ID:'.$id);
    $this->json('idFileGruppo',$id);
    $this->json('fileGruppo',$descrizione);
    return $id;
  }

  //ottengo il percorso univoco per il file
  private function getPercFile(string $nomeFile,$i=""){
    if($i!=""){
      $pre=$i."-";
    }else{
      $pre="";
    }

    $newFile =$this->getDirUpload().$pre.basename($nomeFile);

    if(file_exists($newFile)){
      $nomeFile=$nomeFile; //aggiungo un contatore per avere il file univoco in cartella e in db
      $newFile= $this->getPercFile($nomeFile,$i+1);
    }
    return $newFile;
  }

  //setta l'array di ritorno con gli id inseriti
  private function setIdRes($nome,$id){
    $post=$this->getPostWork();
    // $this->web->resJson['idFile'][$post][$this->getPostWork()]['nome']=$nome;
    // $this->web->resJson['idFile'][$post][$this->getPostWork()]['id']=$id;

    $this->resJson['idFile']['nome']=$post;
    $this->web->resJson['idFile']['id']=$id;
    $this->web->resJson['idFile']['nomeFile']=$nome;
    $gr=$this->getIdFileGroup();

    // $this->web->resJson['idFile'][$post][$this->getPostWork()]['fileGroup']=$gr;
    $this->web->resJson['idFile']['fileGroup']=$gr;

  }


  //carica il file nel database
  private function insDbFile($nome,$path,$size='null'){
    $nome=$this->estr($nome);
    $path=$this->estr($path);
    $size=$this->estr($size);
    $gruppo=$this->getIdFileGroup();
    if(!$gruppo){$gruppo='null';}
    $post=$this->getPostWork();
    if(empty($post)){$post='null';}else{$post="'".$this->estr($post)."'";}
    $str=" insert into file(nome,size,path,idGruppo,id,dataInsert)values('$nome',0,'$path',$gruppo,$post,now()) ";
    $this->query($str);
    $idFile=$this->getMyLastIns();
    $this->log("File $nome POST $post inserito con id: $idFile");
    $this->setIdRes($nome,$idFile);
  }

  //carica il file nella cartella corretta e salva l'id - sempre dall'array $_FILES
  private function doStoreFilePost($file){
    $nomeFile=$file['name'];
    $path=$file['tmp_name'];
    $size=$file['size'];
    $newFile=$this->getPercFile($nomeFile);

    //spostamento del file nella directory giusta
    if (move_uploaded_file($path, $newFile)) {
      $this->log("File $newFile spostato nella cartella corretta ");
      $this->insDbFile($nomeFile,$newFile,$size);
    }else{
      $this->log(" Errore spostamento file $newFile ");
      $this->setRes('errore','Errore nel caricamento dei file -->'.$newFile.' non caricato');
    }
  }


  //metodo per il caricamento del file tramite post
  public function storeFileFromPost(){
    //controllo l'esistenza di più file esistenti
    $this->clearFilePost();

    foreach ($_FILES as $file) {

      $nome=$file['name'];
      $post=$this->getPost('file');
      $this->postWork($post);
      $this->doStoreFilePost($file);
      $this->incFilePost();
    }
  }


  /**
  * Metodo per caricare il file nel fileStore - handler standard per la chiamata ajax
  */
  public function upload(){
    $this->deleteOldFile(); //controllo se devo sostituire il vecchio file
    $this->storeFileFromPost(); //salva il file presente nell'array standard
    $this->halt(); //fine routine
  }


  /**
  * Devo eliminare il vecchio file? Procedo
  */
  private function deleteOldFile(){
    $old=$this->post('oldFile');
    if(!empty($old)){
      $this->deleteFile($old,false);
    }
  }

  //restituisce il dataset con il file cercato
  private function getFileInfo(){
    $this->startPrepare('SELECT nome,path,idFile from file where idFile=:idFile ');
    $this->bindP("idFile",$this->getIdFile());
    $this->executePrepare();
    if($this->isDatasetEmpty()){
      $this->error("File non trovato");
    }
    return $this->fetch();
  }

  private function getPathTmpFile($file){
    $tmp=$this->getPath('A_TMP_UPLOAD');
    if(empty($tmp)){ //cartella upload disabilitata, procedo con il file diretto
      return false;
    }
    $rand=$this->web->codiceRandom(6);
    $tmp=$tmp.$rand."/";
    mkdir($tmp);
    return $tmp;
  }

  /**
   * Metto in scarica il file che devo mostrare
   */
  public function retrieveFile(){
    $row=$this->getFileInfo();
    $this->getUtility()->headerFile($row['nome'],$row['path']);
  }

  //elimina il file e elimina il record
  public function deleteFile($idFile,$printInfo=true){
    $row=$this->getFileInfo();
    if(unlink($row['path'])){
      $this->log('File Eliminato '.$idFile);
    }else{
      $this->error('Impossibile eliminare il file');
    }

    $str="DELETE from file where idFile=:idFile";
    $this->startPrepare($str);
    $this->bindP("idFile",$idFile);
    if($this->executePrepare($str)){
      $this->success('File eliminato con successo');
    }

  }

} //end class
