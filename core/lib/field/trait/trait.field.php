<?php

/** metodi per la creazione degli elementi veri e propri
 *
 **/
trait newField
{
	public function title(string $title)
	{
		$this->titolo($title);
	}

	/**
	 * Carica i css
	 * @return
	 */
	public function loadCss(string $css, array $options = [])
	{
		$this->addHtml($this->getUtility()->loadCss($css, $options));
	}

	/**
	 * Aggiunge una view a questa sessione
	 **/
	public function loadView(string $nameView, array $data = [], array $options = [])
	{
		$this->addHtml($this->getUtility()->loadView($nameView, $data, $options));
	}

	private function getPreInput()
	{
		$id = $this->getparamId();
		$label = $this->getLabel($id);
		$subLabel = $this->getSubLabel($id);
		$preHtml = $this->getWork('preHtml');
		return $label . $subLabel . $preHtml;
	}


	/**
	 * input Select standard
	 * @param  array $param parametri per la creazione della class
	 * @return addHtml
	 */
	public function newSelect($param = [])
	{
		if (empty($param['noDefault'])) {
			$param = $param + $this->BinputModel($param); //aggiungo i valor di default ai parametri
		}
		$this->setParamWork($param);
		$id = $this->getparamId();
		$attr = $this->getAttr();
	
		$valore = $this->getWork("val");   //$this->procVal($id, $type);
	
		if ($this->getWork('resArray')) { //genero le option della select
			$option= $this->optionSelectArray($valore);
		} else {
			$option = $this->optionSelectMysql($valore);
		}
		
		$wScheda = $this->getWorkingMultiScheda();
		$this->mergeClasse();
		$class = $this->getParamHtml("class", "class");
		$style = $this->getParamHtml("style", "style");
		$event = $this->getWork('event');
		$this->groupByName();
		$this->setRegex();
		$this->procAutoGrid(" col-6 col-md-4 ");

		$fineHtml = $this->getWork('fineHtml');
		$app =  $this->getPreInput() . "<select $event $attr  active-scheda='$wScheda' val-sel='$valore' $class $style id='$id' type='select' >";
		//posso creare il dataset da un arrya formatatto come $res[0]['col'] / $res[0]['valore'] o da dataset mysql
		$app.=$option;
		$app .= "</select>" . $fineHtml;
		$this->printElement($app);

		$type = $this->getWork("selectType");
		if (!$type) {
			$type = "int"; //defualt tipo per i select
		}
		$this->setWork("type", $type);
	}



	private function procRadioRows($r, &$idVal, &$val)
	{
		if (is_array($r)) {
			if (count($r) > 1) {
				$idVal = array_shift($r);
				$val = array_shift($r);
			} else {
				$idVal = array_shift($r);
				$val = $idVal;
			}
		} else {
			$idVal = $r;
			$val = $r;
		}
	}


	/**
	 * input Radio standard
	 * @param  array $param parametri per la creazione della class
	 * @return addHtml
	 */
	public function newRadio($param = [])
	{
		if (empty($param['noDefault'])) {
			$param = $param + $this->BinputModel($param); //aggiungo i valor di default ai parametri
		}
		$this->setParamWork($param);
		$id = $this->getparamId();
		$attr = $this->getAttr();
		$this->procAutoGrid("col-12 col-md-12");
		$preHtml = $this->getWork('preHtml');
		$this->setWork('preHtml', "");
		$fineHtml = $this->getWork('fineHtml');

		$valore = $this->getWork("val");   //$this->procVal($id, $type);
		$this->mergeClasse();
		$class = $this->getParamHtml("class", "class");
		$style = $this->getParamHtml("style", "style");
		$event = $this->getWork('event');
		$table = $this->getWork("table");
		// $this->groupByName();
		$reverse = $this->getWork("reverse");
		$app =  $this->getPreInput();
		$res = $this->getWork('resArray');
		$wScheda = $this->getWorkingMultiScheda();
		$classContainer = $this->getWork("classContainer");

		$i = 0;
		$customValue = $valore;
		if ($valore == "") {
			$customCheck = "";
		} else {
			$customCheck = "checked";
		}
		$app .= "<div id='radio-$id-$table' active-scheda='$wScheda' class='radio-container col-12 radio-$id-$table '>" . $preHtml;
		foreach ($res as $r) {
			$this->procRadioRows($r, $idVal, $val);

			if (($valore == $idVal) && (isset($valore))) {
				$che = "checked";
				$customCheck = "";
				$customValue = "";
			} else {
				$che = "";
			}
			$input = "<input type='radio' active-scheda='$wScheda'  number='$i' custom-input='false' val-check='$che'
			$che $attr $event $class $style table='$table' id-col='$id'
			name='$id-$table' id='$id-$i' value='$idVal'>";
			$label = " class='label-radio label-radio-$id-$table'
			for='$id-$i'> ";
			$i++;
			if (!$reverse) {
				$el = "<label" . $label . $input . $val . "</label>";
			} else {
				$el = "<label" . $label . $val . "</label>" . $input;
			}
			$app .= "<div class='container-radio-value $classContainer ' >" . $el . "</div>";
		} //fine stampa radio


		if ($this->getWork("customInput")) {
			$input = "<input type='radio' active-scheda='$wScheda' number='$i' custom-input='true'
			$customCheck val-check='$customCheck' value='custom-input'
			$attr $event $class $style table='$table' id-col='$id'
			name='$id-$table' id='$id-$i'>";
			$label = "<label class='label-radio label-radio-$id-$table'

			for='$id-$i'>altro</label>
			<input type='text' active-scheda='$wScheda' $style $attr class='form-control custom-value-radio custom-value-radio-$id-$table'
			onchange='liveValidation.validationCustomInputMulti(this)'
			onblur='liveValidation.validationCustomInputMulti(this)'
			onfocus='enterCustomInputCheck(this)'
			value='$customValue' for='$id-$i'>";
			$i++;
			if (!$reverse) {
				$el = $input . $label;
			} else {
				$el = $label . $input;
			}
			$app .= "<div class='container-radio-value'>" . $el . "</div>";
		}



		$app .= $fineHtml;
		$app .= "<div class='clearspace'></div>";
		$app .= "</div>";
		$this->printElement($app);
		$type = $this->getWork("selectType");
		if (!$type) {
			$type = "int"; //defualt tipo per i select
		}
		$this->setWork("type", $type);
	}







	/**
	 * input multi checkbox standard
	 * @param  array $param parametri per la creazione della class
	 * @return addHtml
	 */
	public function newMultiCheck($param = [])
	{
		if (empty($param['noDefault'])) {
			$param = $param + $this->BinputModel($param); //aggiungo i valor di default ai parametri
		}

		$json = true;
		$this->setParamWork($param);
		$id = $this->getparamId();
		$attr = $this->getAttr();
		$valore = $this->getWork("val");   //$this->procVal($id, $type);
		$wScheda = $this->getWorkingMultiScheda();
		$this->procAutoGrid("col-12 col-md-12");
		if ($json) {
			if (!empty($valore)) {
				$res=[];
				$valore = json_decode($valore);
				foreach ($valore as $val) {
					$res[$val] = $val;
				}
				$valore = $res;
			} else {
				$valore = [];
			}
		}

		$this->mergeClasse();
		$class = $this->getParamHtml("class", "class");
		$style = $this->getParamHtml("style", "style");
		$event = $this->getWork('event');
		$table = $this->getWork("table");

		$fineHtml = $this->getWork('fineHtml');
		$app =  $this->getPreInput();
		$res = $this->getWork('resArray');
		$i = 0;

		$customValue = $valore;
		if ($valore == "") {
			$customCheck = "";
		} else {
			$customCheck = "checked";
		}
		$app .= "<div id='multicheck-$id-$table' active-scheda='$wScheda' class='multicheck-container col-12 multicheck-$id-$table '>";
		foreach ($res as $r) {
			if (is_array($r)) {
				if (count($r) > 1) {
					$idVal = array_shift($r);
					$val = array_shift($r);
				} else {
					$idVal = array_shift($r);
					$val = $idVal;
				}
			} else {
				$idVal = $r;
				$val = $r;
			}
			if ((in_array($idVal, $valore)) && (!empty($valore))) {
				$che = "checked";
				unset($valore[$idVal]);
			} else {
				$che = "";
			}

			$input = "<input type='checkbox' active-scheda='$wScheda' tipo='multicheck' multicheck='true'
			json='true' number='$i' custom-input='false'
			$che $attr $event $class $style table='$table' id-col='$id'
			name='$id-$table' id='$id-$i' value='$idVal'>";
			$label = "<label class='label-radio label-radio-$id-$table'
			for='$id-$i'>$val</label>  ";
			$i++;
			$app .= "<div class='container-multicheck-value' >" . $input . $label . "</div>";
		} //fine stampa radio


		if ($this->getWork("customInput")) {


			if (!empty($valore)) {
				$customCheck = "checked";
				$customValue = array_pop($valore);
			} else {
				$customCheck = "";
				$customValue = "";
			}

			$input = "<input type='checkbox' active-scheda='$wScheda' tipo='multicheck' multicheck='true' json='true'
			number='$i' custom-input='true' $customCheck
			$che $attr $event $class $style table='$table' id-col='$id'
			name='$id-$table' id='$id-$i'>";
			$label = "<label class='label-multicheck label-multicheck-$id-$table' for='$id-$i'>altro</label>
			<input type='text' $attr $style active-scheda='$wScheda' class='form-control custom-value-multicheck custom-value-multicheck-$id-$table'
			onchange='liveValidation.validationCustomInputMulti(this)'
			onblur='liveValidation.validationCustomInputMulti(this)'
			onfocus='enterCustomInputCheck(this)'
			value='$customValue' for='$id-$i'>";
			$i++;
			$app .= "<div class='container-multicheck-value'>" . $input . $label . "</div>";
		}
		$app .= "<div class='clearspace'></div>";
		$app .= "</div><!-- container multicheck --> ";
		$app .= $fineHtml;
		$this->printElement($app);
		$type = $this->getWork("selectType");
		if (!$type) {
			$type = "int"; //defualt tipo per i select
		}
		$this->setWork("type", $type);
	}








	/**
	 * Crea la select da un array
	 * @param  [type] $valore [description]
	 * @return [type]         [description]
	 */
	private function optionSelectArray(string $valore = "")
	{
		$res = $this->getWork('resArray');
		$class = $this->getParamHtml("classChild", "class");
		$style = $this->getParamHtml("styleChild", "style");
		$app = "";

		if (!$this->getWork("required")) {
			$def = "--";
		} else {
			$def = $this->getWork('defNull');
		}



		if ($def) {
			if (empty($def)) {
				$def = 'TUTTI';
			}
			$app .= "<option  $class $style value=''>" . $def . "</option>";
		}
		$cRes = count($res);
		for ($i = 0; $i < $cRes; $i++) {
			$app .= "<option  $class $style " . $this->procOptionSelectAr($res[$i], $valore);
		}
		return $app;
	}

	private function procOptionSelectAr($res, string $valore = "")
	{
		if (is_array($res)) {
			$val = $res['val'];
			$id = $res['id'];
		} else { //fine is array
			$val = $res;
			$id = $res;
		} //end else
		if ($id == $valore) {
			$sele = " selected=\"selected\" ";
		} else {
			$sele = "";
		}
		return " $sele value='" . $id . "'>" . $val . "</option>";
	}

	/**
	 * Aggiunge elementi a un array
	 * @param  array  $var    [description]
	 * @param  [type] $valore [description]
	 * @param  string $id     [description]
	 * @return [type]         [description]
	 */
	public function adResArray(&$var = [], string $valore = "", string $id = '')
	{
		$i = count($var);
		if ($id == '') {
			$var[$i]['id'] = $id;
			$var[$i]['val'] = $valore;
		}
	}

	/**
	 * Genera le option mysql partendo da un dataset
	 * @param  string $valore [description]
	 * @return [type]         [description]
	 */
	public function optionSelectMysql(string $valore = "")
	{ //stampa gli elementi partendo da un dataset mysql
		$app = "";
		$res = $this->getWork('res');
		if (!$res) {
			return false; //dataset non settato, html vuoto
		}
		$indOption = $this->getWork('indOption'); //indice per prendere l'id della option
		if (!$indOption) {
			$indOption = 0;
		}
		$valOption = $this->getWork('valOption'); // indice per il valore della option
		if (!$valOption) {
			$valOption = 1;
		}
		$titleOption = $this->getWork('titleOption'); // indice per il valore della option
		if (!$titleOption) {
			$titleOption = 2;
		}
		$class = $this->getParamHtml("classChild", "class");
		$style = $this->getParamHtml("styleChild", "style");

		$defNull = $this->getWork("defNull");

		if (!$this->getWork("required") && ($this->getWork("defNull") == false)) { //con not required devo disattivare il null
			$defNull = "--";
		}

		// die;

		if ($defNull) {
			$event = $this->getEventParam("", 'event');
			$attr = $this->getEventParam("", 'dataAttr');
			$app .= "<option  $event $attr $class $style value=''>" . $defNull . "</option>";
		}
		
		while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
			$this->makeArrayFetch($row);
			if ($valore == $row[$indOption]) {
				$sele = " selected=\"selected\" ";
			} else {
				$sele = "";
			}
			$event = $this->getEventParam($row, 'event');
			$attr = $this->getEventParam($row, 'dataAttr');

			if (isset($row["title"])) {
				$title = $this->getUtility()->escapeHtml($row[$titleOption]);
				$title = "title='$title'";
			} else {
				$title = "";
			}	

			$attr="";	


			if (isset($row['--label'])) {
				try {
					$r = explode(".", $row['--label'], 2);
					if (count($r) < 2) {
						return "";
					}
					$row['colore'] = $r[0];
				} catch (Exception $err) {
					$this->error("ERRORE BALOON CREALISTA");
				}
			}

		

			if(!empty($row['colore'])){
				$nCl="inlineSearch-label-".$row['colore'];
				$attr= "onclick='changeColorSelect(this)'  label-class='$nCl' ";
				if($sele){
					$attr=$this->getWork("attr");
					$attr.=" $nCl ";
					$this->setWork("attr",$attr);

					$class=$this->getWork("adClass");
					$class.=" $nCl ";
					$this->setWork("adClass",$class);
				}
			}

			$value = $this->getUtility()->escapeHtml($row[$indOption]);
			$desc = $this->getUtility()->escapeHtml($row[$valOption]);
			$app .= "<option $attr  $sele  $event $attr $class $title $style value='$value'>$desc</option>";
		}
		
		return $app;
	}

	/**
	 *Alias per un nuovo button
	 * @param  array  $param [description]
	 * @return [type]        [description]
	 */
	public function newBtn(array $param = [])
	{
		return $this->newButton($param);
	}

	/**
	 * Genera un button con i componenti standard
	 * @param  [type] $param [description]
	 * @return [type]        [description]
	 */
	public function newButton(array $param = [])
	{ //crea l'elemento bottone

		if (empty($param['noDefault'])) {
			$param = $param + $this->BbuttonModel(); //aggiungo ai parametri il default
		}
		$this->setParamWork($param);
		$id = $this->getparamId();

		$click = $this->getWork("click");
		if ($click) {
			$click = ' onclick="' . $click . '" ';
		}

		$event = $this->getWork('event');
		$html = $this->getWork('html');
		$attr = $this->getWork('attr');
		$icon = $this->getWork("icon");
		if (empty($icon)) {
			$icon = $this->getWork("ico");
		}

		if (!empty($icon)) {
			$icon = $this->getButtonIco($icon);
		}


		if ($this->getWork("round")) {
			$this->setWork('adClass', "btn-rounded centerFormBtn");
		}
		if ($this->getWork("hideOnInsert")) {
			if ($this->isOnInsert()) {
				$this->setWork('adClass', " masterSubTab ");
				$this->setWork('style', " display:none; ");
			}
		}



		if ((!empty($this->getWork('adClass'))) || (!empty($this->paramWork['class']))) {
			$this->setWork('class', $this->getWork('class') . " " . $this->getWork('adClass'));
		}


		$class = $this->getParamHtml("class", "class");
		$style = $this->getParamHtml("style", "style");

		$button = "<button id='$id' $attr $click $event $class $style  > " . $html . $icon . "  </button>";
		if ($this->getWork('retStr')) {
			return $button;
		} else {
			$this->printElement($button);
		}
	}


	private function makeArrayFetch(&$row)
    {	
		if(!$row){
			return;
		}
        $i=0;
        foreach ($row as $key => $value) {
           $row[$i]=$value;
           if(strpos($key,"--label") ){
                $row['--label']=$value;
            }
            $i++;
        }
    }


	public function newVimeo(array $param){
		if(empty($param['fineHtml'])){
			$param['fineHtml']="";
		}
		if(empty($param['adClass'])){
			$param['adClass']="";
		}
		$param['adClass'].=" vimeo-bind ";
		$video=$param['val'];
		$video=str_replace("https://vimeo.com/","",$video);

		if(empty($video)){
			$add=" style='display:none'";
		}else{
			$add="";
		}

		$html="<div class='col-12 vimeo-col' $add> 
		<div class='vimeo-container'>
		<iframe src='https://player.vimeo.com/video/$video' class='vimeo-frame'
			 frameborder='0' allow='autoplay; fullscreen; picture-in-picture' allowfullscreen></iframe>
		 </div><script src='https://player.vimeo.com/api/player.js'></script>
		 <script>bindVimeo();</script>		
		 </div>
		 ";
		
		$param['fineHtml'] = $param['fineHtml'].$html;
		$this->newInput($param);
	}

	/**
	 *Crea un elemento di ricerca man mano viene digitato
	 **/
	public function newInputSearch(array $param = [])
	{ //edit di ricerca con elementi in lista div sotto (abbinata a cerca.php /cerca.js
		$this->setParamWork($param);
		$table = $this->getWork('table');
		$id = $this->getWork('id');
		$val = $this->getWork('val');
		$wScheda = $this->getWorkingMultiScheda();

		$source = $this->getWork("dataSource");
		if ($this->getWork("suggest")) {
			$suggest = " suggest='suggest' ";
		} else {
			$suggest = '';
		}

		if (!$source) {
			$source = $id;
		}

		if (!$this->getWork("suggest")) {
			
			$this->getUtility()->cleanLastDataset();


			$this->getUtility()->getInSearchRefresh($source . "-get", $val);
			if(!$this->getUtility()->isLastdatasetLoaded()){ //trovato sulla classe in esecuzione o nel tSearchRefresh
				$model=$this->getUtility()->getModel(str_replace("id", "", str_replace("id_", "", $source)));      
				$method="dsSearchGet";
				$model->$method($val);
			}
		   
			$fetch = $this->getUtility()->fetch();
			$this->makeArrayFetch($fetch);

			if (!empty($fetch[1])) {
				$param['val'] = $fetch[1];
			} else {
				$param['val'] = "";
			}
		}

		// if (!empty($fetch[2])) {
		// 	$param['val'] .= " (" . $fetch[2] . ")";
		// }


		if (isset($fetch['--label'])) {
            try {
                $r = explode(".", $fetch['--label'], 2);
                if (count($r) < 2) {
                    return "";
                }
                $color = $r[0];
                $label = $r[1];
            } catch (Exception $err) {
                $this->error("ERRORE BALOON CREALISTA");
            }
			if(empty($param['adClass'])){
				$param['adClass']="";
			}
            $adClass = "inlineSearch-label-$color";
			$param['adClass'].=" $adClass ";
			if(empty($param['attr'])){
				$param['attr']="";
			}
			$param['attr'].=" label-class='$adClass' ";
        }





		$callback = $this->getWork("callback");
		$addParam = $this->getWork("addParam");
		$change = ""; //verifico se devo attivare un evento dopo il change
		if (!empty($this->getWork("changeEvent"))) {
			$change = $this->getWork("changeEvent");
			$change = " onChangeEvent='$change'";
		}

		if(empty($param['fineHtml'])){
			$param['fineHtml']="";
		}

		// $adAttr
		$add="";
		if(!empty($param['attr'])){
			$add=$param['attr'];
		}

		$param['attr'] =$add." $suggest  callBackSelect='$callback' data-source='$source' $change  
		addParam='$addParam' idSelect='$val' tok='$table'
		 active-scheda='$wScheda' sezione='{$this->getSezione()}' ";
		$param['type'] = "search";
		$idCerca = "$id-$table";
		$html = "<div class='lista $idCerca-Lista ' id='$idCerca-lista'></div>";
		$html .= '<script>inSearch.bindEventSearch(".' . $idCerca . '");</script>';
		$param['fineHtml'] = $param['fineHtml'].$html;
		$this->newInput($param);
	}


	/**
	 * Dati hiden
	 * @param  [type] $param [description]
	 * @return [type]        [description]
	 */
	public function newHidden(array $param = [])
	{
		if (empty($param['type'])) {
			$param['type'] = "hidden";
		}
		$this->newInput($param);
	}


	/**
	 *Elemento che mostra il dato non modificabile
	 **/
	public function newLabelValue(array $param = [])
	{
		if (empty($param['adClass'])) {
			$param['adClass'] = "";
		}
		$param['adClass'] .= " spanLabelValue labelValue ";
		if (empty($param['noDefault'])) {
			$param = $param + $this->BLabelValueModel(); //aggiungo i valor di default ai parametri
		}
		$this->setParamWork($param); //aggiungo l'array alla variabile di lavoro per la generazione dell'elemento
		$id = $this->getparamId();
		$valore = $this->procVal($id, $type);

		$style = $this->getParamHtml("style", "style");
		$adTipo = $this->getParamHtml('adTipo', 'type');
		$attr = $this->getAttr();
		$event = $this->getWork('event');
		// $this->groupByName();
		$this->mergeClasse();

		$class = $this->getParamHtml("class", "class");
		$fineHtml = $this->getWork('fineHtml');
		$input = ($this->getPreInput() . "\n<span  $event $class $style id='$id' >" . $valore . "</span>\n" . $fineHtml);
		$this->printElement($input);
	}



	public function newOpenModule($param)
	{
		if (empty($param['adClass'])) {
			$param['adClass'] = "";
		}
		if (empty($param['noDefault'])) {
			$param = $param + $this->BLabelValueModel(); //aggiungo i valor di default ai parametri
		}
		$this->setParamWork($param); //aggiungo l'array alla variabile di lavoro per la generazione dell'elemento
		$id = $this->getparamId();
		$type="";
		$valore = $this->procVal($id, $type);
		if (empty($valore)) { //salto la sezione in caso di parametro vuoto perchè non devo vedere il bottone
			return;
		}
		$attr = $this->getAttr();
		$event = $this->getWork('event');
		$this->mergeClasse();
		$color = $this->getWork("color");
		if (empty($color)) {
			$color = "success";
		}
		$modulo = $this->getWork("modulo");
		if(empty($modulo)){
			$modulo=str_replace("id","",$id);
			$modulo=ucfirst($modulo);
		}
		$labelBtn=$this->getWork("span");

		if(empty($labelBtn)){
			$labelBtn = $this->getUtility()->humanCase($modulo)." ".$valore;
		}
		
		$class = $this->getParamHtml("class", "class");
		$fineHtml = $this->getWork('fineHtml');
		$ico = $this->getWork("ico");
		if (!empty($ico)) {
			$ico = $this->getButtonIco($ico) . " ";
		}

		$input = ($this->getPreInput() . "\n<div class='col-12'><span  onclick=\"openNewTabDettaglio('$modulo','$valore');\"  $attr $event class='pointer label label-$color $class'>{$ico}{$labelBtn}</span></div>\n" . $fineHtml);
		$this->printElement($input);
	}

	/**
	 * Alias per creare un nuovo input (newElement)
	 * @param  array  $param [description]
	 * @return [type]        [description]
	 */
	public function newElement(array $param = [])
	{
		$this->newInput($param);
	}

	public function newDatetime(array $param = [])
	{
		// return 0;
		$p = $param;
		$target = $p['id'] . "-target-dateTime-time";
		$param['attr'] = " datetime-time='" . $target . "' ";
		$param['type'] = "date";
		$param['adTipo'] = "datetime";
		$param['label'] = "Data " . $p['label'];
		$param['val'] = substr($p['val'], 0, 10);
		$this->newInput($param);

		$param['label'] = "Ora " . $p['label'];
		$param['id'] = $target;
		$param['type'] = "time";
		$param['insert'] = "0";
		$param['update'] = "0";
		$param['adClass'] = "noSelectForm";
		$param['val'] = substr($p['val'], 11, 5);

		$this->newInput($param);
		$this->setParamWork($p);
	}


	/**
	 * Nuova edit semplice
	 * @param  [type] $param [description]
	 * @return [type]        [description]
	 */
	public function newInput(array $param = [])
	{ //crea un nuovo elemento HTML parametrizzabile con le opzioni tramite l'array PARAM
		// id - type - valore(da arrya valore della class o come elemento nell'arrya) class style
		//SELEZIONE DATI
		if (empty($param['noDefault'])) {
			$param = $param + $this->BinputModel($param); //aggiungo i valor di default ai parametri
		}
		$this->setParamWork($param); //aggiungo l'array alla variabile di lavoro per la generazione dell'elemento
		$id = $this->getparamId();
		$valore = $this->procVal($id, $type);
		$wScheda = $this->getWorkingMultiScheda();

		$table = $this->getWork("table");
		$style = $this->getParamHtml("style", "style");
		$adTipo = $this->getWork('adTipo');
		$event = $this->getWork('event');
		$maxLen = $this->getWork('maxLenght');
		if ($maxLen) {
			$maxLen = ' maxlength="' . $maxLen . '" ';
		}
		$this->groupByName();
		$this->setRegex();
		$this->mergeClasse();

		if ($this->getWork('dataSourceCompile')) { //attiva il getDataJ per il campo
			$dataJ = " dataSourceCompile='{$this->getWork("dataSourceCompile")}'";
		} else {
			$dataJ = "";
		}

		$sezione=$this->getSezione();
		$attr = $this->getAttr();
		$class = $this->getParamHtml("class", "class");
		$fineHtml = $this->getWork('fineHtml');
		$input = ($this->getPreInput() . " <input autocomplete='off' $attr type='$type' 
		tipo='$adTipo' active-scheda='$wScheda' $dataJ sezione='$sezione'  source='$table' $event $class
		$adTipo $maxLen $style id='$id' $valore >" . $fineHtml);
		if ($this->getWork('retStr')) {
			return $input;
		} else {
			$this->printElement($input);
		}
	}





	//metodi per la gestione degli elementi
	public function newCheckbox(array $param = [])
	{
		// $param['paramAutoDiv']['class']=' forCheckBox ';
		$this->newInput($param);
	}


	/**
	 * Edit con il bottone per mostrare la password
	 * @param  [type] $param [description]
	 * @return [type]        [description]
	 */
	public function newShowPass(array $param = [])
	{ //crea un nuovo elemento HTML parametrizzabile con le opzioni tramite l'array PARAM
		// id - type - valore(da arrya valore della class o come elemento nell'arrya) class style
		//SELEZIONE DATI
		if (empty($param['noDefault'])) {
			$param = $param + $this->BinputModel(); //aggiungo i valor di default ai parametri
		}
		$param['adClass'] .= " showPass ";
		$this->setParamWork($param); //aggiungo l'array alla variabile di lavoro per la generazione dell'elemento
		$id = $this->getparamId();
		$valore = $this->procVal($id, $type);
		$style = $this->getParamHtml("style", "style");
		//  $type = $this->getParamHtml("type", "type");
		$adTipo = $this->getParamHtml('adTipo', 'type');
		$attr = $this->getAttr();
		$event = $this->getWork('event');
		$this->groupByName();
		$this->setRegex();
		$this->mergeClasse();
		$class = $this->getParamHtml("class", "class");
		$bc = $this->paramWork;
		//bottone per l'invio del file
		$paramB['retStr'] = '1'; //inva l'html
		$paramB['html'] = $this->getButtonIco(' fa-unlock-alt ');
		$paramB['id'] = 'mostraPass';
		$paramB['adClass'] = 'btnShowPass';
		$paramB['event'] = 'onclick="showPass(this);"';
		$btn = $this->newButton($paramB);
		$this->paramWork = $bc;
		$fineHtml = $this->paramWork['fineHtml'];
		$input = ($this->getPreInput() . " <div class=\"showPassInputgroup\">
		<div class=\"col-sm-11\" style=\"padding-left: 0px; padding-right: 0px;\">
		<input $attr type='password' $event $class $adTipo type='$type' $style id='$id' $valore >
		</div>
		<div class=\"col-sm-1\" style=\"padding-left: 0px; padding-right: 0px;\">
		$btn
		</div>
		<div style=\"clear:both;\"></div>
		</div>" . $fineHtml);
		$this->printElement($input);
	}


	/**
	 * Creazione di una textarea
	 * @param  [type] $param [description]
	 * @return [type]        [description]
	 */
	public function newText(array $param = [])
	{ //crea un nuovo elemento HTML parametrizzabile con le opzioni tramite l'array PARAM
		// id - type - valore(da arrya valore della class o come elemento nell'arrya) class style
		//SELEZIONE DATI
		if (empty($param['noDefault'])) {
			$param = $param + $this->BtextModel($param); //aggiungo i valor di default ai parametri
		}

		$this->setParamWork($param); //aggiungo l'array alla variabile di lavoro per la generazione dell'elemento
		$id = $this->getparamId();
		$valore = $this->procVal($id, $type);
		$wScheda = $this->getWorkingMultiScheda();

		$this->mergeClasse();
		$class = $this->getParamHtml("class", "class");
		$style = $this->getParamHtml("style", "style");
		$type = $this->getParamHtml("type", "type");
		$attr = $this->getWork('attr');
		$event = $this->getWork('event');
		$table = $this->getWork("table");

		if ($this->getWork('wysiwyg')) { //attiva l'editor trumbowyg wysiwyg
			$req = $this->getWork('table');
			$type = "type='wysiwyg'";
			$add = "<script>$('.$id-$req').trumbowyg({

				btns:[
					['viewHTML'],
					['undo', 'redo'],
					['foreColor','backColor']
					['formatting'],
					['strong', 'em', 'del'],
					['superscript', 'subscript'],
					['link'],
					['insertImage'],
					['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
					['unorderedList', 'orderedList'],
					['horizontalRule'],
					['removeformat'],
					['fullscreen'],
					['table'],
				]
			});</script>";
			$input = ($this->getPreInput() . " <div $attr $event $class $type
			source='$table' $style id='$id' >$valore</div>") . $add;
		} else {
			$input = ($this->getPreInput() . " <textarea active-scheda='$wScheda' $attr $event $class $type
			source='$table' $style id='$id' >$valore</textarea >");
		}

		$this->printElement($input);
	}



	/**
	 * event per creare gli eventi con i replace nelle select
	 * @param  [type] $row  riga del dataset
	 * @param  [type] $type type richiesto
	 */
	private function getEventParam($row, $type)
	{ //creo l'event multiplo per ogni riga
		if (!isset($this->paramWork[$type . 'Child'])) {
			return "";
		}
		$event = $this->paramWork[$type . 'Child'];
		$eventAr = $this->paramWork[$type . 'Array'];
		foreach ($eventAr as $id) {
			$event = str_replace('%sost-' . $id, $row[$id], $event);
		}
		return $event;
	}

	/**
	 * Ritorna
	 * @return [type] [description]
	 */
	private function getAttr()
	{
		$adAttr = '';
		if (null == ($this->getWork('noAutoRequired'))) { //gestione dei required
			$this->paramWork['reqTarget'] = $this->getWork('id') . "-" . $this->getProfile();
			$adAttr = ' targetError=".' . $this->getWork('reqTarget') . '"';
		}
		$attr = $this->getWork('attr') . $adAttr;
		if($this->getWork('unique')){
			$attr=" unique='1' ";
		}
		return $attr;
	}


	private function setRegex()
	{
		if (!empty($this->getWork("noRegex"))) {
			return 0;
		}
		$regex = $this->getWork("regex");
		if (!$regex) {
			return;
		}

		$space = $this->getWork("regex-no-space");
		if ($space) {
			$ad = " regex-no-space='true' ";
		} else {
			$ad = "";
		}

		$this->setWork("attr", " $ad auto-regex='$regex' $space ", true);
		$this->setWork("adClass", " auto-regex ", true);
	}

	private function mergeClasse()
	{ //metodo per aggiungere delle classi al modello sul vettore di lavorazione
		// if ((!empty($this->getWork('adClass'))) || (!empty($this->paramWork['class']))) {
		if (!empty($this->workingMultiScheda)) {
			$multiScheda = " scheda-" . $this->workingMultiScheda;
		} else {
			$multiScheda = "";
		}
		$this->paramWork['class'] = $this->getWork('class') . " " .
			$this->getWork('adClass') . " " . $this->getWork('reqTarget') . " " .
			$this->getProfile() . " " . $multiScheda;
		// }
	}

	/**
	 * Contorlla se esiste l'autoDiv nell'elemento
	 * @return bool esito autoDiv
	 */
	private function workIndiv()
	{
		$auto = $this->getWork("autoDiv");
		// $auto=$paramWork['autoDiv'];
		if ($auto == true || $auto == '1') {
			return true;
		}
	}


	/**
	 * Template per impostare gli standard grid per i campi soliti tipo date
	 * @param  array  $arTmp array temporaneo
	 * @return void
	 */
	private function procAutoGrid(string $grid): void
	{ //da spostare in procVal?
		$gridOld = $this->getWorkDiv('grid');
		if (!empty($gridOld)) {
			return;
		}
		$this->setWorkDiv("grid", $grid);
	}





	/**
	 * [procVal description]
	 * @param  [type] $id   [description]
	 * @param  [type] $type [description]
	 * @return [type]       [description]
	 */
	private function procVal(string $id, &$type)
	{ //processa l'arrya per codificare il type e il valore corretto per l'elemento
		$type = $this->getWork('type');
		$typeCheck = $type;

		if ($type == "wysiwyg") {
			$valore = $this->getUtility()->checkHtmlContent($this->getWork('val'));
		} else {
			$valore = $this->getUtility()->escapeHtml($this->getWork('val'));
		}
		$typeAd = "";


		if ($this->ifBlow($type)) { //è una variabile blowfish?? Decrypto. Il type viene già sistemato per usarlo nel case
			$valore = $this->getUtility()->decrypt($valore);
			$typeAd = "-blow"; // sono pigro uso questa variabile per mantenere il suffisso -blow
		}

		switch ($typeCheck) {
			case 'checkbox':
				if (($valore == '1') || (strtoupper($valore) == 'ON') || ($valore)) {
					$valore = ' checked ';
				}
				$this->procAutoGrid(" col-2 col-md-2 col-lg-2");
				break;

			case 'openModule':
				$this->procAutoGrid(" col-6 col-md-3 ");
				return $valore;

				break;

			case 'labelValue':
				$this->procAutoGrid(" col-6 ");
				$valore = $valore;
				break;

			case 'color':
				$type = "color";
				$valore = " value='$valore' ";
				$this->procAutoGrid(" col-12 col-sm-6 col-md-4 col-lg-3 col-xl-1 ");
				break;


			case 'DATE':
			case 'date':
			case 'data':
				$this->procAutoGrid(" col-xs-12 col-md-4 col-lg-3 dataInDiv");
				// $valore = $this->getUtility()->dateFormatEu($valore);
				if(empty($valore) && $this->getWork("required") ){
					$valore=date("Y-m-d"); //se è required metto la data di oggi 
				}
				$valore = " value='$valore' ";
				$type = "date";

				$id = $this->getWork('id');
				// $this->setWork('adClass', $this->getWork('adClass').' datepicker');
				// $this->setWork('dopoElemento', " <script>dat('$id');</script>");
				break;

			case 'time':
			case 'TIME':
			case 'Time':
				$this->procAutoGrid("col-xs-12 col-md-3 col-lg-2");
				// $valore = $this->getUtility()->dateFormatEu($valore);
				if (!$this->getWork("seconds")) {
					$valore = substr($valore, 0, 5);
				}
				$valore = " value='$valore' ";
				$type = "time";
				$id = $this->getWork('id');
				$this->eleTime();
				// $this->setWork('adClass', $this->getWork('adClass').' datepicker');
				// $this->setWork('dopoElemento', " <script>dat('$id');</script>");
				break;

			case 'dateNow':
				$valore = " value='now()' ";
				$type = "hidden";
				$this->setWork('adTipo', 'dateNow');
				$this->setWork('label', '');
				$this->setWork('autoDiv', '');
				$this->setWork('autoDiv', '');
				break;

			case 'utente':
				$valore = " value='' ";
				$type = "hidden";
				$this->setWork('adTipo', 'utente');
				$this->setWork('label', '');
				$this->setWork('autoDiv', '');
				$this->setWork('autoDiv', '');
				break;

			case 'mail':
				$type = "mail";
				$valore = " value='$valore' ";
				break;

			case 'hidden':
				$valore = " value='$valore' ";
				$type = "hidden";
				$this->setWork('label', '');
				$this->setWork('noDefault', '1');
				$this->setWork('autoDiv', 'noClass');
				break;

			case 'iban':
				$this->setWork('attr', $this->getWork('attr') . ' maxlength="27" ');
				$valore = " value='$valore' ";
				break;

			case 'password':
				$type = "password";
				$valore = " value='$valore' ";

				break;


			case 'wysiwyg':
			case 'text':
				$this->procAutoGrid(" col-12 ");
				$valore = $valore;
				$type = 'text';
				break;

			case 'number':
			case 'integer':
			case 'int':
				$this->procAutoGrid("  col-xs-6 col-md-3 col-lg-3 col-xl-2  ");
				$valore = "value='$valore'";
				$type = 'number';
				$ev = $this->getWork('event');
				$this->setWork('event', $ev . ' onkeyup="return checkIntegerInput(this);"; ');
				break;

			case 'idUtente':
			case 'idUser':
				$valore = "value='1' "; //normale sia a 1!
				$type = 'hidden';
				$this->setWork('adTipo', 'idUser'); //setta direttamente l'idUtente in salvataggio
				break;

			case 'dec':
			case 'float':
			case 'decimal':
				$this->procAutoGrid("  col-xs-6 col-md-2 col-lg-2 ");
				$prec = $this->getWork("precision");
				if (!isset($prec) || $prec == false) {
					$prec = 2;
				}
				if ((isset($valore)) && ($valore != "")) {
					if (!is_numeric($valore)) {
						$valore = "0";
					}

					$valore = number_format($valore, $prec);
				}
				$valore = "value= '$valore'";
				$type = 'decimal';
				$this->paramWork['attr'] = " precision='$prec' ";
				$this->paramWork['event'] = ' onkeydown="return checkDecimalInput(event);" onblur="formatDecimal(this);" ';
				break;

			case 'link':
				$this->procAutoGrid("col-12 col-md-6 ");

				$valore = "value='" . $valore . "'";
				$type = 'text';
				$this->eleLink();
				break;

			case 'search':
				// $this->eleCerca();
				$valore = ' value="' . $valore . '" ';
				$type = "search";
				break;

			case 'img':
			case 'file':
				if (empty($valore)) {
					$valore = "";
				} else {
					$this->setWork("file", $valore);
					$valore = "name-file='$valore' ";
				}
				break;

			case 'gruppoFile':
				$type = "fileGroup";
				break;



			default:
				$valore = ' value="' . $valore . '" ';
				$type = "text";
				$this->procAutoGrid("col-12 col-sm-6");
				break;
		} //fine switch
		$type .= $typeAd;
		return $valore;
	}



	/**
	 * torna la label generata per l'elemento in lavorazione
	 * @return string   label
	 */
	private function getLabel()
	{ //crea la Label associata all'elemento attuale in lavorazione
		$label = $this->getWork('label');
		if (!$label) {
			return "";
		}
		$id = $this->getWork('id');
		$label = " <label for='$id' " . $this->getLabelClass() . "  >$label</label> ";
		return $label;
	}

	/**
	 * torna la Sublabel generata per l'elemento in lavorazione
	 * @return string   label
	 */
	private function getSubLabel()
	{ //crea la Label associata all'elemento attuale in lavorazione
		$label = $this->getWork('subLabel');
		if (!$label) {

			if (!$this->autoSubLabel) {
				return "";
			} else {
				$label = "";
			}
		}
		$id = $this->getWork('id');
		$label = " <label for='$id' class='subLabel'>$label</label> ";
		return $label;
	}


	/**
	 * Genera l'auto div in fase di aggiunta html
	 */
	private function printElementDiv(): void
	{
		if (!$this->workIndiv()) { //div disattivato? esco
			return;
		}
		if (empty($this->getWorkDiv('id'))) {
			$this->setWorkDiv('id', $this->getWork('id') . "-INDIV");
			$adClass=$this->getWorkDiv("adClass");
			$this->setWorkDiv('adClass', $this->getWork('id') . "-" . $this->getWork("table") . "-INDIV $adClass ");
		}
		$this->inDiv($this->getWork('paramAutoDiv'));
	}



	/**
	 * Stampa l'elemento che sto elaborando
	 * @param  string       html generato
	 * @return void
	 */
	private function printElement($html)
	{ //stampa l'elemento e il div se impostato il parametro autoDiv

		$eventBefore = $this->getWork("beforePrintCallback");

		if (!empty($eventBefore)) {
			$this->bckParamWork(); //devo tenermi lo stato per la registrazione dei token
			$eventBefore();
			$this->restoreParamWork(); //ripristino senza modificarlo
		}

		$this->printElementDiv(); //crea l'auto div nel caso sia richiesto

		$this->addHtml($html); //aggiunge l'effettivo elemento al buffer

		$this->addHtml($this->getWork('dopoElemento')); //potrebbe esserci dell'html dopo l'elemento
		if ($this->workIndiv()) { //devo anche chiudere il div in lavorazione
			$this->chiudi_InDiv();
		}

		$dopo = $this->getWork('dopoElementoDiv'); //html a fianco dell'elemnto con il div chiuso? lo aggiungo qui
		if ($dopo) {
			$this->addHtml($dopo);
		}

		$eventAfter = $this->getWork("afterPrintCallback");

		if (!empty($eventAfter)) {
			$this->bckParamWork(); //devo tenermi lo stato per la registrazione dei token
			$eventAfter();
			$this->restoreParamWork(); //ripristino senza modificarlo
		}

		// if ($this->workIndiv()) {
		//     $this->paramWork=[];
		// }
	}

	/**
	 * Torna il parametro di Tipo html formattato
	 * @param  [type] $valore [description]
	 * @param  [type] $tag    [description]
	 * @return [type]         [description]
	 */
	private function getParamHtml(string $valore, string $tag)
	{ //formatta già il valore con $tag=' $paramWork['valore'] '
		$app = $this->getWork($valore);
		if ($app) {
			$app = " $tag='$app' ";
		}
		return $app;
	}

	/**
	 * Ritorna l'id dell'elemento in lavorazione
	 * @return [type] [description]
	 */
	private function getparamId()
	{ //estrae il id dall'array parametro
		$id = $this->getWork('id');
		if (!$id) {
			return false;
		}
		return $id;
	}

	/**
	 * Mostra del titolo in alto sulla barra
	 * @param  [type] $titolo [description]
	 * @return [type]         [description]
	 */
	public function showTitolo(string $titolo)
	{
?>
		<script>
			proto.showTitolo('<?php echo $titolo; ?>');
		</script>
	<?php
	}


	public function showSubTitolo($titolo)
	{
	?>
		<script>
			subProto.showSubTitolo('<?php echo $titolo; ?>');
		</script>
<?php
	}
} //fine trait



?>