<?php

include_once __DIR__."/trait/trait.selectHelper.php";
include_once __DIR__."/trait/trait.whereHelper.php";


class QueryBuild extends WrapperUtility
{
  use qBuildSelectHelper,qBuildWhereHelper;

  public $qSelect = "";
  public $qJoin = "";
  public $qWhere = "";
  public $qFrom = "";
  public $qAlias ="";
  public $qOrder = "";
  public $qGroup = "";
  public $qLimit = "";
  public $qUnion = "";
  public $qHaving = "";
  public $activeSelect = false; //controlla se ho avviato la query con active select
  
  //bind
  public $qBind = []; //array per il bind dell'oggetto
  private $qNumber = 0; //totale bind
  //form da esterno
  public $postName = "filter"; //variabile per il bind Form
  
  //variabili interne / option
  public $activeDebug = false; //stampa la query prima di eseguirla, per debuggare
  private $cCacheQuery = false; //lascia la query attiva dopo l'execute
  public $activeState = false;
  public $disableMainSoftDelete = false; //se messo a true disabilita il soft delete per la main query
  public $lJoin = null; //identifica l'ultimo join svolto, usato per gli alias
  public function isCacheQuery(): bool
  {
    return $this->cCacheQuery;
  }

  public function cacheQuery(bool $bool)
  {
    $this->cCacheQuery = $bool;
  }





  public function select(string $col)
  {
    $this->activeSelect = true;
    if (empty($this->qFrom)) { //se vuoto imposto il modulo dove sono ora
      $this->qFrom = strtolower($this->getUtility()->getCurModulo());
    }
    if (!empty($this->qSelect)) {
      $this->qSelect .= ",";
    }
    $this->qSelect .= $col;
    return $this;
  }

  public function union()
  {
    $this->qUnion .= $this->getBuildQuery() . " UNION ALL ";
    $this->qSelect = "";
    $this->qFrom = "";
    $this->qWhere = "";
    $this->qJoin = "";
    $this->qHaving =""; 
    return $this;
  }

  public function getMainTableName(){
    if(!empty($this->qAlias)){
      return $this->qAlias;
    }else{
      return $this->qFrom;
    }
  }

  public function alias(string $aliasName = "main")
  { 
    $this->qAlias=$aliasName;
    $this->qFrom = " ( " . $this->getBuildQuery() . " )  as $aliasName ";
    $this->qSelect = "";
    $this->qJoin = "";
    $this->qWhere = "";
    $this->qGroup = "";
    $this->qOrder = "";
    $this->qLimit = "";
    $this->qUnion = "";
    $this->qHaving = "";
    $this->disableMainSoftDelete = true; //negli alias ignoro il softdelete
    return $this;
  }


  public function from(string $table)
  {
    $this->qFrom = " $table ";
    return $this;
  }


  private function setHaving($add = " AND ")
  {
    if (!empty($this->qHaving)) {
      $this->qHaving .= " $add ";
    }
  }


  private function setWhere($add = " AND ")
  {
    if (!empty($this->qWhere)) {
      $this->qWhere .= " $add ";
    }
  }

  

  public function addBind(&$where, $val)
  {
    $where = $where . "_" . $this->qNumber;
    $this->qBind[$where] = $val;
    $this->qNumber++;
    return $this;
  }

  public function getBindName($where)
  {
    $bindWhere = str_replace(".", "_", $where);
    $bindWhere = str_replace(" ", "_", $bindWhere);
    $bindWhere = str_replace("%", "_", $bindWhere);
    $bindWhere = str_replace("'", "", $bindWhere);
    $bindWhere = str_replace(",", "", $bindWhere);
    $bindWhere = str_replace("(", "", $bindWhere);
    $bindWhere = str_replace(")", "", $bindWhere);
    return $bindWhere;
  }

  public function addWhereBind($where, $bind, $operator)
  {
    $bindWhere = $this->getBindName($where);
    $this->addBind($bindWhere, $bind);
    $col = " $operator :$bindWhere   ";
    $this->qWhere .= " $where $col  ";
  }


  public function addHavingBind($where, $bind, $operator)
  {
    $bindWhere = $this->getBindName($where);
    $this->addBind($bindWhere, $bind);
    $col = " $operator :$bindWhere   ";
    $this->qHaving .= " $where $col  ";
  }

  public function getIdName()
  {
    return "id" . ucwords($this->qFrom);
  }

  public function where($where, $bind = null, $operator = " like ")
  {
    if ($bind === null) {
      if (is_array($where)) {
        foreach ($where as $where => $bind) {
          $this->where($where, $bind);
        }
        return $this;
      }
      if (!is_array($where) && (is_numeric($where) || $where == "")) {
        $bind = $where;
        $where = $this->getIdName();
      }
    }
    $this->setWhere("AND");
    if ($bind === null) {
      if ($where == "") {
        return $this;
      }
      $this->qWhere .= "$where";
    } else {
      $this->addWhereBind($where, $bind, $operator);
    }
    return $this;
  }


  public function having($where, $bind = null, $operator = " = ")
  {
    if ($bind === null) {
      if (is_array($where)) {
        foreach ($where as $where => $bind) {
          $this->having($where, $bind);
        }
        return $this;
      }
      if (!is_array($where) && (is_numeric($where) || $where == "")) {
        $bind = $where;
        $where = $this->getIdName();
      }
    }
    $this->setHaving(" AND ");
    if ($bind === null) {
      if ($where == "") {
        return $this;
      }
      $this->qWhere .= "$where";
    } else {
      $this->addHavingBind($where, $bind, $operator);
    }
    return $this;
  }

  
  
  public function whereLike($where,$bind=null,$start=true,$end=true){
    if($bind!==null){
      if($start){
        $bind="%".$bind;
      }
      if($end){
        $bind=$bind."%";
      }
    }
    return $this->where($where,$bind);
  }

  public function orWhereLike($where,$bind=null,$start=true,$end=true){
    if($bind!==null){
      if($start){
        $bind="%".$bind;
      }
      if($end){
        $bind=$bind."%";
      }
    }
    return $this->orWhere($where,$bind);
  }



  public function getPostVar(string $id)
  {
    $data = $this->post($this->postName);
    if (isset($data)) {
      if (isset($data[$id])) {
        return $data[$id];
      }
    }
    return false;
  }


  /**
   * Mette l'autolike sulle stringhe
   * @param  [type] $inp [description]
   * @return [type]      [description]
   */
  public function autoLike(&$inp, $option)
  {
    if ($inp['type'] == "date") {
      return false;
    }
    if (!is_numeric($inp['val']) && (!isset($option['like']) || $option['like'] !== false)) {
      $inp['val'] = "%{$inp['val']}%";
    }
  }

  /**
   * Automatismi per il like di whereForm
   * @return [type] [description]
   */
  private function processLike(&$inp, $option)
  {
    if (empty($option['likeStart']) && empty($option['likeEnd']) && empty($option['like'])) { //se è una string autolike
      $this->autoLike($inp, $option);
    } else { //abilito il like da option
      if (!empty($option['likeStart']) || !empty($option['like'])) {
        $inp['val'] = "%{$inp['val']}";
      }
      if (!empty($option['likeEnd']) || !empty($option['like'])) {
        $inp['val'] = "{$inp['val']}%";
      }
    } //end else
  }


  /**
   * Processa la condition per generarla o attivare quella manuale
   * @return [type] [description]
   */
  public function processCondition($id, $inp, $option)
  {
    if (empty($option['condition'])) { //genero il where standard
      if (empty($option['or'])) {
        $this->where($id, $inp); //uso l'and
      } else {
        $this->orWhere($id, $inp); //ativo l'or
      }
    } else { //uso la condizione con il bind dalla condiion manuale
      $bindName = $id . "Form";
      $bindName = str_replace(".","_",$bindName);
      $this->addBind($bindName, $inp); //salvo il bind per dopo
      $condition = $option['condition'];
      $condition = str_replace("?", ":$bindName", $condition); //se ci sono ? li aggiorno
      if (empty($this->qWhere)) {
        $this->qWhere = " 1=1";
      }
      $this->qWhere .= "  $condition  "; //Aggiorno il qWhere con la condition manuale
    }
  }

  public function whereForm(string $id, $condition = "", array $option = [])
  {
    if (is_array($condition)) {
      $option = $condition;
      $condition = "";
    } else {
      $option['condition'] = $condition;
    }

    $inp = $this->getPostVar($id);
    if ($inp && ($inp['val'] != "")) { //verifico la presenza del post in filter $_POST
      $this->processLike($inp, $option); //se lo trovo processo i like automatici
      $this->processCondition($id, $inp, $option); //genero la condition standard o uso quella custom
    }
    return $this;
  }


  /**
   * Processa la condition per generarla o attivare quella manuale
   * @return [type] [description]
   */
  public function processConditionHaving($id, $inp, $option)
  {
    if (empty($option['condition'])) { //genero il where standard
      if (empty($option['or'])) {
        $this->where($id, $inp); //uso l'and
      } else {
        $this->orWhere($id, $inp); //ativo l'or
      }
    } else { //uso la condizione con il bind dalla condiion manuale
      $bindName = $id . "Form";
      $this->addBind($bindName, $inp); //salvo il bind per dopo
      $condition = $option['condition'];
      $condition = str_replace("?", ":$bindName", $condition); //se ci sono ? li aggiorno
      if (empty($this->qHaving)) {
        $this->qHaving = " 1=1";
      }
      $this->qHaving .= "  $condition  "; //Aggiorno il qqHaving con la condition manuale
    }
  }


  public function havingForm(string $id, $condition = "", array $option = [])
  {
    if (is_array($condition)) {
      $option = $condition;
      $condition = "";
    } else {
      $option['condition'] = $condition;
    }

    $inp = $this->getPostVar($id);
    if ($inp && ($inp['val'] != "")) { //verifico la presenza del post in filter $_POST
      $this->processLike($inp, $option); //se lo trovo processo i like automatici
      $this->processConditionHaving($id, $inp, $option); //genero la condition standard o uso quella custom
    }
    return $this;
  }



  public function orWhere($where, $bind, $operator = " like ")
  {
    $this->setWhere("OR");
    $this->addWhereBind($where, $bind, $operator);
    return $this;
  }

  public function autoStato()
  {
    $base = $this->qFrom;
    //sintassi classica
    $joinTab = $base . "_stato";
    $id = "id" . ucwords($joinTab);
    return $this->leftJoin("$joinTab as stato")->using($id);
  }


  public function join(string $table, bool $soft = true)
  {
    return $this->innerJoin($table, $soft);
  }

  /**
   * casta la tabella con il as e prende l'alias
   * @param  string $table tabella con l'as
   * @return [type]        solo alias o tab
   */
  public function castAsTable(string $table)
  {
    $exp = explode(" as ", $table); //gestisco l'as
    if (count($exp) > 1) {
      $softTable = str_replace(" ", "", $exp[1]);
      $softTable = str_replace("`", "", $softTable);
    } else {
      $softTable = $table;
    }
    return $softTable;
  }

  /**
   * Processa effettivamente le variabili per il join
   * @param [type] $table [description]
   * @param [type] $soft  [description]
   */
  private function addJoin($table, $soft)
  {
    $on = " ON 1=1 ";
   
    $tab = explode(" as ", $table);
    $count=count($tab);
    if ($count > 1) {
      $this->lJoin = $tab[$count-1];
    } else {
      $this->lJoin = $table;
    }
    if ($soft) {
      $softTable = $this->castAsTable($this->lJoin);
      $on .= $this->getUtility()->softDelWhere($softTable);
    }
    $this->qJoin .= " $table $on ";
  }


  public function innerJoin(string $table, bool $soft = true)
  {
    $this->qJoin .= " INNER JOIN ";
    $this->addJoin($table, $soft);
    return $this;
  }

  private function hasOperator(string $string, $bind): bool
  {
    if (strpos($string, "<") === false && strpos($string, ">") === false  &&  strpos($string, "=") === false &&  strpos($string, " like ") === false  && $bind !== null) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Aggiunge una clausola on alla query in corso
   * @param  string $on     [description]
   * @param  [type] $bind   [description]
   * @param  string $onCond [description]
   * @return [type]         [description]
   */
  public function on(string $on, $bind = null, $onCond = " AND ")
  {
    if (empty($this->qJoin)) {
      $this->error("Errore composizione join su ON ");
    }
    if ($this->hasOperator($on, $bind) == false) {
      $on = $on . " like "; //auto like in caso uso la sintassi ->on("colonna",$idColonna)
    }

    $this->qJoin .= " $onCond " . $on;
    if (isset($bind)) {
      $bindOn = str_replace(".", "_", $on);
      $bindOn = str_replace(" ", "", $bindOn);
      $bindOn = str_replace("=", "", $bindOn);
      $bindOn = str_replace("__", "_", $bindOn);
      $bindOn = $bindOn . "_on";
      $this->qJoin .= " :$bindOn ";
      $this->qBind[$bindOn] = $bind;
    }
    return $this;
  }


  public function using(string $using)
  {
    $this->qJoin .= " and {$this->qFrom}.$using = {$this->lJoin}.$using ";
    return $this;
  }

  public function leftJoin(string $table, bool $soft = true)
  {
    $this->qJoin .= " LEFT JOIN ";
    $this->addJoin($table, $soft);
    return $this;
  }
  /**
   * Casta le colonne con il ` ` usata un po' ovunque
   * @param  string $col [description]
   * @return [type]      [description]
   */
  private function castCol(string &$col)
  {
    $cols = explode(",", $col);
    $newCol="";
    foreach ($cols as $col) {
      $col=trim($col);
      if(!empty($newCol)){
        $newCol.=" , ";
      }
      $t = explode(".", $col);
      if (count($t) > 1) {
        $newCol.= "`$t[0]`.`$t[1]`";
      } else {
        $newCol.= "`$col`";
      }
    }
    $col=$newCol;
  }

  public function group($group)
  {
    if (!empty($this->qGroup)) {
      $this->qGroup .= ",";
    } else {
      $this->qGroup .= " GROUP BY ";
    }
    $this->castCol($group);
    $this->qGroup .= " $group ";
    return $this;
  }

  public function order($order)
  {
    if (!empty($this->qOrder)) {
      $this->qOrder .= ",";
    } else {
      $this->qOrder .= " ORDER BY ";
    }
    // $this->castCol($order);
    $this->qOrder .= " $order ";
    return $this;
  }

  public function limit($limit, $offset = "")
  {
    $this->qLimit = " LIMIT ";
    if (!empty($offset)) {
      $this->qLimit .= " $offset , $limit ";
    } else {
      $this->qLimit .= " $limit ";
    }

    return $this;
  }


  /**
   * Pulisce la sessione attiva (fine query)
   */
  public function clearSession()
  {
    if (!$this->isCacheQuery()) {
      $this->qSelect = "";
      $this->qFrom = "";
      $this->qJoin = "";
      $this->qWhere = "";
      $this->qGroup = "";
      $this->qOrder = "";
      $this->qLimit = "";
      $this->qUnion = "";
      $this->qHaving = "";
      $this->qBind = [];
      $this->qNumber = 0;
      $this->activeSelect = false;
    }
  }

  /**
   * verifica se è attivo l'hardelete
   */
  public function activeHardDelete(): bool
  {
    if ($this->disableMainSoftDelete) {
      return true;
    }
    if (method_exists($this, "isHardDelete")) { //Sono un model!!
      return $this->isHardDelete();
    } else {
      return $this->getUtility()->isHardDelete();
    }
  }


  /**
   * Torna la query buildata
   */
  public function getBuildQuery(): string
  {
    $soft = "";
    if (!$this->activeHardDelete()) {
      $tab = $this->castAsTable($this->qFrom);
      $soft = $this->softDelWhere($tab);


      if (empty($this->qWhere)) {
        $this->qWhere = " 1=1 ";
      }
    }

    $union = $this->getUnionQuery();
    if (empty($this->qWhere)) {
      $where = "";
    } else {
      $where = " WHERE ";
    }

    if (empty($this->qHaving)) {
      $having = "";
    } else {
      $having = " HAVING ".$this->qHaving;
    }

    $select = $this->qSelect;
    if (empty($select)) {
      $select = "*";
    }

    $str = " $union  SELECT {$select} FROM {$this->qFrom} {$this->qJoin}
    $where {$this->qWhere} {$soft} {$this->qGroup}  {$having}  {$this->qOrder}   {$this->qLimit} ";
    
    return $str;
  }


  /**
   * Esegue il prepare con la query attiva
   */
  public function executeState()
  {
    // if($this->activeState){
    //   return;
    // }
    $str = $this->getBuildQuery();
    if ($this->activeDebug) {
      echo "<pre>$str</pre>";
    }
    if (!$this->startPrepare($str)) {
      $this->error("Errore esecuzione STATE QUERY BUILD - " . $str);
    }
    $this->activeState = true;
  }

  /**
   * TOrna la query unione costruita ad ora
   */
  public function getUnionQuery()
  {
    return $this->qUnion;
  }


  /**
   * Aggiunge all'array di bindQ i valori - puoi usarlo dall'esterno
   */
  public function bindQ($string,$val){
    $this->qBind[$string] = $val;
    $this->qNumber++;
    return $this;
  }

  /**
   * Wrapper classico del bindP
   */
  public function bind(string $bind, $val, $type = "")
  {
    $this->bindP($bind, $val, $type);
    return $this;
  }

  /**
   * esegue il fetch dal dataset 
   */
  public function fetch(string $ds = "")
  {
    if ($this->activeState) {
      $this->execute();
    }
    parent::fetch($ds);
  }


  /**
   * Interfaccia
   */
  public function guardQuery(){
    //silence is golden! -- serve per estendere nel model la guard
  }

  /**
   * Esegue la query costruita
   */
  public function execute($array = [])
  {
    $this->guardQuery();

    $this->executeState();
    $array = $this->qBind;

    foreach ($array as $key => $r) {
      if (is_array($r)) {
        $type = $r['type'];
        $r = $r['val'];
      } else {
        $type = "";
      }
      $this->bind($key, $r, $type);
    }
    $this->activeState = false;
    $res = $this->executePrepare();
    $this->clearSession();
    return $this;
  }


  /**
   * Torna il primo record del
   */
  public function first()
  {
    if ($this->activeSelect) { //sono ancora attivo con la ricerca attuale
      $this->execute();
    }
    return $this->fetch();
  }



    /**
     * Torna tutti i record del dataset attivo
     */
  public function all()
  {
    if ($this->activeSelect) { //sono ancora attivo con la ricerca attuale
      $this->execute();
    }
    return $this->fetchAll();
  }

} //fine classe
