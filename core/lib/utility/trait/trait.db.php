<?php

/**
* Trait con i metodi di accesso ai database
* Il trait permette l'utilizzo di varie connessioni nella stessa istanza utilizzando
*/
trait Db
{
  private $connArr=[]; //array con le connessioni istanziate
  private $activeConnection="default";//indice della connessione di default
  private $confDb; //oggetto con la configurazione ai database - presente in config/class.confDb.php
  private $dataset=[]; //lista dei dataset eseguiti
  private $lastDataset=null; //salva l'ultimo dataset eseguito
  private $prepareStatement=null; //variabile per il prepare statement
  private $lastQuery="";//Ultima query eseguita
  private $debugPrepare=false; //avvia le funzioni per permettere il debug del prepare statament
  private $bindParamDebug=[]; //colleziona i bind per il debug
  private $storeStmt=[]; //store dei prepare

  

  /**
  *Imposta l'ultima query eseguita
  * @param string $query Ultima query eseguita. Serve poi anche per i log
  */
  private function setLastQuery(string $query)
  {
    $this->lastQuery=$query;
  }

  /**
  * Ritorna l'ultima query eseguita
  * @return string ultima query richiesta
  */
  public function getLastQuery()
  {
    return $this->lastQuery;
  }




  /**
  * Controlla la validita dell'ultimo prepare e ritorna lo statement
  * @return mixed bool false prepare fallito | PDOStatement
  */
  private function getPrepareStatement(string $stmtName="")
  {
    if(!empty($stmtName)){
      if(!empty($this->storeStmt[$stmtName])){
        return $this->storeStmt[$stmtName];
      }
    }

    $p= $this->prepareStatement;
    if (empty($p)) {
      $this->logPdo("Errore accesso a prepare statement");
      $this->warning("GetPreparedStatement", "errore accesso a ultimo prepare, non allocato");
      return false;
    } else {
      return $p;
    }
  }


  /**
  * Avvia la procedura di debug con prepare
  */
  public function startDebugPrepare(){
    $this->debugPrepare=true;
  }

  /**
  * Controlla se ho attivato la procedura di debug del prepare
  * @return bool controllo se ho avviato la procedura di debug
  */
  private function ifDebugPrepare():bool{
    return $this->debugPrepare;
  }


  public function escapeQuery($str=""){
    return $this->getConn()->quote($str);
  }

  /**
  * Converti un prepare in una query classica
  * @param  string $query [description]
  * @param  array/ null $param array con i parametri da caster sul bind
  * @return string query convertita
  */
  public function prepareToQuery(string $query,$param):string{
    foreach ($param as $key => $val) {
      $query=$this->strReplaceFirst(":".$key,$this->escapeQuery($val),$query);
    }
    return $query."\n";
  }

  /**
  * stampa la query con i prepare sistemati
  */
  public function executePrepareDebug(){
    if(!$this->ifDebugPrepare()){
      $this->warning("Prima di lanciare la procedura c'è bisogno di usare startDebugPrepare altrimenti l'analisi non può raccogliere tutti i dati");
    }
    $query=$this->getLastQuery();
    $i=0;

    foreach ($this->bindParamDebug as $key => $val) {
      $oldQuery=$query;
      $query=$this->strReplaceFirst(":".$key,$this->escapeQuery($val),$query);
      if($query==$oldQuery){ //confronto se c'è stato un cambio di valore o se il bind  è errato
        $this->log("Valore $val in bind numero $i - campo $key non trovato nella query prepare.
        Probabilmente è in più o ti sei scordato il riferimento in query");
      }
    }
    $this->log("Query eseguita --> ".$query);
    if($this->getUtility()->ifDebug()){
      $this->json("campi bindati",$this->bindParamDebug);
    }

  }

  /**
  * Utile per l'esecuzione di tante query con lo stesso prepare
  * @param  array  $data    dati per il bind
  * @param  string $dataset datasetda eseguire
  * @return bool esito query
  */
  public function executePrepareBind(array $data,string $dataset=""):bool{
    $stmt=$this->getPrepareStatement($dataset);
    if (!$stmt) {
      return false;
    }
    if ($stmt->execute($data)) {
      $this->setDatasetLastQuery($stmt, $dataset);
      return true;
    } else {
      $this->invalidQuery("Errore prepare", "Errore istanza su prepare");
      return false;
    }

  }

  /**
  * Eseguo il prepare in sessione
  * @return bool  eseito esecuzione
  */
  public function executePrepare(string $dataset="")
  {
    if($this->ifDebugPrepare()){
      $this->bindParamDebug=[];
    }
    $stmt=$this->getPrepareStatement($dataset);
    if (!$stmt) {
      return false;
    }
    try{
      if ($stmt->execute()) {
        $this->setDatasetLastQuery($stmt, $dataset);
        $this->prepareStatement=null;
        return true;
      } else {
        $this->invalidQuery("Errore prepare", "Errore istanza su prepare");
        return false;
      }
    }catch(Exception $err){
      if($this->ifDebug()){
        $add=$this->getLastQuery();
      }else{
        $add="";
      }
      $this->error("<pre>ERRORE ESECUZIONE QUERY - ".$add."</pre>  <pre>".$err->getMessage()."</pre>");
      $this->executePrepareDebug();
    }
  }


  /**
  * Insert da un array su una tabella
  * @param  string $tabName tabella su cui inserire
  * @param  array  $data    array con id ati [chiave]=valore;
  * @param  string $dataset dataset - opzionale
  * @return ritorno del prepare
  */
  public function dbUpdateArray(string $tabName,array $data=[],$where,string $dataset=""){
    $query=" UPDATE `$tabName` set ";
    $cols="";
    $bind="";
    if(!is_array($where)){
      $where=array($where);
    }

    //Costruzione della query
    foreach ($data as $col => $value) {
      if(in_array($col,$where)){ //salto la chiave del where
        continue;
      }
      if(!empty($cols)){
        $cols.=",";
      }
      $bind=$this->getBind($col,$value); 
      $cols.=" `$col` = $bind ";
    } //end foreach

    //bind del where
    $whe="";
    foreach ($where as $col) {
      if(!empty($whe)){
        $whe.=" and ";
      }
      $whe=" `$col` = :$col ";
    }
    $query=$query.$cols." WHERE  ".$whe;
    $this->startPrepare($query,$dataset);
    //bind dei dati
    foreach ($data as $col => $value) {
      if(is_array($value)){ // posso passare un form diretto o un array
        $this->bindP($col,$value['val'],$value['type']);
      }else{
        if($this->isRaw($value,$col)){ //salto il bind per le raw 
          continue;
        }
        $this->bindP($col,$value);
      }
    }
    return $this->executePrepare();
  }


  /**
  * Delete effettivo del record con DELETE
  * @param  string $tabName [description]
  * @param  array  $data    [description]
  * @param  string $dataset [description]
  * @return [type]          [description]
  */
  public function doHardDeleteArray(string $tabName,array $data=[],string $dataset=""){
    $query=" DELETE FROM `$tabName` where ";
    $cols="";

    //Costruzione della query
    foreach ($data as $col => $value) {
      if(!empty($cols)){
        $cols.=" and ";
      }
      $cols.=" `$col` = :$col ";
    } //end foreach

    $query=$query.$cols;
    $this->startPrepare($query,$dataset);
    //bind dei dati
    foreach ($data as $col => $value) {
      $this->bindP($col,$value);
    }
    return $this->executePrepare();
  }

  /**
  * ESECUZIONE del doSoftDeleteArray
  * @param  string $tabName [description]
  * @param  array  $data    [description]
  * @param  string $dataset [description]
  * @return [type]          [description]
  */
  public function doSoftDeleteArray(string $tabName,array $data=[],string $dataset=""){
    $add="";
    if($this->isLogUserUpdated()){
      $add=" , lastUpdatedUser ='".$this->getIdUser()."' ";
    }

    $query=" UPDATE `$tabName` set deletedDate=now(),deletedRecord=1 $add  where ";
    $cols="";

    //Costruzione della query
    foreach ($data as $col => $value) {
      if(!empty($cols)){
        $cols.=" and ";
      }
      $cols.=" `$col` = :$col ";
    } //end foreach

    $query=$query.$cols;
    $this->startPrepare($query,$dataset);
    //bind dei dati
    foreach ($data as $col => $value) {
      $this->bindP($col,$value);
    }
    return $this->executePrepare();
  }

  /**
  * Delete del record tramite array
  * @param  string $tabName nome tabella
  * @param  array  $data    array per il where
  * @param  string $dataset dataset della connessione
  * @return eisto
  */
  public function dbDeleteArray(string $tabName,array $data=[],string $dataset=""){
    if($this->isHardDelete()){
      return $this->doHardDeleteArray($tabName,$data,$dataset);
    }else{
      return $this->doSoftDeleteArray($tabName,$data,$dataset);
    }
  }


  /**
  * where con un array bindato di and
  * @param  [type] $tab   [description]
  * @param  array  $where [description]
  * @return [type]        [description]
  */
  public function dbWhereArray($tab,$where=[],string $col="",$option=[]){
    if(empty($col)){
      $col=" * ";
    }
    $str=" SELECT $col FROM  `$tab` ";
    $whstr=" WHERE 1=1 ";
    foreach( $where as $whe=> $val){
      $whstr.= " and `$whe` like :$whe ";
    }

    $query=$str.$whstr;
    if(!isset($option['notSoft']) || $option['notSoft']==false){
      $query.=$this->getSoftDeleteWhere($tab);
    }
    $this->startPrepare($query );
    foreach($where as $whe=> $val){
      $this->bindP($whe,$val);
    }

    return $this->executePrepare();
  }


  private function isRaw(&$value,&$col):bool{
    if($value=='now()'){
      return true; 
    }

    if(strpos($col,"--raw")){
      $col=str_replace("--raw","",$value);
      return true;
    }
    return false;
  }

  /**
   * Controlla i metodi raw rapidi con model altrimenti prepara il bind classico
   */
  private function getBind(string $col,$value):string{
    if($this->isRaw($value,$col)){
      return $value;
    }else{
      return ":$col";
    }
  }

  /**
  * Insert da un array su una tabella
  * @param  string $tabName tabella su cui inserire
  * @param  array  $data    array con id ati [chiave]=valore;
  * @param  string $dataset dataset - opzionale
  * @return ritorno del prepare
  */
  public function dbInsertArray(string $tabName,array $data=[],string $dataset=""){
    $query=" INSERT INTO  `$tabName` ";
    $cols="";
    $bind="";
    //Costruzione della query
    foreach ($data as $col => $value) {
      if(!empty($cols)){
        $cols.=",";
        $bind.=",";
      }
      $bind.=$this->getBind($col,$value); 
      $cols.=" `$col` ";
    } //end foreach
    $query=$query." (".$cols.") values (".$bind.")";
    $this->startPrepare($query,$dataset);
    //bind dei dati
    foreach ($data as $col => $value) {
      if(is_array($value)){ // posso passare un form diretto o un array
        $this->bindP($col,$value['val'],$value['type']);
      }else{ //array diretto
        if($this->isRaw($value,$col)){ //salto il bind per le raw 
          continue;
        }
        $this->bindP($col,$value);
      }
    }
    return $this->executePrepare();
  }


  public function addDataForm(array &$dataForm,string $col,$val,$type="string"){
    $dataForm[$col]=['val'=>$val,'id'=>$col,'type'=>$type];
  }


  /**
  * Bind from post - prende il valore direttamente dal post
  * @param  string $key  chiave del post
  * @param  string $tipo [description]
  * @return [type]       [description]
  */
  public function bindPost(string $key, string $tipo="")
  {
    return $this->bindP($key, $this->post($key), $tipo);
  }


  private function checkSospiciousData($val)
  {
    if (!is_string($val)) {
      return;
    }
    if (strpos($val, "<script>")) {
      $this->error("Blocco l'inserimento - errore 679");
    }
  }


  /**
  * Binda la variabile all'ultimo prepare
  * @param  string $key  Chiave della colonna
  * @param  string $val  valore
  * @param  string $tipo tipo del dato, default stringa
  * @return bool   Esito operazione
  */
  public function bindP(string $key, $val, string $tipo="",$dataset="")
  {
    $this->checkSospiciousData($val);

    $stmt=$this->getPrepareStatement($dataset);
    if (!$stmt) {
      return false;
    }

    $tipo=strtolower($tipo);
    //seleziono il tipo per il prepare
    switch ($tipo) {
      case 'datetime':
      if (empty($val)) {
        $val=null;
      } else {
        $val=$this->datetimeFormatUs($val);
      }
      $tipo=PDO::PARAM_STR;

      break;
      case 'date':
      if (empty($val)) {
        $val=null;
      } else {
        $val=$this->dateFormatUs($val);
      }
      $tipo=PDO::PARAM_STR;
      break;

      case 'time':
      if(empty($val)){
        $val=null;
      }
      break;

      case 'decimal':
      case 'dec':
      case 'float':
      $tipo=PDO::PARAM_STR;
      if (!isset($val)||$val=="" ) {
        $val=null;
      }else{
        $val=str_replace(",","",$val);
      }
      break;

      case 'checkbox':
      case 'boolean':
      case 'flag':
      case 'bool':
      $tipo=PDO::PARAM_BOOL;
      break;

      case 'radio':
      case 'number':
      case 'integer':
      case 'decimal':
      case 'int':  
      $tipo=PDO::PARAM_INT;
      if (!isset($val)||$val=="" ) {
        $val=null;
      }
      break;
      case '':
      case 'str':
      case 'string':
      $tipo=PDO::PARAM_STR;
      break;
      
      case 'hidden':
      case 'search':
      if(empty($val)){
        $val=null;
      }else{
        $tipo=PDO::PARAM_STR;
      }
      break;

      case 'password':
      if(!empty($val)){ 
      if(!$this->isHashed($val)){
        $val=$this->hash($val);
      }
      }else{
        $val=null;
      }
      $tipo=PDO::PARAM_STR;
      break;


      default:
      $tipo=PDO::PARAM_STR;
      break;
    }//end switch
    if($this->ifDebugPrepare()){
      $this->bindParamDebug[$key]=$val;
    }
    $stmt->bindParam(":".$key, $val);
  }


  /**
  * Avvia un prepare sulla connessione scelta
  * @param  string $query   Query prepare da eseguire
  * @param  string $dataset dataset dove eseguire la query
  * @return boolean         esito operazoine
  */
  public function startPrepare(string $query, string $conName="")
  {
    $this->setLastQuery($query);
    $conn=$this->getConn($conName);
    $stmt=$conn->prepare($query);
    if ($stmt) {
      $this->prepareStatement=$stmt;
      return true;
    } else {
      $this->logPdo("Errore esecuzione prepare  ".$query." ".$conName);
      $this->warning("prepare-PDo", "Errore esecuzione prepare ".$query." ".$conName);
      $this->prepareStatement=null;
      return false;
    }
  }

  public function storeStmt(string $stmName){
    $this->storeStmt[$stmName]=$this->getPrepareStatement();
  }

  /**
  * Controllo se ci sono record nel dataset
  * @param string $dataset nome del dataset, nome vuoto prende l'ultimo
  */
  public function isDatasetEmpty(string $dataset=""):bool
  {
    $ds=$this->getDataSet($dataset);
    if (!$ds) {
      return false;
    }
    if ($ds->rowCount()==0) {
      return true;
    } else {
      return false;
    }
  }

  /**
  * Ritorna le righe del dataset
  * @param  string $dataset Nome del dataset
  * @return int             numero righe trovate
  */
  public function numRows(string $dataset="")
  {
    $ds=$this->getDataSet($dataset);
    if (!$ds) {
      return false;
    }
    return $ds->rowCount();
  }

  /**
  * Torna le colonne per il dataset aperto
  * @param  string $dataset in memoria, default ultima query eseguita
  * @return array  name query
  */
  public function getColumnName(string $dataset="")
  {
    $ds=$this->getDataSet($dataset);
    if (!$ds) {
      $this->warning('Apertura dataset GETCOLUMNNAME', "Dataset: $dataset non trovato");
      return false;
    }

    $countCol = $ds->columnCount();
    $col=[];
    for ($i = 0; $i < $countCol; $i ++) {
      $col[] = $ds->getColumnMeta($i)['name'];
    }
    return $col;
  }


  public function getTableNameLastDs(){
    $struct=$this->getColumnStruct();
    $col= array_shift($struct);
    return $col['table'];
  }

  /**
  * Torna i dati strutturati della colonna aperta
  * @param  string $dataset [description]
  * @return [type]          [description]
  */
  public function getColumnStruct(string $dataset="")
  {
    $ds=$this->getDataSet($dataset);
    if (!$ds) {
      $this->warning('Apertura dataset GETCOLUMNStruct', "Dataset: $dataset non trovato");
      return false;
    }

    $countCol = $ds->columnCount();
    $col=[];
    for ($i = 0; $i < $countCol; $i ++) {
      $meta=$ds->getColumnMeta($i);
      $name=$meta['name'];
      $col[$name]['name'] =$name;
      $col[$name]['type'] =$meta['native_type'];
      $col[$name]['table'] =$meta['table'];
      $col[$name]['lenght'] =$meta['len'];
      $flags=$meta['flags'];

      if(in_array("unique_key",$flags)){
        $col[$name]['unique']=1;
      }else{
        $col[$name]['unique']=0;
      }

      if ((!empty($flags[0]))&&($flags[0]=='not_null')) {
        $col[$name]['required']="1";
      } else {
        $col[$name]['required']='0';
      }
      if ((!empty($flags[1])) && ($flags[1]=='primary_key')) {
        $col[$name]['primary']='1';
        $col[$name]['unique']=1;
      } else {
        $col[$name]['primary']='0';
      }
    }//end for
    return $col;
  }

  /**
  * Torna il campo richiesto dall'ultimo fetch
  * @param  string $colName colonna richiesta
  * @param  string $dataset nome del dataset a cui sto cercando di accedere
  * @return mixed           valore del dataset / false in caso di errore colonna o altro
  */
  public function getFetchField(string $colName, string $dataset="")
  {
    $ds=$this->getLastFetch($dataset);
    if (!isset($ds[$colName])) {
      if ($this->debug()) {
        echo "COLONNA $colName non esiste in questo fetch";
      }
      $this->warning("lastFetch", "$colName non esiste in questo fetch");
      return false;
    } else { //colonna settata
      return $ds[$colName];
    }
  }

  /**
  * Buffer dell'ultimo fetch nel caso serva più avanti
  * @param mixed $ds     ds da usare
  * @param string $dsName Nome del dataset
  */
  private function setLastFetch($ds, string $dataset="")
  {
    $this->lastFetch=$ds;
    if (!empty($dataset)) {
      $this->lastFetchArray[$dataset]=$ds;
    }
  }

  /**
  * Torna l'ultimo fetch richiesto
  * @return ultimo fetch eseguito
  */
  public function getLastFetch(string $dataset="")
  {
    if (empty($dataset)) { //torno l'ultimo standard senza cercare per nome
      $get= $this->lastFetch;
    } else { //ok mi serve per nome. Procedo a vedere se è valido
      if (empty($this->lastFetchArray[$dataset])) { //mi sa l'utente si sta sbagliando o il fetch è andato a vuoto
        $this->warning("getLastFetch", "dataset $dataset non valido");
        $get=false;
      } else { //trovato
        $get=$this->lastFetchArray[$dataset];
      }
    }
    return $get;
  }



  /**
  * Fetch dei dati presi dal dataset
  * @param  string $dataset  nome del dataset
  * @return mixed bool EOF del dataset | pdo_fetch_assoc
  */
  public function fetchArray(string $dataset="")
  {
    $ds=$this->getDataSet($dataset);
    if (!$ds) {
      $this->warning('Apertura dataset FETCH', "Dataset: $dataset non trovato");
      return false;
    }
    $result = $ds->fetch(PDO::FETCH_NUM);
    $this->setLastFetch($result, $dataset);
    return $result;
  }



  /**
  * Fetch dei dati presi dal dataset
  * @param  string $dataset  nome del dataset
  * @return mixed bool EOF del dataset | pdo_fetch_assoc
  */
  public function fetch(string $dataset="")
  {
    $ds=$this->getDataSet($dataset);
    if (!$ds) {
      $this->warning('Apertura dataset FETCH', "Dataset: $dataset non trovato");
      return false;
    }
    $result = $ds->fetch(PDO::FETCH_ASSOC);
    $this->setLastFetch($result, $dataset);
    return $result;
  }




  /**
  * FetchAll dei dati presi dal dataset
  * @param  string $dataset  nome del dataset
  * @return mixed bool EOF del dataset | pdo_fetch_assoc
  */
  public function fetchAll(string $dataset="")
  {
    $ds=$this->getDataSet($dataset);
    if (!$ds) {
      return false;
    }
    $result = $ds->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  /**
  * Recuper del dataset per nome
  * @param  string $dataset nome del dataset
  * @return pdo_dataset
  */
  public function getDataSet(string $dataset="")
  {
    if (empty($dataset)) {
      return $this->getLastDataset();
    }
    if (empty($this->dataset[$dataset])) {
      $this->logPdo("dataset $dataset non impostato");
      $this->warning("Dataset mysql", "dataset $dataset non impostato");
      return false;
    } else {
      return $this->dataset[$dataset];
    }
  }


  public function cleanLastDataset(){
    $this->lastDataset=null;
  }

  public function isLastdatasetLoaded(){
    if($this->lastDataset===null){
      return false;
    }
    return true;
  }

  /**
  * Ritorna l'ultimo dataset eseguito, in caso di query fallita torna falso
  */
  public function getLastDataset()
  {
    if (!empty($this->lastDataset)) {
      return $this->lastDataset;
    } else {
      $this->logDb("DATASET dell'ultima query {$this->getLastQuery()} RICHIESTO NON ESEGUITO. CONTROLLA MEGLIO LA QUERY!");
      $this->warning("DB - RICHIESA DATASET", "DATASET dell'ultima query {$this->getLastQuery()}  RICHIESTO NON ESEGUITO. CONTROLLA MEGLIO LA QUERY!");
      return false;
    }
  }

  /**
  * Torna l'ultimo id inserito
  * @param  string $conName nomeConnesione
  * @return string          idInserito
  */
  public function getLastInsertId(string $conName="")
  {
    return $this->getConn($conName)->lastInsertId();
  }

  /**
  * Gestione di una query errata
  * @return void
  */
  private function invalidQuery(string $que, string $conName)
  {
    $this->logPdo($que." NON ESEGUITA - ERRORE - DATABASE $conName");
    $this->warning("QueryDB", "Query $que non eseguita - connessione $conName");
    $this->latDataset=false;
  }

  /**
  * Imposta il dataset per l'ultima query eseguita con successo
  * @param PDOStatement $res dataset pdo eseguito
  */
  private function setDatasetLastQuery(PDOStatement $res, string $dataset="")
  {
    $this->lastDataset=$res;
    if (!empty($dataset)) {
      $this->setDatasetName($dataset);
    }
  }


  /**
  * Imposta un nome al dataset in memoria
  * @param string $dataSetName nome del dataset
  *@return bool risultato assegnazione
  */
  public function setDatasetName(string $datasetName)
  {
    $ds=$this->getLastDataset();
    if (!$ds) {
      return false;
    } else {
      $this->dataset[$datasetName]=$ds;
      return true;
    }
  }



  /**
  * Metodo per eseguire una query sulla connessione scelta. UTILE SOLO PER LE QUERY INTERNE. Dove c'è user input bisogna usare il prepare
  * @param  string $query   Raw query da eseguire
  * @param  string $conName nome della connessione da eseguire
  * @return pdo_resultSet   Risultato pdo della query
  */
  public function query(string $query, string $conName="", bool $debug=false)
  {
    $this->setLastQuery($query);
    $conn=$this->getConn($conName);
    if ($debug) {
      $this->logPdo("QUERY ESEGUITA IN DEBUG ".$query);
    }
    try{
    $res= $conn->query($query);
  }catch(Exception $err){
    $this->warning("Errore query ".$query);
    $this->log($err->getMessage());
    return false;

  }
    if ($res) {
      $this->setDatasetLastQuery($res);
    } else {
      $this->invalidQuery($query, $conName);
    }
    return $res;
  }

  /**
  * Log per le chiamate PDO sul db
  * @param  string $mess messaggio da mettere in log
  * @return void
  */
  private function logPdo(string $mess)
  {
    if ($this->ifDebug()) {
      $this->setJsonMess("logDB", $mess);
    }
  }

  /**
  * Connessione a server sqlserver
  * @param  string  $host     [description]
  * @param  string  $user     [description]
  * @param  string  $password [description]
  * @param  string  $dbName   [description]
  * @param  integer $port     [description]
  * @return [type]            [description]
  */
  private function connectSqlsrv(string $host, string $user, string $password, string $dbName="", int $port=3306){
    try {
      return new PDO("sqlsrv:server=$host;Database=$dbName", $user,$password);
    } catch (PDOException $e) {
      //dettaglio dell'errore esporto solo nel caso siamo in debug, per evitare di esporre informazioni non utili per l'utente finale
      $this->warning("connessione pdo", $e->getMessage());
      // notifica in caso di errore nel tentativo di connessione
      $this->error("ERRORE CONNESSIONE DATABASE ");
    }
  }


  /**
  * Connessione a tabelle mdb
  * @param  [type] $path   [description]
  * @param  string $dbName [description]
  * @return [type]         [description]
  */
  public function connectMdb($path,$dbName=""){
    $db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)};charset=UTF-8; DBQ=$dbName; Uid=; Pwd=;");
    return $db;
  }

  /**
  * Connessione a mysql
  * @param  string  $driver   [description]
  * @param  string  $host     [description]
  * @param  string  $user     [description]
  * @param  string  $password [description]
  * @param  string  $dbName   [description]
  * @param  integer $port     [description]
  * @return [type]            [description]
  */
  private function connectMy(string $driver, string $host, string $user, string $password, string $dbName="", int $port=3306)
  {
    try {
      //C'è un database specifico per la connessione?
      if (!empty($dbName)) {
        $dbName=";dbname=$dbName";
      }

      $options = [
        // PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
        PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => 1,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"
      ];

      // stringa di connessione al DBMS
      $connessione = new PDO("$driver:host=$host".$dbName.";port=$port", $user, $password, $options);
    } catch (PDOException $e) {
      //dettaglio dell'errore esporto solo nel caso siamo in debug, per evitare di esporre informazioni non utili per l'utente finale
      $this->warning("connessione pdo", $e->getMessage());
      // notifica in caso di errore nel tentativo di connessione
      $this->error("ERRORE CONNESSIONE DATABASE ");
    }
    return $connessione;
  }



  /**
  * Cerco i paramtri di connessione per il database richiesto
  * @param  string $dbName nome del database
  * @return array  configurazionePDO
  */
  public function getParamConnection(string $dbName="")
  {
    if (empty($this->confDb)) { //alloco l'oggetto per le configuione in config class.confDb.php
      $path=$this->getPath("A_CONFIG");
      include_once $path."class.confDb.php";
      $this->confDb=new ConfigDb();
    }
    $pConf=$this->confDb->getConnection($dbName);

    if(!$pConf){
      try{
        $pConf=$this->externalConfigDb($dbName); //caricamento da un trait esterno (vedi tools / bench o altre casistiche)
      }catch(Exception $err ){
        $this->error("Errore configurazione non trovata per il database. Controlla i parametri dell'istanza");
      }
    }


    if (!$pConf) {
      $this->warning("ISTANZA", "ERRORE CONFIGURAZIONE ISTANZA ");
      $this->error("Errore configurazione non trovata per il database. Controlla i parametri dell'istanza");
    }
    if (count($pConf)<5) {
      $this->warning("ERRORE CONFIGURAZIONE ISTANZA ");
      $this->error("Errore configurazione non valida per il database. Controlla i parametri dell'istanza in uso");
    }
    if (empty($dbName)) {
      $dbName="DEFAULT";
    }
    $this->logPdo("Configurazione $dbName caricata con successo");
    return $pConf;
  }



  /**
  * Metodo per il passaggio delle connessioni già istanziate,
  * nel caso sia la prima connessione connette al database e salva in cache per i prossimi utilizzi della richiesta
  * @param  string $conName Connessione richiesta, vuoto prende il default impostato
  * @return pdo_connection
  */
  public function getConn(string $conName='')
  {
    if (empty($conName)) { //devo prendere una specifica connessione o prendo l'attuale impostata?
      $conName=$this->activeConnection;
    }

    if (empty($this->connArr[$conName])) { //connessione già richiesta?
      $pConn=$this->getParamConnection($conName);
      $conName=$pConn['name'];
      if (!empty($this->connArr[$conName])) { //connessione già richiesta ma con un alias?
        return $this->connArr[$conName];
      }

      switch ($pConn['driver']) {
        case 'mdb':
        $conn=$this->connectMdb($pConn['path'],$pConn['dbname']);
        break;

        case "mysql":
        $conn=$this->connectMy($pConn['driver'], $pConn['host'], $pConn['user'], $pConn['password'], $pConn['dbName'], $pConn['port']);
        break;

        case "sqlsrv":
        $conn=$this->connectSqlsrv( $pConn['host'], $pConn['user'], $pConn['password'], $pConn['dbName'], $pConn['port']);
        break;

        default:
        $this->error("Driver non supportato ".$pConn['driver']);
        break;
      }



      $this->connArr[$conName]=$conn;
      $this->logPdo("Connessione database configurazione $conName  avvenuta con successo ");
    }

    return $this->connArr[$conName];
  }



  /**
  * Inizia la transazione sul database assegnato
  * @param  string $conName connessione designata per l'azione.  Nessun valore = database di default
  * @return pdo_result
  */
  public function startTransaction(string $conName="")
  {
    return $this->query("start transaction", $conName);
  }

  /**
  * Rolback della transazione sulla connessione designata
  * @param  string $conName connessione scelta per il rollback. Nessun valore = database di default
  * @return pdo_result
  */
  public function rollback(string $conName="")
  {
    return $this->query("rollback", $conName);
  }

  /**
  *Commit della transazione sulla connessione designata
  * @param  string $conName connessione designata per l'azione.  Nessun valore = database di default
  * @return pdo_result
  */
  public function commit(string $conName="")
  {
    return $this->query("commit", $conName);
  }

  /**
  * Imposta la connessione di default per l'esecuzione delle query
  * @param string $dbName nome della connessione da utilizzare
  */
  public function setActiveConnection(string $dbName)
  {
    $this->activeConnection=$dbName;
  }


  /**
  * Formatta la data da EU a Usa per le query mysql
  * @param  string $data [description]
  * @return mixed bool FALSE in caso di errore / string dataFormatta per mysql
  */
  public function hourMy(string $hour="",string $alias=""):string
  {
    if (!empty($alias)) {
      $ad=" as `$alias` ";
    }else{
      $ad="";
    }

    return " SUBSTR($hour,1,5 ) ".$ad;
  }


  public function colorMyTable(string $col,$alias=''){
    if(empty($alias)){
      $alias=$col;
    }

    return "<div class='tab-color'></div>  ";
  }



  public function detailBtnMy($modulo,$col="",$blank=true){
    if(empty($col)){
      $uMod=ucwords($modulo);
      $col=" $modulo.id$uMod ";
    }
    if($blank){
      $ad="--blank";
    }
    return "concat('$modulo.',$col) as '--open-detail $ad'";
  }

  /**
  * Formatta la data da EU a Usa per le query mysql
  * @param  string $data [description]
  * @return mixed bool FALSE in caso di errore / string dataFormatta per mysql
  */
  public function dateMy(string $data="",$alias=null):string
  {
    if ( $alias!=null ) {
      $ad=" as `$alias` ";
    }else{
      $ad="";
    }

    return "date_format($data,'%d/%m/%Y') ".$ad;

  }



  /**
  * Formatta la data da EU a Usa per le query mysql
  * @param  string $data [description]
  * @return mixed bool FALSE in caso di errore / string dataFormatta per mysql
  */
  public function dateTimeMy(string $data="", $alias=null):string
  {
    if ( $alias!=null ) {
      $ad=" as `$alias` ";
    }else{
      $ad="";
    }
    return "date_format($data,'%d/%m/%Y %H:%i') $ad ";
  }
}//fine trait
