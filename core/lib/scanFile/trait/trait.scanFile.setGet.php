<?php


/**
* Metodi di set e get della classe scanFile
*/
trait scanFileSetGet
{

  /**
  * Torna l'id riferimento
  * @return string [description]
  */
  public function getIdRiferimento():string{
    return $this->idRif;
  }

  /**
  * Torna l'istanza attiva ora
  * @return string istanza attiva
  */
  private function getIstance():string{
    return $this->istance;
  }

  /**
  * Imposta l'id dal token form
  */
  public function setIdFromToken(){
      $id=$this->getUtility()->getIdTokenField();
      if(empty($id)){
          $this->noFileFound();
      }
    $this->setIdRiferimento($id);
  }

  /**
  * Torna l'id riferimento
  * @return string [description]
  */
  public function setIdRiferimento(string $idRiferimento):string{
    return $this->idRif=$idRiferimento;
  }


  /**
  * Ritorno dell'errore
  * @return stirng error
  */
  public function getError():string
  {
    return $this->error;
  }

  //gestione degli errori in un'unica variabile
  public function error(string $mess):void
  {
    if(!$this->getUtility()->ifDebug()){

      echo "ERROR 2221123!";
      die;
    }

    $this->getUtility()->error($mess);
    $this->error.=" - ".$mess;
  }



  /**
  * cerco il nome univoco per non sovrascrivere il file vecchio
  * @param  integer $level livello in cui sono arriato
  * @return string         nome univoco del file
  */
  private function getNameUpload(int $level=0):string
  {
    $file=$this->getFileWork();
    if ($level==0) {//primo livello
      $newFile=$file;
    } else {
      $info=pathinfo($file); //livello X per cerare un file univoco aggiungendo numeri

      $name=$info['filename'];
      $ext=$info['extension'];

      $newFile=$name." (".$level.").".$ext;
    }
    if (file_exists($this->getDir().$newFile)) { //file già inesistente, aumento il numero
      return $this->getNameUpload($level+1); //ricorsione fino a trovare un numero valido
    } else {
      return $newFile; //uscita di ritorno, il nome a questo è univoco
    }
  }


  /**
  * Controllo se la cartella è aperta
  * @return bool esito controllo
  */
  public function ifOpenDir():bool
  {
    if (empty($this->rootDir)) {
      return false;
    } else {
      return true;
    }
  }




  /**
  * Ritorna la cartella di root impostata con altre routine
  * @return string valore della cartella
  */
  private function getDir():string
  {
    if ($this->ifOpenDir()) {
      return $this->rootDir;
    } else {
      $this->error("Cartella non aperta correttamente");
      return false;
    }
  }

  /**
  * Torna la mappa del file collezionata
  * @return array [description]
  */
  public function getMapFile():array
  {
    return $this->mapFile;
  }


  /**
  * Tornato il file in lavorazione
  * @param  string $param ==P percorso assoluto
  * @return html / false in caso di file non impostato
  */
  private function getFileWork(string $param="")
  {
    if (empty($this->fileWork)) { //controllo se effettivamente c'è un file in lavorazione
      $this->error("File di lavoro non impostato");
      return false;
    } else {
      //file esistente ritorno il valore
      if ($param=='P') { //con P torna la PATH assoluta
        return $this->getDir().$this->fileWork;
      }
      return $this->fileWork; //default path relativa
    }
  }


  /**
  * Imposta il file in lavorazione. Ogni funzione ha lo stesso riferimento per caricare il file
  * @param file da caricare
  */
  private function setFileWork(string $file):void
  {
    $this->fileWork=$file;
  }


  /**
  * Modo standard per aggiungere un file alla mappa dell'oggetto
  * @param array da aggiungere alla mappa file
  */
  private function addMapFile(array $res):void
  {
    array_push($this->mapFile, $res);
  }




}//end trait




?>
