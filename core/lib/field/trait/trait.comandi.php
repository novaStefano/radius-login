<?php

//trait contenente tutti i comandi, come tasti o div di raggruppo
trait comandi
{


  /**
  * div per i sotto dati di aggiunta dettaglio sotto i tasti salva
  * @param  string $visible [description]
  * @param  [type] $param   [description]
  * @return [type]          [description]
  */
  public function printAdDetDiv(string $visible='', array $param=[])
  {
    $param['id']='adDetDiv';
    $param['class']='adDetDiv';
    if ($visible=='') {
      $param['style']=' display:none; ';
    }
    $this->inDiv($param);
  }

  /**
  * Chiusura del div aggiunta dati
  * @return [type] [description]
  */
  public function chiudAdDetDiv()
  {
    $this->chiudi_inDiv('Chiusura div aggiunta dati');
  }


  /**
  * stampa del button rapido di stampa
  * @param  [type] $report [description]
  * @return [type]         [description]
  */
  public function printButton($report, $title="Stampa")
  {
    $id = "printBtn";
    $ico = $this->getButtonIco('fa-print');
    $param['html'] = "$title ".$ico;
    $event=" onclick=printReport('$report','$this->getTokenField()');";
    $param['event'] = $event;
    if ($this->getFormAction() == 'ins') {
      $param['style'] = 'display: none;';
      $param['adClass'] = 'printBtn';
    }
    return $this->newButton($param);
  }

  /**
  * Bottone per la stampa dell'excel
  * @param  [type] $report [description]
  * @param  string $titolo [description]
  * @param  string $option [description]
  * @return [type]         [description]
  */
  public function excelButton($report, $titolo='Esporta XLS', $option='')
  {
    $id = "printBtn";
    $ico = $this->getButtonIco('fa-print');
    $param['html'] = $titolo." BISOGNA CAMBIARE ICONA--_> ".$ico;
    $event=" onclick=printReport('$report','$this->getTokenField()');";
    $param['event'] = $event;
    return $this->newButton($param);
  }



  /**
  * stampa i comandi standard del modulo
  * @param  array  $adOption [description]
  * @return [type]           [description]
  */
  public function printComandi($adOption=[])
  {
    if(!empty($adOption['disable'])){
      return;
    }


    if(!isset($adOption['noGdiv'])){
      $this->chiudi_gDiv();
      $adClass="";
      if(!empty($adOption['adClass'])){
        $adClass=$adOption['adClass'];
      }
      $this->gDiv(["adClass"=>"printComandi ".$adClass]);
    }
    if(!$this->isactiveSchedaWizard()||($this->countSchede<=1)){
      $this->backButton();
    }else{
      $this->backPageSchedaButton();
    }
    if(empty($option['skipSave'])){
      if (!empty($adOption['save'])) {
        $save=$adOption['save'];
      } else {
        $save=[];
      }
      $this->saveButton($save);
    }

    if (isset($adOption['espExcel']) ) {
      $this->printExcelButton($adOption['espExcel']);
    }

    if (isset($adOption['excel']) ) {
      $this->printExcelButton($adOption['excel']);
    }

    if (isset($adOption['printPdf'])) {
      $this->printPdfButton($adOption['printPdf']);
    }

    if (isset($adOption['pdf'])) {
      $this->printPdfButton($adOption['pdf']);
    }

    if (!empty($adOption['delete'])) {
      $del=$adOption['delete'];
    } else {
      $del=[];
    }

    if (!empty($adOption['del'])) {
      $del=$adOption['del'];
    } else {
      $del=[];
    }
    $this->deleteButton($del);
    $this->executeAddBtnComandi($adOption);
    if( (!isset($adOption['notCloseDiv'])) && (!isset($adOption['noGdiv'])) ) {
      // $this->clearSpace();
      $this->chiudi_gDiv("Auto close comandi");
    }
  }


  private function addBtnComandi($add){
    if(!isset($add['hideOnInsert'])){
      $add['hideOnInsert']=true;
    }
    $this->newBtn($add);
  }

  private function executeAddBtnComandi($adOption){

    if(isset($adOption['addBtns'])){ //aggiunta dei bottoni
      foreach($adOption['addBtns'] as $add){
        $this->addBtnComandi($add);
      }
    }
    if(isset($adOption['addBtn'])){ //aggiunta dei bottoni
      $this->addBtnComandi($adOption['addBtn']);
    }
  }



  public function printComandiInsVel($lib, $adOption)
  {
    $this->chiudi_gDiv();
    // $this->clearSpace();
    // $this->gDiv();
    $this->gDiv();
    $this->web->post("azione", "", $tipo);
    if (!empty($adOption['sezione'])) {
      $sezione=$adOption['sezione'];
    } else {
      $sezione='';
    }
    $this->insVelSaveBtn($lib, $tipo, $sezione);
    $this->inLineBackButton();
    $this->chiudi_gDiv();
  }





  /**
  * bottone per il salvataggio, controlla i permessi e se non viene passato un event utilizza l'event di prototipo per il salvataggio
  * @param  array  $param [description]
  * @return [type]        [description]
  */
  public function saveButton(array $param=[])
  {
    if($this->getLockRecord()){ //in caso di record lockato non mostro il pulsante salva
      return 0;
    }

    $custom=$this->customSaveEvent;
    if(!empty($custom)){
      $param['event']=$this->customSaveEvent;
    }

    if (empty($param['modulo'])) {
      $perm=$this->getUtility()->getCurModulo();
    } else {
      $perm=$param['modulo'];
    }


    if ($this->isOnInsert()) {
      $permesso="inserisci";
      if($this->checkPermissionTableModule($perm,"modifica")){
        $this->addHtml("<script> proto.setUpdatePermission('1'); </script> ");
      }else{
        $this->addHtml("<script> proto.setUpdatePermission('0'); </script> ");
      }
    } else {
      $permesso="modifica";
    }

    if ($this->checkPermissionTableModule($perm, $permesso)) { //controllo i permessi
      if (empty($param['event'])) {

        if(isset($param['activeToken'])){
          $tok=$this->getCurToken();
        }else{
          $tok="";
        }

        $event = 'onclick="'.$this->getObjJs().'.salva(\'' . $tok . '\');"';
      } else {
        $event=' onclick="'.$param['event'].';" ';
      }
      $param['ico'] = 'save';
      $param['html'] = 'Salva';
      $param['event'] = $event;
      $param['adClass']=" btnSave  btnSave-".$this->getSezione()." ";
      $this->newButton($param);
    }
  }



  public function checkPermissionTableModule($perm,$permission){
    if (!$this->getUtility()->checkPermesso($perm, $permission)) { //controllo i permessi
      return false; //permessi globali
    }

    $table=$this->getLastTableSession();
    if(!empty($table)){
      if (!$this->getUtility()->checkPermesso($table, $permission)) { //controllo i permessi

        return false; //permessi globali
      }
    }
    return true;
  }

  /**
  * Crea il bottone di elimina standard
  * @param  array  $param parametri
  */
  public function deleteButton(array $param=[])
  { //bottone per l'elimina, controlla i permessi e se non viene passato un event utilizza l'event standard di prototipo

    if($this->getLockRecord()){ //in caso di record lockato non mostro il pulsante salva
      return 0;
    }

    if (empty($param['modulo'])) {
      $perm=$this->getUtility()->getCurModulo();
    } else {
      $perm=$param['modulo'];
    }

    if(!$this->checkPermissionTableModule($perm,"elimina")){
      return;
    }


    if (empty($param['event'])) { //event standard
      $param['event'] = 'onclick="'.$this->getObjJs().'.elimina(\''.$this->getCurToken().'\');"';
    }

    $param['ico']="trash";
    $param['html'] = 'Elimina';

    $param['adClass']="";
    if ($this->isOnInsert()) {
      $param['style'] = 'display: none;';
      $param['adClass'] = 'delBtn  ';
    }
    $param['adClass'].=" btnDel  btnDel-".$this->getSezione()." ";
    $this->newButton($param);

  }


  /**
  * Crea il bottone di elimina standard
  * @param  array  $param parametri
  */
  public function printPdfButton($param=[])
  { //bottone per l'elimina, controlla i permessi e se non viene passato un event utilizza l'event standard di prototipo
    $btn=[];
    if (empty($param['modulo'])) {
      $perm=$this->getUtility()->getCurModulo();
    } else {
      $perm=$param['modulo'];
    }
    if (empty($param['event'])) { //event standard
      if(empty($param['reportName'])){
        $reportName="pdfDettaglio";
      }else{
        $reportName=$param['reportName'];
      }
      $btn['event'] = 'onclick="report.execute(\''.$reportName.'\')"';
    }
    $btn['ico']='file-pdf';
    if(empty($param['label'])){
      $label="Pdf";
    }else{
      $label=$param['label'];
    }
    $btn['html'] = $label;
    if ($this->isOnInsert()) {
      $btn['style'] = 'display: none;';
      $btn['adClass'] = 'delBtn';
    }
    $this->newButton($btn);
  }



  /**
  * Crea il bottone di elimina standard
  * @param  array  $param parametri
  */
  public function printExcelButton($param=[])
  { //bottone per l'elimina, controlla i permessi e se non viene passato un event utilizza l'event standard di prototipo
    $btn=[];
    if (empty($param['modulo'])) {
      $perm=$this->getUtility()->getCurModulo();
    } else {
      $perm=$param['modulo'];
    }
    if (empty($param['event'])) { //event standard
      if(empty($param['reportName'])){
        $reportName="excelDettaglio";
      }else{
        $reportName=$param['reportName'];
      }
      $btn['event'] = 'onclick="report.execute(\''.$reportName.'\')"';
    }
    $btn['ico']='file-excel';
    if(empty($param['label'])){
      $label="Excel";
    }else{
      $label=$param['label'];
    }
    $btn['html'] = $label;
    if ($this->isOnInsert()) {
      $btn['style'] = 'display: none;';
      $btn['adClass'] = 'delBtn';
    }
    $this->newButton($btn);
  }



  //
  //
  // //metodo con defualt e correzzioni
  // //bottone: caricamento della form veloce
  // public function insVelBtn($class, $tipo)
  // {
  //     //if($this->web->canI('Corso','modfica')){
  //     $id="btnInsVel";
  //
  //     $event=" onclick=\"apriInsVelDiv('','$class','$tipo');\" ";
  //
  //     $ico = $this->getButtonIco('fa-plus');
  //     $param['html'] = ' ' . $ico;
  //     $param['event'] = $event;
  //     $param['id'] = 'btnInsVel';
  //     $param['adClass']='btnInsVel';
  //     $this->newButton($param);
  //     //}
  // }

  //
  // //bottone: caricamento della form veloce
  // public function insVelInsBtn($lib, $par="")
  // {
  //     //if($this->web->canI('Corso','modfica')){
  //     $id="btnInsVel";
  //     if ($par!="") {
  //         $adHtml=",$par";
  //     } else {
  //         $adHtml="";
  //     }
  //     $event=" onclick=\"apriInsVel('$lib'$adHtml);\" ";
  //     $ico = $this->getButtonIco('fa-plus');
  //     $param['html'] = ' ' . $ico;
  //     $param['event'] = $event;
  //     $param['id'] = 'btnInsVel';
  //     $param['adClass']='btnInsVel';
  //     $this->newButton($param);
  //     //}
  // }

  //
  // //bottone di salvataggio della form rapida
  // public function insVelSaveButton($lib, $event = '', $tabName = '', $eventDefault='', $filePhp='')
  // { //bottone per il salvataggio, controlla i permessi e se non viene passato un event utilizza l'event di prototipo per il salvataggio
  //     if ($this->web->canI($lib, 'modifica')) { //controllo i permessi
  //         $id = 'insVelIns';
  //
  //         if ($tabName != "") { //id tabella diverso da permesso
  //             $lib = $tabName;
  //         }
  //
  //
  //         if ($event == "") { //event standardcostuito con i vari parametri
  //             $adEvent="";
  //             if ($filePhp!="") {
  //                 $adEvent=",'$filePhp'";
  //             } else {
  //                 $adEvent=",''";
  //             }
  //             if ($eventDefault!="") {
  //                 $adEvent.=",$eventDefault";
  //             }
  //
  //             $event = 'onclick="salvaInsVel(\'' . $lib . '\' '.$adEvent.');"';
  //         }
  //
  //         $ico = $this->getButtonIco('fa-save');
  //         $param['html'] = 'Salva ' . $ico;
  //         $param['event'] = $event;
  //         $param['id'] = $id;
  //         $this->newButton($param);
  //     }
  // }

  //
  // //nuovo insVelSaveBtn, l'altro sarà deprecato
  // //bottone di salvataggio della form rapida
  // public function insVelSaveBtn($lib, $tipo, $sezione, $event='')
  // { //bottone per il salvataggio, controlla i permessi e se non viene passato un event utilizza l'event di prototipo per il salvataggio
  //     if ($this->web->canI($lib, 'modifica')) { //controllo i permessi
  //         $id = 'subIns';
  //
  //         if ($event!="") {
  //             $event=",".$event;
  //         }
  //
  //         $event = "onclick='saveInsVel(\"$tipo\",\"$sezione\" $event);' ";
  //
  //         $ico = $this->getButtonIco('fa-save');
  //         $param['html'] = 'Salva ' . $ico;
  //         $param['event'] = $event;
  //         $param['id'] = $id;
  //         $this->newButton($param);
  //     }
  // }
  //



  public function nextPageSchedaButton(array $param=[])
  { //bottone per tornare indietro da form di dettaglio, già configurato con l'event da prototipo
    $event = "onclick=\"scheda.next('{$this->getSezione()}');\" ";
    $ico = $this->getButtonIco('fa-arrow-right');
    $param['html'] = 'Avanti ' . $ico;
    $param['event'] = $event;
    $this->newButton($param);
  }



  public function backPageSchedaButton(array $param=[])
  { //bottone per tornare indietro da form di dettaglio, già configurato con l'event da prototipo
    $event = "onclick=\"scheda.prev('{$this->getSezione()}');\" ";
    $param['ico']="arrow-left";
    $param['html'] = 'Indietro';
    $param['event'] = $event;
    $this->newButton($param);
  }



  public function getBackEvent(){
    return $this->getObjJs().".indietro()";
  }


  public function backButton(array $param=[])
  { //bottone per tornare indietro da form di dettaglio, già configurato con l'event da prototipo
    if(!empty($this->notBackButton)){
      return 0;
    }
    $param['ico']="arrow-left";
    if(!isset($param['html'])){
      $param['html'] = ' Indietro ';
    }
    $param['click'] = $this->getBackEvent();
    if(!isset($param['adClass'])){
      $adClass="";
    }else{
      $adClass=$param['adClass'];
    }
    $param['adClass']=" $adClass btnBack-general btnBack  btnBack-".$this->getSezione()." ";
    $this->newButton($param);
  }

  public function subBackButton($event = "")
  { //bottone per tornare indietro da una sottoform di dettaglio, già configurato con l'event da prototipo
    $id = "subBackButton";
    if ($event == "") {
      $event = "onclick=\"subIndietro();\" ";
    }

    $ico = $this->getButtonIco('fa-arrow-left');
    $param['html'] = $ico.' Indietro';
    $param['event'] = $event;
    $param['id'] = $id;
    $param['adClass']=" btnBack  btnBackSub btnBack-".$this->getSezione()." ";

    $this->newButton($param);
  }

  public function inLineBackButton($class='', $event="")
  {
    $id="insVelIndBtn";

    if ($class!='') {
      $class="'$class'";
    }
    if ($event == "") {
      $event = "onclick=\"exitInsVel($class);\" ";
    }

    $ico = $this->getButtonIco('fa-close');
    $param['html'] = $ico.' Annulla';
    $param['event'] = $event;
    $param['id'] = $id;
    $param['adClass']=" btnBack  btnBackInLine btnBack-".$this->getSezione()." ";
    $this->newButton($param);
  }


  public function subDeleteButton($lib, $event = '', $tabName = '', $titolo)
  { //bottone per l'elimna, controlla i permessi e se non viene passato un event utilizza l'event di prototipo per il salvataggio

    if (!$this->web->canI($lib, 'elimina')) { //controllo i permessi
      return 0; //esco perchè non ho i permessi
    }
    $id = 'subIns';

    if ($tabName != "") { //id tabella diverso da permesso
      $event = "";
      $lib = $tabName;
    }

    if ($event == "") { //event standard
      $event = 'onclick="subElimina(\'' . $lib . '\',\'\',\'\',\''.$titolo.'\');" ';
    }

    $ico = $this->getButtonIco('fa-trash');
    $param['html'] = 'Elimina ' . $ico;
    $param['event'] = $event;
    $param['id'] = $id;
    if ($this->getFormAction() == 'ins') {
      $param['style'] = 'display: none;';
      $param['adClass'] = 'delBtnSub';
    }
    $this->newButton($param);
  }


  /**
  * Metodi per creare bottoni con l'icona di inserimento
  * @param  [type] $event [description]
  * @param  array  $param [description]
  * @return [type]        [description]
  */
  public function insButton($event,array $param=[],array $adParam=[])
  {

    $id = "insBtn";
    if(empty($adParam['html'])){
      $html="";
    }else{
      $html=$adParam['html']." ";
    }
    $param['html'] = $html.$this->getButtonIco('fa-plus');
    $param['event'] = $event;
    $param['id'] = $id;
    return $this->newButton($param);
  }




  private function eleLink()
  { //crea l'input contenente un link
    $this->inputGroup('fa-external-link-alt', 'onclick=groupLink(this); ');
  }

  private function eleData()
  { //crea l'input contenente una data
    $this->inputGroup('fa-calendar-day');
  }


  private function eleTime(array $options=[])
  { //crea l'input contenente una data
    $this->inputGroup('fa-clock','',$options);
  }

  private function eleCerca()
  { //crea l'input contenente una selettore di ricerca (es: seleziona atletta)
    $this->inputGroup('fa-search', 'onclick=groupCerca(this);');
  }

  private function eleTelefono()
  {
    $this->inputGroup('fa-phone', 'onclick=groupChiama(this);');
  }


  private function groupByLikeName($id)
  { //gruppi come id o stato
  }
}//fine trait
