<?php

/**
* Classe standard per creare tabelle
*/

class CreaTab extends WrapperUtility
{
  public $clas; //classe della tabella
  public $style; //stile tabella
  public $id; //id tabella
  public $idRiga="id";
  public $hiddenColumns=["autoClass","autoPlus", "autoTrash",
  "autoCheckbox","autoCheckEvent","checkedEvent"]; //array per nascondere la colonna - colonna a true allora viene saltata  //eventi td
  private $row; //riga in lavorazione
  private $eventTd; //modello evento per colonne
  //event tr
  public $eventRow; //evento per la riga
  public $classRow=""; //classe per la riga

  private $countRow=0; //conta le righe che ha generato

  private $tab; //contenitore html
  public $printHtml=true; // default parametrizza il print dell'html, altrimenti ritorna
  private $resWork; //dataset in elaborazione

  private $adBlockHtmlTd=[]; //metadata per le aggiunte dei blocchi html

  private $activeAutoEscape=true; //controllo se è attiva la modalità di auto escape anti xss

  private $excludeEscapeCol=[]; //array con le colonne da non controllare per html
  private $addEventStopProp=false; //attiva lo stop propagation / usato per i cmd da query con --
  private $exportExcel=true; //esporta le righe in excel
  private $isDataTableOn=true; //attiva / disattiva la datatable
  private $skipPaging=false; //salta il paging su datable se a true
  public $exportDataTable=false; //serve per il paging su gestione.. non toccare


  public function initializeObject()
  {
    if(isset($this->getUtility()->hiddenColumns)){
      $this->addHiddenColumn($this->getUtility()->hiddenColumns);
    }

    if(isset($this->getUtility()->autoExportExcelTable) && $this->getUtility()->autoExportExcelTable==false ){
      $this->disableExportExcel();
    }
    
      

  }

  public function getBlockHtmlTd(int $i){
    if(!isset($this->adBlockHtmlTd[$i])){
      return false;
    }
    return $this->adBlockHtmlTd[$i];
  }

  /**
  * Aggiunge una colonna con html generato in sostistuzione dei dati del dataset
  * @param  string  $nome  nome Colonna da vedere a nel tHead
  * @param  integer $numTd numero di colonna
  * @param  string  $html  html da sostituire
  * @param  array   $array array con i campi da fare il replace
  */
  public function adTdColHtml(string $nome, string $numTd, string $html,array $option=[])
  {

    $block['nome']=$nome;
    $block['numTd']=1;
    $block['html']=$html;
    if(isset($option['stopPropagation'])){
      $block['stopPropagation']=1;
    }
    $this->adBlockHtmlTd[$numTd]=$block;

  }



  /**
  * Aggiunge una colonna con html generato in sostistuzione dei dati del dataset

  * @param  string  $html  html da sostituire
  * @param  array   $array array con i campi da fare il replace
  */
  public function colHtml( string $html)
  {
    $this->adTdColHtml("",0,$html);
  }


  public function setActiveEscape(bool $active){
    $this->activeAutoEscape=$active;
  }

  /**
  * Setta il ritorno html al posto dell'auto echo
  */
  public function setReturnHtml()
  {
    $this->printHtml=false;
  }

  public function creaQuery($que, $printHtml=false)
  {
    $this->printHtml=$printHtml;
    $this->getUtility()->query($que);
    return $this->crea();
  }

  /**
  * Imposta il nome del dataet su cui operare
  * @param string $res nome del dataset
  */
  private function setRes(string $res="")
  {
    $this->resWork=$res;
  }

  /**
  * Torna il dataset richiesto
  * @return string dataset in memoria
  */
  private function getRes():string
  {
    return $this->resWork;
  }


  private function initTrashButton(){
    $field=$this->getUtility()->getField();
    $mod=$this->getUtility()->getCurModulo();
    $html=$field->newButton(['retStr'=>'1','icon'=>"trash",
    'event'=>"onclick=\"%sost-autoTrash('%sost-id');\""]);
    $this->adTdColHtml('',0,$html,['stopPropagation'=>'1']);
  }


  private function initCheckEvent($col){
    $field=$this->getUtility()->getField();
    $mod=$this->getUtility()->getCurModulo();
    $mod=strtolower($mod);
    $html=$field->newInput(['retStr'=>'1','type'=>"checkbox","attr"=>" %sost-checkedEvent ",
    'event'=>"onclick=\"$mod.%sost-autoCheckEvent(this,'%sost-id');\""]);
    $this->adTdColHtml('',0,$html,['stopPropagation'=>'1']);
  }

  private function initPlusButton(){
    $field=$this->getUtility()->getField();
    $mod=$this->getUtility()->getCurModulo();
    $html=$field->newButton(['retStr'=>'1','icon'=>"plus",
    'event'=>"onclick=\"%sost-autoPlus('%sost-id');\""]);
    $this->adTdColHtml('',0,$html,['stopPropagation'=>'1']);
  }


  private function initCheckbox(array $option=[]){
    if(!isset($option['event'])){
      $ev='clickCheckTab(event)';
    }else{
      $ev=$option['event'];
    }
    $html="<input type='checkbox' class='table-check single-check' check-id='%sost-id' onclick='$ev' %sost-autoCheckbox  >";
    $titleHtml="<input type='checkbox' class='table-check' onclick='clickAllCheckTab(event,this)' >";
    $this->adTdColHtml($titleHtml,0,$html,['stopPropagation'=>'1']);
  }


  /**
  * Pulisce il titolo dai comandi programmati direttamente in query
  * @param  string $property titolo da pulire
  * @return string           titolo pulito
  */
  private function cleanThCmd(string $property):string{

    if((strpos($property,"--")===false)){ //nessun cmd da pulire
      return $property;
    }

    if(!(strpos($property,"--btn-event")===false)){
      return "";
    }


    if(!(strpos($property,"--open-detail")===false)){
      return "";
    }

    if(!(strpos($property,"--btn-delete")===false)){
      return "";
    }

    $property=str_replace("--link","",$property);
    $property=str_replace("--html","",$property);
    $property=str_replace("--check-fa","",$property);
    $property=str_replace("--color","",$property);
    $property=str_replace("--baloon-class","",$property);
    $property=str_replace("--label","",$property);
    $property=str_replace("--baloon","",$property);
    $property=str_replace("--check-event","",$property);
    $property=str_replace("--check-icon","",$property);



    return $property;
  }


  public function tableContainer($id,$class){
    $this->addHtml('<table id="' . $id . '" class=" table display dataTable  '.$class . '">');
    $this->addHtml('<thead>');
    $this->addHtml('<tr>');
  }


  public function printTableTag(){
    if ($this->ifEventRow()) { //controllo se devo mostrare il cursore e se devo evidenziare le righe
      $ad=" tableSelectRow  tableStripe ";
    } else {
      $ad=" tableStripe ";
    }
    if(empty($this->id)){
      $this->id=$this->getUtility()->getRandomString();
    }
    $this->tableContainer($this->id,$ad.$this->clas);
  }

  public function checkAutoColHeader($col){

    if(in_array("autoClass",$col)){
      $this->setClassRow("autoClass");
    }

    if(in_array("autoPlus",$col)){
      $this->initPlusButton($col);
    }

    if(in_array("autoCheckEvent",$col)){
      $this->initCheckEvent($col);
    }

    if(in_array("autoTrash",$col)){
      $this->initTrashButton($col);
    }


    if(in_array("autoCheckbox",$col)){
      $this->initCheckbox($col);
    }
  }

  public function creaTableOnlyHeader($col){
  
    $this->tableContainer("tableSelectRow","");
    $this->processThead($col); 
    $this->addHtml("<tbody style='text-align:center'><tr><td colspan=100>Dataset vuoto</td></tr></tbody>");
    $this->addHtml("</table>");
    return $this->tab;
  }



  public function processThead($col){
    $i=0;
    $this->checkAutoColHeader($col);
    foreach ($col as $property) {
      if (!in_array($property, $this->hiddenColumns)) { //controllo colonna da stampare
        $i=$this->adColHtml('th', $i); //colonna da aggiunta html e non da array / query
        $property=$this->cleanThCmd($property);
        $this->addHtml("<th>" . $property. "</th>"); //stampa della solonna vera e propria
        $i++;
      }
    }
    $this->adColHtml('th', $i); //controllo se ho altri colonne
    $this->addHtml('</thead>');
    $this->addHtml('</tr>');

  }


  /**
  * stampa dell'header della tabella
  * @return void, aggiunge html
  */
  private function printHeadTabArray():void
  {
    $this->printTableTag();
    $resArray=$this->getResArray();
    $col=array_keys( reset($resArray));
    $this->processThead($col); 
  }





  /**
  * stampa dell'header della tabella
  * @return void, aggiunge html
  */
  private function printHeadTab():void
  {       
    $this->printTableTag();
    $res=$this->getRes();
    $col=$this->getUtility()->getColumnName($res);
    $this->processThead($col); 
  }


  /**
  * Ritorna la riga in lavorazoine
  * @return [type] [description]
  */
  private function getRowWork()
  {
    return $this->rowWork;
  }

  /**
  * IMposta la riga in lavorazione
  * @param PDO_FETCH $row
  */
  private function setRowWork($row):void
  {
    $this->rowWork=$row;
  }


  /**
  * Stampa il corpo della tabella
  * @return void aggiunge sulla varaibile HTML
  */
  private function printBodyTable():void
  {
    $res=$this->getRes();




    //stampa corpo
    $this->addHtml('<tbody>');
    while ($row=$this->fetch($res)) {
      $this->processRowBody($row);    
    } //fine ciclo tabella
    $this->closePrintBodyTable();
  }


  public function processRowBody($row){
    $this->setRowWork($row);
      $event = $this->getEventRow();
      $classe=$this->getClassRow();
      if (!empty($row[$this->idRiga])) {
        $idRiga=$this->escapeHtml($row[$this->idRiga]);
      } else {
        $idRiga="";
      }
      $this->addHtml("<tr id=\"riga_" . $idRiga . "\" $classe  $event >");

      $i = 0;
      foreach ($row as $key => $value) {
        $value=$this->escapeHtml($value,$key);
        $this->row = $row; //riga in lavorazione
        if (!in_array($key, $this->hiddenColumns)) {

          $i=$this->adColHtml('td', $i);
          $this->addHtml("<td " . $this->getEventCol($key, $row) . " >" . $value . "</td>");
          $i++;
        }
      }

      $this->adColHtml('td', $i); //controllo se deve essere una colonna aggiunta nell'ultima colonna
      $this->addHtml("</tr>");
      $this->countRow++;

  }

  public function closePrintBodyTable(){
    $this->addHtml('</tbody>');
    $this->addHtml('</table>');
    $this->loadDataTable();
  }


  /**
  * Stampa il corpo della tabella
  * @return void aggiunge sulla varaibile HTML
  */
  private function printBodyTableArray():void
  {
    $res=$this->getResArray();
    //stampa corpo
    $this->addHtml('<tbody>');
    foreach($res as $row){
      $this->processRowBody($row);    
    }
   $this->closePrintBodyTable();
  }



  /**
  * Attiva il datable sulle tabelle generate
  * @return [type] [description]
  */
  private function loadDataTable(){
    if(!$this->isDataTableOn){
      return;
    }
    $ad="";
    if($this->skipPaging){
      $ad="paging:false";
    }
    if(!empty($ad)){
      $ad="{ $ad }";
    }
    if($this->exportDataTable){
      $exp=" var tableGest= ";
    }else{
      $exp="";
    }
    $this->addHtml("<script> $exp $('#{$this->id}').DataTable($ad);</script>");
  }

  /**
   * Imposta l'array per la creazione della tabella
   */
  public function setResArray(array $res){
    $this->resArray=$res;
  }

  public function getResArray():array{
    return $this->resArray;
  }

  public function printContainer(){
    $this->addHtml('<div class="panel-body table-responsive" >');
  }

  public function closeContainer(){
    $this->addHtml('</div>');
  }

  /**
   * Crea una tabella da un array
   */
  public function creaArray(array $array)
  { //metodo di bootstrap per la creazione della tabella
    $res=$array;
    $this->countRow=0;
    $this->setResArray($array);
    $this->printContainer();
    $this->printHeadTabArray();
    $this->printBodyTableArray();
    if($this->exportExcel){
      $event="report.exportToExcelAuto(\"#{$this->id}\");";
      $this->addHtml("<button onclick='$event' class='btn btn-info'>Excel <i class='fa fa-file-excel'></i> </button>");
    }

    // "    <p><b>Record trovati: $this->countRow</b></p>");
    $this->closeContainer();
    if ($this->printHtml) {
      echo $this->tab;
    } else {
      return $this->tab;
    }
  }
  //end crea tabella



  public function crea(string $dataset='')
  { //metodo di bootstrap per la creazione della tabella
    $res=$dataset;
    $this->countRow=0;
    $this->setRes($res);
    $this->printContainer();    
    $this->printHeadTab();
    $this->printBodyTable();
    if($this->exportExcel){
      $event="report.exportToExcelAuto(\"#{$this->id}\");";
      $this->addHtml("<button onclick='$event' class='btn btn-info'>Excel <i class='fa fa-file-excel'></i> </button>");
    }

    // "    <p><b>Record trovati: $this->countRow</b></p>");
    $this->closeContainer();
    if ($this->printHtml) {
      echo $this->tab;
    } else {
      return $this->tab;
    }
  }
  //end crea tabella

  /**
  * Imposta le colonne che non devono essere viste nell'outpu
  * @param array $array [description]
  */
  public function setHiddenColumns(array $array=['id']):void
  { //imposta le colonne da nascondere di default
    $this->hiddenColumns = $array;
  }

  public function addHiddenColumn($col):void{
    if(is_array($col)){
      $this->hiddenColumns= array_merge($this->hiddenColumns,$col);
    }else{
      array_push($this->hiddenColumns,$col);
    }

  }

  /**
  * Imposta l'evendo per la riga, sostituisce le colonne con un replace
  * @param string $evento      evento con i replace field
  * @param array  $colonnaSost campi con cui operare i replace
  */
  public function setEventRow(string $evento, array $colonnaSost=['id']):void
  { //imposta l'evento per la colonna
    $this->eventRow['evento'] = $evento;
    $this->eventRow['eventArray'] = $colonnaSost;
  }

  /**
  * Controlla se il click per riga è impostato
  * @return bool controllo se l'evento per click riga esiste
  */
  private function ifEventRow():bool
  {
    if (empty($this->eventRow['evento'])) {
      return false;
    } else {
      return true;
    }
  }


  /**
  *setta la colonna che il valore deve essere usato come classe
  */
  public function setClassRow(string $nomeColonna):void
  {
    $this->classRow=$nomeColonna;
  }

  /**
  * Torna la classe impostata
  * @return string classe impostata
  */
  private function getClassRow():string
  {
    if (empty($this->classRow)) {
      return "";
    }
    $row=$this->getRowWork();
    return "class='".$this->escapeHtml($row[$this->classRow])."'";
  }


  /**
  * Controllo se la colonna che devo aggiungere esiste
  * @param  int  $i della colonna
  * @return bool esito controllo
  */
  private function adColIsSet($i):bool
  { //controllo se la colonna da aggiungere esiste
    if ($this->getBlockHtmlTd($i)) {
      if ($this->getBlockHtmlTd($i)['numTd'] == '1') {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  /**
  * $controllo se devo aggungere html a quella colonna
  * @param  string $tipo tipo di colonna da processare
  * @param  int $i       indice della colonna
  */
  private function adColHtml(string $tipo,int &$i)
  { //controllo se è presente html da aggiungere
    if (!$this->adColIsSet($i)) { //indice non settato esco senza incrementare l'indice di esecuzione
      return $i;
    }
    //tipo di gestione? body o testata?
    if ($tipo == 'td') { //body
      return $this->getHtmlTd($i);
    }

    if ($tipo == 'th') { //testata
      return $this->getHtmlTr($i);
    }
  }


  public function skipEscapeCols(array $cols){
    $this->excludeEscapeCol=$cols;
  }



  private function btnOpenDetail($string,$col){
    $val=explode(".",$string);
    $modulo=$val[0];
    $id=$val[1];
    if(strpos($col,"--blank")){
      $event="openNewTabModulo";
    }else{
      $event="getProto().loadDettaglioDirect";
    }
    $modulo=ucwords($modulo);
    $field=$this->getUtility()->getField();
    $html=$field->newButton(['retStr'=>'1','icon'=>"external-link-alt",
    'click'=>"$event('$modulo','$id') "] );
    return $html;
  }




  private function escapeHtml($string,$col=""){


    if(!(strpos($col,"--open-detail")===false)){
      $this->addEventStopProp=true; //attivo lo stop propagation
      return $this->btnOpenDetail($string,$col);
    }

    if($col=='--btn-delete'){
      $r=explode(".",$string,2);
      if(count($r)<2){
        return "";
      }
      $id=$r[0];
      $tab=$r[1];
      return "<button class='btn btn-primary btn-delete-inline' onclick='event.stopPropagation(); fastDelete(\"$tab\",\"$id\");'><i class='fa fa-trash'></i></button> ";
    }

    if(!(strpos($col,"--check-event")===false)){
      $r=explode("/",$string);
      if(count($r)<1){
        return "";
      }
      $event=$r[0];

      $val=false;
      if(isset($r[1])){
        $val=$r[1];
      }

      if($val){
        $val="checked";
      }else{
        $val="";
      }
      return "<input type='checkbox'  $val 
      onclick='event.stopPropagation(); $event '>";
    }


    if(!(strpos($col,"--check-icon")===false)){
      if($string){
        $val="checked";
      }else{
        $val="";
      }
      return "<input type='checkbox' $val >";
    }

    if($col=='--btn-event'){ //label/evento es Modifica evento/test.eseguireProva('12')/icon/color
      $r=explode("/",$string);
      if(count($r)<2){
        return "";
      }
      $event=$r[0];
      $label=$r[1];
      
      $ico="";
      if(isset($r[2])){
        $ico=$r[2];
      }
      $color="primary";
      if(isset($r[3])){
        $color=$r[3];
      }
      
      return "<button class='btn btn-$color btn-delete-inline' 
      onclick='event.stopPropagation(); $event '><i class='fa fa-$ico'></i> $label</button> ";
    }

    if(strpos($col,"--link")){
      $res=explode("|",$string);
      if(count($res)>1){
        $link=$res[0];
        $string=$res[1];
      } else{
        $link=$string;
      }
      return "<span class='a-hover animate label label-info'
       onclick='event.stopPropagation();openLinkTab(\"$link\");'>$string</span>";
    }

    if(strpos($col,"--html")){
      return $string;
    }

    if(strpos($col,"--check-fa")){
      if($string=='1'){
        return "<i class='fa fa-check'></i>";
      }else{
        return "";
      }
      return $string;
    }

    if(strpos($col,"--color")){
      return "<div class='tab-color' style='background-color:$string'></div>";
    }




    if(strpos($col,"--baloon-alert") || strpos($col,"--label") ){
      try{
        $r=explode(".",$string,2);
        if(count($r)<2){
          return "";
        }
        $color=$r[0];
        $label=$r[1];
      }catch(Exception $err){
        $this->error("ERRORE BALOON CREATAB");
      }
      return "<span class='label label-$color'>$label</span>";
    }

    if(strpos($col,"--baloon")){
      try{
        $r=explode(".",$string,2);
        if(count($r)<2){
          return "";
        }
        $color=$r[0];
        $label=$r[1];
      }catch(Exception $err){
        $this->error("ERRORE BALOON CREATAB");
      }
      return "<span class='label' style='background-color: $color;'>$label</span>";
    }


    if(in_array($col,$this->excludeEscapeCol)){
      return $string;
    }

    if($this->activeAutoEscape){
      return $this->getUtility()->escapeHtml($string);
    }else{
      return $string;
    }
  }

  /**
  * crea l'html dell'array di aggiunta delle colonne (th)
  * @param  int $i della colonna da prendere
  *
  */
  private function getHtmlTr(int &$i)
  { //

    $nome = $this->getBlockHtmlTd($i)['nome'];
    $html = "<th>$nome</th>";
    $this->addHtml($html);
    $i++;
    if ($this->adColIsSet($i)) {
      $i=$this->getHtmlTr($i);
    }
    return $i;
  }

  /**
  * crea l'html dall'array di aggiunta dell colonne (body)
  * @param  int    $i [description]
  * @return [type]    [description]
  */
  private function getHtmlTd(int &$i)
  {
    $block=$this->getBlockHtmlTd($i);
    $html = $block["html"]; //HTML
    $arr =  $this->getRowWork();
    $row = $this->row;
    foreach ($arr as $nome => $val) {
      $val=$this->escapeHtml($val);
      $html = str_replace('%sost-' . $nome, $val, $html);
    }
    if(isset($block['stopPropagation'])){
      $ev="onclick='event.stopPropagation();'";
    }else{
      $ev="";
    }
    $app = "<td $ev >$html</td>";
    $this->addHtml($app);
    $i++;
    if ($this->adColIsSet($i)) { //controllo se l'indice successivo è settato
      $i=$this->getHtmlTd($i);
    }
    return $i;
  }

  /**
  * Aggiunge html al buffer della classe
  * @param string $html aggiunge Dom alla variabile standard
  */
  private function addHtml(string $html):void
  {
    $this->tab.=$html;
  }

  /**
  * Torna l'evento della tabella RIGA
  * @return string evento in memoria generato
  */
  private function getEventRow():string
  {
    $row=$this->getRowWork();
    if (!isset($this->eventRow['evento'])) {
      return ""; //nessun evento trovato, fine routine
    } else {
      $event = $this->eventRow['evento'];
      $eventAr = $this->eventRow['eventArray'];
    }
    //Casto l'evento se deve far qualche replace con i campi
    foreach ($eventAr as $nome) {
      if (!empty($row[$nome])) {
        $event = str_replace('%sost-' . $nome, $this->escapeHtml($row[$nome]), $event);
      }
    }
    return $event;
  }

  /**
  * Imposta l'evento per la colonna
  * @param string $nomeColonna nome della colonna
  * @param string $evento      dichiarazione dell'evento con il sost
  * @param array $colonnaSost  campi della tabella da sostituire
  */
  public function setEventCol(string $nomeColonna, string  $evento, array $colonnaSost=['id'])
  {
    $this->eventTd[$nomeColonna]['evento'] = $evento;
    $this->eventTd[$nomeColonna]['eventArray'] = $colonnaSost;
  }

  /**
  * Torna l'evento per la colonna
  * @param  [type] $val [description]
  * @param  [type] $row [description]
  * @return [type]      [description]
  */

  /**
  * Torna l'evento per la colonna
  * @param  string $val [description]
  * @param  [type] $row [description]
  */
  private function getEventCol(string $val, $row)
  { //creo l'evento multiplo per ogni riga


    if( $this->addEventStopProp){ //stop propagation per cmd su query
      $ev="onclick='event.stopPropagation();'";
      $this->addEventStopProp=false;
      return $ev;
    }


    if (!isset($this->eventTd[$val])) {
      return ""; //nessun evento trovato, fine routine
    } else {
      $event = $this->eventTd[$val]['evento'];
      $eventAr = $this->eventTd[$val]['eventArray'];
    }
    foreach ($eventAr as $nome) {
      $event = str_replace('%sost-' . $nome, $this->escapeHtml($row[$nome]), $event);
    }
    return $event;
  }



  /**
  * Torna l'idRiga impostato
  * @return string  idRiga
  */
  public function getIdRiga():string
  {
    return $this->idRiga;
  }

  /**
  * Imposta idRiga sulla tabella
  * @param string $id idRiga da impostare
  */
  public function setIdRiga(string $id="id"):void
  {
    $this->idRiga=$id;
  }

  /**
  * Imposta l'evento standard per il dettaglio
  * @param string $id [description]
  */
  public function setDetEvent(string $id='id'):void
  {
    if ($id=='') {
      $id=$this->getIdRiga();
    }
    $event="%sost-".$id;
    $this->setEventRow('onclick="proto.dettaglio('.$event.');"', [$id]);
  }


  public function addCollImgForm($col,$label=""){
    $r=explode(".",$col);
    $col=$r[1];
    $tab=$r[0];
    $this->addHiddenColumn($col);
    $pImg=$this->getPath("A_IMMAGINI");
    $noImg=$pImg."/noimg.png";
    $base= $this->getUtility()->getFileForm()->getPublicPathForTab($tab,$col);
    $html="<img class='table-img-form' onError=\"this.onerror=null;$(this).addClass('error-img');
    this.src='$noImg'\" data-src='$base%sost-$col'
    src='$base%sost-$col'>";
    $this->adTdColHtml($label,1,$html);
  }

  public function disableExportExcel(){
    $this->exportExcel=false;
  }

  public function disableDataTable(bool $disable=false):void{
    $this->isDataTableOn=$disable;
  }

  public function disablePaging(bool $disable=true):void{
    $this->skipPaging=$disable;
  }



}//fine class
