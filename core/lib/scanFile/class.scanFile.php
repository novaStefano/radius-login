<?php
/**
*  Libreria per la mappatura in tempo reale dei file di una directory e la manipolazione dei file
* Scansione di una cartella e creazione oggetto JSON
* Upload della cartella di riferimento
* Estensibile per la parte permessi e caricamento cartelle
* Visualizzazione file in area sicura temp
*/

include_once __DIR__."/trait/trait.scanFile.view.php";
include_once __DIR__."/trait/trait.scanFile.db.php";
include_once __DIR__."/trait/trait.scanFile.setGet.php";



class ScanFile extends WrapperUtility
{

	use scanFileView,scanFileDb,scanFileSetGet;
	private $rootDir=""; //cartella che sto processando
	private $workFile=[]; // aray dei file in lavorazione
	private $mapFile=[]; //array associativo con i file che sto legggendo TIPO - Nome - dimensione - data
	private $error=''; //contiene gli errori
	private $dir=""; //cartella aperta
	private $file=""; //parametro file
	private $classJs=""; //classe di riferimento js
	public $adDir=""; //percorso di navigazione partendo dalla root del record
	private $istance=""; //istanza allocata

	private $idRif=""; //id di riferimento / usato solo per log o mappatura db. Non obbligatorio
	/**
	* Costruttore dell'oggetto
	* @param Utility $utility [description]
	*/
	public function __construct(Utility $utility)
	{
		parent::__construct($utility); //alloco il launcher solito
		$this->checkPermesso("visualizza"); //devo controllare posso almeno vedere il permesso di visualizzazione
	}


	/**
	* Controllo di sicurezza per verificare che l'utente no richieda livelli a cui non potrebbe accedere
	* @param  string $dir cartella da verificare
	* @return void   ferma la request in caso di input sospetto
	*/
	private function checkSecurityDir(string $dir=""){
		$exps=array("/..","../","\..","..\"");
		foreach ($exps as $exp ) {
			$pos = strpos($dir, $exp);
			if ($pos ) {
				$this->error("Errore");
			}
		}
	}

	/**
	 * Da conf devo configurare qualche directory? La faccio qua!
	 * @return
	 */
	private function checkAutoDir($dir){
		$init=$this->getUtility()->getConfModulo("scanFile_initDir");
		if(!$init){
			return;
		}
		foreach($init as $a){
			$nDir=$dir.$a."/";
			if(!file_exists($nDir)){
				$this->getUtility()->makeDir($nDir);
			}
		}
	}

	public function checkPermesso(string $modulo,string $permesso=""){
		$res= parent::checkPermesso($modulo,$permesso);
		if(!$res){
		  $this->error("Non hai i permessi per accedere alla risorsa");
		}
	  }
	


	/**
	* metodo di bootstrap con la lista delle chiamate di base
	* @param  [type] $dir    directory in cui devo operare
	* @param  string $azione Azione richiesta - presa dal post in caso di valore empty
	* @param  string $param  parameotro della richiesta (file ad esempio)
	* @return esecuzione rihiesta
	*/
	public function startScan(string $dir="", string $param='', string $azione='')
	{


		if (empty($azione)) {
			$azione=$this->postStr('metodo'); //metodo effettivo da eseguire
		}
		$this->classJs=$this->post("nameIstance")."ScanFile";
		$this->istance=$this->post("nameIstance");

		if(empty($dir)){
			$id=$this->getIdRiferimento();
			if(!empty($id)){
				$id=$id."/";
			}
			$dir=$this->getUtility()->getPath("A_UPLOAD").$this->istance."/".$id; //di default carico i file dentro il modulo
			if(!file_exists($dir)){
				mkdir($dir,0755,true);
			}
		}
		$this->checkAutoDir($dir); //inizializzo le cartelle normali

		$this->checkSecurityDir($dir); //controllo se l'utente vuole entrare dove non potrebbe usando qualche .. di troppo
		$this->baseDir=$dir;
		$adDir=$this->post("dir"); //directory su cui devo operare a partire dalla radice
		$this->checkSecurityDir($adDir); //cartella in processazione
		$this->adDir=$adDir;

		$dir.=$adDir;
		$this->openDir($dir);
		switch ($azione) {
			case 'readDir': //lettura delle directory
			$this->readDir();
			break;

			case 'downloadFile': //scaricamento del file scelto
			$param=$this->post("file");
			$this->logFileDb("scaricamento",$param,"1");
			return $this->startDownloadFile($param);
			break;

			case 'uploadFile': //upload del file
			$this->checkPermesso("inserisci");
			$this->uploadFile(); //carico i file presi dal post
			$this->readDir(); //rimando la lista dei file per il refresh
			break;

			case 'newDir': //creazione della cartella su richiesta
			$this->checkPermesso("inserisci");
			$this->mkdir();
			break;

			case 'moveFile':
			case 'rename': //rinominazione di un file o di una cartella
			$this->checkPermesso("modifica");
			$this->rename();
			break;

			case 'delete':
			$this->delete();
			break;


			default:
			$this->warning("AZIONE NON MAPPATA");
			die;
			break;
		}
	}



	/**
	* funzione per la lettura della Cartella impostata
	* @param  string $dir lettura della cartella
	* @return void
	*/

	public function readDir():void
	{
		$this->executeReadDir();
		$this->printFile();
		echo "<script>".$this->getClassJs().".bindContextMenu();</script>";
		die(); //a fine della routine devo mostrare l'html, quindi mi fermo qui
	}


	/**
	* Esecuzione della rinominazione di un file  o di una cartella
	* @return bool
	*/
	private function rename(){
		$this->checkPermesso("modifica");
		$dir=$this->getDir();
		$old=$this->postStr("oldFile"); //file attuale
		$oldName=$old;
		$new=$this->postStr("newFile");//nuovo nome
		$newName=$new;
		//controllo sicurezza se l'utente sta facendo il furbone
		$this->checkSecurityDir($old);
		$this->checkSecurityDir($new);
		//percorsi assoluti per la rinomianzione
		$new=$dir.$new;
		$old=$dir.$old;
		if(is_dir($old)){ //controllo se sto modificando una directory o un file
			$lab="Directory";
			$tipo="2";
		}else{
			$lab="File";
			$tipo="1";
		}
		//controllo se il rename ha un elemento valido
		if (!file_exists($old)) {
			$this->getUtility(	)->error("$lab non ESISTENTE");
		}

		//controllo se il nome è libero
		if (file_exists($new)) {
			$this->getUtility(	)->error("$lab già esistente");
		}
		//esecuzione del rename della cartella
		if (!rename($old,$new)){
			$this->getUtility()->error("Errore creazione $lab ");
		} else {
			$this->logFileDb("Rinominazione $lab $oldName in $newName",$oldName,$tipo);
			$this->dbRenameFile($oldName,$newName,$tipo);
			$this->getUtility()->success("$lab rinominata con successo"); //refresh della cartella
		}
	}

	/**
	* Ritorno della path del cestino
	* @return string path del cestino
	*/
	private function getTrashPath():string{
		$dir=$this->post("dir");
		if(empty($dir)){
			$dir=$dir."/";
		}
		$trash=$this->getPath("A_UPLOAD_TRASH").$this->postStr("nameIstance")."/".$this->getIdRiferimento()."/".$dir;
		if(!file_exists($trash)){
			mkdir( $trash,0766, true );
		}
		return $trash;
	}



	/**
	* Esecuzione della rinominazione di un file  o di una cartella
	* @return bool
	*/
	private function delete(){
		$this->checkPermesso("elimina");
		$dir=$this->getDir();
		$file=$this->postStr("file"); //file attuale
		$fileName=$file;
		//controllo sicurezza se l'utente sta facendo il furbone
		$this->checkSecurityDir($file);
		//percorsi assoluti per la rinomianzione
		$trash=$this->getTrashPath();
		$trash=$trash.$file; //percorso del cestino
		$file=$dir.$file; //percorso del file da eliminare
		$old=$file;
		if(is_dir($old)){ //controllo se sto modificando una directory o un file
			$lab="Directory";
			$tipo="2";
		}else{
			$lab="File";
			$tipo="1";
		}
		//controllo se il rename ha un elemento valido
		if (!file_exists($old)) {
			$this->getUtility()->error("$lab non ESISTENTE");
		}
		if(file_exists($trash)){ //se già esistente
			$this->getUtility()->removeFile($trash);
		}

		if (!rename($file,$trash)){
			$this->error("Impossibile eliminare il file");
		}

		$this->logFileDb("Eliminazione $lab $fileName",$fileName,$tipo);
		$this->dbDeleteFile($fileName,$tipo);
		$this->getUtility()->success("Eliminazione $lab eseguita con successo"); //refresh della cartella

	}

	/**
	* Crea la cartella
	* @param  string $dir    [description]
	* @param  string $newDir [description]
	* @return bool           [description]
	*/
	private function mkdir():bool
	{
		$this->checkPermesso("inserisci");
		$dir=$this->getDir();
		$new=$this->postStr("newDir"); //cartella da creare
		$newName=$new;
		$this->checkSecurityDir($new); //controllo input maligno tipo ..
		$new=$dir.$new;
		if (file_exists($new)) { //se esiste già una cartella fermo la routine
			$this->getUtility()->error("cartella esistente");
		}
		if (!mkdir($new,0766,true)) { //creazione effettiva della cartella nell'area
			$this->getUtility()->error("Errore creazione nuova cartella ");
		} else {
			$this->logFileDb("Creazione nuova cartella",$newName,"2"); //log su db
			$this->dbFileInsert($newName,"2"); //mappo la cartella a db
			$this->getUtility()->success("Cartella creata con successo"); //refresh della cartella
		}
		die;
	}


	/**
	* Procedura per il download del file. Viene scaricato come header
	* @param  string $dir   Cartella
	* @param  string $param File da scaricare
	* @return bool          esito operazione
	*/
	public function startDownloadFile(string $file):bool
	{
		$this->checkPermesso("visualizza");
		if (file_exists($this->getDir().$file)) {
			$this->setFileWork($file);
		} else {
			$this->error("File inesistente");
			return false;
		}
		$file=$this->getFileWork();
		$fileUrl=$this->getFileWork("P");
		$this->getUtility()->getHeaderFile($file,$fileUrl);
		return true;
	}


	/**
	* Esegue la letture dei file e ritorna l'oggetto preso
	* @return [type] [description]
	*/
	public function executeReadDir()
	{
		if ($this->readFileDir()) {
			return $this->getMapFile();
		} else {
			return false;
		}
	}


	/**
	* controlla e carica il file UPLOAD nella cartella di lavoro
	* @param  $file [description]
	*/
	private function procFileUpload($file)
	{
		$nome=$file['name'][0];
		$this->setFileWork($nome);
		$newFile=$this->getNameUpload();
		if (!move_uploaded_file($file['tmp_name'][0], $this->getDir().$newFile)) {
			$this->error("Errore spostamento FILE $nome");
		} else {
			$this->logFileDb("Upload nuovo file",$newFile,"1"); //log di inserimento file
			$this->dbFileInsert($newFile,"1"); //mappo il file su database
			return true;
		}
	}

	/**
	*  carica il file preso dal post nella cartella di lavoro impostato
	*/
	public function uploadFile()
	{
		$this->checkPermesso("inserisci");
		if (!isset($_FILES)) {
			$this->error("Errore POST upload, file non caricato");
			return false;
		}
		foreach ($_FILES as $file) {
			$this->procFileUpload($file);
		}
	}

	/**
	* Segna la cartella aperta e controlla validità
	* @param  string $dir cartella
	* @return       [description]
	*/
	public function openDir(string $dir):bool
	{
		if (file_exists($dir)) {
			$this->rootDir=$dir;
			return true;
		} else {
			$this->error("Cartella non valida $dir");
			return false;
		}
	}


	/**
	* processo ogni file della cartella e estraggo i dati che mi servono
	*/
	private function fetchFile():void
	{
		$file=$this->getFileWork();
		if (($file=='.')||($file=='..')) { //scarto i cambi di cartella
			return;
		}
		$res=[];
		$res['name']=$file;
		$dir=$this->getDir();
		$pFile=$dir.$file;
		if (is_dir($pFile.DIRECTORY_SEPARATOR)) {
			$res['dir']='1';
			$res['size']="";
		} else {
			$res['size']=$this->getUtility()->formatFileSize($pFile);
			$res['dir']='0';
		}
		$res['dataModified']=date("d/m/Y H:i:s.", filemtime($pFile));
		$this->addMapFile($res);
	}

	/**
	* Legge la cartella in lavorazione e bufferizza il contenuto
	*/
	public function readFileDir():bool
	{
		if (!$dir=$this->getDir()) { //controllo se esiste una cartella valida
			return false;
		}
		$this->workFile=scandir($dir); //scansione della cartella
		$find=false;
		foreach ($this->workFile as $file) { //
			$find=true;
			$this->setFileWork($file); //salvataggio dei file nell'array map dell'oggetto
			$this->fetchFile();
		}
		return true;
	}




} //fine classe caricamento File


?>
