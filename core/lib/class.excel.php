<?php

/**
 * Wrapper del progetto per php Spreadsheet
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExcelPhp extends WrapperUtility
{
  private $spreadsheet = null;
  private $sheet = null;

  public $boldTitle = true;

  public $bufferArray = []; //aggiunge più array 

  /**
   * Torna la sheet attiva
   */
  private function getSheet()
  {
    if (empty($this->sheet)) {
      $this->sheet = $this->getSpread()->getActiveSheet();
    }
    return $this->sheet;
  }

  /**
   * Torna lo spread attivo
   */
  private function getSpread()
  {
    if (empty($this->spreadsheet)) {
      $this->spreadsheet = new Spreadsheet();
    }
    return $this->spreadsheet;
  }

  /**
   * Imposta la cellValue attiva
   * @param string $cell cella da modificare
   * @param string $val  valore da salvare
   */
  public function setCellValue(string $cell, string $val)
  {
    $this->getSheet()->setCellValue($cell, $val);
  }

  /**
   * Carica un dataset dall'ultima query
   * @param  string $ds dataset da caricare
   */
  public function printDataset(string $ds = "")
  {
    $data = $this->getUtility()->fetchAll($ds);
    if (empty($data)) {
      return;
    }
    $cols = [];
    foreach ($data[0] as $col => $val) {
      //aggiungo il bold?
      if ($this->boldTitle) {
        $col = "<b>" . $col . "</b>";
      }


      $cols[] = $col;
    }
    array_unshift($data, $cols);
    $this->addToArray($data);
  }


  /**
   * Carica un array nella sheet attuale
   * @param  array  $arr array da caricare
   * @return
   */
  public function loadArray()
  {
    $arr = $this->bufferArray;

    $res = [];
    $i = 0;
    foreach ($arr as $r) {
      if (isset($r[0]) && is_array($r[0])) {
        foreach ($r as $subR) {
          $res[$i] = $subR;
          $i++;
        }
        continue;
      }

      $res[$i] = $r;
      $i++;
    }
    $arr = $res;

    if ($this->boldTitle) {
      $row = 1;
      foreach ($arr as &$r) {
        $col = "A";
        foreach ($r as &$c) {
          if (strpos($c, "<b>") !== false) {
            $c = str_replace("<b>", "", $c);
            $c = str_replace("</b>", "", $c);
            $this->cellBold($row, $col);
          }
          $col++;

        }
        $row++;
      }
      //aggiungo lo stile bold dove serve
    }
    $this->getSheet()->fromArray($arr);
  }

  public function cellBold($row, $col): void
  {
    $cell = $col . $row;
    $this->getSheet()->getStyle($cell)->getFont()->setBold(true);
  }

  public function addToArray($arr)
  {
    array_push($this->bufferArray, $arr);
  }




  /**
   * salva e scarica il file
   * @param  string $filename [description]
   * @return [type]           [description]
   */
  public function executeDownload(string $filename)
  {
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
    header('Cache-Control: max-age=0');
    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this->getSpread(), 'Xlsx');
    $writer->save('php://output');
  }

  /**
   * Converte un excel in array
   * @param  string $fileName nome del file da caricare
   * @return array            result array
   */
  public function getExcelInArray(string $fileName): array
  {
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
    $worksheet = $spreadsheet->getActiveSheet();
    $rows = $worksheet->toArray(); //array gigante, devo pulirlo
    $row = $rows[0];

    //do per scontato che le colonne devono essere tutte attaccate, se trovo uno spazio vuoto considero finito l'array
    $nCol = count($row);
    $end = false;
    $i = 0;
    while ($i < $nCol && $end == false) {
      if ($row[$i] == "") {
        $end = true;
      } else {
        $i++;
      }
    } //end while pulizia colonne

    if ($i == 0) { //nessuna colonna formale valida
      return [];
    }
    $res = [];
    foreach ($rows as $row) {
      $new = [];
      for ($j = 0; $j < $i; $j++) {
        $new[$j] = $row[$j];
      }
      array_push($res, $new);
    }
    $res = $this->getUtility()->getArrayHeaderFirstRow($res);
    return $res;
  }

  /**
   * Aggiunge una riga all'excel
   */
  public function addRow($row = "")
  {
    if (empty($row)) {
      $data = [];
    }

    if (is_array($row)) {
      $data[0] = $row;
    }
    $this->addToArray($data);
  }

  /**
   * Crea l'excel e lo salva
   * @param  string $fileName [description]
   * @return [type]           [description]
   */
  public function executeSave(string $fileName)
  {
    $writer = new Xlsx($this->getSpread());
    $writer->save($fileName);
  }
} //fine classe
